#!/usr/bin/php -q
<?php

	//$argv[1];
	//$callNum = $argv[1];
	ob_implicit_flush(false);
	set_time_limit(0);
	require('phpagi.php');
	error_reporting(E_ALL);

	/* Conexion con AGI */
	$agi = new AGI();

	/* Datos de configuracion */
	$db_database = 'sitios_zicom';
	$db_user = 'sitios_zicom';
	$db_pass = 'r29a76';
	$db_hostname = 'localhost';

	/* Conexion a la base de datos  */
	try {
		$pdo = new PDO('mysql:host='.$db_hostname.';dbname='.$db_database, $db_user, $db_pass);
	} catch (PDOException $e) {
		$agi->verbose("Fallo: Error al conectarse a la base de datos! " . $e->getMessage());
		$agi->hangup();
		die();
	}
	$agi->verbose("Conectado con la base de datos…");

	$agi->answer();
	$uniqueid    = $agi->request['agi_uniqueid'];
	$calleridnum = $agi->request['agi_callerid'];
	$Variables   = $agi->get_variable('var1');
	$Respuesta   = json_decode($Variables['data'], true);
	$callNum     = $agi->request['agi_arg_1'];
	$agi->verbose('Numero: '.$callNum.' Agente: '.$Respuesta[0].' Registro: '.$Respuesta[1]. ' Llamada: '.$uniqueid .' Supervisor: '.$Respuesta[2]);

	/* Buscar la troncal asignada al subervisor */
	$IdSupervisor = $Respuesta[2];
	$sql = $pdo->prepare('SELECT * FROM tbl_troncal_asignada_supervisor INNER JOIN tbl_troncales ON tbl_troncales.IdTroncal=tbl_troncal_asignada_supervisor.IdTroncal WHERE tbl_troncal_asignada_supervisor.IdSupervisor = :IdSupervisor');
	$sql->execute(array('IdSupervisor' => $IdSupervisor));
	$Troncal = $sql->fetchAll(PDO::FETCH_ASSOC);
	$TroncalAsignada = $Troncal[0]['NombreTroncal'];
	$agi->verbose('Troncal: '.$TroncalAsignada);

	//$agi->exec('DIAL', 'SIP/'.$callNum.'@localphone,30,tr');
	$agi->exec('DIAL', 'SIP/'.$callNum.'@'.$TroncalAsignada.',30,tr');
	$dialstatus = $agi->get_variable('DIALSTATUS');

	if($dialstatus['data'] == 'CHANUNAVAIL' OR $dialstatus['data'] == 'CONGESTION' OR $dialstatus['data'] == 'FAILED'){
		$agi->verbose('Fallo al realizar la llamada');
		$sql = $pdo->prepare("INSERT INTO tbl_gestion_telefonica(IdDatoAgenda, IdAgente, uniqueid, NumeroTelefono, Grabacion, StatusLlamada) VALUES(?, ?, ?, ?, ?, ?)");
		$sql->execute(array($Respuesta[1], $Respuesta[0], $uniqueid, $callNum, 'NO', 'FALLO'));
	}
	elseif($dialstatus['data'] == "ANSWER"){
		//$agi->exec('MixMonitor', '/var/spool/asterisk/monitor/'.$uniqueid.'.gsm');
		$agi->verbose('Contestada');
		$sql = $pdo->prepare("INSERT INTO tbl_gestion_telefonica(IdDatoAgenda, IdAgente, uniqueid, NumeroTelefono, Grabacion, StatusLlamada) VALUES(?, ?, ?, ?, ?, ?)");
		$sql->execute(array($Respuesta[1], $Respuesta[0], $uniqueid, $callNum, 'SI', 'CONTESTADO'));
	}
	elseif($dialstatus['data'] == "NOANSWER" OR $dialstatus['data'] == "BUSY"){
		$agi->verbose('Linea ocupada o no contestan');
		$sql = $pdo->prepare("INSERT INTO tbl_gestion_telefonica(IdDatoAgenda, IdAgente, uniqueid, NumeroTelefono, Grabacion, StatusLlamada) VALUES(?, ?, ?, ?, ?, ?)");
		$sql->execute(array($Respuesta[1], $Respuesta[0], $uniqueid, $callNum, 'NO', 'NOCONTESTADO'));
	}
	elseif($dialstatus['data'] == "CANCEL"){
		$agi->verbose('Llamada cancelada por el agente');
		$sql = $pdo->prepare("INSERT INTO tbl_gestion_telefonica(IdDatoAgenda, IdAgente, uniqueid, NumeroTelefono, Grabacion, StatusLlamada) VALUES(?, ?, ?, ?, ?, ?)");
		$sql->execute(array($Respuesta[1], $Respuesta[0], $uniqueid, $callNum, 'NO', 'CANCELADA'));
	}

	$agi->hangup();

?>