#!/usr/bin/php -q
<?php
	ini_set('memory_limit', '512M');
	$_SERVER['HTTP_HOST'] = 'http';
	$_SERVER['REMOTE_ADDR'] = '10.10.10.1';
	
	require implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Neural', 'Cargador.php'));
	$Directorio = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'App', 'ZICOM', 'App', 'Ayudas'));
	if(is_dir($Directorio) == true) {
		if($ListadoAyudas = opendir($Directorio)) {
			while (($Archivo = readdir($ListadoAyudas)) !== false){
				if($Archivo <> '.' AND $Archivo <> '..' AND $Archivo <> '.htaccess') {
					require implode(DIRECTORY_SEPARATOR, array($Directorio, $Archivo));
				}
			}
			closedir($ListadoAyudas);
		}
	}

	require implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'App', 'ZICOM', 'App', 'Modulos', 'Supervisor', 'Controladores', 'ImportarExcelConsola.php'));
	require implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'App', 'ZICOM', 'App', 'Modulos', 'Supervisor', 'Modelos', 'ImportarExcelConsola_Modelo.php'));

	$controlador = 'ImportarExcelConsola';
	$metodo = 'CargarArchivoExcelConsola';
	$parametros = array(false);
	//Ayudas::print_r(get_included_files());
	call_user_func_array(array(new $controlador, $metodo), $parametros);
