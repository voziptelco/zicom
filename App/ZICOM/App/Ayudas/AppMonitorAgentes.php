<?php

	class AppMonitorAgentes{
		
		
		/**
		 * AppMonitorAgentes::__construct()
		 * 
		 * Se genera conexion de BD
		 */
		function __construct() {
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * StatusAgente($IdSupervisor = false, $DatosAgente = false)
		 * 
		 * Busca el estado de la estension de los agentes
		 * @param $IdSupervisor:   Identificador del agente
		 * @param $DatosAgente: matris de datos de los agentes 
		 */
		public function StatusAgente($IdSupervisor = false, $DatosAgente = false){
			if($IdSupervisor == true AND $DatosAgente == true AND is_array($DatosAgente) == true){
				$ColumnaStaus = $this->ExtensionesDisponibles($DatosAgente);
				return $ColumnaStaus;
			}
		}
		
		/**
		 *  Obtiene las extensiones disponibles para el traspaso de llamadas.
		 *  @return string
		 */
		 private function ExtensionesDisponibles($InformacionAgentes = false){
		 	if($InformacionAgentes == true AND is_array($InformacionAgentes) == true){
	      		$Conexion = $this->Conectar();
	      		if($Conexion == 'ErrorConexion' OR $Conexion == 'ErrorAcceso'){
	      			return "Error";	
	      		}
      			else{
		        	$Respuesta = $Conexion->EstadoExtensiones();
					$Conexion->Desconectar();
					if(isset($Respuesta[0]['Response']) == true AND $Respuesta[0]['Response'] == 'Success'){
	   					if(isset($Respuesta[2]['Response']) == true AND $Respuesta[2]['Response'] == 'Success'){
	       					$i = 0;
	           				foreach($InformacionAgentes AS $Registro){
	 							foreach($Respuesta as $Agente){
		             				if(isset($Agente['Event']) == true AND $Agente['Event'] == 'PeerStatus'){
	   									if($Registro['Peer'] == $Agente['Peer']){
	           								if($Agente['PeerStatus'] == 'Reachable'){
	   											$InformacionAgentes[$i]['Status'] = "CONECTADO";	
	           								}
										   	elseif($Agente['PeerStatus'] == 'Unknown'){
	           									$InformacionAgentes[$i]['Status'] = "NO CONECTADO";
	           								}
	           								break;
	           							}
									   	else{
											$InformacionAgentes[$i]['Status'] = 'NO EXISTE';
	 									}
									}
	     						}
	 							$i++;
		         			}
					  	}
	     			}
	     			return $InformacionAgentes;
				}
 			}
		}

		/**
		 * Realiza la conexion al conmutador
		 * @return AppControlAgentes; Devuelve el conector.
		 * @throws NeuralException
		 */
		private function Conectar(){
			$ConfiguracionConmutador = $this->ConsultarConfiguracionConmutador();
  			if($ConfiguracionConmutador == true AND is_array($ConfiguracionConmutador)){
     			$this->ParametrosAcceso = array(
		         'host'=>$ConfiguracionConmutador[0]['host'],
		         'port'=>($ConfiguracionConmutador[0]['port'] == '') ? 5038 : $ConfiguracionConmutador[0]['port'],
		         'user'=>$ConfiguracionConmutador[0]['user'],
		         'password'=>NeuralCriptografia::DeCodificar($ConfiguracionConmutador[0]['password'], APP),
		      	);
		      	$Conexion = new AppConexionConmutador($this->ParametrosAcceso);
		      	if($Conexion->ObtenerConexion() == true) {
       	 			$Conexion->AutenticarConexion();
         			return $Conexion;
		      	}
			  	else{
					return 'ErrorConexion';
		      	}
   			}
		   	else{
		   		return 'ErrorAcceso';
		   	};
		}
		
		/**
		 * Metodo Privado 
		 * ConsultarConfiguracionConmutador()
		 * 
		 * Consulta los datos de Configuracion del Conmutador
		 * @return Matriz de datos de consulta
		 */
		private function ConsultarConfiguracionConmutador(){
			$SQL = "SELECT host, port, user, password, prefijo, callerid, contexto FROM tbl_configuracion_conmutador";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
	}