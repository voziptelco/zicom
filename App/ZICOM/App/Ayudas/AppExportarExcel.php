<?php

	class AppExportarExcel {

		/**
		 * Contenedor de Datos de Lectura Excel
		 */
		private static $Contenedor = false;

		/**
		 * Contenedor del Objeto Exportar Excel
		 */
		private $PHPExcel = false;
		
		/**
		 * NumeroColumnas
		 * Variable publica
		 */
	 	public static $Columnas = false;
	 	
		/**
		 * AppExportarExcel::__construct()
		 *
		 * Constructor que genera el require al archivo correspondiente
		 * Instancia la clase PHPExcel para menajo de Exportacion
		 * @return void
		 */
		function __construct() {
			self::requirePHPExcel();
			$this->PHPExcel = new PHPExcel();
			$this->PHPExcel->setActiveSheetIndex(0);
			$this->PHPExcel->getActiveSheet()->getStyle('D1:E999')
				->getAlignment()->setWrapText(false);
			$companySignature = ' ©2015 Tec Capacitate';
			$this->PHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G'.'&R Exportado De Las Gestiones');
			$this->PHPExcel->getActiveSheet()->getHeaderFooter()->setEvenHeader('&L&G'.'&R Exportado De Las Gestiones');
			$this->PHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&C'.$companySignature.'&R Pag &P');
			$this->PHPExcel->getActiveSheet()->getHeaderFooter()->setEvenFooter('&C'.$companySignature.'&R Pag &P');
			$this->PHPExcel->getActiveSheet()->getPageMargins()->setTop(1.0);
		}


		/**
		 * AppPHPExcel::LeerExcelColumnas()
		 *
		 * Genera proceso de lectura de columnas y los datos del documento excel
		 * @param bool $Archivo: archivo excel.
		 * @return: primera fila del archivo excel, Almacena los datos en la variable estatica contenedor.
		 * Se obtienen los datos con la llamada al metodo OBJExcel->DatosExcel();
		 */
		public function LeerExcelColumnas($Archivo = false){
			if(file_exists($Archivo) == true):
					return $this->LeerExcelColumnasProceso($Archivo);
			else:
				return 'El Archivo de Excel No Existe';
				exit();
			endif;
		}

		/**
		 * AppPHPExcel::LeerExcelColumnasProceso()
		 *
		 * Genera el proceso de lectura archivo excel
		 * @param string $Archivo
		 * @return mixed
		 */
		private function LeerExcelColumnasProceso($Archivo = false) {
			$inputFileType = PHPExcel_IOFactory::identify($Archivo);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($Archivo);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet):
				$Hoja[] = $worksheet->getTitle();
				$Matrix[$worksheet->getTitle()] = $worksheet->toArray();
			endforeach;
			$MatrixDatos = (count($Hoja) >= 1) ? $Matrix[$Hoja[0]] : $Matrix[$Hoja[0]];
			$Columnas = $MatrixDatos[0];
			self::$Columnas = $MatrixDatos[0];
			unset($MatrixDatos[0]);
			self::$Contenedor = $MatrixDatos;
			unset($inputFileType, $objReader, $objPHPExcel, $MatrixDatos, $Matrix, $Hoja);
			return $Columnas;
		}

		/**
		 * Metodo publico
		 * LeerColumnas()
		 *
		 * Devuelve el valor de las columnas leidas
		 * @return bool
		 */
		public function LeerColumnas(){
			return self::$Columnas;
		}

		/**
		 * AppPHPExcel::DatosExcel()
		 *
		 * Retorna valores del excel
		 * @return mixed
		 */
		public function DatosExcel() {
			return self::$Contenedor;
		}

		/**
		 * Metodo Publico
		 * ObtenerColumnasFaltantes($ColumnasFormato = false, $ColumnasRecibidas = false)
		 *
		 * Devuelve las columnas faltantes que no se recibieron del archivo excel.
		 * @param bool|false $ColumnasFormato: Las columnas que el archivo escel debe contener, array('ColumnaRequerida', ...)
		 * @param bool|false $ColumnasRecibidas: Columnas del documento excel recibido. array('ColumnaRecibida', ...)
		 * @return array: Columnas Requerida faltante.
		 */
		public function ObtenerColumnasFaltantes($ColumnasFormato = false, $ColumnasRecibidas = false){
			if($ColumnasFormato == true AND is_array($ColumnasFormato) == true AND $ColumnasRecibidas == true AND is_array($ColumnasRecibidas) == true)
				$Faltantes = false;
				foreach($ColumnasFormato as $Requerido){
					if(!in_array($Requerido, $ColumnasRecibidas))
						$Faltantes[] = $Requerido;
				}
			return $Faltantes;
		}

		/**
		 * Metodo Publico
		 * ObtenerColumnasSobrantes($ColumnasFormato = false, $ColumnasRecibidas = false){
		 *
		 * Devuelve las columnas sobrantes que se recibieron del archivo excel.
		 * @param bool|false $ColumnasFormato: Las columnas que el archivo excel debe contener, array('ColumnaRequerida', ...)
		 * @param bool|false $ColumnasRecibidas: Columnas del documento excel recibido. array('ColumnaRecibida', ...)
		 * @return array: Columna recibida sobrante.
		 */
		public function ObtenerColumnasSobrantes($ColumnasFormato = false, $ColumnasRecibidas = false){
			if($ColumnasFormato == true AND is_array($ColumnasFormato) == true AND $ColumnasRecibidas == true AND is_array($ColumnasRecibidas) == true)
				$Sobrantes = false;
				foreach($ColumnasRecibidas as $Recibido){
					if(!in_array($Recibido, $ColumnasFormato))
						$Sobrantes[] = $Recibido;
				}
			return $Sobrantes;
		}

		/**
		 * Metodo Publico
		 * ObtenerIndices($NombreColumnas = false)
		 *
		 * Obtiene el indice que tienen de las columnas del archivo excel.
		 * @param bool|false $NombreColumnas: Las columnas de las cuales se quiere saber el indice.
		 * @return array: arreglo de indices, array('indice1', 'indice2', ...)
		 */
		public function ObtenerIndices($NombreColumnas = false){
			if($NombreColumnas == true AND is_array($NombreColumnas)){
				foreach($NombreColumnas as $Nombre){
					if(in_array($Nombre, Self::$Columnas)){
						$Llaves[] = array_search ($Nombre, Self::$Columnas);
					}
				}
				return $Llaves;
			}
		}

		/**
		 * AppPHPExcel::Abc()
		 *
		 * Genera abecedario correspondiente
		 * @param integer $Indice
		 * @return mixed
		 */
		public static function Abc($Indice = false) {
			for ($i= 65; $i<=90; $i++):
				$Lista[] = chr($i);
			endfor;
			if($Indice == true AND is_bool($Indice) == false):
				if(is_numeric($Indice) == true AND $Indice<=25 AND $Indice>=0):
					return $Lista[$Indice];
				endif;
				return 'El Indice No Puede ser Devuelto';
			else:
				return $Lista;
			endif;
		}

		/**
		 * AppPHPExcel::ValidarExtension()
		 *
		 * @param mixed $extension
		 * @return
		 */
		private static function ValidarExtension($extension = false){
			if($extension == 'application/vnd.ms-excel'):
				return "xls";
			elseif($extension == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'):
				return "xlsx";
			endif;
			return false;
		}

		/**
		 * AppPHPExcel::requirePHPExcel()
		 *
		 * @return void
		 */
		private static function requirePHPExcel(){
			$Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'PHPExcel', 'PHPExcel.php'));
			$Validacion = array_search($Archivo, get_included_files());
			if($Validacion == false AND is_numeric($Validacion) == false):
				if(file_exists($Archivo) == true):
					require_once implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'PHPExcel', 'PHPExcel.php'));
				else:
					throw new AppExcepcion('Libreria PHPExcel No Existe');
					exit();
				endif;
			endif;
			unset($Archivo, $Validacion);
		}

		/**
		 * AppPHPExcel::ExportarInfoUsuario()
		 *
		 * Genera el proceso de la instancia de PHPExcel y carga la informacion
		 * del archivo general que se creara en excel
		 * @param string $Nombre
		 * @param string $Titulo
		 * @param string $Categoria
		 * @return void
		 */
		public function ExportarInfoUsuario($Nombre = false, $Titulo = false, $Categoria = false) {
			$this->PHPExcel->getProperties()->setCreator("Gestion y Cobranzas, ZICOM Group");
			$this->PHPExcel->getProperties()->setLastModifiedBy($Nombre);
			$this->PHPExcel->getProperties()->setTitle($Titulo);
			$this->PHPExcel->getProperties()->setSubject("");
			$this->PHPExcel->getProperties()->setDescription("");
			$this->PHPExcel->getProperties()->setKeywords("");
			$this->PHPExcel->getProperties()->setCategory($Categoria);
		}

		/**
		 * AppPHPExcel::ExportarAsignarCelda()
		 *
		 * Asigna un valor a una celda indicada
		 * @param string $Celda
		 * @param string $Valor
		 * @return void
		 */
		public function ExportarAsignarCelda($Celda = false, $Valor = false) {
			if($Celda == true AND is_bool($Celda) == false AND $Valor == true):
				$this->PHPExcel->setActiveSheetIndex(0)->setCellValue($Celda, $Valor);
			endif;
		}

		/**
		 * AppPHPExcel::ExportarArchivoExcel()
		 *
		 * Genera el proceso de descarga del archivo excel correspondiente
		 * @param string $Extension
		 * @param string $Nombre
		 * @return
		 */
		public function ExportarArchivoExcel($Extension = false, $Nombre = false) {
			if(mb_strtolower($Extension) == 'xls'):
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$Nombre.'.xls"');
				header('Cache-Control: max-age=0');
				header('Cache-Control: max-age=1');
				header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
				header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
				header ('Pragma: public'); // HTTP/1.0
				$objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel5');
				$objWriter->save('php://output');
				exit();
			elseif(mb_strtolower($Extension) == 'xlsx'):
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$Nombre.'.xlsx"');
				header('Cache-Control: max-age=0');
				header('Cache-Control: max-age=1');
				header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
				header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
				header ('Pragma: public'); // HTTP/1.0
				$objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel2007');
				$objWriter->save('php://output');
				exit();
			endif;
			return false;
		}

		/**
		 * AppPHPExcel::ExportarSalvar()
		 *
		 * Genera el proceso de guardar en la carpeta Temporales el archivo creado para
		 * su postuma eliminacion
		 * @param string $Extension
		 * @param string $Nombre
		 * @return raw
		 */
		public function ExportarSalvar($Extension = false, $Nombre = false) {
			if($Extension == true AND is_bool($Extension) == false AND $Nombre == true AND is_bool($Nombre) == false):
				$Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', $Nombre.'.'.mb_strtolower($Extension)));
				$objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel2007');
				$objWriter->save($Archivo);
				return $Archivo;
			endif;
		}

		/**
		 * Metodo Publico
		 * EstablecerValorFila($ArrayDatos = false)
		 *
		 * Establece el valor de las celdas en las celdas seleccionadas.
		 * @param bool|false $ArrayDatos: array('Celda' => 'Valor', ...)
		 */
		public function EstablecerValoresDeArreglo($ArrayDatos = false){
			if(isset($ArrayDatos) == true AND is_array($ArrayDatos) == true){
				foreach($ArrayDatos as $Celda => $Valor){
					self::ExportarAsignarCelda($Celda, $Valor);
				}
			}
		}

		/**
		 * Metodo Publico
		 * ExportarArregloDatos($Datos = false, $FilaInicio)
		 *
		 * Exporta los datos del arreglo.
		 * @param bool|false $Datos; array('dato', ...), Se recibe como argumento de datatable.
		 * @param $Columnas: A partir de que fila de va a empezar a escribir los datos
		 */
		public function ExportarArregloDatos($Datos = false, $FilaInicio = false, $Columnas = false, $ColumnaFormato = false, $Colores = false){
			if(isset($Datos) == true AND is_array($Datos) == true AND $FilaInicio == true AND $Columnas == true){
				$FilaActual = $FilaInicio;
				if(isset($Colores) AND !empty($Colores))
					$CodigoColores = AppUtilidades::arrayColumn($Colores, 'Codigo');
				foreach($Datos as $Arreglo){
					$ArregloDatos = array();
					foreach($Arreglo as $Indice=>$Valor){
						if(in_array($Indice, $Columnas)){
							$celda = array_search($Indice, $Columnas).$FilaActual;
							$ArregloDatos[$celda] = $Valor;
							if(isset($ColumnaFormato) AND $Indice == $ColumnaFormato){
								$Color = array_search($Valor, $CodigoColores);
								self::EstablecerColor($celda, str_replace('#', '', $Colores[$Color]['Color']));
							}
						}
					}
					self::EstablecerValoresDeArreglo($ArregloDatos);
					$FilaActual++;
				}

				$ColumnaInicio = array_search(reset($Columnas), $Columnas);
				$ColumnaFin = array_search(end($Columnas), $Columnas);

				foreach(range($ColumnaInicio, $ColumnaFin) as $columnID) {
					$this->PHPExcel->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
				}
				self::EstablecerAlineacionHorizontal($ColumnaInicio.$FilaInicio.':'.$ColumnaFin.($FilaActual-1), 'Izquierda');
				self::EstablecerBordeDelgado($ColumnaInicio.$FilaInicio.':'.$ColumnaFin.($FilaActual-1));
				if(in_array('NumeroTelefono', $Columnas)){
					self::EstablecerFormatoTexto('D'.$FilaInicio.':'.'D'.($FilaActual-1));	
				}
			}
		}
		

		/**
		 * Metodo Publico
		 * EstablecerAlineacionHorizontal($RangoCeldas = false, $Alineacion = false)
		 *
		 * Establece la alineacion del texto
		 * @param bool|false $RangoCeldas: Rango apara el objeto PHPExcel
		 * @param bool|false $Alineacion: 'Izquierda', 'Centrado', 'Derecha', 'Justificado'
		 * @throws PHPExcel_Exception
		 */
		public function EstablecerAlineacionHorizontal($RangoCeldas = false, $Alineacion = false){
			if($RangoCeldas == true AND $Alineacion == true){
				$ArregloAlineacion = array();
				switch($Alineacion){
					case 'Izquierda':
						$ArregloAlineacion = array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,);
						break;
					case 'Centrado':
						$ArregloAlineacion = array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,);
						break;
					case 'Derecha':
						$ArregloAlineacion = array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,);
						break;
					case 'Justificado':
						$ArregloAlineacion = array(
							'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,);
						break;
				}
				$style = array(
					'alignment' => $ArregloAlineacion
				);
				$this->PHPExcel->getActiveSheet()->getStyle($RangoCeldas)->applyFromArray($style);
			}
		}

		/**
		 * Metodo publico
		 * EstablecerColorFondo($RangoCeldas = false, $Color = false)
		 * Establece un color de fondo en un rango de celdas.
		 * @param bool|false $RangoCeldas: Para el objeto PHPExcel
		 * @param bool|false $Color: En formato Hexadecimal, 'FFFFFF'
		 * @throws PHPExcel_Exception:
		 */
		public function EstablecerColorFondo($RangoCeldas = false, $Color = false){
			$this->PHPExcel->getActiveSheet()->getStyle($RangoCeldas)->getFill()->applyFromArray(array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => $Color
				)
			));
		}
		
		/**
		 * Metodo publico
		 * EstablecerColorFondo($RangoCeldas = false, $Color = false)
		 * Establece un color de fondo en un rango de celdas.
		 * @param bool|false $RangoCeldas: Para el objeto PHPExcel
		 * @param bool|false $Color: En formato Hexadecimal, 'FFFFFF'
		 * @throws PHPExcel_Exception:
		 */
		public function EstablecerColor($Celda = false, $Color = false){
			$this->PHPExcel->getActiveSheet()->getStyle($Celda)->getFill()->applyFromArray(array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => $Color
				)
			));
		}

		/**
		 * Metodo Publico
		 * EstablecerBordeDelgado($RangoCeldas = false)
		 *
		 * Establece un borde delgado (THIN) a un rango de celdas.
		 * Establece un borde a un rango de Celdas.
		 * @param bool|false $RangoCeldas: Para el objeto PHPExcel
		 * @throws PHPExcel_Exception:
		 */
		public function EstablecerBordeDelgado($RangoCeldas = false){
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			$this->PHPExcel->getActiveSheet()->getStyle($RangoCeldas)->applyFromArray($styleArray);
		}
		
		/**
		 * @param bool|false $RangoCeldas
		 * @throws PHPExcel_Exception
		 */
		public function EstablecerFormatoTexto($RangoCeldas = false){
		   $this->PHPExcel->getActiveSheet()
		      ->getStyle($RangoCeldas)
		      ->getNumberFormat()
		      ->setFormatCode(
		         PHPExcel_Style_NumberFormat::FORMAT_NUMBER
		      );
		
		}

		/**
		 * Metodo publico
		 * ExportarRegistros($Consulta = false, $RangoColumnas = false, $FilaInicio = false)
		 *
		 * Exporta los registros de una consulta a un documento excel
		 * @param bool|false $Consulta: Arreglo con los registros a exportar.
		 * @param bool|false $RangoColumnas: Rango de las columnas a donde se van a asignar los valores de la consulta.
		 * @param bool|false $FilaInicio: Fila donde se comienza el exportado de datos.
		 */
		public function ExportarRegistros($Consulta = false, $RangoColumnas = false, $FilaInicio = false){
			if(isset($Consulta) == true AND is_array($Consulta) == true AND $FilaInicio == true AND $RangoColumnas == true){
				$Fila = $FilaInicio;
				foreach($Consulta as $ArregloRegistro){
					$ValoresArreglo = array_values($ArregloRegistro);
					$FilaDatos = array();
					for($i = 0; $i < count($RangoColumnas);  $i++){
						$FilaDatos[$RangoColumnas[$i].$Fila] = $ValoresArreglo[$i];
					}
					Self::EstablecerValoresDeArreglo($FilaDatos);
					$Fila++;
				}
				foreach($RangoColumnas as $columnID) {
					$this->PHPExcel->getActiveSheet()->getColumnDimension($columnID)
						->setAutoSize(true);
				}
			}
		}
	}