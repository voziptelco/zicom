<?php

	/**
	 * NEURAL FRAMEWORK PHP
	 *
	 * App - AppAsterisk
	 * Clase para el manejo de AMI de Asterisk
	 *
	 * @access public
	 * @since 1.0
	 * @version 3.0
	 */
	class AppAsterisk {

		private $Host = false;
		private $Port = false;
		private $User = false;
		private $Pass = false;
		private $Prefijo = false;
		private $CallerdId = false;
		private $Context = false;
		private $astManager = false;
		private $astRess = false;
		
		/**
		 * Metodo Constructor
		 * 
		 * Ininializa los atributos de la clase
		 * @param $Arreglo: Datos de conexion
		 */
		function __Construct($Arreglo = false){
			if(is_array($Arreglo)):
				self::requirePHPAgi();
				$this->Host = $Arreglo['host'];
				$this->Port = $Arreglo['port'];
				$this->User = $Arreglo['user'];
				$this->Pass = $Arreglo['password'];
				$this->Prefijo = $Arreglo['Prefijo'];
				$this->CallerdId = $Arreglo['callerid'];
				$this->Context = $Arreglo['contexto'];
				$this->astManager = new AGI_AsteriskManager();
			endif;
		}
		
		/**
		 * Metodo Privado
		 * APPAsterisk::Conexion()
		 * 
		 * Genera la conexion con el conmutador
		 * @return false si hubo un fallo o true si la conexion pudo realizarse.
		 */
		private function Conexion(){
			$this->astRess = $this->astManager->connect($this->Host, $this->User, $this->Pass);
			if(!$this->astRess):
				return false;
			else:
				return true;
			endif;
		}
		
		/**
		 * AppAsterisk::Desconectar()
		 * 
		 * Cierra la conexion.
		 */
		private function Desconectar(){
			$this->astManager->disconnect();
		}

		/**
		 * Metodo Publico
		 * ComprobarConexion()
		 *
		 * Comprueba el estado de la conexion al servidor asterisk
		 * @return Estado de la conexion
		 */
		public function ComprobarConexion(){
			return $this->Conexion();
		}

		/**
		 * Metodo Publico
		 * Marcar($Canal = false, $Extension = false, $IdAgente = false, $IdDatoAgenda = false, $Telefono = false)
		 *
		 * Realiza la marcacion a numero del cliente
		 * @param bool $Canal
		 * @param bool $Extension
		 * @param bool $IdAgente
		 * @param bool $IdDatoAgenda
		 * @param bool $Telefono
		 * @return string
		 */
		public function Marcar($Canal = false, $Extension = false, $IdAgente = false, $IdDatoAgenda = false, $Telefono = false, $IdSupervisor = false){
			if($Canal == true AND $Extension == true AND $Telefono == true AND $IdSupervisor == true):
				if($this->Conexion()):
					$channel = $Canal . '/'. $Extension;
					$exten = (self::DeterminarNumeroFijoMovil($Telefono) == 'MOVIL') ? '9' . $this->Prefijo . self::FormatoNumero($Telefono) : '9' . $this->Prefijo . self::FormatoNumero($Telefono);
				  	$priority = 1;
				  	$Datos = array($IdAgente, $IdDatoAgenda, $IdSupervisor);
				  	$Datos = json_encode($Datos);
				  	$Variable = "var1=$Datos";
			  	 	$this->astRess = $this->astManager->Originate($channel, $exten, $this->Context, $priority, null, null, null, $this->CallerdId, $Variable);
					$this->Desconectar();
				    return $this->astRess['Response'];
				else:
					return "Fallo";
				endif;
			endif;
		}
		
		/**
		 * Realiza el monitoreo de la extension.
		 * @param bool|false $Canal
		 * @param bool|false $ExtSupervisor
		 * @param bool|false $ExtAgente
		 * @return string
		 */
		public function MarcarSupervisor($Canal = false, $ExtAgente = false, $ExtSupervisor = false){
		   if($Canal == true AND $ExtSupervisor == true AND $ExtAgente == true):
		      if($this->Conexion()):
		         $channel = $Canal . '/'. $ExtAgente;
		         $exten = $ExtSupervisor;
		         $Contexto = 'conmutador';
		         $priority = 1;
		         $this->astRess = $this->astManager->Originate($channel, $exten, $Contexto, $priority);
		         $this->Desconectar();
		         return $this->astRess['Response'];
		      else:
		         return "Fallo";
		      endif;
		   endif;
		}

		/**
		 * Metodo Publico
		 * MonitorExtension($Extension = false)
		 *
		 * Monitorea el uso de la extension
		 * @param bool $Extension: Extension del agente
		 * @return array|string: Status de la extension
		 */
		public function MonitorExtension($Extension = false){
			if($Extension):
				if(self::Conexion()):
					$status = $this->astManager->Command('core show hint '.$Extension);
					return self::BuscaEstado($status['data']);
				else:
					return 'Error';
				endif;
			else:
				return 'Error';
			endif;
		}

		/**
		 * Metodo Publico
		 * MonitorAgentes($Canal = false, $ExtSupervisor = false, $ExtAgente = false)
		 *
		 * Monitorea el la llamada del agente
		 * @param boolean $Canal         :tipo canal del agente
		 * @param boolean $ExtSupervisor :extension asociada del supervisor
		 * @param boolean $ExtAgente     :Estensociada del agente
		 */
		public function MonitorAgentes($Canal = false, $ExtSupervisor = false, $ExtAgente = false){
		   if($Canal == true AND $ExtSupervisor == true AND $ExtAgente == true):
		      if($this->Conexion()):
		         $channel = $Canal . '/'. $ExtSupervisor;
		         $exten = '5' . $ExtAgente;
		         $Contexto = 'monitor_agente';
		         $priority = 1;
		         $this->astRess = $this->astManager->Originate($channel, $exten, $Contexto, $priority);
		         $this->Desconectar();
		         return $this->astRess['Response'];
		      else:
		         return "Error";
		      endif;
		   endif;
		}

		/**
		 * Metodo Publico
		 * BuscaEstado($CadenaEstado = false)
		 *
		 * Busca el estado del agente
		 * @param bool $CadenaEstado: Cadena de la consulta de estado
		 * @return mixed: Estado del Agente
		 */
		private function BuscaEstado($CadenaEstado = false){
			if ($CadenaEstado){
				$ArregloEstados = array('Idle', 'Unavailable', 'InUse', 'Busy', 'Ringing');
				foreach ($ArregloEstados AS $Valor) {
					if (strpos($CadenaEstado, $Valor)) {
						return $Valor;
					}
				}
			}
		}
		

		/**
		 * Metodo Privado
		 * FormatoNumero($Numero = false)
		 *
		 * Convierte el formato de marcacion a digitos
		 * @param bool $Numero
		 * @return mixed
		 */
		private function FormatoNumero($Numero = false){
			if($Numero):
				$Telefono = str_replace(')', '',  str_replace('(', '', $Numero));
				$Telefono = str_replace(' ', '', $Telefono);
				return $Telefono;
			endif;
		}

		/**
		 * Metodo Privado
		 * DeterminarNumeroFijoMovil($Numero = false)
		 *
		 * Determina el tipo de marcacion
		 * @param bool $Numero: Numero telefonico
		 * @return string: Movil o Fijo
		 */
		private function DeterminarNumeroFijoMovil($Numero = false){
			if($Numero):
				return (strlen($Numero)>8) ? 'MOVIL' : 'FIJO';
			endif;
		}

		/**
		 * Metodo Privado
		 * requirePHPAgi()
		 *
		 * Carga la clase de PHPAgi
		 * @throws AppExcepcion
		 */
		private static function requirePHPAgi(){
			$Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'PHPAGI', 'phpagi.php'));
			$Validacion = array_search($Archivo, get_included_files());
			if($Validacion == false AND is_numeric($Validacion) == false):
				if(file_exists($Archivo) == true):
					require_once implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'PHPAGI', 'phpagi.php'));
				else:
					throw new AppExcepcion('Libreria PHPAGI No Existe!');
					exit();
				endif;
			endif;
			unset($Archivo, $Validacion);
		}

	}
