<?php
	
	class AppArchivoConsola {
		
		private $conexion = false;
		private $tarea = false;
		private $supervisor = false;
		private $errores = false;
		private $cantCabecera = 19;
		private $cabecera = array('Cliente', 'Gerencia', 'Cartera', 'ClienteUnico', 
									'NombreTitular', 'DomicilioContractual', 'Provincia', 
									'Distrito', 'NombreAval', 'DomicilioAval', 'DistritoAval', 
									'SemanaAtraso', 'Saldo', 'SaldoTotal', 'Telefono1', 'Telefono2', 
									'Telefono3', 'Telefono4', 'Codigo');
				
		/**
		 * AppArreglosExcel::__construct()
		 * 
		 * Se genera conexion de BD
		 * @param string $conexion
		 * @return void
		 */
		function __construct($conexion = false, $tarea = false, $supervisor = false) {
			$this->conexion = NeuralConexionDB::DoctrineDBAL($conexion);
			$this->tarea = $tarea;
			$this->supervisor = $supervisor;
		}
		
		public function init($array = false) {
			if(is_array($array) == true):
				$this->compararColumnas($array);
			else:
				$this->CambiarEstatusTarea("ERROR", "Error de Carga de Archivo", AppFechas::ObtenerDatetimeActual());
				echo "Error de Carga de Archivo";
			endif;
		}
		
		/**
		 * AppArreglosExcel::compararColumnas()
		 * 
		 * compara cantidad de cabeceras
		 * @param array $array
		 * @return void
		 */
		private function compararColumnas($array = false) {
			if(count($array[0]) == $this->cantCabecera):
				$this->compararColumnasDiff($array);
			else:
				$this->CambiarEstatusTarea("ERROR", "La cantidad de Cabeceras no es correcto", AppFechas::ObtenerDatetimeActual());
				echo "La cantidad de Cabeceras no es correcto";
			endif;
		}
		
		/**
		 * AppArreglosExcel::compararColumnasDiff()
		 * 
		 * valida la diferencia de columnas
		 * @param array $array
		 * @return void
		 */
		private function compararColumnasDiff($array = false) {
			if(count(array_diff_assoc($array[0], $this->cabecera)) == 0):
				unset($array[0]);
				$this->validacionBase($array);
			else:
				$this->CambiarEstatusTarea("ERROR", "Las Cabeceras no es son en orden y valor correcto", AppFechas::ObtenerDatetimeActual());
				echo "Las Cabeceras no es son en orden y valor correcto";
			endif;
		}
		
		private function validacionBase($array = false) {
			foreach ($array AS $llave => $valor):
		 		$this->validacionSeleccion($valor, $llave);
				unset($valor, $llave); 
			endforeach;
		}
		
		/**
		 * AppArreglosExcel::validacionSeleccion()
		 * 
		 * Genera la validacion de las filas correspondientes
		 * junto con su formato
		 * 
		 * @param array $array
		 * @param integer $registro
		 * @return void
		 */
		private function validacionSeleccion($array = false, $registro = false) {
			foreach ($array AS $llave => $valor):
				if($llave == 0):
					$cont = $this->formatoCapital(trim($valor), 'Cliente');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 1):
					$cont = $this->formatoCapital(trim($valor), 'Gerencia');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 2):
					$cont = $this->formatoCapital(trim($valor), 'Cartera');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);			
				elseif($llave == 3):
					$cont = $this->formatoClienteUnico(trim($valor));
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 4):
					$cont = $this->formatoCapital(trim($valor), 'NombreTitular');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 5):
					$cont = $this->formatoCapital(trim($valor), 'DomicilioContractual');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 6):
					$cont = $this->formatoCapital(trim($valor), 'Provincia');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 7):
					$cont = $this->formatoCapital(trim($valor), 'Distrito');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 8):
					$cont = $this->formatoCapital(trim($valor), 'NombreAval');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 9):
					$cont = $this->formatoCapital(trim($valor), 'DomicilioAval');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 10):
					$cont = $this->formatoCapital(trim($valor), 'DistritoAval');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 11):
					$cont = $this->formatoNumerico(trim($valor), 'SemanaAtraso');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 12):
					$cont = $this->formatoNumerico(trim($valor), 'Saldo');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 13):
					$cont = $this->formatoNumerico(trim($valor), 'SaldoTotal');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 14):
					$cont = $this->formatoTelefono(trim($valor), 'Telefono1');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 15):
					$cont = $this->formatoTelefono(trim($valor), 'Telefono2');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 16):
					$cont = $this->formatoTelefono(trim($valor), 'Telefono3');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 17):
					$cont = $this->formatoTelefono(trim($valor), 'Telefono4');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				elseif($llave == 18):
					$cont = $this->formatoNumerico(trim($valor), 'Codigo');
					$array[$llave] = (is_bool($cont) == false) ? $cont : trim($valor);
				endif;
			endforeach;
			if(is_array($this->errores) == true):
				$this->ejecutarError($registro);
				$Bandera = 0;
				foreach ($this->errores as $Error){
					if(isset($Error['columna'])):
						if($Error['columna'] == "ClienteUnico" OR $Error['columna'] == "NombreTitular" OR $Error['columna'] == "SamanaAtraso" OR $Error['columna'] == "Saldo" OR $Error['columna'] == "SaldoTotal" OR $Error['columna'] == "Codigo"):
							$Bandera = 1;
						endif;
					endif;						
				}
				$this->ejecutarProcedimientoAlm($array, $Bandera, $registro);
			else:
				$this->ejecutarProcedimientoAlm($array, 0, $registro);
			endif;
			$this->errores = false;
			unset($array, $cont, $llave, $valor);			
		}
		
		/**
		 * AppArreglosExcel::ejecutarProcedimientoAlm()
		 * 
		 * Genera el proceso del almacenamiento alamacenado
		 * @param array $array
		 * @param integer $error_php
		 * @param integer $registro
		 * @return void
		 */		
		private function ejecutarProcedimientoAlm($array = false, $error_php, $registro) {
			$consulta = $this->conexion->prepare('CALL CARGA_REGISTRO(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, @STATUS)');
			$cont = 1;
			foreach ($array AS $valor):
				$consulta->bindValue($cont, $valor);
				$cont++;
			endforeach;
			$consulta->bindValue($cont, $error_php);
			$consulta->bindValue($cont+1, $this->supervisor);
			$consulta->bindValue($cont+2, $this->tarea);
			$consulta->bindValue($cont+3, $registro+1);
			$consulta->execute();
			$consulta = $this->conexion->prepare('SELECT @STATUS AS ERROR');
			$consulta->execute();
			unset($array, $valor, $cont, $error_php, $registro, $consulta);			
		}
		
		/**
		 * AppArreglosExcel::ejecutarError()
		 * 
		 * Guarda los registros de error
		 * @param integer $registro
		 * @return void
		 */
		private function ejecutarError($registro = false) {
			$SQL = new NeuralBDGab($this->conexion, 'tbl_log_registro');
			foreach ($this->errores AS $valor):
				$SQL->Sentencia('Idtarea', $this->tarea);
				$SQL->Sentencia('NumeroRegistro', $registro + 1);
				$SQL->Sentencia('Columna', $valor['columna']);
				$SQL->Sentencia('TipoError', $valor['mensaje']);
				$SQL->Insertar();
			endforeach;
		}
		
		/**
		 * AppArreglosExcel::CambiarEstatusTarea($Status = false)
		 * 
		 * Actualiza el estatus de la tarea.
		 * @return void
		 */
		public function CambiarEstatusTarea($Status = false, $Error = false, $Fecha = false){
			$SQL = new NeuralBDGab($this->conexion, 'tbl_archivos_programados');
			$SQL->Sentencia('Status', $Status);
			$SQL->Sentencia('FechaHoraTerminado', $Fecha);
			$SQL->Condicion('IdArchivosProgramados', $this->tarea);
			$SQL->Actualizar();
			$this->ejecutarErrorTarea($Error);
		}
		
		/**
		 * AppArreglosExcel::CambiarEstatusTarea($Status = false)
		 * 
		 * Agrega el nombre del archivo log a la tabla
		 * @return void
		 */
		public function AgregarArchivoLog($ruta = false){
			$SQL = new NeuralBDGab($this->conexion, 'tbl_archivos_programados');
			$SQL->Sentencia('ArchivoLog', $ruta);
			$SQL->Condicion('IdArchivosProgramados', $this->tarea);
			$SQL->Actualizar();
		}
		
		/**
		 * AppArreglosExcel::ejecutarErrorTarea($Error = false)
		 * 
		 * Guarda en log un error general de la tarea
		 * @param string $Error: mensaje de error
		 * @return void
		 */
		private function ejecutarErrorTarea($Error = false){
			$SQL = new NeuralBDGab($this->conexion, 'tbl_log_registro');
			$SQL->Sentencia('Idtarea', $this->tarea);
			$SQL->Sentencia('NumeroRegistro', 0);
			$SQL->Sentencia('TipoError', $Error);
			$SQL->Sentencia('StatusRegistro', "ERROR");
			$SQL->Insertar();
		}
		
		/**
		 * AppArreglosExcel::formatoTelefono()
		 * 
		 * Genera formato
		 * @param string $valor
		 * @param string $columna
		 * @return integer
		 */
		private function formatoTelefono($valor = false, $columna = false) {
			$formato = str_replace(array(' ', '-', '_', '.', ';', ',', '(', ')'), '', $valor);
			if(is_numeric($valor) == true):
				return $formato;
			else:
				$this->errores[] = array('columna' => $columna, 'mensaje' => 'NO ES UN VALOR NUMERICO');
			endif;
			return false;
		}
		
		/**
		 * AppArreglosExcel::formatoNumerico()
		 * 
		 * genera formato numerico
		 * @param integer $valor
		 * @param string $columna
		 * @return integer
		 */
		private function formatoNumerico($valor = false, $columna = false) {
			if(is_numeric($valor) == true):
				$formato = number_format(str_replace(',', '', $valor), 2, '.', '');
				if(is_float($valor) == false):
					return $formato;
				else:
					$this->errores[] = array('columna' => $columna, 'mensaje' => 'EL FORMATO NO ES CORRECTO');
				endif;
			else:
				$this->errores[] = array('columna' => $columna, 'mensaje' => 'NO ES UN VALOR NUMERICO');
			endif;
			return false;
		}
		
		/**
		 * AppArreglosExcel::formatoNombreTitular()
		 * 
		 * genera formato 
		 * @param string $valor
		 * @return string
		 */
		private function formatoCapital($valor = false, $columna = false) {
			if(is_string($valor) == true AND empty($valor) == false):
				return mb_convert_case(ucwords(mb_strtolower($valor, "UTF-8")), MB_CASE_TITLE, "UTF-8");
			else:
				$this->errores[] = array('columna' => $columna, 'mensaje' => 'NO ES UN VALOR DE TEXTO');
			endif;
			return false;
		}
		
		/**
		 * AppArreglosExcel::formatoClienteUnico()
		 * 
		 * formato de cliente unico 
		 * @param string $valor
		 * @return string
		 */
		private function formatoClienteUnico($valor = false) {
			if(empty($valor) == false):
				$formato = str_replace(' ', '', $valor);
				if(preg_match("/^([0-9]{1})-([0-9]{1,2})-([0-9]{3,4})-([0-9]{3,5})$/", $formato) == true):
					return $formato;
				else:
					$this->errores[] = array('columna' => 'ClienteUnico', 'mensaje' => 'FORMATO NO VALIDO');
				endif;
			else:
				$this->errores[] = array('columna' => 'ClienteUnico', 'mensaje' => 'CAMPO VACIO');
			endif;
			return false;
		}
		
		/**
		 * Metodo Publico
		 * ConsultaLog($IdTarea = false)
		 * 
		 * Consulta los errores 
		 */
		public function ConsultaLog(){
			$Consulta = new NeuralBDConsultas($this->conexion);
			$Consulta->Tabla('tbl_log_registro');
			$Consulta->Columnas('NumeroRegistro, Columna, TipoError, StatusRegistro');
			$Consulta->Condicion("Idtarea = '$this->tarea'");	
			$Resultado = $Consulta->Ejecutar(false, true);
			$this->EliminarLogTarea();	
			return $Resultado;	
		}
		
		/**
		 * metodo publico 
		 * NumeroActualizaciones()
		 * 
		 * Contabiliza el numero de actualizaciones
		 */
		public function NumeroActualizaciones(){
			$Consulta = new NeuralBDConsultas($this->conexion);
			$Consulta->Tabla('tbl_log_registro');
			$Consulta->Columnas('IdLogRegistro');
			$Consulta->Condicion("StatusRegistro ='ACTUALIZADO'");
			return $Consulta->Ejecutar(true, true);
		}
		
		/**
		 * metodo publico 
		 * NumeroNuevos()
		 * 
		 * Contabiliza el numero de registros nuevos
		 */
		public function NumeroNuevos(){
			$Consulta = new NeuralBDConsultas($this->conexion);
			$Consulta->Tabla('tbl_log_registro');
			$Consulta->Columnas('IdLogRegistro');
			$Consulta->Condicion("StatusRegistro ='NUEVO'");
			return $Consulta->Ejecutar(true, true);
		}
		
		/**
		 * Metodo privado
		 * EliminarLogTarea()
		 * 
		 * elimina los errores de determinada tarea
		 */
		private function EliminarLogTarea(){
			$SQL = new NeuralBDGab($this->conexion, 'tbl_log_registro');
			$SQL->Condicion('Idtarea', $this->tarea);
			$SQL->Eliminar();
		}	
	}