<?php
	
	class AppUtilidades extends AppSQLConsultas {
		
		/**
		 * Metodo Publico 
		 * ArregloEstados($Arreglo = false)
		 * 
		 * Elimina datos vacios de un Arreglo
		 * @param $Arreglo: Arreglo de datos
		 * @return $Lista: Arreglo de resultados
		 */
		public static function ArregloEstados($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo) == true){
				$Lista = $Arreglo;
				foreach($Arreglo as $Llave => $Valor){
					if($Valor == ""){
						unset($Lista[$Llave]);
					}
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ValidarCero($Arreglo = false)
		 * 
		 * Evalua si exiten ceros en las entradas y los cambia por 1
		 * @param $Arreglo: arreglo de datos ejem. $_POST
		 * @return $Arreglo: Arreglo con datos evaluados.
		 */
		public static function ValidarCero($Arreglo = false, $Campos = false){
			if($Arreglo == true){
				foreach($Arreglo AS $Llave => $Valor){
					if(in_array($Llave, $Campos)){
						$Arreglo[$Llave] = ($Valor == 0 AND $Valor != "") ? 1: $Arreglo[$Llave];	
					}
				}
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo Publico 
		 * CompruebaConsultas($Constula1 = false, $Consulta2 = false, $Consulta3 = false, $Consulta4 = false, $Consulta5 = false)
		 * 
		 * Comprueba si los arreglos de consultas tienen datos
		 * @param $ConsultaX: de 1 a 5 arreglos de consultas
		 * @return false si por lo menos un arreglo esta vacio, verdadero si todos tienen datos 
		 */
		public static function CompruebaConsultas($Constula1 = false, $Consulta2 = false, $Consulta3 = false, $Consulta4 = false, $Consulta5 = false, $Consulta6 = false){
	 		if(!$Constula1 OR empty($Constula1) OR !$Consulta2 OR empty($Consulta2) OR !$Consulta3 OR empty($Consulta3) OR !$Consulta4 OR empty($Consulta4) OR !$Consulta5 OR empty($Consulta5) OR !$Consulta6 OR empty($Consulta6)){
	 			return false;
	 		}
			 else{
	 			return true;
	 		}
		}
		
		/**
		 * Metodo Publico 
		 * AcortarRegistros($Matriz = false, $TotalRequeridos = false)
		 * 
		 * Simplifica una matriz a un numero de registros dado
		 * @param $Matriz: Matriz de datos
		 * @param $TotalRequeridos: Longitud de la nueva Matriz
		 * @return $Lista: Matriz de resultados
		 */
		public static function AcortarRegistros($Matriz = false, $TotalRequeridos = false){
			if($Matriz == true AND is_array($Matriz) == true AND $TotalRequeridos == true){
				return array_slice($Matriz, 0, $TotalRequeridos);
			}
		}
		
		/**
		 * Metodo Publico 
		 * MatrizCargaLaboral($IdAgente = false, $TotalRegistros)
		 * 
		 * Construye una matriz 
		 * @param $IdAgente: Datos Id
		 * @param $TotalRegistros: Longitud de la nueva Matriz
		 * @return $Lista: Matriz de resultados
		 */
		public static function MatrizCargaLaboral($TotalRegistros = false, $Fecha = false){
			if($TotalRegistros == true AND $Fecha == true){
				$Datos = array("FechaAsginacion" => $Fecha, "Status" => 'ASIGNADO');
				$Matriz = array_fill(0, $TotalRegistros, $Datos);
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConfirmaCampo($Arreglo = false, $Campo = false)
		 * 
		 * Elimina datos vacios y Confimar si un dato debe ser omitido o no
		 * @param $Arrglo: arreglo original de datos
		 * @param $Campo: Nombre del campo a confirmar
		 * @return $Arreglo: Arrglo resultado 
		 */
		public static function ConfirmaCampo($Arreglo = false, $Campos = false){
			if($Arreglo == true and $Campos == true){
				$Omitidos = array();
				foreach($Campos as $Llave => $Campo){
					if(empty($Arreglo[$Campo])){
						unset($Arreglo[$Campo]);
						$Omitidos[] = $Campo;
					}
					else{
						$Arreglo[$Campo] = ($Campo == 'password') ? NeuralCriptografia::Codificar($Arreglo[$Campo], APP): $Arreglo[$Campo];
					}
				}
				$Arreglo['Omitidos'] = $Omitidos;
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo Publico
		 * ArregloIndices($Arreglo = false)
		 * 
		 * Obtiene un nuevo Arreglo con los datos requeridos
		 * @param $Arreglo: Un arreglo de datos
		 * @return $ArregloIndices: Arreglo con los indices requeridos
		 */
		public static function ArregloIndices($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo) == true){
				$ArregloIndices = false;
				foreach($Arreglo AS $Llave => $Valor){
					if($Llave == 'IdGerencia' OR $Llave == 'IdCartera' OR $Llave == 'IdProvincia' OR $Llave == 'IdDistrito'){
						$Valor = trim($Valor);
						$ArregloIndices[$Llave] = $Valor; 
					}
				}
				return $ArregloIndices;
			}
		}
		
		/**
		 * Metodo Publico
		 * ObtenerRangos($Rango = false)
		 * 
		 * Obtiene el minimo y el maximo apartir de una cadena
		 * @param $Rango: String "min,max"
		 * @return Un Arreglo con dos elementos [0] = min, [1] = max
		 */
		public static function ObtenerRangos($Rango = false){
			if($Rango == true){
				if($Rango != '1'){
					$Rango= trim($Rango);
					return explode(",", $Rango);
	  			}	
			}
		}
		
		/**
		 * Metodo Publico
		 * RangosFecha($Rango = false)
		 * 
		 * Obtiene el minimo y el maximo apartir de una cadena
		 * @param $Rango: String "min-max"
		 * @return Un Arreglo con dos elementos [0] = min, [1] = max
		 */
		public static function RangosFecha($Rango = false){
			if($Rango == true){
				if($Rango != '1'){
					$Rango= trim(str_replace(" ", "", $Rango));
					return explode("-", $Rango);
	  			}	
			}
		}
		
		/**
		 * Metodo Publico
		 * CriteriosIndices($Arreglo = false)
		 * 
		 * Construye una Arreglo de Condiciones del tipo (campo = valor)
		 * @param $Arreglo: Arreglo de datos requeridos
		 * @return $Criterios: Arreglo de condiciones
		 */
		public static function CriteriosIndices($Arreglo = false){
			$Criterios = false;
			if($Arreglo == true AND is_array($Arreglo) == true){
				foreach($Arreglo AS $Campo => $Valor){
					$Criterios[] = $Campo.' = '.$Valor;
				}
			}
			return $Criterios;
		} 
		
		/**
		 * Metodo Publico
		 * CriteriosRango($Rango = false, $Campo = false)
		 * 
		 * Construye un cadena de condicion para una consulta de Rango
		 * @param $Rango: Arreglo de Rangos 
		 * @param $Campo: Campo donde buscar el rango
		 * @return condicion de busqueda en Rango
		 */
		public static function CriteriosRango($Rango = false, $Campo = false){
			if($Rango == true AND $Campo == true ){
				return $Campo. ' BETWEEN ' . $Rango[0] . ' AND ' . $Rango[1];
			}
		}
		
		/**
		 * Metodo Publico
		 * CondicionRangoFecha($Rango = false, $Campo = false)
		 * 
		 * Construye un cadena de condicion para una consulta de Rango de fechas
		 * @param $Minimo: número inicial del rango
		 * @param $Maximo: número final del rango 
		 * @param $Campo: Campo donde buscar el rango
		 * @return condicion de busqueda en Rango
		 */
		public static function CondicionRangoFecha($Minimo = false, $Maximo = false, $Campo = false){
			if($Minimo == true AND $Maximo == true AND $Campo == true){
				return $Campo. ' BETWEEN "' . $Minimo. '" AND "' . $Maximo.'"';
			}
		}
		
		/**
		 * Método Público
		 * CondicionRango($Minimo = false, $Maximo = false, $Campo = false)
		 * 
		 * Crea una una condicion de busqueda de datos por rango
		 * @param $Minimo: número inicial del rango
		 * @param $Maximo: número final del rango
		 * @return Una condicion de rango en un String
		 */
		public static function CondicionRango($Minimo = false, $Maximo = false, $Campo = false){
			if($Minimo == true AND $Maximo == true AND $Campo == true){
				return $Campo. ' BETWEEN ' . $Minimo . ' AND ' . $Maximo;	
			}
		}
		
		/**
		 * Metodo Private
		 * ObtenerRango($CadenaRango = false)
		 * 
		 * Obtiene rangos apartir de una cadena (Especificamente rangos de fechas, aunque 
		 *  funcina para cadenas que se requiran separa por un guin medio)
		 * @param $CadenaRango: String con datos a separar
		 * @return Arreglo con elemnetos separado por -
		 */
		private static function ObtenerRango($CadenaRango = false){
			if($CadenaRango == true){
				$CadenaRango = trim(str_replace(" ", "", $CadenaRango));
				return explode('-', $CadenaRango);
			}
		}
		
		/**
		 * Metodo Privado
		 * CondicionComodines($Arreglo = false, $Campo = false, $Requeridos = false)
		 * 
		 * Crea una condicion de busqueda por comodines
		 * @param $Arreglo: Array de datos
		 * @param $Campo: Columna de la tabla donde buscar
		 * @param $Requeridos: Campos del arreglo que se usaran para la consulta
		 * @return Condicion en String
		 */
		private static function CondicionComodines($Arreglo = false, $Campo = false, $Requeridos = false){
			if($Arreglo == true and $Requeridos == true ){
				foreach($Arreglo AS $LLave => $Valor){
					if(in_array($LLave, $Requeridos)){
						if(empty($Valor)){
							$Comodines[] = '%';
						}else{
							$Comodines[] = $Valor;
						}	
					}
				}
				return self::GeneraCondicion($Comodines, $Campo);	
			}
		}
		
		/**
		 * Metodo Publico
		 * AppUtilidades::ArregloCriterios($Arreglo = false, $DatosPost = false)
		 * 
		 * Genera un arreglo de condiciones segun los criterios de busqueda.
		 * @param $Arreglo: Arreglo de datos sin datos vacios.
		 * @param $DatosPost: Arreglo de datos posiblemente con datos vacios.
		 * @return $ArregloCriterios: Arreglo con condiciones.
		 */
		public static function ArregloCriterios($Arreglo = false, $DatosPost = false){
			if($Arreglo == true){
				$ArregloCriterios = false;
				if(isset($Arreglo['IdGerencia']) OR isset($Arreglo['IdCartera']) OR isset($Arreglo['IdDistrito']) OR isset($Arreglo['IdProvincia'])){
					$ArregloCriterios = self::CriteriosIndices(self::ArregloIndices($Arreglo));
				}
				if(isset($Arreglo['Semanas'])){
					$ArregloCriterios[] = self::CondicionRango($Arreglo['minimoSemanas'], $Arreglo['maximoSemanas'], 'SemanaAtraso');
				}
				if(isset($Arreglo['RangoSaldo'])){
					$ArregloCriterios[] = self::CondicionRango($Arreglo['minimoSaldo'], $Arreglo['maximoSaldo'], 'Saldo');
				}
				if(isset($Arreglo['RangoSaldoTotal'])){
					$ArregloCriterios[] = self::CondicionRango($Arreglo['minimoSaldoTotal'], $Arreglo['maximoSaldoTotal'], 'SaldoTotal');
				}
				if(isset($Arreglo['NombreCliente'])){
					$ArregloCriterios[] = "tbl_datos_agenda.NombreTitular LIKE '%". $Arreglo['NombreTitular']."%'";
				}
				if(isset($Arreglo['Aval'])){
					$ArregloCriterios[] = "tbl_datos_agenda.NombreAval LIKE '%". $Arreglo['NombreAval']."%'";
				}
				if(isset($Arreglo['Fecha'])){
					$Arreglo['FechaAsginacion']= AppFechas::FormatoFecha($Arreglo['FechaAsginacion']);// date("Y-m-d", strtotime($Arreglo['FechaAsginacion']));
					$ArregloCriterios[] = "tbl_asignacion_registros.FechaAsginacion LIKE '%".$Arreglo['FechaAsginacion']."%'";
				}
				if(isset($Arreglo['ClienteUnico'])){
					$ArregloCriterios[] = self::CondicionComodines($DatosPost, 'ClienteUnico', array('Pais', 'Producto', 'NumeroEconomico', 'Operacion'));
				}
				if(isset($Arreglo['Telefono'])){
					$ArregloCriterios[] = "(tbl_datos_agenda.Telefono1 = '". $Arreglo['TelefonoCasa']."' OR tbl_datos_agenda.Telefono2 = '". $Arreglo['TelefonoCasa']."' OR tbl_datos_agenda.Telefono3 = '". $Arreglo['TelefonoCasa']."' OR tbl_datos_agenda.Telefono4 = '". $Arreglo['TelefonoCasa']."') ";
				}
				if(isset($Arreglo['Compromiso'])){
					$ArregloCriterios[] = "Compromiso = 'Si'";
				}
				if(isset($Arreglo['FechaGestion'])){
					$Rango = self::ObtenerRango($Arreglo['FechasGestion']);	
					foreach($Rango as $Columna => $Valor){
						$Rango[$Columna] = AppFechas::FormatoFecha($Valor);
					}
					$ArregloCriterios[] = self::CondicionesRango($Rango[0], $Rango[1], 'FechaHora_Captura');
				}
				if(isset($Arreglo['FechaCompromiso'])) {
					$Rango = self::ObtenerRango($Arreglo['FechasCompromiso']);	
					foreach($Rango as $Columna => $Valor){
						$Rango[$Columna] = AppFechas::FormatoFecha($Valor);
					}
					$ArregloCriterios[] = self::CondicionesRango($Rango[0], $Rango[1], 'Fecha');
				}
				
				if(isset($Arreglo['Agente'])){
					$ArregloCriterios[] = "tbl_agentes_asignado_supervisor.IdAgente = ".$Arreglo['IdAgente'];	
				}
				
				if(isset($Arreglo['CodigoColor'])){
					$ArregloCriterios['Color'] = "tbl_datos_agenda.IdCodigo = ".$Arreglo['IdCodigo'];
				}
				return $ArregloCriterios;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ArregloExtra($Arreglo = false)
		 * 
		 * Genera condiciones como cadenas para fechas en un arreglo.
		 * @param $Arreglo: Arreglo de datos
		 * @return $ArregloCondiciones: condiciones en string
		 */
		public static function ArregloExtra($Arreglo = false){
			if($Arreglo == true){
				$ArregloCondiciones = array();
				if(isset($Arreglo['FechaCompromiso'])){
					$Rango = self::RangosFecha($Arreglo['FechasCompromiso']);
					$ArregloCondiciones['FechasCompromiso'] = self::CondicionRangoFecha(AppFechas::FormatoFecha($Rango[0]), AppFechas::FormatoFecha($Rango[1]), "Fecha");
				}
				if(isset($Arreglo['FechaGestion'])){
					$Rango = self::RangosFecha($Arreglo['FechasGestion']);
					$ArregloCondiciones['FechasGestion'] = self::CondicionRangoFecha(AppFechas::FormatoFecha($Rango[0]), AppFechas::FormatoFecha($Rango[1]), "FechaHora_Captura");
				}
				return $ArregloCondiciones;
			}
		}
		
				/**
		 * Metodo Private
		 * CondicionesRango($Minimo = false, $Maximo = false, $Campo = false)
		 * 
		 * Crea condiciones de rango para rango tiempo y fechas
		 * @param $Minimo: El valor del minimo
		 * @param $Maxima: El valor del maximo
		 * @return $Campo: Nombre del campo de la tabla.
		 */
		private static function CondicionesRango($Minimo = false, $Maximo = false, $Campo = false){
			if(isset($Minimo) and $Maximo == true and $Campo == true){
				if(is_numeric($Minimo) AND is_numeric($Maximo)){
					return 'ROUND('. $Campo .')/60 >= '. $Minimo .' AND ROUND('. $Campo .')/60 <= '. $Maximo;
				}
				else{
					return 'DATE('.$Campo .') BETWEEN "'. $Minimo .'" AND "'. $Maximo .'"';
				}
			}
		}
		
		/**
		 * Metodo Privado
		 * GeneraCondicion($Arreglo = false, $Campo = false)
		 * 
		 * Crea la condición a partir de una arreglo con comodines
		 * @param $Arreglo: Arreglo evaluado con valores y comodines
		 * @param $Campo: Columna de la tabla a consultar
		 * @return Cadena de condicion
		 */
		private static function GeneraCondicion($Arreglo = false, $Campo = false){
			if($Arreglo == true and $Campo == true){
				for($i = 0; $i < count($Arreglo); $i++){
					if($i > 0){
						if($Arreglo[($i-1)] != $Arreglo[$i]){
							$Comodines[] = $Arreglo[$i];
						}
					}
					else{
						$Comodines[] = $Arreglo[$i];
					}
				}
				return $Campo. " LIKE '". implode('-',$Comodines). "'";
			}
		}
		
		/**
		 * Metodo Publico
		 * EliminaRegistro($Matriz = false, $Id = false)
		 * 
		 * Elimina el registro que contenga el dato seleccionado
		 * @param $Matriz: Matriz de datoss
		 * @param $Id: Dato de busquda, criterio de eliminacion
		 * @return $MatrizReducida: Matriz sin el registro solicitado
		 */
		public static function EliminaRegistro($Matriz = false, $Id = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Id == true){
				$MatrizReducida = false;
				foreach($Matriz as $Arreglo){
					if(!(in_array($Id, $Arreglo))){
						$MatrizReducida[] = $Arreglo;
					}
				}
				return $MatrizReducida;
			}
		}
		
		/**
		 * Metodo Publico 
		 * MatrizInsercion($MatrizRegistros = false, $ArregloAgentes = false, $Fecha = false)
		 * 
		 * Crea una matriz de registros a insertar 
		 * @param $MatrizRegistros: Matriz de llaves de los registros
		 * @param $ArregloAgentes: Arreglo de Ids de los Agentes
		 * @param $Fecha: La en que se hace la transaccion
		 * @return $Matriz: matriz de resultados
		 */
		public static function MatrizInsercion($MatrizRegistros = false, $ArregloAgentes = false, $Fecha = false){
			if($MatrizRegistros == true AND is_array($MatrizRegistros) == true AND $ArregloAgentes == true AND is_array($ArregloAgentes) == true AND $Fecha == true){
				$Contador = 0;
				$CantidadRegistros = count($MatrizRegistros); 
				$CantidadAgentes = count($ArregloAgentes);
				for($i = 0; $i < $CantidadRegistros; $i++){
					for($j = 0; $j < $CantidadAgentes; $j++){
						$Matriz[$Contador]['IdAsignacionRegistro'] = $MatrizRegistros[$i]['IdAsignacionRegistro'];
						$Matriz[$Contador]['IdAgente'] = $ArregloAgentes[$j];
						$Matriz[$Contador]['FechaAsginacion'] = $Fecha;
						$Contador++;
					}
				}
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Publico
		 * ArregloAgentes($Arreglo = false, $Campos = false)
		 * 
		 * Construye un arreglo de llaves de los agentes seleccionados
		 * @param $Arreglo: Arreglo de datos, puede ser $_POST
		 * @param $Campos: Indices del nuevo Arreglo
		 * @return $Lista: Arreglo simplificado de IDs de Agentes
		 */
		public static function ArregloAgentes($Arreglo = false, $Campos = false){
			if($Arreglo == true AND $Campos == true){
				foreach($Arreglo as $Campo => $Valor){
					if(in_array($Campo, $Campos)){
						$Lista[]= $Valor;
					}
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaCantidadRegistros($Arreglo = false)
		 * 
		 * Consulta el numero de registro asignados a cada Agente
		 * @param $Arreglo: Arreglo con los datos de los agentes
		 * @return $Arregoo: Arreglo con la nueva columna (catidad)
		 */
		public static function ConsultaCantidadRegistros($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				for($i = 0; $i < count($Arreglo); $i++){
					if($Arreglo[$i]['IdAgente']){
						$Consulta = self::ConsultaRegistrosAsignadosAgentes($Arreglo[$i]['IdAgente']);
						$Arreglo[$i]['Cantidad'] = $Consulta['Cantidad'];
					}
				}
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaRegistrosAsignadosAgentes($IdAgente = false)
		 * 
		 * Consulta el numero de registros asignados a un Agente
		 * @param $IdAgente: Identificador del Agente o criterio de busqueda
		 * @return Resultado de la consulta. numero de registros asignados a un Agente
		 */
		private static function ConsultaRegistrosAsignadosAgentes($IdAgente = false){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_agente_asignado');
				$Consulta->Columnas('tbl_agente_asignado.IdAgente');
				$Consulta->InnerJoin('tbl_asignacion_registros', 'tbl_agente_asignado.IdAsignacionRegistro', 'tbl_asignacion_registros.IdAsignacionRegistro');
				$Consulta->Condicion("tbl_asignacion_registros.Status = 'ASIGNADO'");
				$Consulta->Condicion("tbl_agente_asignado.IdAgente = '$IdAgente'");
				return $Consulta->Ejecutar(true, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaLlavesdeRegistrosNoAsignados($Matriz = false)
		 * 
		 * Intercambia el IdDatoAgenda por las llaves principales de la tabla
		 * @param $Matriz: Matriz con los Ids o datos a buscar.
		 * @return El resultado de la consulta
		 */
		public static function ConsultaLlavesdeRegistrosNoAsignados($MatrizLlaves = false){
			if($MatrizLlaves == true AND is_array($MatrizLlaves) == true){	
				foreach($MatrizLlaves AS $Arreglo){
					foreach($Arreglo AS $Columna => $Valor){
						$Matriz[]['IdAsignacionRegistro'] = self::ConsultaLlave($Columna, $Valor);
					}
				}
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaLlave($Columna, $Valor)
		 * 
		 * Consulta un dato, mediande el valor de busqueda
		 * @param $Columna: Campo en el que se realiza la busqueda
		 * @param $Valor: Datos que se quiere buscar
		 * @return Resultado indice del registro
		 */
		private static function  ConsultaLlave($Columna = false, $Valor = false){
			if($Columna == true AND $Valor == true){			
			    $SQL = 'SELECT IdAsignacionRegistro FROM tbl_asignacion_registros WHERE '.$Columna.'='.$Valor;
  				$Conexion = NeuralConexionDB::PDO(APP);
  				$Consulta = $Conexion->prepare($SQL);
  				$Consulta->execute();
 		    	$Resultado = $Consulta->fetch(PDO::FETCH_ASSOC);
 				return $Resultado['IdAsignacionRegistro'];
			}			
		}
		
		/**
		 * Metodo Publico 
		 * FormatoNumeros($Matriz = false, $Requeridos = false)
		 * 
		 * Da formato a las columnas con datos numericos, y valida que sean numeros
		 * @param $Matriz: Matriz de datos
		 * @param $Requeridos: Columnas con datos numericos
		 * @return $Registros: Matriz con datos numericos validados
		 */
		public static function FormatoNumeros($Arreglo = false, $Requeridos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Requeridos == true AND is_array($Requeridos) == true){
				foreach($Arreglo as $Campo => $Valor){
						if(in_array($Campo, $Requeridos)){
							$Busqueda = array('$', ',');
							$Numero = trim(number_format(str_replace($Busqueda, '', $Valor), 2, '.', ''));
							$Lista[$Campo] = (is_float($Numero) == true) ? '': $Numero;					
						}
						else{
							$Lista[$Campo] = $Valor;
						}
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Publico
		 * AppUtilidades::FormatoFecha($Arreglo = false)
		 * 
		 * da formato a una fecha d/m/Y a d-m-Y
		 * @param $Arreglo: arreglo con fechas tipo d/m/Y
		 * @return $Arreglo: arreglo con fechas d-m-Y
		 * */
		public static function FormatoFecha($Arreglo = false){
			if($Arreglo == true){
				for($i = 0; $i < count($Arreglo); $i++){
					$Arreglo[$i] = date("Y-m-d", strtotime($Arreglo[$i]));
				}
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo Publico 
		 * EstadosGestion($MatrizConsulta = false, $Tabla = false, $Llave = false)
		 * 
		 * Verifica si los registros consultados ha sido gestionados
		 * @param $MatrizConsulta: Datos de la consulta
		 * @param $Tabla: Tabla de gestion (Campo o Telefonica)
		 * @param $Llave: Campo de comparacion.
		 * @return $Lista: Matriz con status de gestion
		 */
		public static function EstadosGestion($MatrizConsulta = false, $Tabla = false, $Llave = false){
			if($MatrizConsulta == true and $Tabla == true and $Llave == true){
				$Consulta = self::ConsultaEstatusGestion($Tabla, $Llave);
				$Consulta = self::arrayColumn($Consulta, $Llave);
				foreach($MatrizConsulta as $Arreglo){
					if(isset($Arreglo['StatusGestion']) AND $Arreglo['StatusGestion'] == "GESTIONADO"){
						$Lista[] = $Arreglo;
					}else{
						$Arreglo['StatusGestion'] = (in_array($Arreglo[$Llave], $Consulta)) ? "GESTIONADO" : "NOGESTIONADO";
						$Lista[] = $Arreglo;
					}
				}
				return $Lista;	
			}
		}
		
		/**
		 * Metodo Privado
		 * arrayColumn(array $array, $column_key, $index_key = null)
		 * 
		 * Conviarte una columna de una matriz en un vector
		 */
		public static function arrayColumn(array $array, $column_key, $index_key = null){
	        if(function_exists('array_column')){
	            return array_column($array, $column_key, $index_key);
	        }
	        $result = [];
	        foreach($array as $arr){
	            if(!is_array($arr)) continue;
	            if(is_null($column_key)){
	                $value = $arr;
	            }else{
	                $value = $arr[$column_key];
	            }
	
	            if(!is_null($index_key)){
	                $key = $arr[$index_key];
	                $result[$key] = $value;
	            }else{
	                $result[] = $value;
	            }
	
	        }
	        unset($array);
	        return $result;
	    }
	    
		/**
		 * Metodo Publico 
		 *
		 * En caso de que la versión de PHP no soporte array_column se utiliza esta función
		 * @param  [type] $input     [description]
		 * @param  [type] $columnKey [description]
		 * @param  [type] $indexKey  [description]
		 * @return [type]            [description]
		 */
		public static function array_column($input = null, $columnKey = null, $indexKey = null){
	        $argc = func_num_args();
	        $params = func_get_args();
	        if ($argc < 2) {
	            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
	            return null;
	        }
	        if (!is_array($params[0])) {
	            trigger_error(
	                'array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given',
	                E_USER_WARNING
	            );
	            return null;
	        }
	        if (!is_int($params[1])
	            && !is_float($params[1])
	            && !is_string($params[1])
	            && $params[1] !== null
	            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
	        ) {
	            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
	            return false;
	        }
	        if (isset($params[2])
	            && !is_int($params[2])
	            && !is_float($params[2])
	            && !is_string($params[2])
	            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
	        ) {
	            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
	            return false;
	        }
	        $paramsInput = $params[0];
	        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;
	        $paramsIndexKey = null;
	        if (isset($params[2])) {
	            if (is_float($params[2]) || is_int($params[2])) {
	                $paramsIndexKey = (int) $params[2];
	            } else {
	                $paramsIndexKey = (string) $params[2];
	            }
	        }
	        $resultArray = array();
	        foreach ($paramsInput as $row) {
	            $key = $value = null;
	            $keySet = $valueSet = false;
	            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
	                $keySet = true;
	                $key = (string) $row[$paramsIndexKey];
	            }
	            if ($paramsColumnKey === null) {
	                $valueSet = true;
	                $value = $row;
	            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
	                $valueSet = true;
	                $value = $row[$paramsColumnKey];
	            }
	            if ($valueSet) {
	                if ($keySet) {
	                    $resultArray[$key] = $value;
	                } else {
	                    $resultArray[] = $value;
	                }
	            }
	        }
	        return $resultArray;
	    }
		
		/**
		 * Metodo Privado
		 * ConsultaEstatusGestion($Valor = false, $Tabla = false, $Llave = false)
		 * 
		 * Consulta uno a uno si los registros han  sido gestionados
		 * @param $Valor: Valor a buscar.
		 * @param $Tabla: Tabla de gestion (Campo o Telefonica)
		 * @param $Llave: Campo de comparacion.
		 * @return $Lista: Matriz con status de gestion
		 */
		private static function ConsultaEstatusGestion($Tabla = false, $Llave = false){
			if($Tabla == true and $Llave == true){
				$SQL = "SELECT ".$Llave ." FROM ".$Tabla;
				$Conexion = NeuralConexionDB::DoctrineDBAL(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);				
			}
		}
		
		/**
		 * AppUtilidades::ComparaFechaSistema($ArregloDatos = false)
		 * 
		 * Compara la fecha de progrmacion de cada tarea con la actual.
		 * @param $ArregloDatos Arreglo con una columna de fecha
		 */
		public static function ComparaFechaSistema($ArregloDatos = false){
 			if($ArregloDatos == true){
 				$Lista = array();
 				$FechaSistema = AppFechas::ObtenerDatetimeActual();
 				foreach ($ArregloDatos as $Arreglo){
					if($Arreglo['FechaHoraCarga'] <= $FechaSistema){
						$Lista[] = $Arreglo;	
					}				
 				}
 				return $Lista;
 			}
		}

		/**
		 * Metodo Publico
		 * CombinaDosArrelos($ArregloFechas = false, $ArregloImportes = false, $Columna1 = false, $Columna2 =false)
		 *
		 * Combina dos arreglos de indices incrementales en una matriz
		 * @param $ArregloFechas: como ejemplo un arreglo de fechas
		 * @param $ArregloImportes: como ejemplo un arreglo de importes
		 * @param String $Columna1: Nombre que tendra la primer columana (Correspondiente al primer arreglo)
		 * @param String $Columna2: Nombre que tendra la segunda columana (Correspondiente al segundo arreglo)
		 * @return Array $Lista: Matriz de indices asocioados al valor.
		 */
		public static function CombinaDosArrelos($ArregloFechas = false, $ArregloImportes = false, $Columna1 = false, $Columna2 =false){
			if($ArregloFechas == true and $ArregloImportes == true and $Columna1 == true and $Columna2 == true){
				if(count($ArregloFechas) == count($ArregloImportes)){
					for($i = 0; $i < count($ArregloFechas); $i++){
						$Lista[$i][$Columna1] = $ArregloFechas[$i];
						$Lista[$i][$Columna2] = $ArregloImportes[$i];
					}
					return $Lista;
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * OrdenarMatrizColumna($MatrizRegistros = false, $Columna = false, $Orden = false)
		 * 
		 * Ordena una matriz de registros en funcion de la columna indicada
		 * @param $MatrizRegistros: Matriz de registros desordenados
		 * @param $Columna: String Nombre de columna 
		 * @param $Orden: String ASC o DESC
		 * @return $$MatrizRegistros: Matriz de registros Ordenados
		 */
		public static function OrdenarMatrizColumna($MatrizRegistros = false, $Columna = false, $Orden = false){
			if($MatrizRegistros == true and $Columna == true and $Orden == true){
				if($Orden == "ASC"){
					$Orden = SORT_ASC;
				}elseif($Orden == "DESC"){
					$Orden = SORT_DESC;
				}else{
					return false;
				}
				foreach($MatrizRegistros as $Arreglo){
					$Lista[] = $Arreglo[$Columna];
				}
				array_multisort($Lista, $Orden, $MatrizRegistros);
				return $MatrizRegistros;
			}
		}
		
		/**
		 * Metodo Publico
		 * AgruparMatriz($Matriz = false, $Columna = false)
		 * 
		 * Agrupar los registros de una matriz en función de una columna especifica
		 * @param $Matriz: arreglo de registros
		 * @param $Columna: nombre de columna por la cual se desea agrupar
		 * @return $temp_array: Matriz con registros agrupados
		 */
		public static function AgruparMatriz($Matriz = false, $Columna = false){
			if($Matriz == true and $Columna == true){
				$temp_array = array();
			    $i = 0;
			    $key_array = array();
				foreach($Matriz as $Arreglo){
			        if(!in_array($Arreglo[$Columna],$key_array)){
			            $key_array[$i] = $Arreglo[$Columna];
			            $temp_array[$i] = $Arreglo;
			        }
			        $i++;
    			}
				return $temp_array;
			}
		}
		
		/**
		 * Metodo Publico
		 * ObtenerTotalColumna($ArregloDatos = false, $Columna = false)
		 * 
		 * hace una suma de los datos de una columna
		 * @param $ArregloDatos: Arreglo de donde se extraeran los datos a sumar
		 * @param $Columna: nombre de la columna a sumar
		 * @return $Total: Suma total
		 */
		public static function ObtenerTotalColumna($ArregloDatos = false, $Columna = false){
			if($ArregloDatos == true and $Columna == true){
				$Total = 0;
				foreach($ArregloDatos as $Arreglo){
					$Total += $Arreglo[$Columna];
				}
				return $Total;
			}
		} 
		
		/**
		 * Metodo Publico 
		 * DescartarRegistro($ArregloDatos = false, $Llaves = false)
		 * 
		 * Elimina los registro que ya han sido seleccionados
		 * @param $ArregloDatos: Matriz de datos
		 * @param $Llaves: Arrglo con datos a descartar
		 * @return $ArregloDatos: Matriz reducida
		 */
		public static function DescartarRegistro($ArregloDatos = false, $Llaves = false){
			if($ArregloDatos == true and $Llaves == true){
				$AuxArreglo = $ArregloDatos;
				for($i = 0; $i < count($AuxArreglo); $i++){
					foreach($Llaves as $columna => $Valor){
		   				if((in_array($Valor, $AuxArreglo[$i]))){
							unset($ArregloDatos[$i]);	
					    }	
					}	
				}
				return $ArregloDatos;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ComprobarTroncalesAsignadas($DatosSupervisor = false)
		 * 
		 * busca el id de un supervisor en la asignacionde troncales
		 * @param $DatosSupervisor: Consulta de los supervisores
		 * @return $Lista, en caso de encuentre conicidencias se agregan nuevas columnas
		 */
		public static function ComprobarTroncalesAsignadas($DatosSupervisor = false){
			if($DatosSupervisor == true){
				foreach($DatosSupervisor as $Arreglo){
					$Consulta = self::ConsultaTroncalAsignada($Arreglo['idUsuario']);
					if($Consulta['Cantidad'] > 0){
						$Arreglo['IdTroncalAsignada'] = $Consulta[0]['IdTroncalAsignada'];
						$Arreglo['NombreTroncal'] = $Consulta[0]['NombreTroncal']; 
						$Arreglo['Status'] = "ASIGNADO";
					}else{
						$Arreglo['Status'] = "NOASIGNADO";
					}
					$Lista[] = $Arreglo;
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaTroncalAsignada($IdSupervisor = false)
		 * 
		 * Consulta en la base la coincidencia
		 * @param $IdSupervisor: Identificador del supervisor
		 * @return datos de consulta.
		 */
		private static function ConsultaTroncalAsignada($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_troncal_asignada_supervisor');
				$Consulta->Columnas('IdTroncalAsignada, tbl_troncales.NombreTroncal');
				$Consulta->InnerJoin('tbl_troncales', 'tbl_troncal_asignada_supervisor.IdTroncal', 'tbl_troncales.IdTroncal');
				$Consulta->Condicion("Idsupervisor = '$IdSupervisor'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * EstatusTelefonos($Consulta = false, $Campo = false)
		 * 
		 * Consulta la disponibilidad de telefonos para un registro
		 * @param $Consulta: Array de datos.
		 */
		public static function EstatusTelefonos($Consulta = false, $Campo = false){
			if($Consulta == true){
				foreach($Consulta as $Arreglo){
					foreach($Arreglo as $Columna=>$Valor){
						if($Columna == $Campo){
							$Arreglo['StatusTelefono'] = self::ConsultaStatusTelefonos($Valor);
						}
					}
					$Lista[] = $Arreglo;
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaStatusTelefonos($IdAgenda = false)
		 * 
		 * Consulta si existe telefonos en la bd para un cliente dado.
		 * @param $IdAgenda: Identificador del agente
		 * @return string de estado (Disponible o nodisponible)
		 */
		private static function ConsultaStatusTelefonos($IdAgenda = false){
			if($IdAgenda == true){
				$SQL = 'SELECT Telefono1, Telefono2, Telefono3, Telefono4 FROM tbl_datos_agenda WHERE IdDatoAgenda="'.$IdAgenda.'"';
				$Conexion = NeuralConexionDB::PDO(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetch();
				$Resultado = self::ArregloEstados($Resultado);
				$Tamano = count($Resultado);
				unset($Resultado, $Consulta, $SQL, $Conexion);
				return ($Tamano > 0) ? "DISPONIBLE" : "NODISPONIBLE";
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarEliminadoActivo($ArregloDatos = false, $Consulta = false, $Tabla = false, $Omitidos = false)
		 *
		 * Actualiza el estatus de un registro en caso de ser ELIMINADO
		 * @param boolean $ArregloDatos [array de datos a actualizar]
		 * @param boolean $Consulta     [array de id y estatus de los registros ya existentes]
		 * @param boolean $Tabla        [tabla selccionada]
		 * @param boolean $Omitidos     [array de datos omitidos]
		 */
		public static function ActualizarEliminadoActivo($ArregloDatos = false, $Consulta = false, $Tabla = false, $Omitidos = false){
			if($ArregloDatos == true and $Consulta == true and $Tabla == true and $Omitidos == true){				
				foreach($Consulta as $Registro){
					if(in_array('ELIMINADO', $Registro)){
						$Condiciones = array("IdUsuario" => $Registro['IdUsuario'], "Status" => "ELIMINADO");
						self::ActualizarDatos($ArregloDatos, $Condiciones, $Tabla, $Omitidos, APP);
						return true;
					}
				}
			}
		}

		/**
		 * [ObtenerIndices description]
		 * @param  boolean $Arreglo [description]
		 * @return boolean $Arreglo [<description>]
		 */
		public static function ObtenerIndices($Arreglo = false){
			if($Arreglo == true and is_array($Arreglo)){
				foreach ($Arreglo as $key => $value) {
					$Lista[] = array("Telefono" => $value, "Indice" => $key);
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Publico 
		 * EscribirErrores($Arreglo = false, $Ruta = false, $Delimitador = false)
		 * 
		 * Crear el archivo de log
		 */
		public static function EscribirErrores($Arreglo = false, $Ruta = false, $Delimitador = false){
			if($Arreglo == true){				
			    $Archivo = fopen($Ruta, 'w');
			    foreach ($Arreglo as $line) {
			        fputcsv($Archivo, $line, $Delimitador);
			    }
			    fseek($Archivo, 0);
			    fpassthru($Archivo);
			    return fclose($Archivo);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ObtenerSubArreglo($Arreglo, $Inicio, $Cantidad)
		 * 
		 * Obtener un sub arreglo a partir de otro
		 * @param [type] $Arreglo  [description]
		 * @param [type] $Inicio   [description]
		 * @param [type] $Cantidad [description]
		 */
		public static function ObtenerSubArreglo($Arreglo, $Inicio, $Cantidad){
			if($Inicio == true and $Cantidad == true){
				return array_slice($Arreglo, $Inicio, $Cantidad);
			}	
		}
		
		/**
		 * Metodo Publico 
		 * ColumnaFinalLlamada($Arreglo = false)
		 * 
		 * Genera formato una columna  apartir de otra para un arreglo
		 * @param $Arreglo: arreglo base
		 * @return $Arreglo: arreglo con nueva columna
		 */
		public static function ColumnaFinalLlamada($Arreglo = false){
			if($Arreglo == true){
				$i = 0;
				foreach($Arreglo AS $Registro){
					$Inicio = new DateTime($Registro['calldate']);
					$d = (int)($Registro['duration']);
					$Inicio->add(new DateInterval('PT'. $d .'S'));
					$Arreglo[$i]['Fin'] = $Inicio->format('Y-m-d H:i:s');
					$i++;
				}
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo Publico
		 * AppUtilidades::EviarCorreo
		 * 
		 * Eviad un correo con el repor adjunto.
		 * @param $Ruta: Rurta fisica del archivo
		 * @param $CorreoEviar: direccion de correo del agente
		 * @param $DatosReporte: Tipo de gestion
		 */
		public static function EnviarCorreoArchivo($Ruta = false, $CorreoEnviar = false, $DatosReporte = false, $MensajePersonalizado = false){
			$Correo = new NeuralCorreoSwiftMailer(APP, 3, 'html', 'UTF-8');
			$Mensaje = "Archivo adjunto del reporte de Gestion ". $DatosReporte;
			$Asunto = "Reporte de Gestion ".$DatosReporte." ".AppFechas::ObtenerFechaActual();
			if($MensajePersonalizado):
				$Mensaje = $MensajePersonalizado." ". $DatosReporte;
				$Asunto = "Reporte de Registros de clientes".AppFechas::ObtenerFechaActual();
			endif;
			$Correo->Asunto($Asunto);
			$Correo->EnviarA($CorreoEnviar);
			$Correo->Remitente('.:: ZICOM Group ::.', 'contacto@zicomgroup.com');
			$Correo->MensajeAlternativo($Mensaje);
			$Correo->Mensaje($Mensaje);
			$Correo->ArchivoAdjunto($Ruta);
			$Correo->EnviarCorreo();
			unset($Mensaje, $Correo);
		}
		
		/**
		 * Metodo Publico 
		 * RemplazaIndicesValores($Base = false, $Remplazos = false, $Campos = false)
		 * 
		 * Remplaza los idices por los valores correpondientes, de varios arreglos
		 * @param $Base: arreglo base
		 * @param $Remplazos: Matriz de remplazos 
		 * @param $Campos: Array de columnas indica Indice y valor de remplazos de reemplazo (el orede debe ser acorde al orden de los remplazos)
		 */
		public static function RemplazaIndicesValores($Base = false, $Remplazos = false, $Campos = false){
			if($Base == true AND is_array($Base) == true){
				$i = 0;
				$j = 0;
				foreach($Base as $Arreglo){
					foreach($Remplazos as $Remplazo){
						foreach($Remplazo AS $Registro){
							if($Registro[$Campos[$j]['Id']] == $Arreglo[$Campos[$j]['Id']]){
								$Base[$i][$Campos[$j]['Id']] = $Registro[$Campos[$j]['valor']]; 
							}	
						}
						$j++;
				    }
				    $j = 0;
					$i++;
				}
				return $Base;
			}
		}
		
		/**
		 * Metodo publico 
		 * PorcentajeCompromiso($ArregloDatos = false)
		 * 
		 * Calcula el porcentaje de compromiso de pago
		 * @param $ArregloDatos: array con columnas a cuantificar
		 */
		public static function PorcentajeCompromiso($ArregloDatos = false){
				$ArregloDatos = array_count_values($ArregloDatos);
				$ArregloDatos['SI'] = (isset($ArregloDatos['SI']) == true ) ? $ArregloDatos['SI'] : 0;
				$ArregloDatos['NO'] = (isset($ArregloDatos['NO']) == true ) ? $ArregloDatos['NO'] : 0;
				$Compormiso['SI'] = ($ArregloDatos['SI'] != 0) ? ($ArregloDatos['SI'] * 100) / ($ArregloDatos['SI'] + $ArregloDatos['NO']) : 0;
				$Compormiso['NO'] = ($ArregloDatos['NO'] != 0) ? ($ArregloDatos['NO'] * 100) / ($ArregloDatos['SI'] + $ArregloDatos['NO']) : 0;
				return $Compormiso;
		
		}
		
		/**
		 * Metodo Publico
		 * ObtenerColumnas($Arreglo = false, $Columnas = false)
		 * 
		 * Obtene columnas especificas del arreglo 
		 */
		public static function ObtenerColumnas($Arreglo = false, $Columnas = false){
			if($Columnas == true AND $Arreglo == true){
				$Datos = array();
				foreach($Arreglo as $Registro){
					foreach($Registro as $columna => $Valor){
						if(in_array($columna, $Columnas)){
							$Dato[] = $Valor;
						}
					}
					$Datos[] = $Dato;
					$Dato = false;
				}
				return $Datos;
			}
		}
		
		/**
		 * Metoido publico 
		 * ObtenerExension($Canal = false)
		 * 
		 * Obtiene el string de interes de la cadena
		 */
		public static function ObtenerExension($Canal = false){
			if($Canal == true){
				$Extension = explode('/', $Canal);
				$Extension = explode('-', $Extension[1]);
				return $Extension[0];
			}
		}
		/**
		 * Metodo Publico
		 * ObtenerImagen($IdUsuario)
		 * 
		 * se obtiene la imagen de perfil del usuario
		 * @param $IdUsuario
		 */
		public static function ObtenerImagen($IdUsuario = false){
			if($IdUsuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_imagen_usuario');
				$Consulta->Columnas('Imagen');
				$Consulta->Condicion("IdUsuario = '$IdUsuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * CallerID($Arreglo = false)
		 * 
		 * Conviarte los datos de callerid a una cadena json
		 */
		public static function CallerID($Arreglo = false){
			if(isset($Arreglo['TipoCallerId'])){
				$callerid = ($Arreglo['TipoCallerId'] == 'Fijo') ? $Arreglo['callerid'] : array('NumeroBase'=>$Arreglo['NumeroBase'], 'Cantidad'=>$Arreglo['Cantidad']);
				return json_encode( array('CallerIdTipo'=>$Arreglo['TipoCallerId'], 'callerid'=>$callerid), JSON_UNESCAPED_UNICODE);
			}

		}
		
		/**
		 * Metodo Publico
		 * ObtenerCallerId($Datos = false)
		 * 
		 * Determina el callerid para supervisor
		 */
		public static function ObtenerCallerId($Datos = false){
			if($Datos == true AND is_array($Datos)){
				$Datos = json_decode($Datos['callerid'], true);
				if($Datos['CallerIdTipo'] == "Fijo"){
					return $Datos['callerid'];
				}
				elseif($Datos['CallerIdTipo'] == "Aleatorio" AND isset($Datos['callerid']['Cantidad']) AND isset($Datos['callerid']['NumeroBase'])){
					$Cantidad = $Datos['callerid']['Cantidad'];
					$Base = $Datos['callerid']['NumeroBase'];
					$CantidadRango = pow(10, $Cantidad);
					$Inicio = $CantidadRango / 10;
					$Aleatorio = rand($Inicio, $CantidadRango -1);
					$Base = substr($Base, 0, -($Cantidad));
					return $Base.''.$Aleatorio;					
				}
			}
		}
	}