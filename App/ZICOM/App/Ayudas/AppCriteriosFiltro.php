<?php
	
	class AppCriteriosFiltro{
		
		/**
		 * Metodo Publico 
		 * GeneraCondicones($Arreglo = false)
		 * 
		 * Conviarte las entradas a condicines de distintos tipos según sea el caso
		 * @param $Arreglo: Arreglo de datos de entrada
		 */		
		public static function GeneraCondicones($Arreglo = false){
			if($Arreglo == true){
				$Condiciones = array();
				if(isset($Arreglo['Agente']) OR isset($Arreglo['Grabacion']) OR isset($Arreglo['StatusLlamada'])){
					$Condiciones = self::CondicionesIndiceValor($Arreglo, array('tbl_agentes_asignado_supervisor.IdAgente', 'Grabacion', 'StatusLlamada')); 
				}
				if(isset($Arreglo['Duracion'])){
					$Condiciones[] = self::CondicionesRango($Arreglo['minimoDuracion'], $Arreglo['maximoDuracion'], 'tbl_cdr.duration');
				}
				if(isset($Arreglo['FechaGestion'])){
					$Rango = self::ObtenerRango($Arreglo['FechasGestion']);
					foreach($Rango as $Columna => $Valor){
						$Rango[$Columna] = AppFechas::FormatoFecha($Valor);
					}
					$Condiciones[] = self::CondicionesRango($Rango[0], $Rango[1], 'FechaHora_Captura');
				}
				if(isset($Arreglo['NombreCliente'])){
					$Condiciones[] = "tbl_datos_agenda.NombreTitular LIKE '%". $Arreglo['NombreTitular']."%'";
				}
				if(isset($Arreglo['Telefono']) OR isset($Arreglo['Cliente'])){
					$Condiciones = array_merge($Condiciones, self::Condicioneslike($Arreglo, array('NumeroTelefono', 'NombreTitular')));
				}
				return $Condiciones;
			}
		}
		
		/**
		 * Metodo Private
		 * CondicionesIndiceValor($ArregloDatos = false, $Campos = false)
		 * 
		 * Genera condiciones del tipo Columna = Valor
		 * @param $ArregloDatos: Arreglo de datos de entrada
		 * @param $Campos = array incremental de columnas requeridas para el proceso
		 * @return $Condiciones: array de condiciones
		 */
		private static function CondicionesIndiceValor($ArregloDatos = false, $Campos = false){
			if($ArregloDatos == true and is_array($ArregloDatos) and $Campos == true){
				$Condiciones = array();
				foreach($ArregloDatos as $Campo => $Valor){
					if(in_array($Campo, $Campos)){
						$Condiciones[] = $Campo ." = "."'". $Valor ."'"; 
					}
				}
    			return $Condiciones;
			}
		}
		
		/**
		 * Metodo Private
		 * CondicionesRango($Minimo = false, $Maximo = false, $Campo = false)
		 * 
		 * Crea condiciones de rango para rango tiempo y fechas
		 * @param $Minimo: El valor del minimo
		 * @param $Maxima: El valor del maximo
		 * @return $Campo: Nombre del campo de la tabla.
		 */
		private static function CondicionesRango($Minimo = false, $Maximo = false, $Campo = false){
			if(isset($Minimo) and $Maximo == true and $Campo == true){
				if(is_numeric($Minimo) AND is_numeric($Maximo)){
					return 'ROUND('. $Campo .')/60 >= '. $Minimo .' AND ROUND('. $Campo .')/60 <= '. $Maximo;
				}
				else{
					return 'DATE('.$Campo .') BETWEEN "'. $Minimo .'" AND "'. $Maximo .'"';
				}
			}
		}
		
		/**
		 * Metodo Private
		 * ObtenerRango($CadenaRango = false)
		 * 
		 * Obtiene rangos apartir de una cadena (Especificamente rangos de fechas, aunque 
		 *  funcina para cadenas que se requiran separa por un guin medio)
		 * @param $CadenaRango: String con datos a separar
		 * @return Arreglo con elemnetos separado por -
		 */
		private static function ObtenerRango($CadenaRango = false){
			if($CadenaRango == true){
				$CadenaRango = trim(str_replace(" ", "", $CadenaRango));
				return explode('-', $CadenaRango);
			}
		}
		
		/**
		 * metodo privado 
		 * Condicioneslike($Valor = false, $Campo = false)
		 * 
		 * crea una condicion like
		 */
		private static function Condicioneslike($Arreglo = false, $Campos = false){
			if($Arreglo == true){
				foreach($Arreglo AS $Columna => $Valor){
					if(in_array($Columna, $Campos)){
						if(!empty($Valor)){
							$Condiciones[] = $Columna." LIKE '%".$Valor."%'";
						}
					}	
				}
				return $Condiciones;
			}
		}
	}