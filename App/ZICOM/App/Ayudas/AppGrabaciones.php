<?php

	class AppGrabaciones {

		private $ArchivoGrabacion = false;
		private $NombreGrabacion = false;
		private $PathGrabaciones = false;
		private $PathTempGrabacion = false;
		private $FormatoAudio = false;

		/**
		 * Metodo Constructor
		 *	
		 * Inicializa la clase
		 * @param bool $Uniqueid: Identificador de la grabacion
		 * @param string $DefaultFormatoAudio: Formato de audio soporte sobre gsm, wav y mp3
		 */
		function __Construct($Uniqueid = false, $DefaultFormatoAudio = 'mp3'){
			if($Uniqueid):
				$this->NombreGrabacion = $Uniqueid;
				$this->ArchivoGrabacion = $Uniqueid . '.gsm';
				$this->FormatoAudio = $DefaultFormatoAudio;
				$this->PathGrabaciones = implode(DIRECTORY_SEPARATOR, array(__SysNeuralFileRoot__, 'Asterisk', 'Data', 'Grabaciones'));
				$this->PathTempGrabacion = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', 'Grabaciones'));
			endif;
		}

		/**
		 * Metodo Privado
		 * BuscarGrabacion($Archivo = false)
		 *
		 * Busca si se encuentra el registro de la grabacion
		 * @param bool $Archivo: Archivo a buscar
		 * @return bool: Retorna true si existe el archivo y false si no existe
		 */
		private function BuscarGrabacion($Archivo = false){
			if(is_string($Archivo) == true):
				return (file_exists($Archivo) == true AND is_file($Archivo) == true) ? true : false;
			endif;
		}

		/**
		 * Metodo Privado
		 * ConvierteAudio()
		 *
		 * Convierte audio a un formato especifico
		 */
		private function ConvierteAudio(){
			switch($this->FormatoAudio){
				case 'gsm':
					$this->generarDescargaArchivo($this->NombreGrabacion, 'gsm', $this->PathGrabaciones . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. $this->FormatoAudio);
					break;
				case 'wav':
					if(self::BuscarGrabacion($this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. $this->FormatoAudio) == false){
						exec('sox ' . $this->PathGrabaciones . DIRECTORY_SEPARATOR . $this->ArchivoGrabacion . ' ' . $this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. $this->FormatoAudio, $retval);
					}
					$this->generarDescargaArchivo($this->NombreGrabacion, 'wav', $this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. $this->FormatoAudio);
					break;
				case 'mp3':
					if(self::BuscarGrabacion($this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion . $this->FormatoAudio) == false){
						exec('rm -f ' . $this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. $this->FormatoAudio, $retval);
						exec('sox ' . $this->PathGrabaciones . DIRECTORY_SEPARATOR . $this->ArchivoGrabacion . ' ' . $this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. 'wav', $retval);
						exec('ffmpeg -i ' . $this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. 'wav' . ' ' . $this->PathTempGrabacion . DIRECTORY_SEPARATOR . $this->NombreGrabacion .'.'. $this->FormatoAudio, $retval);
					}
					$this->generarDescargaArchivo($this->NombreGrabacion, 'mp3', $this->PathTempGrabacion . DIRECTORY_SEPARATOR. $this->NombreGrabacion .'.'. $this->FormatoAudio);
					break;
			}
		}

		/**
		 * Metodo Publico
		 * generarDescargaArchivo($nombre = false, $extension = false, $rutaCompleta = false)
		 *
		 * Genera el proceso de descarga del archivo correspondiente
		 * se agregan cabeceras para compatibilidad con IExplorer,
		 * Spartan, Safari, genera proceso de no toma de cache de navegador
		 * y genera compatibilidad extendida a todos los navegadores
		 *
		 * @param bool $nombre
		 * @param bool $extension
		 * @param bool $rutaCompleta
		 */
		private function generarDescargaArchivo($Nombre = false, $Extension = false, $RutaCompleta = false) {
			header("Content-Type: application/force-download");
			//header('Content-Description: File Transfer');
			//header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.$Nombre.'.'.$Extension);
			//header('Content-Transfer-Encoding: binary');
			//header('Expires: 0');
			//header('Cache-Control: must-revalidate');
			//header('Pragma: public');
			//header('Content-Length: ' . filesize($RutaCompleta));
			//ob_clean();
			//sflush();
			@readfile($rutaCompleta);
			exit();
		}

		/**
		 * Metodo Publico
		 * ObtenerAudio()
		 *
		 * Obtiene el audio del registro de grabacion
		 * @return string|void: Archivo de Audio
		 */
		public function ObtenerAudio(){
			if(self::BuscarGrabacion($this->PathGrabaciones . DIRECTORY_SEPARATOR .$this->ArchivoGrabacion)):
				return self::ConvierteAudio();
			else:
				return 'ERROR';
			endif;
		}

	}