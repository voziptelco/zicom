<?php
	class AppFiltradoClientes{
	
		/**
		 * AppFiltradoClientes::__construct()
		 * 
		 * Se genera conexion de BD
		 */
		function __construct($conexion = false) {
			$this->Conexion = NeuralConexionDB::DoctrineDBAL($conexion);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaRegistros($ArregloCriterios = false, $IdAgente = false)
		 * 
		 * Consulta los registros que cumplen con los criterios de busqueda
		 * @param $ArregloCriterios: Arreglo con las condiciones de busqueda
		 * @param $IdAgente: Id del Agente de los registro a buscar
		 * @return Matriz Resultado de la consulta
		 */
		public function ConsultaRegistros($ArregloCriterios = false, $IdAgente = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($IdAgente == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal, tbl_datos_agenda.StatusCobro, tbl_codigos.Color ".
			           "FROM tbl_asignacion_registros INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
			           "INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
			           "LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					   "WHERE tbl_agente_asignado.IdAgente = '$IdAgente' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO' ";
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
					foreach($ArregloCriterios AS $Condicion){
						$SQL .= " AND ". $Condicion;
					}	
				}
				$SQL .= " AND (tbl_datos_agenda.ClienteUnico LIKE '%$Criterio%' OR  tbl_datos_agenda.NombreTitular LIKE '%$Criterio%' OR tbl_datos_agenda.SemanaAtraso LIKE '%$Criterio%' OR tbl_datos_agenda.Saldo LIKE '%$Criterio%' OR tbl_datos_agenda.SaldoTotal LIKE '%$Criterio%' OR tbl_datos_agenda.StatusCobro LIKE '%$Criterio%') GROUP BY NombreTitular ";
				if($Cantidad != -1):
					$SQL .=" LIMIT $Inicio, $Cantidad "; 
				endif;
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaRegistros($ArregloCriterios = false, $IdAgente = false)
		 * 
		 * Consulta los registros que cumplen con los criterios de busqueda
		 * @param $ArregloCriterios: Arreglo con las condiciones de busqueda
		 * @param $IdAgente: Id del Agente de los registro a buscar
		 * @return Matriz Resultado de la consulta
		 */
		public function ConsultaRegistrosTotal($ArregloCriterios = false, $IdAgente = false){
			if($IdAgente == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda ".
			           "FROM tbl_asignacion_registros INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
			           "INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
			           "LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
			           "WHERE tbl_agente_asignado.IdAgente = '$IdAgente' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO'";	
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
					foreach($ArregloCriterios AS $Condicion){
						$SQL .= " AND ". $Condicion;
					}	
				}
				$SQL .=" GROUP BY NombreTitular";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->rowCount();
			}
		}
		
		/**
		 * Metodo Publico 
		 * EstadosGestion($MatrizConsulta = false, $Tabla = false, $Llave = false)
		 * 
		 * Verifica si los registros consultados ha sido gestionados
		 * @param $MatrizConsulta: Datos de la consulta
		 * @param $Tabla: Tabla de gestion (Campo o Telefonica)
		 * @param $Llave: Campo de comparacion.
		 * @return $Lista: Matriz con status de gestion
		 */
		public function EstadosGestion($MatrizConsulta = false, $Tabla = false, $Llave = false){
			if($MatrizConsulta == true and $Tabla == true and $Llave == true){
				$Consulta = $this->ConsultaEstatusGestion($Tabla, $Llave);
				foreach($MatrizConsulta as $Arreglo){
					if(isset($Arreglo['StatusGestion']) AND $Arreglo['StatusGestion'] == "GESTIONADO"){
						$Lista[] = $Arreglo;
					}else{
						$Compara = array($Llave => $Arreglo[$Llave]);
						$Arreglo['StatusGestion'] = (in_array($Compara, $Consulta)) ? "GESTIONADO" : "NOGESTIONADO";
						$Lista[] = $Arreglo;
					}
				}
				return $Lista;	
			}
		}
		
		public function ConsultaColores($MatrizConsulta = false){
			if($MatrizConsulta == true){
				$Codigos = self::ConsultarCodigos();
			//	Ayudas::print_r($Codigos);
				foreach($MatrizConsulta AS $Llave => $Arreglo){
					foreach($Codigos AS $key => $Registro){						
						if($Arreglo['IdDatoAgenda'] == $Registro['IdDatoAgenda']){
							$MatrizConsulta[$Llave]['Color'] = $Registro['Color'];
							break;
						}
						else{
							$MatrizConsulta[$Llave]['Color'] = "";	
						}
					}
				}
				return $MatrizConsulta;
			}
		}
	
		/**
		 * Metodo Publico
		 * ConsultarCodigos()
		 * 
		 * Consulta lso codigos de gestion disponibles
		 */
		private function ConsultarCodigos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_codigo_registros');
			$Consulta->Columnas('tbl_codigo_registros.IdCodigo, IdDatoAgenda, Codigo, Descripcion, Color');
			$Consulta->InnerJoin('tbl_codigos', 'tbl_codigo_registros.IdCodigo', 'tbl_codigos.IdCodigo');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Privado
		 * ConsultaEstatusGestion($Valor = false, $Tabla = false, $Llave = false)
		 * 
		 * Consulta uno a uno si los registros han  siudo gestionados
		 * @param $Valor: Valor a buscar.
		 * @param $Tabla: Tabla de gestion (Campo o Telefonica)
		 * @param $Llave: Campo de comparacion.
		 * @return $Lista: Matriz con status de gestion
		 */
		private function ConsultaEstatusGestion($Tabla = false, $Llave = false){
			if($Tabla == true and $Llave == true){
				$SQL = "SELECT ".$Llave ." FROM ".$Tabla;
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);				
			}
		}
		
		/**
		 * Metodo Publico 
		 * EstatusTelefonos($Consulta = false, $Campo = false)
		 * 
		 * Consulta la disponibilidad de telefonos para un registro
		 * @param $Consulta: Array de datos.
		 */
		public function EstatusTelefonos($Consulta = false, $Campo = false){
			if($Consulta == true){
				foreach($Consulta as $Arreglo){
					foreach($Arreglo as $Columna=>$Valor){
						if($Columna == $Campo){
							$Arreglo['StatusTelefono'] = $this->ConsultaStatusTelefonos($Valor);
						}
					}
					$Lista[] = $Arreglo;
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaStatusTelefonos($IdAgenda = false)
		 * 
		 * Consulta si existe telefonos en la bd para un cliente dado.
		 * @param $IdAgenda: Identificador del agente
		 * @return string de estado (Disponible o nodisponible)
		 */
		private function ConsultaStatusTelefonos($IdAgenda = false){
			if($IdAgenda == true){
				$SQL = 'SELECT Telefono1, Telefono2, Telefono3, Telefono4 FROM tbl_datos_agenda WHERE IdDatoAgenda="'.$IdAgenda.'"';
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetch();
				$Resultado = AppUtilidades::ArregloEstados($Resultado);
				$Tamano = count($Resultado);
				unset($Resultado, $Consulta, $SQL, $Conexion);
				return ($Tamano > 0) ? "DISPONIBLE" : "NODISPONIBLE";
			}
		}
		
			public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 * 
		 * Consulta todas la Carteras
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 * 
		 * Consulta todos las Provincias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaDistritos()
		 * 
		 * Consulta todos los Distritos
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaDistritos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
			/**
		 * Metodo Publico 
		 * ConsultaClientes()
		 * 
		 * Consulta los clientes asignados a el supervisor de la sesion
		 * @param $IdSupervisor: Condicon de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultaClientes($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_clientes_asignados_supervisor');
				$Consulta->Columnas('tbl_informacion_usuarios.idUsuario as IdCliente, tbl_informacion_usuarios.RazonSocial');
				$Consulta->InnerJoin('tbl_sistema_usuarios', 'tbl_clientes_asignados_supervisor.IdCliente', 'tbl_sistema_usuarios.IdUsuario');
				$Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_sistema_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
				$Consulta->Condicion("tbl_clientes_asignados_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre = 'Cliente'");
				return $Consulta->Ejecutar(false, true);	
			}
		}
		
		public function Consultas($IdSupervisor = false){
			$Consultas['Clientes'] = $this->ConsultaClientes($IdSupervisor);
			$Consultas['Gerencias'] = $this->ConsultaGerencias();
			$Consultas['Carteras'] = $this->ConsultaCarteras();
			$Consultas['Provincias'] = $this->ConsultaDistritos();
			$Consultas['Distritos'] = $this->ConsultaProvincias();
			return $Consultas;
		}
		
	}