<?php
	class AppConsultasGenerales{
	
		/**
		 * AppFiltradoClientes::__construct()
		 * 
		 * Se genera conexion de BD
		 */
		function __construct($conexion = false) {
			$this->Conexion = NeuralConexionDB::DoctrineDBAL($conexion);
		}
		
		public function Consultas(){
			return $Consultas = array('Carteras' =>$this->ConsultarCarteras(), 'Gerencias' => $this->ConsultarGerencias(), 'Provincias' => $this->ConsultarProvincias(), 'Distritos' => $this->ConsultarDistritos());
			
		}
		
		/**
		 * Metodo privado
		 * ConsultarCarteras()
		 * 
		 * Consulta las carteras existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		private function ConsultarCarteras(){
			$SQL = "SELECT IdCartera, Descripcion FROM tbl_carteras WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGerencias()
		 * 
		 * Consulta las Gerencias existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		private function ConsultarGerencias(){
			$SQL = "SELECT IdGerencia, Descripcion FROM tbl_gerencias WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarProvincias()
		 * 
		 * Consulta las Provincias existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		private function ConsultarProvincias(){
			$SQL = "SELECT IdProvincia, Descripcion FROM tbl_provincia WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarDistritos()
		 * 
		 * Consulta los distritos existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		private function ConsultarDistritos(){
			$SQL = "SELECT IdDistrito, Descripcion FROM tbl_distritos WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
	}