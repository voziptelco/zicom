<?php
	
	class AppFiltrosRapidos{
		
		/**
		 * Metodo Publico
		 * QuitarPrefijo($Telefono = false)
		 * 
		 * formato para número de telfonos, para no mostrar el prefijo de marcacion
		 * @param $Telefono: Teleono devuelto de la base de ddatso
		 * @return $Telefono: numero de telefono sin prefijo
		 */
		public static function QuitarPrefijo($Telefono = false, $Prefijo = false){
			if($Telefono == true){
				$Patrones = false;
				foreach($Prefijo as $registro){
					foreach($registro as $key=>$Value){
						$Patrones[] = '/^'.$Value.'/';
					}
				}
				return $Telefono = preg_replace($Patrones, '' , trim($Telefono), 1);
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaPrefijo()
		 * 
		 * Consulta el prefijo de marcacion 
		 * @return matriz de consulta.
		 */
		private static function ConsultaPrefijo(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_troncales');
			$Consulta->Columnas('CodigoPais');
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * QuitarPrefijoArchivo($NombreArchivo = false)
		 * 
		 * Formato para Nombres de archivos
		 * @param $NombreArchivo
		 * @return $Nombre 
		 */
		public static function QuitaPrefijoArchivo($NombreArchivo = false){
			if($NombreArchivo == true){
				$Nombre = explode("_", $NombreArchivo);
				return array_pop($Nombre);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ComparaTiempo($Fecha = false)
		 * 
		 * Compara que la fecha ingresada sea mayor a la fecha actual.
		 */
		public static function ComparaTiempo($Fecha = false){
			if($Fecha == true){
				$Fecha = AppFechas::FormatoFechaTiempo($Fecha);
				return ($Fecha > AppFechas::ObtenerDatetimeActual()) ? "true" : "false";
			}
		}
		
		/**
		 * AplicarFiltrosColumnas($Arreglo = false, $Columnas = false)
		 *
		 * Aplica filtros especificos al arreglo
		 * @param boolean $Arreglo  [description]
		 * @param boolean $Columnas [description]
		 */
		public static function AplicarFiltrosColumnas($Arreglo = false, $Columnas = false){
			if($Arreglo == true){
				$i = 0;
				$Prefijo = self::ConsultaPrefijo();
				foreach ($Arreglo as $Fila) {
					$Arreglo[$i]["NumeroTelefono"] = self::QuitarPrefijo($Fila["NumeroTelefono"], $Prefijo);
					$Arreglo[$i]["duration"] = AppFechas::ConvertirSegundosTime($Fila["duration"]);
					$i++;
				}		
				return $Arreglo;
			}
		}
		
		public static function FormatoTelefonoSinPrefijo($ArregloDatos = false){
			if($ArregloDatos == true){
				$i = 0;
				$Prefijo = self::ConsultaPrefijo();
				foreach($ArregloDatos as $Fila){
					$ArregloDatos[$i]['NumeroTelefono'] = self::QuitarPrefijo($Fila['NumeroTelefono'], $Prefijo);
					$i++;
				}
				return $ArregloDatos;
			}
		}
		
		/**
		 * AplicarFiltrosColumnas($Arreglo = false, $Columnas = false)
		 *
		 * Aplica filtros especificos al arreglo
		 * @param boolean $Arreglo  [description]
		 * @param boolean $Columnas [description]
		 */
		public static function AplicarFiltrosCifradoMoneda($Arreglo = false){
			if($Arreglo == true){
				$i = 0;
				foreach ($Arreglo as $Fila) {
					$Arreglo[$i]["IdDatoAgenda"] = NeuralCriptografia::Codificar($Fila['IdDatoAgenda']);
					$Arreglo[$i]["Saldo"] = "S/.".number_format($Fila['Saldo'], 2, '.', ',');
					$Arreglo[$i]["SaldoTotal"] = "S/.". number_format($Fila['SaldoTotal'], 2, '.', ',');
					$i++;
				}
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo publico
		 * AplicarFiltrosCifrado($Arreglo = false)
		 * 
		 * filtros para id de agenda y gestion telefinica
		 * @param $Arreglo: datos de la consulta
		 */
		public static function AplicarFiltrosCifrado($Arreglo = false, $Campo = false){
			if($Arreglo == true){
				foreach ($Arreglo as $key => $Fila) {
					if(isset($Arreglo[$key]["IdDatoAgenda"]))
						$Arreglo[$key]["IdDatoAgenda"] = NeuralCriptografia::Codificar($Fila['IdDatoAgenda']);
					$Arreglo[$key][$Campo] = NeuralCriptografia::Codificar($Fila[$Campo]);
				}
				return $Arreglo;
			}
		}
	}