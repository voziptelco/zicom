<?php

	class AppConexionConmutador{
		private $Host = false;
		private $Port = false;
		private $User = false;
		private $Pass = false;
		private $Conexion = false;
		function __Construct($Arreglo = false){
			if(is_array($Arreglo)):
				$this->Host = $Arreglo['host'];
				$this->Port = $Arreglo['port'];
				$this->User = $Arreglo['user'];
				$this->Pass = $Arreglo['password'];
				$this->Conexion();
				endif;
		}

		/**
		 * Realiza la autenticacion con el servidor
		 */
		public function AutenticarConexion(){
			fwrite($this->Conexion,"Action: Login\r\n");
			fwrite($this->Conexion,"Username: $this->User\r\n");
			fwrite($this->Conexion,"Secret: $this->Pass\r\n\r\n");
		}

		/**
		 * Obtiene la lista de las campañas y sus agentes.
		 * @return array|bool
		 */
		public function ObtenerCampanias(){
			fwrite($this->Conexion,"Action: QueueStatus\r\n\r\n");
			fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
			return $this->LeerRespuesta();
		}

		/**
		 * Consulta las llamadas activas.
		 * @return array|bool
		 */
		public function ObtenerLlamadas(){
			fwrite($this->Conexion,"Action: Command\r\n");
			fwrite($this->Conexion,"Command: core show calls\r\n\r\n");
			fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
			return $this->LeerRespuesta();
		}

		/**
		 * Obtiene los canales DAHDI.
		 */
		public function ObtenerCanalesDAHDI(){
			fwrite($this->Conexion,"Action: DAHDIShowChannels\r\n\r\n");
			fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
			$Respuesta = $this->LeerRespuesta();
			if(isset($Respuesta[0]['Response']) == true AND $Respuesta[0]['Response'] == 'Success'){
				if(isset($Respuesta[2]['Response']) == true AND $Respuesta[2]['Response'] == 'Success'){
					$this->Desconectar();
					$Canales = false;
					foreach($Respuesta as $Arreglo){
						if(isset($Arreglo['Event']) == true AND $Arreglo['Event'] == 'DAHDIShowChannels'){
							$Canales[] = $Arreglo;
						}
					}
					return $Canales;
				}else{
					$this->Desconectar();
					unset($Respuesta, $Llamadas, $Conexion);
					return 'Error';
				}
			}else{
				unset($Respuesta, $Conexion);
				return 'Error';
			}
		}

		/**
		 * Obtiene los canales activos.
		 * @return array|bool|string
		 */
		public function ObtenerCanales(){
			fwrite($this->Conexion,"Action: CoreShowChannels\r\n\r\n");
			fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
			$Respuesta = $this->LeerRespuesta();
			if(isset($Respuesta[0]['Response']) == true AND $Respuesta[0]['Response'] == 'Success'){
				if(isset($Respuesta[2]['Response']) == true AND $Respuesta[2]['Response'] == 'Success'){
					$this->Desconectar();
					$Canales = false;
					foreach($Respuesta as $Arreglo){
						if(isset($Arreglo['Event']) == true AND $Arreglo['Event'] == 'CoreShowChannel'){
							$Canales[] = $Arreglo;
						}
					}
					return $Canales;
				}else{
					$this->Desconectar();
					unset($Respuesta, $Llamadas, $Conexion);
					return 'Error';
				}
			}else{
				unset($Respuesta, $Conexion);
				return 'Error';
			}
		}

		/**
		 * Obtiene el estado de la campaña.
		 * @param bool|false $NombreCampania
		 * @return array|bool
		 */
		public function EstadoCampania($NombreCampania = false){
			if($NombreCampania == true){
				fwrite($this->Conexion,"Action: QueueSummary\r\n\r\n");
				fwrite($this->Conexion,"Queue: $NombreCampania\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Muestra las estadisticas de la campaña.
		 * @param bool|false $NombreCampania
		 * @return array|bool
		 */
		public function EstadoMiembrosCampana($NombreCampania = false){
			if($NombreCampania == true) {
				fwrite($this->Conexion, "Action: QueueStatus\r\n\r\n");
				fwrite($this->Conexion, "Queue: $NombreCampania\r\n\r\n");
				fwrite($this->Conexion,"ActionID: 1\r\n");
				fwrite($this->Conexion, "Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Muestra las extensiones.
		 * @return array|bool
		 */
		public function ListarExtensiones(){
			fwrite($this->Conexion,"Action: SIPpeers\r\n\r\n");
			fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
			return $this->LeerRespuesta();
		}

		/**
		 * Muestra el estado y estadisticas basicas de las extensiones.
		 * @return array|bool
		 */
		public function EstadoExtensiones(){
			fwrite($this->Conexion,"Action: SIPpeerstatus\r\n\r\n");
			fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
			return $this->LeerRespuesta();
		}

		// Inicio gestion canal del canal
		/**
		 * Termina la llamada.
		 * @param bool|false $Canal
		 * @return array|bool
		 */
		public function ColgarLlamada($Canal = false){
			if($Canal == true){
				fwrite($this->Conexion,"Action: Command\r\n");
				fwrite($this->Conexion,"Command: channel request hangup $Canal\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Realiza el parking de la llamada.
		 * @param bool|false $Canal
		 * @return array|bool
		 */
		public function EsperaLlamada($Canal = false){
			if($Canal == true){
				fwrite($this->Conexion,"Action: Command\r\n");
				fwrite($this->Conexion,"Command: channel request hangup $Canal\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Realiza la transferencia de la llamada.
		 * @param bool|false $Canal
		 * @return array|bool
		 */
		public function TransferirLlamada($Canal = false, $Extension = false){
			if ($Canal == true AND $Extension == true) {
				fwrite($this->Conexion,"Action: Command\r\n");
				fwrite($this->Conexion,"Command:  channel redirect $Canal conmutador,$Extension,1\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		// Fin gestion canal

		/**
		 * Agrega un agente a la campaña.
		 * @param bool|false $Campania
		 * @param bool|false $CanalExtAgente
		 * @return array
		 */
		public function AgregarAgente($Campania = false, $CanalExtAgente = false){
			if($Campania == true AND $CanalExtAgente == true){
				fwrite($this->Conexion,"Action: QueueAdd\r\n");
				fwrite($this->Conexion,"Queue: $Campania\r\n");
				fwrite($this->Conexion,"Interface: $CanalExtAgente\r\n");
				fwrite($this->Conexion,"Penalty: 0\r\n");
				fwrite($this->Conexion,"Paused: 0\r\n");
				fwrite($this->Conexion,"MemberName: $CanalExtAgente\r\n");
				fwrite($this->Conexion,"MonitorInterface: $CanalExtAgente\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Elimina un agente de la campaña.
		 * @param bool|false $Campania
		 * @param bool|false $CanalExtAgente
		 * @return array|bool
		 */
		public function EliminarAgente($Campania = false, $CanalExtAgente = false){
			if($Campania == true AND $CanalExtAgente == true){
				fwrite($this->Conexion,"Action: QueueRemove\r\n");
				fwrite($this->Conexion,"Queue: $Campania\r\n");
				fwrite($this->Conexion,"Interface: $CanalExtAgente\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Pausa o despausa una extension.
		 * @param bool|false $CanalExtAgente: El canal de Agente  / su extension.
		 * @param bool|false $Accion: 'true' para pausar, 'false' para despausar.
		 * @return array|bool
		 */
		public function PausarAgente($CanalExtAgente = false, $Accion = false){
			if($CanalExtAgente == true AND $Accion == true){
				fwrite($this->Conexion,"Action: QueuePause\r\n");
				fwrite($this->Conexion,"Queue: \r\n");
				fwrite($this->Conexion,"Interface: $CanalExtAgente\r\n");
				fwrite($this->Conexion,"Paused: $Accion\r\n\r\n");
				fwrite($this->Conexion,"Action: Logoff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Monitorea el estado del Agente.
		 * @param bool|false $ExtensionAgente: Solo la extension del agente.
		 * @return array|bool
		 */
		public function EstadoAgente($ExtensionAgente = false){
			if($ExtensionAgente == true){
				fwrite($this->Conexion,"Action: ExtensionState\r\n");
				fwrite($this->Conexion,"ActionID: 1\r\n");
				fwrite($this->Conexion,"Context: BLF\r\n");
				fwrite($this->Conexion,"Exten: $ExtensionAgente\r\n\r\n");
				fwrite($this->Conexion,"Action: LogOff\r\n\r\n");
				return $this->LeerRespuesta();
			}
		}

		/**
		 * Abre una conexion con el socket servidor.
		 */
		private function Conexion(){
			$this->Conexion = @pfsockopen($this->Host, $this->Port, $errno, $errstr, 3000);
			if(!$this->Conexion){
				$this->Conexion = false;
				//echo "\nFracaso en la conexion: $errstr ($errno)\n";
			}
		}

		public function ObtenerConexion(){
			return $this->Conexion;
		}

		/**
		 * Lee la respuesta del servidor.
		 * @return array|bool
		 */
		public function LeerRespuesta(){
			$Respuesta = false;
			$Actual = false;
			while(!feof($this->Conexion)){
				$Salida = fgets($this->Conexion,60000);
				$Salida = str_replace("\n","",$Salida);
				$Salida = str_replace("\r","",$Salida);
				if($Salida != ''){
					$Valor = explode(':', $Salida);
					$Actual[trim($Valor[0])] = (isset($Valor[1])) ? trim($Valor[1]) : '';
				}else{
					$Respuesta[] = $Actual;
					$Actual = false;
				}
			}
			if($Respuesta == true AND is_array($Respuesta))
				return array_values($Respuesta);
			else
				return false;
		}

		/**
		 * Termina la conexion con el socket servidor.
		 */
		public function Desconectar(){
			$desconexion = fclose($this->Conexion);
			if($desconexion){
				//print_r($Queue_Params['6000']);
			}else{
				exit("\nLa conexion no pudo ser finalizada\n");
				exit();
			}
		}
	}