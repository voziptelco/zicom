<?php

	class AppArreglosExcel extends Modelo {
		
		/**
		 * Metodo publico
		 * ComparaColumnas($ColumnasArchivo = false, $ColumnasRequeridas = false)
		 * 
		 * Compara el tamaño de los arreglos y si sus encabezados son iguales
		 * @param $ColumnasArchivo: Lista de las columnas en el Archivo Excel
		 * @param $ColumnasRequeridas: Lista de Columnas que debe tener el Archivo Excel
		 * @return Un valor bool que nos indica si Coiciden las columnas o no
		 */
		public static function ComparaColumnas($ColumnasArchivo = false, $ColumnasRequeridas = false){
			if($ColumnasArchivo == true AND is_array($ColumnasArchivo) == true AND $ColumnasRequeridas == true AND is_array($ColumnasRequeridas) == true){
				if(count($ColumnasArchivo) == count($ColumnasRequeridas)){
					if(count(array_diff_assoc($ColumnasArchivo, $ColumnasRequeridas)) == 0){
						return true;
					}
				}
			}
			return false;	
		}
		
		/**
		 * Metodo Publicoo
		 * ComparaColumnasRequeridas($ColumnasArchivo = false, $ColumnasRequeridas = false)
		 * 
		 * busca que el archivo tenga las columnas que son requeridas
		 * @param $ColumnasArchivo: Arreglo de columnas del archivo
		 * @param $ColumnasRequeridas: Arreglo de columnas que se requieren
		 * @return valor 
		 */
		public static function ComparaColumnasRequeridas($ColumnasArchivo = false, $ColumnasRequeridas = false){
			if($ColumnasArchivo == true AND $ColumnasRequeridas == true){
				if(in_array($ColumnasRequeridas[0], $ColumnasArchivo, true) and in_array($ColumnasRequeridas[1], $ColumnasArchivo, true)){
			 		return true;
				}
				else{
					return false;
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * FormatoUcwords($Matriz = false, $Omitidos = false)
		 * 
		 * Da formato de Texto a un conjuto de datos
		 * @param $Matriz: Matriz de datos
		 * @param $Omitidos: datos a los que no se les dara formato
		 * @return $Lista: Matriz con datos con formato
		 */
		public static function FormatoUcwords($Matriz = false, $Omitidos = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				foreach($Matriz AS $Llave => $Arreglo){
					foreach ($Arreglo as $key => $value) {
						if(in_array($key, $Omitidos)){
							$Matriz[$Llave][$key] = $value;
						}
						else{
							$Matriz[$Llave][$key] = trim(mb_convert_case(ucwords(mb_strtolower($value, "UTF-8")), MB_CASE_TITLE, "UTF-8"));
						}
					}
				}
				unset($Omitidos);
				return $Matriz;
			}
		} 
		
		/**
		 * Metodo Publico 
		 * FormatoTexto($Matriz = false)
		 * 
		 * Da formato de Texto a un conjuto de datos
		 * @param $Matriz: Matriz de datos
		 * @return $Lista: Matriz con datos con formato
		 */
		public static function FormatoTexto($Matriz = false){
			if($Matriz == true AND is_array($Matriz) == true){
				foreach($Matriz AS $Llave => $Arreglo){
					$tamaño = count($Arreglo);
					for($i = 0 ; $i < $tamaño ; $i++){
						$Valor = trim(mb_convert_case(ucwords(mb_strtolower($Arreglo[$i], "UTF-8")), MB_CASE_TITLE, "UTF-8"));
						if(empty($Valor)){
							$Matriz[$Llave][$i] = "ERROR";
						}
						else{
							$Matriz[$Llave][$i] = $Valor;
						}
					}
				}
				return $Matriz;
			}
		} 
		
		/**
		 * Metodo Publico 
		 * FormatoNumeros($Matriz = false, $Requeridos = false)
		 * 
		 * Da formato a las columnas con datos numericos, y valida que sean numeros
		 * @param $Matriz: Matriz de datos
		 * @param $Requeridos: Columnas con datos numericos
		 * @return $Registros: Matriz con datos numericos validados
		 */
		public static function FormatoNumeros($Matriz = false, $Requeridos = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Requeridos == true AND is_array($Requeridos) == true){
				foreach($Matriz AS $Llave => $Arreglo){
					foreach ($Arreglo as $key => $value) {
						if(in_array($key, $Requeridos)){
							if(is_numeric($value)){
								$Valor = trim(number_format(str_replace(',', '', $value), 2, '.', ''));
								$Matriz[$Llave][$key] = (is_float($Valor) == true) ? 'ERROR': $Valor;
							}
							else{
								$Matriz[$Llave][$key] = 'ERROR';
							}						
						}
						else{
							$Matriz[$Llave][$key] = $value;
						}
					}
				}
			}
			unset($Requeridos);
			return $Matriz;
		}
		
		/**
		 * Metodo Publico 
		 * FormatoTelefonoRequerido($Matriz = false, $Requeridos = false)
		 * 
		 * Le da un formato a los campos Telefono que sean requeridos para la inserción
		 * @param $Matriz: Matriz de datos
		 * @param $Requeridos: campos telefono
		 * @return Una matriz de datos con los telefonos con formato (11)1111111
		 */
		public static function FormatoTelefonoRequerido($Matriz = false, $Requeridos = false){
			if($Matriz == true AND $Requeridos == true){
				foreach($Matriz AS $Arreglo){
					for($i = 0; $i < count($Arreglo); $i++){
						if(in_array($i, $Requeridos)){
							$Valor = trim(str_replace('-', '', $Arreglo[$i]));
							$Valor = trim(str_replace(' ', '', $Valor));
							if(strlen($Valor) == 8 OR strlen($Valor) == 9){
								$Lista[] = $Valor;
							}else{
								$Lista[] = "ERROR";
							}
						}else{
							$Lista[] = $Arreglo[$i];	
						}
					}
					$Registros[] = $Lista;
					$Lista = array();
				}
				return $Registros;
			}
		}
		/**
		 * Metodo Publico 
		 * FormatoTelefono($Matriz = false, $Requeridos = false)
		 * 
		 * Le da un formato a los campos Telefono que no sean requeridos para la inserción
		 * @param $Matriz: Matriz de datos
		 * @param $Requeridos: campos telefono
		 * @return Una matriz de datos con los telefonos con formato 111111111
		 */
		public static function FormatoTelefonos($Matriz = false, $Requeridos = false){
			if($Matriz == true AND $Requeridos == true){
				foreach($Matriz AS $Llave => $Arreglo){
					foreach ($Arreglo as $key => $value) {
						if(in_array($key, $Requeridos)){
							if($value != ""){
								$Valor = trim(str_replace('-', '', $value));
								$Valor = trim(str_replace(' ', '', $Valor));
								$Valor = trim(str_replace('(', '', $Valor));
								$Valor = trim(str_replace(')', '', $Valor));
								if(strlen($Valor) == 8 OR strlen($Valor) == 9){
									$Matriz[$Llave][$key] = $Valor;
								}else{
									$Matriz[$Llave][$key] = "";
								}
							}else{
								$Matriz[$Llave][$key] = $value;
							}
						}else{
							$Matriz[$Llave][$key] = $value;	
						}
					}
				}
				unset($Requeridos);
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Publico 
		 * FormatoClaveClienteUnico($Matriz = false, $Requeridos = false)
		 * 
		 * Valida y da formato al campo ClienteUnico
		 * @param $Matriz: Arreglo de datos
		 * @param $Requeridos: Campos tipo clave ClienteUnico
		 * @return Matriz de datos con Clave con formato.
		 */
		public static function FormatoClaveClienteUnico($Matriz = false, $Requeridos = false){
			if($Matriz == true AND $Requeridos == true){
				foreach($Matriz AS $Llave => $Arreglo){
					foreach ($Arreglo as $key => $value) {
						if(in_array($key, $Requeridos)){
							$Clave = trim(str_ireplace(" ", "", $value));
							$Matriz[$Llave][$key] = (preg_match("/^([0-9]{1})-([0-9]{1,2})-([0-9]{3,4})-([0-9]{1,5})$/", $Clave)) ? $Clave : "ERROR";
						}
						else{
							$Matriz[$Llave][$key] = $value;
						}
					}
				}
				unset($Requeridos, $Clave);
				return $Matriz;
			}
		}
		/**
		 * Metodo Publico 
		 * CombinarMatrizLLave($Matriz = false, $Llaves = false)
		 * 
		 * Convierte un arreglo de indices incremnetales a indices asociados a datos
		 * @param $Matriz: Matriz de datos
		 * @param $Llaves Arreglo con las llaves
		 * @return $Lista: Matris de indices asociados
		 */
		public static function CombinarMatrizLLave($Matriz = false, $Llaves = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Llaves == true){
				foreach($Matriz AS $Arreglo){
					$Lista[] = array_combine($Llaves, $Arreglo);
				}
				unset($Matriz, $Llaves);
				return $Lista;	
			}
		}
		
		/**
		 * Metodo public estatico
		 * AgregarColumna()
		 * 
		 * Agrega una nueva columna a la matriz con su repectivo valor.
		 * @param $Matriz: matriz de datos.
		 * @param $Indice: Nombre del nuevo Campo.
		 * @param $Valor: Valor de la nueva columna.
		 * @return $Matriz: La matriz con la neva columna.
		 */
		public static function AgregarColumna($Matriz = false, $Indice = false, $Valor = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Indice == true AND $Valor == true){
				for($i = 0; $i < count($Matriz); $i++){
					$Matriz[$i][$Indice] = $Valor;
				}
				unset($Indice, $Valor);
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Publico 
		 * BuscarEstadoRegistros($Matriz = false, $Campos = false, $Tabla = false)
		 * 
		 * Busca si el dato existe, para saber si es actualización o nuevo registro.
		 * @param $Matriz: Matriz de datos.
		 * @param $Campos: Campos de búsqueda.
		 * @param $Tabla: Nombre de la tabla de búsqueda.
		 * @return $Estados: Un Arreglo de estados.
		 */
		public static function BuscarEstadoRegistros($Matriz = false, $Campos = false, $Tabla = false){
			if($Matriz == true AND is_array($Matriz) AND $Campos == true AND is_array($Campos) AND $Tabla == true){
				$Estados = array();
				$Consulta = self::ConsultarAgenda($Tabla, $Campos[0]);
				$Consulta = self::arrayColumn($Consulta, $Campos[0]);
				foreach($Matriz as $Llave => $Registros){
					if(in_array($Registros[$Campos[0]], $Consulta)){
						$Estados[$Llave] = "ACTUALIZAR REGISTRO";
					}
					else{
						$Estados[$Llave] = "NUEVO REGISTRO";
					}
				}
				unset($Matriz, $Campos, $Tabla);
				return $Estados;
			}
		}

		/**
		 * Metodo Publico 
		 * BuscarEstadoRegistros($Matriz = false, $Campos = false, $Tabla = false)
		 * 
		 * Busca si el dato existe, para saber si es actualización o nuevo registro.
		 * @param $Matriz: Matriz de datos.
		 * @param $Campos: Campos de búsqueda.
		 * @param $Tabla: Nombre de la tabla de búsqueda.
		 * @return $Estados: Un Arreglo de estados.
		 */
		public static function BuscarExistenciasArray($Matriz = false, $Campos = false, $Tabla = false){
			if($Matriz == true AND is_array($Matriz) AND $Campos == true AND is_array($Campos) AND $Tabla == true){
				$Estados = array();
				$Provincias = self::ConsultaProvincias();
				$Provincias = self::arrayColumn($Provincias, $Campos[0]);
				foreach ($Matriz as $key => $value) {
					if(in_array($value[$Campos[0]], $Provincias)){
						$Estados[$key] = "ACTUALIZAR REGISTRO";
					}
					else{
						$Estados[$key] = "NUEVO REGISTRO";
					}
				}
				return $Estados;
			}
		}

		
		/**
		 * Metodo Publico
		 * BuscarExistencias($Matriz = false)
		 * 
		 * Busca que un registro no se repita en la base de datos
		 * @param $Matriz: Registros del archivo
		 * @return $Lista: MAtriz de registros no duplicados
		 */
		public static function BuscarExistencias($Matriz = false){
			if($Matriz == true){
				$BDDatos = self::ConsultaDistrito();
				$Lista = array();
				foreach ($Matriz as $Arreglo) {
					$Existe = false;
					foreach ($BDDatos as $Registro) {
						if($Arreglo['Provincia'] == $Registro['Provincia'] AND $Arreglo['Distrito'] == $Registro['Distrito']){
							$Existe = true;
							break;
						}
					}
					if($Existe == false){
						$Lista[] = $Arreglo;
					}
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaDistrito($IdProvincia, $Descripcion)
		 * 
		 * Busca la exitencia de un distrito
		 * @param $IdProvincia: llave de la procincia
		 * @param $Descripcion: Nombre del distrito
		 * @return Resultado de la consulta.
		 */
		public static function ConsultaDistrito(){		
			$SQL = 'SELECT tbl_provincia.Descripcion AS Provincia, tbl_distritos.Descripcion AS Distrito FROM tbl_distritos '. 
				   'INNER JOIN tbl_provincia ON tbl_distritos.IdProvincia = tbl_provincia.IdProvincia '.
				   'WHERE tbl_distritos.Status != "ELIMINADO"';
			$Conexion = NeuralConexionDB::DoctrineDBAL(APP);
			$Consulta = $Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);		
		}

		private static function ConsultaProvincias(){
			$SQL = "SELECT Descripcion FROM tbl_provincia WHERE Status != 'ELIMINADO'";
			$Conexion = NeuralConexionDB::DoctrineDBAL(APP);
			$Consulta = $Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Privado
		 * ConsultaIndiceProvincia($Descripcion = false)
		 * 
		 * Busca la exitencia de una provincia
		 * @param $Descripcion: Nombre de la provincia
		 * @return Resultado de la consulta.
		 */
		private static function ConsultaIndiceProvincia($Descripcion = false){
			if($Descripcion == true){
				$SQL = 'SELECT IdProvincia, count(*) as Cantidad FROM tbl_provincia WHERE Descripcion="'.$Descripcion.'"';
				$Conexion = NeuralConexionDB::PDO(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetch();
				unset($Consulta, $SQL, $Conexion);
				return $Resultado;				
			}
		}
		
		/**
		 * Metodo Publico 
		 * BuscarInformacionConsulta($Matriz = false, $Consulta = false, $Campo = false, $Compara = false)
		 * 
		 * Busca si los datos de la matriz existen en la base de datos
		 * @param $Matriz: Matriz de datos
		 * @param $Consulta: Arreglo de consulta
		 * @param $Campo: Campo de comparación de la matriz
		 * @param $Compara: Campo de comparacion de la consulta 
		 */
		public static function BuscarInformacionConsulta($Matriz = false, $Consulta = false, $Campo = false, $Compara = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Consulta == true AND is_array($Consulta) == true AND $Campo == true AND $Compara == true){
				$Consulta = self::arrayColumn($Consulta, $Compara);
				foreach($Matriz as $Llave => $Arreglo){
					if(!empty($Arreglo[$Campo])){
						if(in_array($Arreglo[$Campo], $Consulta) == false){
							$Matriz[$Llave][$Campo] = "ERROR";
						}
					}
					else{
						$Matriz[$Llave][$Campo] = "ERROR";
					}
				}
				unset($Consulta, $Campo, $Compara);
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Publico
		 * ValidaEntradas($Arreglo = false)
		 * 
		 * Limpia espacios vaicios
		 * @param $Arreglo: Matriz de datos
		 * @return $Registros: arreglo validado
		 */
		public static function ValidaEntradas($Arreglo = false){
			if($Arreglo == true){
				foreach($Arreglo as $Fila){
					foreach($Fila as $Llave => $Valor){
						if(empty($Valor)){
							$Lista[$Llave] = "ERROR";
						}
						else{
							$Lista[$Llave] = $Valor;
						}
					}
					$Registros[] = $Lista;
					$Lista = array();
				}
				return $Registros;
			}
		}
		
		/**
		 * Metodo publico
		 * SustituyeIndices()
		 * 
		 * Intercambia la descripcion por los Id
		 * @param $Matriz: Matriz de datos
		 * @param $Consulta: Matriz de datos de consulta
		 * @param $Campo: Nombre da campo a buscar
		 * @param $Sustituye: Indice de sustitucion
		 * @return $Registros: Matriz con los nuevos datos
		 */
		public static function SustituyeIndicesProvincias($Matriz = false, $Consulta = false, $Campo = false, $Sustituye = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Consulta == true AND is_array($Consulta) == true AND $Campo == true AND $Sustituye == true){
				$Provincias = self::arrayColumn($Consulta, "Descripcion");
				foreach($Matriz as $Arreglo){
					foreach($Arreglo as $llave => $Valor){
						if($llave == $Campo){
							$Id = array_search($Valor, $Provincias);
							$Lista[$Sustituye] = (in_array($Valor, $Provincias)) ? $Consulta[$Id][$Sustituye] : $Valor;
						}
						else{
							$Lista[$llave] = $Valor;
						}
					}
					$Registros[] = $Lista;
					$Lista = array();
				}
				unset($Matriz, $Consulta, $Campo, $Sustituye, $Lista);
				return $Registros;
			}
		}

		/**
		 * Metodo publico
		 * SustituyeIndices()
		 * 
		 * Intercambia la descripcion por los Id
		 * @param $Matriz: Matriz de datos
		 * @param $Consulta: Matriz de datos de consulta
		 * @param $Campo: Nombre da campo a buscar
		 * @param $Sustituye: Indice de sustitucion
		 * @return $Registros: Matriz con los nuevos datos
		 */
		public static function SustituyeIndices($Matriz = false, $Consulta = false, $Campo = false, $Sustituye = false, $CampoConsulta = false){
			if($Matriz == true AND is_array($Matriz) == true AND $Consulta == true AND is_array($Consulta) == true AND $Campo == true AND $Sustituye == true){
				$ConsultaArray = self::arrayColumn($Consulta, $CampoConsulta);
				foreach($Matriz as $Arreglo){
					foreach($Arreglo as $llave => $Valor){
						if($llave == $Campo){
							$Id = array_search($Valor, $ConsultaArray);
							$Lista[$Sustituye] = (in_array($Valor, $ConsultaArray)) ? $Consulta[$Id][$Sustituye] : 0;
						}
						else{
							$Lista[$llave] = $Valor;
						}
					}
					$Registros[] = $Lista;
					$Lista = array();
				}
				unset($Matriz, $Consulta, $Campo, $Sustituye, $Lista);
				return $Registros;
			}
		}
		
		/**
		 * Metodo Publico 
		 * CombinarArreglos($Matriz = false, $TipoIngresos = false)
		 * 
		 * Añade al final un Arreglo a una matriz.
		 * @param $Matriz: Arreglo Principal de tipo Matriz.
		 * @param $TipoIngresos: Arreglo que se quiere agregar. 
		 * @return $Matriz: Arrego Combinado.
		 */
		public static function CombinarArreglos($Matriz = false, $TipoIngresos = false){
			if($Matriz == true AND is_array($Matriz) == true AND $TipoIngresos == true AND is_array($TipoIngresos) == true){
				foreach ($Matriz as $key => $Arreglo) {
					$Matriz[$key]['EstadoIngreso'] = $TipoIngresos[$key];
				}
				unset($TipoIngresos);
				return $Matriz;
			}
		}
		
		/**
		 * Metodo publico estatico
		 * CreaMatrizDatosNuevos()
		 * 
		 * Crea una nueva Matriz dependiendo de su estado
		 * @param $Matriz: Matriz de datos del archivo
		 * @param $ArregloEstados: Arreglo de estados de los registros
		 * @param $Estado: String puede ser ACTUALIZAR REGISTRO o NUEVO REGISTRO
		 * @return $MatrizNueva: Matriz de resultados
		 */
		public static function CreaMatrizEstados($Matriz = false, $ArregloEstados = false, $Estado = false){
			if($Matriz == true AND is_array($Matriz) AND is_array($ArregloEstados) == true AND $Estado == true){
				$MatrizNueva = false;
				if(count($Matriz) == count($ArregloEstados)){
					foreach($ArregloEstados as $key => $value) {
						if($value == $Estado){
							$MatrizNueva[] = $Matriz[$key];
						}
					}
				}
				return $MatrizNueva;
			}
		}

		/**
		 * Metodo Publico
		 * AppArreglos::MatrizNueva($Datos = false, $Estados = false, $Estado = false)
		 *
		 * Genera un array solo con los datos nuevos
		 * @param boolean $Datos   Array de datos
		 * @param boolean $Estados Array de estados
		 * @param boolean $Estado  "NUEVO REGISTRO"
		 */
		public static function MatrizNueva($Datos = false, $Estados = false, $Estado = false){
			if($Datos == true and $Estados == true and $Estado == true){			
				$MatrizNueva = false;
				if(count($Datos) == count($Estados)){
					foreach($Estados as $key => $value) {
						if($value == $Estado){
							$MatrizNueva[] = $Datos[$key];
						}
					}
				}
				return $MatrizNueva;
			}
		}
		
		/**
		 * Metodo publico estatico
		 * EliminarErrores()
		 * 
		 * Elimina los registros que contengan un error
		 * @param $Matriz: Matriz de datos
		 * @return $Lista: Matriz de resultados
		 */
		public static function EliminarErrores($Matriz = false){
			if($Matriz == true AND is_array($Matriz) == true){
				$Lista = array();
				foreach ($Matriz as $Arreglo) {
					if(!in_array('ERROR', $Arreglo)){
						$Lista[] = $Arreglo;
					}
				}
				unset($Matriz);
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaCoincidencia($Valor = false, $Consulta = false, $Compara = false)
		 * 
		 * Consulta si el dato existe en un arreglo de consulta
		 * @param $Valor: Dato a buscar
		 * @param $Consulta: Arreglo de datos 
		 * @param $Compara: Columna en la cual buscar el dato
		 * @return Devuelve el estdo de la búsqueda
		 */
		private static function ConsultaCoincidencia($Valor = false, $Consulta = false, $Compara = false){
			if($Valor == true AND $Consulta == true AND is_array($Consulta)){
				$Arreglo = self::arrayColumn($Consulta, $Compara);
				if(in_array($Valor, $Arreglo) == true){
					return $Valor;
				}
				else{
					return "ERROR";
				}
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaIndice()
		 * 
		 * Busca el indice de la descripcion y lo retorna
		 * @param $Consulta: Matriz de datos de consulta
		 * @param $Valor: valor a buscar
		 * @param $Sustituye: Indice de sustitucion 
		 * @return retorna el indice del registro
		 */ 
		private static function ConsultaIndice($Consulta = false, $Valor = false, $Sustituye = false){
			if($Consulta == true AND is_array($Consulta) == true AND $Valor == true AND $Sustituye == true){
				foreach($Consulta as $Arreglo){
					if(in_array($Valor, $Arreglo)){
						return $Arreglo[$Sustituye];
					}
				}
			}
		}
		
		/**
		 * Metodo privado
		 * ($ValorRegistro = false, $Tabla = false, $Campo = false)
		 * 
		 * Buscan en la tabla indicada si existe una coicidencia 
		 * @param $ValorRegistro: El dato a buscar
		 * @param $Tabla: nombre de la tabla 
		 * @param $campo: Nombre de la columna
		 */
		private static function BuscarRegistro($ValorRegistro = false, $Tabla = false, $Campo = false){
			if($ValorRegistro == true AND $Tabla == true AND $Campo == true){
				$SQL = 'SELECT count(*) as Cantidad FROM '. $Tabla .' WHERE '.$Campo.'="'.$ValorRegistro.'"';
				$Conexion = NeuralConexionDB::PDO(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetch();
				unset($Consulta, $SQL, $ValorRegistro, $Tabla, $Campo, $Conexion);
				return ($Resultado['Cantidad'] > 0)? 'ACTUALIZAR REGISTRO' : 'NUEVO REGISTRO';
			}
		}

		public static function ConsultarAgenda($Tabla = false, $Campo = false){
			$SQL = "SELECT ".$Campo." FROM ". $Tabla ." WHERE StatusRegistro != 'ELIMINADO'";
			$Conexion = NeuralConexionDB::PDO(APP);
			$Consulta = $Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Privado
		 * arrayColumn(array $array, $column_key, $index_key = null)
		 * 
		 * Conviarte una columna de una matriz en un vector
		 */
		private static function arrayColumn(array $array, $column_key, $index_key = null){
	        if(function_exists('array_column')){
	            return array_column($array, $column_key, $index_key);
	        }
	        $result = [];
	        foreach($array as $arr){
	            if(!is_array($arr)) continue;
	            if(is_null($column_key)){
	                $value = $arr;
	            }else{
	                $value = $arr[$column_key];
	            }
	
	            if(!is_null($index_key)){
	                $key = $arr[$index_key];
	                $result[$key] = $value;
	            }else{
	                $result[] = $value;
	            }
	
	        }
	        unset($array);
	        return $result;
	    }
	    
	    /**
	     * Metodo Publico
	     * EliminarDepartamento($Arreglo = false)
	     * 
	     * elimina una columna unecisarioa especifica
	     * @param $Arrelgo: datos de archivo
	     * @return $Lista: arreglo recortado.
	     */
	    public static function EliminarDepartamento($Arreglo = false){
	    	if($Arreglo == true){
	    		$Lista = array();
	    		foreach($Arreglo as $Arreglo){
  					unset($Arreglo[0]);
  					$Lista[] = $Arreglo;
	    		}
	    		return $Lista;
	    	}
	    }
	    
	    /**
	     * Metodo Publico 
	     * public static function ExtraerColumna($Matriz = false, $Columna = false)
	     * 
	     * Toma los valores de la columna indicada
	     */
	    public static function ExtraerColumna($Matriz = false, $Columna = false){
	    	if($Matriz == true and $Columna == true){
	    		$Lista = array();
	    		$i = 0;
	    		foreach($Matriz as $Arreglo){
	    			$Lista[$i]['Descripcion'] = $Arreglo[$Columna];
	    			$i++;
	    		}
	    		return $Lista;
	    	}
	    }

	    /**
	     * Metodo Publico
	     * EliminarRepeticionesArchivo($DatosArchivo = false)
	     *
	     * Busca Registros repetidos en los datos de un archivo
	     * @param boolean $DatosArchivo Array de datos
	     */
	    public static function EliminarRepeticionesArchivo($DatosArchivo = false){
	    	if($DatosArchivo == true AND is_array($DatosArchivo)){
	    		foreach ($DatosArchivo as $key => $value) {
	    			$DatosArchivo[$key] = json_encode($value);
	    		}
	    		$DatosArchivo = array_unique($DatosArchivo);
	    		foreach ($DatosArchivo as $key => $value) {
	    			$DatosArchivo[$key] = json_decode($value);
	    		}
	    		return $DatosArchivo;
	    	}
	    }

	    /**
	     * Metodo Publico 
	     * ComparaArreglos($DatosArchivo = false, $Consulta = false)
	     *
	     * Compara dos matrices y retorna las diferencias
	     * @param boolean $DatosArchivo [description]
	     * @param boolean $Consulta     [description]
	     */
	    public static function ComparaArreglos($DatosArchivo = false, $Consulta = false){
	    	if($DatosArchivo == true AND is_array($DatosArchivo)){
	    		foreach ($DatosArchivo as $key => $value) {
	    			$DatosArchivo[$key] = json_encode($value);
	    		}
	    		foreach ($Consulta as $key => $value) {
	    			$Consulta[$key] = json_encode($value);
	    		}
	    		$DatosArchivo = array_diff($DatosArchivo, $Consulta);
	    		foreach ($DatosArchivo as $key => $value) {
	    			$DatosArchivo[$key] = json_decode($value, true);
	    		}
	    		return $DatosArchivo;
	    	}
	    }
	}
