<?php

	class AppArchivos {

		/**
		 * Metodo Publico
		 * CargarArchivo($Archivo = false)
		 *
		 * Carga de archivo en directorio temporal
		 * @param bool $Archivo: Arreglo del archivo
		 * @return string
		 */
		public static function CargarArchivo($Archivo = false){
			if($Archivo == true AND is_array($Archivo) == true):
				$Tamano = $Archivo["file-0"]['size'];
				$Extension = $Archivo["file-0"]['type'];
				if(self::ValidarExtension($Extension) == true):
					$Nombre = $Archivo["file-0"]['name'];
					$Prefijo = substr(md5(uniqid(rand())), 0, 6);
					$Destino = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', $Prefijo . "_" . $Nombre));
					if(copy($Archivo['file-0']['tmp_name'], $Destino)):
						return $Destino;
					else:
						return "ERROR";
					endif;
				else:
					return "ERROR";
				endif;
			else:
				return "ERROR";
			endif;			
		}
		
		/**
		 * Metodo Publico
		 * CargarArchivo($Archivo = false)
		 *
		 * Carga de archivo en directorio temporal
		 * @param bool $Archivo: Arreglo del archivo
		 * @return string
		 */
		public static function CargarArchivoTemporal($Archivo = false){
			if($Archivo == true AND is_array($Archivo) == true):
				$Tamano = $Archivo["file-0"]['size'];
				$Extension = $Archivo["file-0"]['type'];
				if(self::ValidarExtension($Extension) == true):
					$Nombre = $Archivo["file-0"]['name'];
					$Prefijo = substr(md5(uniqid(rand())), 0, 6);
					$Destino = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', $Prefijo . "_" . $Nombre));
					$Informacion = array("Destino" => implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', "")), "Nombre" => $Prefijo . "_" . $Nombre);
					if(copy($Archivo['file-0']['tmp_name'], $Destino)):
						return array("Destino" => $Destino, "Nombre" => $Prefijo . "_" . $Nombre);
					else:
						return "ERROR";
					endif;
				else:
					return "ERROR";
				endif;
			else:
				return "ERROR";
			endif;			
		}

		/**
		 * Metodo Publico
		 * ValidarExtension($extension = false)
		 *
		 * Validar extension permitida Excel
		 * @param bool $extension: Datos de la extension
		 * @return bool|string
		 */
		private static function ValidarExtension($extension = false){
			if($extension == 'application/vnd.ms-excel'):
				return "xls";
			elseif($extension == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'):
				return "xlsx";
			endif;
			return false;
		}
	}
