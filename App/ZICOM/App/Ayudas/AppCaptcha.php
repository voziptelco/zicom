<?php
	
	/**
	 * NEURAL FRAMEWORK PHP
	 * 
	 * Helper - AppCaptcha
	 * Clase que ayudas para la creacion de Captcha
	 * 
	 * @access public
	 * @since 1.0
	 * @version 2.0
	 * 
	 */
	class AppCaptcha {
		
		/**
		 * Metodo Publico
		 * Imagen()
		 * 
		 * Construye Captcha Imagen
		 * 
		 * */
		public static function Imagen() {
			$CodigoSeguridad = AppCaptcha::GeneraCodigoSeguridad(6);
			NeuralSesiones::AsignarSession('Captcha', $CodigoSeguridad);
			$Escala = 0.50;
			$Ruta_Imagen = NeuralRutasApp::WebPublico('img/img009.png');
			$Info_Imagen = getimagesize($Ruta_Imagen);
			$Captcha = imagecreatefrompng($Ruta_Imagen);
			$Alto_Nuevo  = round($Info_Imagen[1] * $Escala);
			$Ancho_Nuevo = round($Info_Imagen[0] * $Escala);
			$Copia = imagecreatetruecolor($Ancho_Nuevo, $Alto_Nuevo);
			imagecopyresized($Copia, $Captcha, 0, 0, 0, 0, $Ancho_Nuevo, $Alto_Nuevo, $Info_Imagen[0], $Info_Imagen[1]);
			$ColText = imagecolorallocate($Copia, 0, 0, 0);
			imagestring($Copia, 15, 20, 7, $CodigoSeguridad, $ColText);
			header("Content-type: image/png");
			imagepng($Copia);
			ImageDestroy($Copia);
			ImageDestroy($Captcha);
		}
		
		/**
		 * Metodo Publico
		 * GeneraCodigoSeguridad($Cantidad = 5)
		 * 
		 * Genera Codigo de seguridad
		 * */
		public static function GeneraCodigoSeguridad($Cantidad = 5) {
			$md5_hash = md5(rand(0,99999)); 
			return substr($md5_hash, 25, $Cantidad);
		}
		
		/**
		 * Metodo Publico
		 * ValidarCaptcha($Captcha = false)
		 * 
		 * Valida el captchat
		 * */
		public static function ValidarCaptcha($Captcha = false) {
			if($Captcha == true AND NeuralSesiones::ObtenerSession('Captcha') == $Captcha){
					return true;
			}
			else {
				return false;
			}
		}
		
	}