<?php

	class AppPHPExcel {

		/**
		 * Contenedor de Datos de Lectura Excel
		 */
		private static $Contenedor = false;

		/**
		 * Contenedor del Objeto Exportar Excel
		 */
		private $PHPExcel = false;
		
		/**
		 * NumeroColumnas
		 * Variable publica
		 */
	 	public static $Columnas = false;
	 	
		/**
		 * AppPHPExcel::__construct()
		 *
		 * Constructor que genera el require al archivo correspondiente
		 * Instancia la clase PHPExcel para menajo de Exportacion
		 * @return void
		 */
		function __construct() {
			self::requirePHPExcel();
			$this->PHPExcel = new PHPExcel();
		}


		/**
		 * AppPHPExcel::LeerExcelColumnas()
		 *
		 * Genera proceso de lectura de columnas
		 * @param bool $Archivo
		 * @param bool $Extension
		 * @return
		 */
		public function LeerExcelColumnas($Archivo = false, $Extension = false){
			if(file_exists($Archivo) == true):
				if(self::ValidarExtension($Extension) == true):
					return $this->LeerExcelColumnasProceso($Archivo, self::ValidarExtension($Extension));
				else:
					throw new AppExcepcion('El Archivo No es Formato Excel');
					exit();
				endif;
			else:
				throw new AppExcepcion('El Archivo de Excel No Existe');
				exit();
			endif;
		}

		/**
		 * AppPHPExcel::LeerExcelColumnasProceso()
		 *
		 * Genera el proceso de lectura archivo excel
		 * @param string $Archivo
		 * @return mixed
		 */
		private function LeerExcelColumnasProceso($Archivo = false) {
			$inputFileType = PHPExcel_IOFactory::identify($Archivo);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($Archivo);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet):
				$Hoja[] = $worksheet->getTitle();
				$Matrix[$worksheet->getTitle()] = $worksheet->toArray();
			endforeach;
			$MatrixDatos = (count($Hoja) >= 1) ? $Matrix[$Hoja[0]] : $Matrix[$Hoja[0]];
			$Columnas = $MatrixDatos[0];
			self::$Columnas = $MatrixDatos[0];
			unset($MatrixDatos[0]);
			self::$Contenedor = $MatrixDatos;
			unset($inputFileType, $objReader, $objPHPExcel, $MatrixDatos, $Matrix, $Hoja);
			return $Columnas;
		}
		
		public function LeerColumnas(){
			return self::$Columnas;
		}

		/**
		 * AppPHPExcel::DatosExcel()
		 *
		 * Retorna valores del excel
		 * @return mixed
		 */
		public function DatosExcel() {
			return self::$Contenedor;
		}

		/**
		 * AppPHPExcel::Abc()
		 *
		 * Genera abecedario correspondiente
		 * @param integer $Indice
		 * @return mixed
		 */
		public static function Abc($Indice = false) {
			for ($i= 65; $i<=90; $i++):
				$Lista[] = chr($i);
			endfor;
			if($Indice == true AND is_bool($Indice) == false):
				if(is_numeric($Indice) == true AND $Indice<=25 AND $Indice>=0):
					return $Lista[$Indice];
				endif;
				return 'El Indice No Puede ser Devuelto';
			else:
				return $Lista;
			endif;
		}

		/**
		 * AppPHPExcel::ValidarExtension()
		 *
		 * @param mixed $extension
		 * @return
		 */
		private static function ValidarExtension($extension = false){
			if($extension == 'application/vnd.ms-excel'):
				return "xls";
			elseif($extension == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'):
				return "xlsx";
			endif;
			return false;
		}

		/**
		 * AppPHPExcel::requirePHPExcel()
		 *
		 * @return void
		 */
		private static function requirePHPExcel(){
			$Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'PHPExcel', 'PHPExcel.php'));
			$Validacion = array_search($Archivo, get_included_files());
			if($Validacion == false AND is_numeric($Validacion) == false):
				if(file_exists($Archivo) == true):
					require_once implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Proveedores', 'PHPExcel', 'PHPExcel.php'));
				else:
					throw new AppExcepcion('Libreria PHPExcel No Existe');
					exit();
				endif;
			endif;
			unset($Archivo, $Validacion);
		}

		/**
		 * AppPHPExcel::ExportarInfoUsuario()
		 *
		 * Genera el proceso de la instancia de PHPExcel y carga la informacion
		 * del archivo general que se creara en excel
		 * @param string $Nombre
		 * @param string $Titulo
		 * @param string $Categoria
		 * @return void
		 */
		public function ExportarInfoUsuario($Nombre = false, $Titulo = false, $Categoria = false) {
			$this->PHPExcel->getProperties()->setCreator("Gestion y Cobranzas, ZICOM Group");
			$this->PHPExcel->getProperties()->setLastModifiedBy($Nombre);
			$this->PHPExcel->getProperties()->setTitle($Titulo);
			$this->PHPExcel->getProperties()->setSubject("");
			$this->PHPExcel->getProperties()->setDescription("");
			$this->PHPExcel->getProperties()->setKeywords("");
			$this->PHPExcel->getProperties()->setCategory($Categoria);
		}

		/**
		 * AppPHPExcel::ExportarAsignarCelda()
		 *
		 * Asigna un valor a una celda indicada
		 * @param string $Celda
		 * @param string $Valor
		 * @return void
		 */
		public function ExportarAsignarCelda($Celda = false, $Valor = false) {
			if($Celda == true AND is_bool($Celda) == false AND $Valor == true):
				$this->PHPExcel->setActiveSheetIndex(0)->setCellValue($Celda, $Valor);
			endif;
		}

		/**
		 * AppPHPExcel::ExportarArchivoExcel()
		 *
		 * Genera el proceso de descarga del archivo excel correspondiente
		 * @param string $Extension
		 * @param string $Nombre
		 * @return
		 */
		public function ExportarArchivoExcel($Extension = false, $Nombre = false) {
			if(mb_strtolower($Extension) == 'xls'):
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$Nombre.'.xls"');
				header('Cache-Control: max-age=0');
				header('Cache-Control: max-age=1');
				header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
				header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
				header ('Pragma: public'); // HTTP/1.0
				$objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel5');
				$objWriter->save('php://output');
				exit();
			elseif(mb_strtolower($Extension) == 'xlsx'):
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header('Content-Disposition: attachment;filename="'.$Nombre.'.xlsx"');
				header('Cache-Control: max-age=0');
				header('Cache-Control: max-age=1');
				header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
				header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
				header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
				header ('Pragma: public'); // HTTP/1.0
				$objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel2007');
				$objWriter->save('php://output');
				exit();
			endif;
			return false;
		}

		/**
		 * AppPHPExcel::ExportarSalvar()
		 *
		 * Genera el proceso de guardar en la carpeta Temporales el archivo creado para
		 * su postuma eliminacion
		 * @param string $Extension
		 * @param string $Nombre
		 * @return raw
		 */
		public function ExportarSalvar($Extension = false, $Nombre = false) {
			if($Extension == true AND is_bool($Extension) == false AND $Nombre == true AND is_bool($Nombre) == false):
				$Archivo = implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), 'Temporales', $Nombre.'.'.mb_strtolower($Extension)));
				$objWriter = PHPExcel_IOFactory::createWriter($this->PHPExcel, 'Excel5');
				$objWriter->save($Archivo);
				exit();
			endif;
		}
	}