<?php

	class AppFechas {

		private static $TimeZone = 'America/Lima';

		/**
		 * Metodo publico estatico
		 * ObtenerFechaActual()
		 *
		 * Obtiene la fecha actual.
		 * @return string: Fecha en formato, (Y-m-d) o (AAA-MM-DD)
		 */
		public static function ObtenerFechaActual(){
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-m-d");
		}

		/**
		 * Metodo publico estatico
		 * ObtenerHoraActual.
		 *
		 * Obtiene la hora actual.
		 * @return string: Hora en formato, (H:i:s) o (HH:MM:SS).
		 */
		public static function ObtenerHoraActual(){
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("H:i:s");
		}

		/**
		 * Metodo publico estatico
		 * ObtenerDatetimeActual()
		 *
		 * Obtiene el la fecha y hora actual en formato datetime.
		 * @return string: Hora y Fecha, Y-m-d H:i:s.
		 */
		public static function ObtenerDatetimeActual(){
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-m-d H:i:s");
		}
		
		/**
		 * Metodo publico estatico
		 * ObtenerTiempoFechaActual()
		 *
		 * Obtiene el la fecha y hora actual en formato datetime.
		 * @return string: Hora y Fecha, Y-m-d H:i:s.
		 */
		public static function ObtenerTiempoFechaActual(){
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-m-d-H-i-s");
		}

		/**
		 * Metodo publico estatico
		 * DatetimeInicioDiaActual()
		 *
		 * Devuelve la fecha actual con la hora en ceros.
		 * @return string: Fecha inicio dia actual, Y-m-d 00:00:00.
		 */
		public static function DatetimeInicioDiaActual(){
				$NowDateTime = new \DateTime('now');
				$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
				return $NowDateTime->format("Y-m-d 00:00:00");
		}

		/**
		 * Metodo publico estatico
		 * DatetimeInicioMesActual()
		 *
		 * Desvuelve la fecha de primer dia y la primera hora del mes y a�o actual.
		 * @return string: "Y-m-01 00:00:00"
		 */
		public static function DatetimeInicioMesActual(){
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-m-01 00:00:00");
		}

		/**
		 * Metodo publico estatico
		 *DatetimeInicioAnioActual
		 *
		 * Obtiene el primer mes, hora y dia del a�o actual.
		 * @return string: "Y-01-01 00:00:00"
		 */
		public static function DatetimeInicioAnioActual(){
			$NowDateTime = new \DateTime('now');
			$NowDateTime->setTimezone(new \DateTimeZone(self::$TimeZone));
			return $NowDateTime->format("Y-01-01 00:00:00");
		}

		/**
		 * Metodo publico
		 * ObtenerDatetime($Date = false, $Time = false)
		 *
		 *Transforma el tipo date y time a un datetime.
		 * @param bool|false $Date
		 * @param bool|false $Time
		 * @return bool|string: Devuelve un Datetime.
		 */
		public static function ObtenerDatetime($Date = false, $Time = false){
			if($Date == true AND $Time == true){
				return date('Y-m-d H:i:s', strtotime("$Date $Time"));
			}

		}

		/**
		 * Metodo publico estatico
		 * ObtenerFecha($Datetime = false)
		 *
		 * Devuelve solo la fecha de un Datetime, en formato
		 * @param $Datetime
		 * @return mixed
		 */
		public static function ObtenerFecha($Datetime = false){
			$Dates = explode(' ', $Datetime);
			return $Dates[0];
		}

		/**
		 * Metodo publico estatico
		 * ObtenerHora($Datetime = false)
		 *
		 * Obtiene solo la hora del objeto $Datetime recibido.
		 * @param bool|false $Datetime
		 * @return mixed: Segunda segunda parte despues del espacio.
		 */
		public static function ObtenerHora($Datetime = false){
			$Dates = explode(' ', $Datetime);
			return $Dates[1];
		}

		/**
		 * Metodo publico estatico
		 * ObtenerNombreMes($NumeroMes = false)
		 *
		 * Develve el nombre del mes correspondiente a su numero.
		 * @param bool|false $NumeroMes: (1 = Enero, 2 = Febrero, ...)
		 * @return bool:false|Mes.
		 */
		public static function ObtenerNombreMes($NumeroMes = false){
			if(is_numeric($NumeroMes) AND $NumeroMes >= 0 AND $NumeroMes < 12){
				$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
				return $meses[$NumeroMes - 1];
			}else{
				return false;
			}
		}

		/**
		 * Metodo publico estatico
		 * ObtenerMeses()
		 *
		 * Devuelve los meses del a�o.
		 * @return array: Meses del a�o
		 */
		public static function ObtenerMeses(){
			return array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		}

		/**
		 * Metodo Publico
		 * AppUtilidades::FormatoFecha($Arreglo = false)
		 *
		 * da formato a una fecha d/m/Y a y-m-d
		 * @param $Fecha: fechas tipo d/m/Y
		 * @return string fechas y-m-d
		 * */
		public static function FormatoFecha($Fecha = false){
			if($Fecha == true){
				$Fecha = str_replace("/", "-", $Fecha);
				$NowDateTime = new DateTime($Fecha);
				return $NowDateTime->format("Y-m-d");				 
			}
		}
		
		/**
		 * Metodo Publico
		 * AppUtilidades::FormatoFechaTiempo($Arreglo = false)
		 *
		 * da formato a una fecha d/m/Y a y-m-d
		 * @param $Fecha: fechas tipo d/m/Y
		 * @return string fechas y-m-d
		 * */
		public static function FormatoFechaTiempo($Fecha = false){
			if($Fecha == true){
				$Fecha = str_replace("/", "-", $Fecha);
				$NowDateTime = new DateTime($Fecha);
				return $NowDateTime->format("Y-m-d H:i:s");				 
			}
		}
		
		/**
		 * Metodo publico estatico
		 * ConvertirSegundosTime($Segundos = false)
		 *
		 * Convierte los segundos recibidos a formato minutos.
		 * @param bool|false $Segundos
		 * @return string: 'H:i:s'
		 */
		public static function ConvertirSegundosTime($Segundos = false){
		   if($Segundos == true){
		      return gmdate("H:i:s", $Segundos);
		   }
		}
		
		/**
		 * Metodo Publico 
		 * ConvertirMinutosSegundos($Minutos = false)
		 * 
		 * Hace una conversion rapida de minutos a segundos
		 * @param $Segundos: Dato en minutos
		 * @return Equivalente en segundos
		 */
		public static function ConvertirMinutosSegundos($Minutos = false){
			if($Minutos == true){
				return $Minutos * 60;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConvertirSegundosMinutos($Minutos = false)
		 * 
		 * Hace una conversion rapida de segundos a minutos
		 * @param $Segundos: Dato en segundos
		 * @return Equivalente en minutos
		 */
		public static function ConvertirSegundosMinutos($Segundos = false){
			if($Segundos == true){
				return $Segundos / 60;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConvertirTimeSegundos($Tiempo = false)
		 * 
		 * Convertir Tiempo hh:mm:ss a segundos
		 * @param $Tiempo: tiempo dado en formato hh:mm:ss
		 * @param $Tiempo_en_segundos: tiempo en segundos 
		 */
		public static function ConvertirTimeSegundos($Tiempo = false){
			if($Tiempo == true){
				list($horas, $minutos, $segundos) = explode(':', $Tiempo);
				$Tiempo_en_segundos = ($horas * 3600 ) + ($minutos * 60 ) + $segundos;
				return $Tiempo_en_segundos; 
			}
		}
		
		/**
		 * Metodo Publico 
		 * ValidaHora($Tiempo = false)
		 * 
		 * Comprueba el tiempo para no guardar 00:00 
		 * @param $Tiempo
		 */
		public static function ValidaHora($Tiempo = false){
			if($Tiempo == true){
				return str_replace("00:00", "23:59", $Tiempo);
			}
		}
	}


