<?php
	
	class AppFormatos {
		
		/**
		 * Metodo Publico
		 * ConvertirFormatoFechaNomralMysql($Dato)
		 * 
		 * Convierte un formato normal dd/mm/aaaa a MySQL aaaa-mm-dd
		 * @param $Dato: String del campo fecha
		 * 
		 * */
		public static function ConvertirFormatoFechaNomralMySQL($Dato) {
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $Dato, $fecha);
			$fecha_MySQL = $fecha[3]."-".$fecha[2]."-".$fecha[1];
			return $fecha_MySQL;
		}
		
		/**
		 * Metodo Publico
		 * ConvertirFormatoFechaMySQLNormal($Dato)
		 * 
		 * Convertir un formato mysql aaaa-mm-dd a normal dd/mm/aaaa
		 * @param $Dato: String del campo fecha
		 * 
		 * */
		public static function ConvertirFormatoFechaMySQLNormal($Dato) {
			preg_match("([0-9]{1,2})-([0-9]{1,2})-([0-9]{2,4})", $Dato, $fecha);
			$fecha_Normal = $fecha[3]."/".$fecha[2]."/".$fecha[1];
			return $fecha_Normal;
		}
		
		/**
	     * Metodo Publico
		 * AppUtilidades::FormatoFecha($Arreglo = false)
		 * 
		 * da formato a una fecha d/m/Y a d-m-Y
		 * @param $Arreglo: arreglo incremental con fechas tipo d/m/Y
		 * @return $Arreglo: arreglo con fechas d-m-Y
		 * */
		public static function FormatoFecha($Arreglo = false){
		if($Arreglo == true){
			for($i = 0; $i < count($Arreglo); $i++){
				$Arreglo[$i] = self::ConvertirFormatoFechaNomralMySQL($Arreglo[$i]);
			}
			return $Arreglo;
		}
	}
	}