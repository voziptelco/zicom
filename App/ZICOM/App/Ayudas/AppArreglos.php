<?php
	
	class AppArreglos{
		
		/**
		 * AppArreglos::FiltrarDatos($Matriz = false, $Columna = false, $ColumnaMoneda1 = false, $ColumnaMoneda2 = false)
		 * 
		 * Hace un filtro para los datos de una matriz en columnas especificas
		 * @param $Matriz: matriz de datos
		 * @param $Columna de busqueda
		 * ...
		 */
		public static function FiltrarDatos($Matriz = false, $Columna = false, $ColumnaMoneda1 = false, $ColumnaMoneda2 = false){
			if($Matriz == true AND is_array($Matriz) AND $Columna == true){
				$i = 0;
				foreach($Matriz AS $Arreglo){
					$Matriz[$i][$Columna] = self::Cifrar($Arreglo[$Columna]);
					$Matriz[$i][$ColumnaMoneda1] = self::FiltroModena($Arreglo[$ColumnaMoneda1]);
					$Matriz[$i][$ColumnaMoneda2] = self::FiltroModena($Arreglo[$ColumnaMoneda2]);
					$i++;
				}
			}
			return $Matriz;	
		}
		/**
		 * metodo privado
		 * Cifrar($Valor = false)
		 * 
		 * Cifra el dato
		 */
		private static function Cifrar($Valor = false){
			if($Valor){
				return NeuralCriptografia::Codificar($Valor, APP);	
			}
		}
		
		/**
		 * Metodo privado 
		 * FiltroModena($Valor = false)
		 * 
		 * da formato a los campos moneda
		 */
		private static function FiltroModena($Valor = false){
			if($Valor == true){
				return number_format($Valor, 2, '.', ',');
			}else{
				return $Valor;
			}
		}
	}