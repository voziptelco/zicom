<?php

	class AsignacionTroncales extends Controlador{

		var $Informacion;

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($Menu[2])) ? $Menu[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Index.html')));
			unset($Menu, $MenuSeleccion, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmListado()
		 * 
		 * Pantalla de listado
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarSupervisores();
				$Consulta = AppUtilidades::ComprobarTroncalesAsignadas($Consulta);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * frmVerTroncales()
		 * 
		 * Pantalla de agregar troncales
		 */
		public function frmVerTroncales(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$Troncales = $this->Modelo->ConsultarTroncales();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('IdTroncal');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Troncales', $Troncales);	
				$Plantilla->Parametro('IdSupervisor', $_POST['IdSupervisor']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmVerTroncales'));							
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Agregar', 'Agregar.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Asignar()
		 * 
		 * prepara datos para ser guardados.
		 */
		public function Asignar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVacios($_POST) == false) {
						$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
						$DatosPost['IdSupervisor'] = NeuralCriptografia::DeCodificar($DatosPost['IdSupervisor']);
						$Consulta = $this->Modelo->ConsultarAsignacion($DatosPost['IdSupervisor']);
						if($Consulta['Cantidad'] > 0){
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $Consulta, $Plantilla);
							exit();
						}else{
							$this->Modelo->AsignarTroncal($DatosPost);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $Plantilla);
						    exit();
						}
					}else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}				
			}
		}
		
		/**
		 * Metodo Publico
		 * frmReasignarTroncales()
		 * 
		 * Pantalla de agregar troncales
		 */
		public function frmReasignarTroncales(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$Troncales = $this->Modelo->ConsultarTroncales();
				$DatosAsignacion = $this->Modelo->ConsultaAsignacionTroncal(NeuralCriptografia::DeCodificar($_POST['IdTroncalAsignada']));
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('IdTroncal');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Troncales', $Troncales);	
				$Plantilla->Parametro('Asignacion', $DatosAsignacion[0]);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmVerTroncales'));							
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Editar', 'Editar.html')));
				unset($Plantilla);
				exit();
			}
		}
		
			/**
		 * Metodo Publico
		 * Reasignar()
		 * 
		 * prepara datos para ser actualizados.
		 */
		public function Reasignar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVacios($_POST) == false) {
						$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
						$Condicion = array("IdTroncalAsignada" => $DatosPost['IdTroncalAsignada']);
						$Omitidos = array('IdAsignacionTroncal', 'IdSupervisor');
						$this->Modelo->ReasignarTroncal($DatosPost, $Condicion, $Omitidos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Editar', 'Exito.html')));
						unset($DatosPost, $Consulta, $Condicion, $Omitidos, $Plantilla);
					    exit();
					}else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AsignacionTroncales', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}				
			}
		}
	}