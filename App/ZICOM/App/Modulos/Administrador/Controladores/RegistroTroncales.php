<?php

	class RegistroTroncales extends Controlador{

		var $Informacion;

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($Menu[2])) ? $Menu[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Index.html')));
			unset($Menu, $MenuSeleccion, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmListado()
		 * 
		 * Pantalla de listado
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarTroncales();
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * frmGuardarTroncales()
		 * 
		 * Pantalla de agregar troncales
		 */
		public function frmGuardarTroncales(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('NombreTroncal');
				$Validacion->Requerido('Descripcion');
				$Validacion->Requerido('Ip');
				$Validacion->Requerido('CodigoPais');
				$Validacion->Digitos('CodigoPais');
				$Validacion->Digitos('PrefijoSalida');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmGuardarTroncales'));				
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Agregar', 'Agregar.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarTroncales()
		 * 
		 * prepara datos para ser guardados.
		 */
		public function GuardarTroncales(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('Descripcion', 'PrefijoSalida')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('NombreTroncal', 'Ip', 'CodigoPais', 'PrefijoSalida'));
						$Consulta = $this->Modelo->BuscarTroncal($DatosPost['NombreTroncal']);
						if($Consulta['Cantidad'] > 0){
							$Status = $this->Modelo->BuscarTroncalEliminada($DatosPost['NombreTroncal']);
							if($Status['Cantidad'] > 0){
								$Arreglo = array('Status' => 'ACTIVO');
								$Condicion = array('IdTroncal' => $Status[0]['IdTroncal']);
								$Omitidos = array('IdTroncal', 'NombreTroncal', 'Descripcion');
								$this->Modelo->Eliminar($Arreglo, $Condicion, $Omitidos);
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Agregar', 'Exito.html')));
								unset($DatosPost, $Consulta, $Arreglo, $Condicion, $Omitidos, $Status, $Plantilla);
							    exit();
							}
							else{
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Error', 'ErrorRegistroDuplicado.html')));
								unset($DatosPost, $Consulta, $Plantilla);
								exit();	
							}							
						}else{
							$this->Modelo->AgregarTroncal($DatosPost);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $Plantilla);
						    exit();
						}
						
					}else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}				
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmEditar()
		 * 
		 * Pantalla de edicion
		 */		
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$Consulta = $this->Modelo->ConsultaTroncal(NeuralCriptografia::DeCodificar($_POST['Id'],APP));
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('NombreTroncal');
				$Validacion->Requerido('Descripcion');
				$Validacion->Requerido('Ip');
				$Validacion->Requerido('CodigoPais');
				$Validacion->Digitos('CodigoPais');
				$Validacion->Digitos('PrefijoSalida');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmGuardarTroncales'));
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});				
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Editar', 'Editar.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarTroncales()
		 * 
		 * Prepara datos en caso que se deseen modificar
		 */
		public function ActualizarTroncales(){
	 		if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('Descripcion', 'PrefijoSalida')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('NombreTroncal', 'IdTroncal', 'Ip', 'CodigoPais', 'PrefijoSalida'));
						$Omitidos = array('IdTroncal', 'Status');
						$Condicion = array('IdTroncal' => NeuralCriptografia::DeCodificar($DatosPost['IdTroncal']));
						unset($DatosPost['IdTroncal']);
						$this->Modelo->ActualizarTroncal($DatosPost, $Condicion, $Omitidos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Editar', 'Exito.html')));
						unset($DatosPost, $Consulta, $Plantilla);
					    exit();	
					}
					else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroTroncales', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}				
			}
		}
		
		/**
		 * Metodo Publico
		 * Eliminar()
		 *
		 * Elimina elementos de la base de datos(Cambia su estado)
		 */
		public function Eliminar() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdTroncal' => $Id);
					$Omitidos = array('IdTroncal', 'NombreTroncal', 'Descripcion', 'Ip', 'CodigoPais', 'PrefijoSalida');
					$this->Modelo->Eliminar($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}