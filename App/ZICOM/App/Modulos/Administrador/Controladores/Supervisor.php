<?php

	class Supervisor extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($Menu[2])) ? $Menu[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Index.html')));
			unset($Menu, $MenuSeleccion, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmListado()
		 * 
		 * Pantalla de listado de supervisores
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarSupervisores();
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * frmAgregar()
		 *
		 * Pantalla de Agregar Supervisor
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Nombres', '* Campo Requerido');
				$Validacion->Requerido('ApellidoPaterno', '* Campo Requerido');
				$Validacion->Requerido('ApellidoMaterno', '* Campo Requerido');
				$Validacion->Requerido('Extension', '* Campo Requerido');
				$Validacion->Digitos('Extension','* Solo se pueden ingresar números');
				$Validacion->Requerido('Correo', '* Campo Requerido');
				$Validacion->Requerido('TipoCanal', '* Campo Requerido');
				$Validacion->Requerido('Usuario', '* Campo Requerido');
				$Validacion->Requerido('Password', '* Campo Requerido');
				$Validacion->Email('Correo', '* Ingresa un correo valido');
				$Validacion->CampoIgual('RepitePassword', 'Password', '* La contrasea no coincide');
				$Validacion->Requerido('TipoCallerId');
				$Validacion->Digitos('NumeroBase', '* Solo Números');
				$Validacion->Digitos('Cantidad', '* Solo Números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarSupervisor'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Agregar', 'Agregar.html')));
				unset($Validacion, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * Agregar()
		 *
		 * Funcion Agregar supervisor
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					if($_POST['Password'] == $_POST['RepitePassword'] ) {
						unset($_POST['Key'], $_POST['RepitePassword']);
						if(AppPost::DatosVaciosOmitidos($_POST, array('Cargo', 'TelefonoCasa', 'TelefonoMovil', 'TelefonoCasa2', 'TelefonoMovil2', 'Status', 'callerid', 'NumeroBase', 'Cantidad') ) == false) {
							$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TelefonoCasa', 'TelefonoMovil', 'TelefonoCasa2', 'TelefonoMovil2', 'Extension', 'Correo', 'TipoCanal', 'Usuario', 'Password', 'Status', 'callerid', 'NumeroBase', 'Cantidad'));
							$Perfil = $this->Modelo->BuscarPerfil('Supervisor');
							$DatosPost['Status'] = (isset($DatosPost['Status'])) ? 'ACTIVO' : 'SUSPENDIDO';
							$DatosUsuario = array("IdPerfil" => $Perfil[0]['IdPerfil'], "Usuario" => $DatosPost['Usuario'], "Password" => hash('sha256', $DatosPost['Password']), 'Status' => $DatosPost['Status']);
							$Consulta = $this->Modelo->ConsultaUsuarioExistente($DatosPost['Usuario'], $DatosPost['Correo']);
							if($Consulta['Cantidad'] > 0){
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Error', 'ErrorRegistroDuplicado.html')));
								unset($DatosPost, $Perfil, $DatosUsuario, $Consulta, $Plantilla);
								exit();
							}
							else{
								$this->Modelo->GuardarDatosSupervisor($DatosUsuario);
								$Usuario = $this->Modelo->BuscarUsuario($DatosUsuario['Usuario']);
								$CallerId = AppUtilidades::CallerID($DatosPost);
								$DatosInformacionUsuario = array("idUsuario" => $Usuario[0]['IdUsuario'], "Nombres" => $DatosPost['Nombres'], "ApellidoPaterno" => $DatosPost['ApellidoPaterno'], "ApellidoMaterno" => $DatosPost['ApellidoMaterno'], "Cargo" => $DatosPost['Cargo'], "TelefonoCasa" => $DatosPost['TelefonoCasa'], "TelefonoMovil" => $DatosPost['TelefonoMovil'], "TelefonoCasa2" => $DatosPost['TelefonoCasa2'], "TelefonoMovil2" => $DatosPost['TelefonoMovil2'], "Correo" => $DatosPost['Correo'], "Extension" => $DatosPost['Extension'], "TipoCanal" => "SIP", "callerid" => $CallerId);
								$this->Modelo->GuardarInformacionSupervisor($DatosInformacionUsuario);
								if(isset($_FILES['Imagen']) AND !empty($_FILES['Imagen']['name'])){
									$fp = fopen($_FILES['Imagen']['tmp_name'], "r");
	  								$tfoto = fread($fp, filesize($_FILES['Imagen']['tmp_name']));
							 		fclose($fp);
							 		$resp = $this->Modelo->GuardarImagen(array("Imagen"=>$tfoto, "Tipo"=>$_FILES['Imagen']['type'], "IdUsuario" => $Usuario[0]['IdUsuario']));
								}
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Agregar', 'Exito.html')));
								unset($DatosPost, $Perfil, $DatosUsuario, $Consulta, $Usuario, $DatosInformacionUsuario, $Plantilla, $fp, $tfoto, $resp);
								exit();
							}
						}
						else {
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Error', 'ErrorElementosRequeridos.html')));
							unset($Plantilla);
							exit();
						}
					}
					else {
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Error', 'ErrorPassword.html')));
						unset($Plantilla);
						exit();
					}
				}
			}
		}

		/**
		 * Metodo Publico
		 * frmEditar()
		 *
		 * Pantalla editar supervisor
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Imagen = $this->Modelo->ConsultarImagen($IdSupervisor);
				$Consulta = $this->Modelo->ConsultarUnSupervisor($IdSupervisor);
				$Callid = json_decode($Consulta[0]['callerid'], true);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Nombres', '* Campo Requerido');
				$Validacion->Requerido('ApellidoPaterno', '* Campo Requerido');
				$Validacion->Requerido('ApellidoMaterno', '* Campo Requerido');
				$Validacion->Requerido('Extension', '* Campo Requerido');
				$Validacion->Digitos('Extension','* Solo se pueden ingresar números');
				$Validacion->Requerido('Correo', '* Campo Requerido');
				$Validacion->Email('Correo', '* Ingresa un correo valido');
				$Validacion->CampoIgual('RepitePassword', 'Password', '* La contrasea no coincide');
				$Validacion->Requerido('TipoCallerId');
				$Validacion->Digitos('NueroBase', '* Solo Números');
				$Validacion->Digitos('Cantidad', '* Solo Números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdUsuario', $_POST['Id']);
				$Plantilla->Parametro('Callid', $Callid);
				if(isset($Imagen[0]['Imagen']))
					$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarSupervisor'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Supervisor', 'Editar', 'Editar.html')));
				unset($IdSupervisor, $Consulta, $Validacion, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * Actualizar()
		 *
		 * Funcion Actualizar supervisor
		 */
		public function Actualizar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if((AppPost::DatosVaciosOmitidos($_POST, array('Cargo', 'TelefonoCasa', 'TelefonoMovil', 'TelefonoCasa2', 'TelefonoMovil2', 'Password', 'RepitePassword', 'callerid', 'NumeroBase', 'Cantidad'))) == false ) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TelefonoCasa', 'TelefonoMovil', 'TelefonoCasa2', 'TelefonoMovil2', 'Correo', 'Usuario', 'Password', 'RepitePassword', 'Status', 'callerid', 'NumeroBase', 'Cantidad'));
						$IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
						$IdInformacion = $this->Modelo->BuscarIdInformacionUsuario($IdUsuario);
						$CallerId = AppUtilidades::CallerID($DatosPost);
						$Matriz = array('Nombres' => $DatosPost['Nombres'], 'ApellidoPaterno' => $DatosPost['ApellidoPaterno'], 'ApellidoMaterno' => $DatosPost['ApellidoMaterno'], 'Cargo' => $DatosPost['Cargo'], 'TelefonoCasa' => $DatosPost['TelefonoCasa'], 'TelefonoMovil' => $DatosPost['TelefonoMovil'], 'TelefonoCasa2' => $DatosPost['TelefonoCasa2'], 'TelefonoMovil2' => $DatosPost['TelefonoMovil2'], 'Correo' => $DatosPost['Correo'], 'Extension' => $DatosPost['Extension'], 'callerid' => $CallerId);
						$Omitidos = array('IdInformacion', 'RUC', 'RazonSocial', 'Direccion', 'idUsuario', 'TipoCanal', 'ExportarExcel', 'IdProvincia', 'Status');
						$Condicion = array('IdInformacion' => $IdInformacion[0]['IdInformacion']);
						$this->Modelo->ActualizaInformacionSupervisor($Matriz, $Condicion, $Omitidos);
						if(isset($_POST['Password']) OR isset($_POST['Status'])){
							$DatosPost['Password'] = (!empty($DatosPost['Password'])) ? ($DatosPost['Password'] == $DatosPost['RepitePassword']) ? $DatosPost['Password'] : '' : '';
							$DatosPost['Status'] = (isset($DatosPost['Status'])) ? 'ACTIVO' : 'SUSPENDIDO';
							if(empty($DatosPost['Password'])){
								$Matriz = array('Status' => $DatosPost['Status']);
								$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
								$Condicion = array('IdUsuario' => $IdUsuario);
								$this->Modelo->ActualizarUsuarioSupervisor($Matriz, $Condicion, $Omitidos);
							}
							else {
								$Matriz = array('Password' =>  hash('sha256', $DatosPost['Password']), 'Status' => $DatosPost['Status']);
								$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario');
								$Condicion = array('IdUsuario' => $IdUsuario);
								$this->Modelo->ActualizarUsuarioSupervisor($Matriz, $Condicion, $Omitidos);
							}
						}
						if(isset($_FILES['Imagen']) AND !empty($_FILES['Imagen']['name'])){
							$fp = fopen($_FILES['Imagen']['tmp_name'], "r");
							$tfoto = fread($fp, filesize($_FILES['Imagen']['tmp_name']));
			 				fclose($fp);
			 				$Imagen = $this->Modelo->ConsultarImagen($IdUsuario);
			 				if(isset($Imagen[0]['Imagen'])){
			 					$this->Modelo->ActualizarFoto(array('Imagen'=>$tfoto, 'Tipo'=>$_FILES['Imagen']['type']), $IdUsuario);
			 				}else{
			 					$this->Modelo->GuardarImagen(array("Imagen"=>$tfoto, "Tipo"=>$_FILES['Imagen']['type'], "IdUsuario" => $IdUsuario));
			 				}	
						}
						unset($DatosPost, $IdUsuario, $IdInformacion ,$Matriz, $Omitidos, $Condicion, $fp, $tfoto, $Imagen);
						exit();
					}
				}
			}
		}

		/**
		 * Metodo Publico
		 * Eliminar()
		 *
		 * Elimina elementos de la base de datos(Cambia su estado)
		 */
		public function Eliminar() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdUsuario' => $Id);
					$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
					$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}

	}