<?php

	class Conmutador extends Controlador{

		var $Informacion;

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$MenuSeleccion = (isset($Menu[2])) ? $Menu[2] : 'Index';
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $MenuSeleccion);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Conmutador', 'Index.html')));
			unset($Menu, $MenuSeleccion, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmGuardarConfiguracion()
		 * 
		 * Pantalla para guardar la configuracion del conmutador
		 */
		public function frmGuardarConfiguracion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarConfiguracionConmutador();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('host', '* Campo Requerido');
				$Validacion->Requerido('contexto', '* Campo Requerido');
				$Validacion->Requerido('Prefijo', '* Campo Requerido');
				$Validacion->Requerido('callerid', '* Campo Requerido');
				if(!$Consulta){
					$Validacion->Requerido('user', '* Campo Requerido');
					$Validacion->Requerido('password', '* Campo Requerido');
				}
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmConfigurarConmutador'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Conmutador', 'Configuracion', 'ConfigurarConmutador.html')));
				unset($Consulta, $Validacion, $Plantilla);
				exit();
				
			}
		}

		/**
		 * Metodo Publico
		 * GuardarConfiguracion()
		 *
		 * Guardar configuracion del conmutador
		 */
		public function GuardarConfiguracion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('port', 'user', 'password') ) == false) {
						$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
						$Consulta = $this->Modelo->ConsultarConfiguracionConmutador();
						if($Consulta){
							$Condicion = array('idConfiguracion' => $DatosPost['idConfiguracion']);
							unset($DatosPost['idConfiguracion']);
							$Omitidos = array('idConfiguracion');
							if(empty($DatosPost['user'])){
								$Omitidos[] = "user";
								unset($DatosPost['user']);
							}
							if(empty($DatosPost['password'])){
								$Omitidos[] = "password";
								unset($DatosPost['password']);
							}
							else{
								$DatosPost['password'] = NeuralCriptografia::Codificar($DatosPost['password'], APP);
							}
							$this->Modelo->ActualizarConfiguracion($DatosPost, $Condicion, $Omitidos);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Conmutador', 'Configuracion', 'Exito.html')));
							unset($DatosPost, $Consulta, $Condicion, $Plantilla);
							exit();
						}else{
							if(empty($DatosPost['password']) AND empty($DatosPost['user'])){
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Conmutador', 'Error', 'ErrorElementosRequeridos.html')));
								unset($Plantilla);
								exit();
							}else{
								$DatosPost['password'] = NeuralCriptografia::Codificar($DatosPost['password'], APP);
								$this->Modelo->GuardarConfiguaracion($DatosPost);
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Conmutador', 'Configuracion', 'Exito.html')));
								unset($DatosPost, $Consulta, $Plantilla);
								exit();
							}
						}
					}
					else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Conmutador', 'Error', 'ErrorElementosRequeridos.html')));
						unset($Plantilla);
						exit();
					}
				}
			}
		}

	} 