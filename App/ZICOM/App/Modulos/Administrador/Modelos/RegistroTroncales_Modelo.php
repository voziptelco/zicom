<?php
	
	class RegistroTroncales_Modelo extends AppSQLConsultas{
		
		/**
		 * Constructor
		 */
   		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarTroncales()
		 * 
		 * Consulta todas las troncales.
		 */
		public function ConsultarTroncales(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_troncales');
			$Consulta->Columnas('IdTroncal, NombreTroncal, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		/**
		 * Metodo Publico
		 * ConsultaTroncal($IdTroncal = false)
		 * 
		 * Consulta una troncal
		 * @param $IdTroncal: Identificador de troncal
		 * @return Matriz de consulta 
		 */
		public function ConsultaTroncal($IdTroncal = false){
			if($IdTroncal == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_troncales');
				$Consulta->Columnas('IdTroncal, NombreTroncal, Descripcion, Ip, CodigoPais, PrefijoSalida');
				$Consulta->Condicion("IdTroncal = '$IdTroncal'");
				$Consulta->Condicion("Status != 'ELIMINADO'");
				return $Consulta->Ejecutar(False, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarTroncal($Troncal = false)
		 * 
		 * Busca le existencia de una troncal
		 * @param $Troncal: Nombre de la troncal
		 * @return Matriz de consulta 
		 */
		public function BuscarTroncal($Troncal = false){
			if($Troncal == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_troncales');
				$Consulta->Columnas('NombreTroncal');
				$Consulta->Condicion('Status != "ELIMINAR"');
				$Consulta->Condicion("NombreTroncal = '$Troncal'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarTroncalEliminada($Troncal = false)
		 * 
		 * Busca le existencia de una troncal
		 * @param $Troncal: Nombre de la troncal
		 * @return Matriz de consulta 
		 */
		public function BuscarTroncalEliminada($Troncal = false){
			if($Troncal == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_troncales');
				$Consulta->Columnas('IdTroncal');
				$Consulta->Condicion("NombreTroncal = '$Troncal'");
		    	$Consulta->Condicion("Status = 'ELIMINADO'");
				return $Consulta->Ejecutar(true, true);
			}
		}		
		
		/**
		 * Metodo Publico 
		 * AgregarTroncal($Datos = flase)
		 * 
		 * Guarda los datos de una trocal
		 * @param $Datos: array de datos
		 */
		public function AgregarTroncal($Datos = flase){
			if($Datos == true){
				return self::GuardarDatos($Datos, 'tbl_troncales', array('IdTroncal', 'Status'), APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarTroncal($Datos = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualizar los datos de una trocal
		 * @param $Datos: datos nuevos
		 * @param $Candicion: Criterio de busqueda
		 * @param $Omitidos: Columnas que se omiten
		 */
		public function ActualizarTroncal($Datos = false, $Condicion = false, $Omitidos = false){
			if($Datos == true and $Condicion == true and $Omitidos == true){
				return self::ActualizarDatos($Datos, $Condicion, 'tbl_troncales', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Mdifica el estado del rgistro a ELIMINADO
		 * @param $Arreglo: datos a modificar
		 * @param $Condicion: Id del registro a modificar
		 * @param $Omitidos: Columnas omitidas
		 * @return Resultados
		 */
		public function Eliminar($Arreglo = false, $Condicion = false, $Omitidos = false) {
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_troncales', $Omitidos, APP);
			}
		}
	}