<?php
	
	class AsignacionTroncales_Modelo extends AppSQLConsultas{
		
		/**
		 * Constructor
		 */
   		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAsignacion()
		 * 
		 * Consulta las asignaciones
		 */
		public function ConsultarAsignacion($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_troncal_asignada_supervisor');
				$Consulta->Columnas('NombreTroncal, Nombres, ApellidoPaterno');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_troncal_asignada_supervisor.IdSupervisor', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->InnerJoin('tbl_troncales', 'tbl_troncal_asignada_supervisor.IdTroncal', 'tbl_troncales.IdTroncal');
				$Consulta->Condicion("tbl_troncales.Status != 'ELIMINADO'");
				$Consulta->Condicion("tbl_troncal_asignada_supervisor.IdSupervisor = '$IdSupervisor'");
				return $Consulta->Ejecutar(true, true);	
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAsignacionTroncal()
		 * 
		 * Consulta una asignacion
		 */
		public function ConsultaAsignacionTroncal($IdTroncalAsignada = false){
			if($IdTroncalAsignada == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_troncal_asignada_supervisor');
				$Consulta->Columnas('IdTroncalAsignada, IdTroncal');
				$Consulta->Condicion("tbl_troncal_asignada_supervisor.IdTroncalAsignada = '$IdTroncalAsignada'");
				return $Consulta->Ejecutar(true, true);	
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarTroncales()
		 * 
		 * Consulta todas las troncales.
		 */
		public function ConsultarTroncales(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_troncales');
			$Consulta->Columnas('IdTroncal, NombreTroncal, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarSupervisores()
		 * 
		 * Consulta una lista de supervisores
		 * @return Matriz de datos de supervisor
		 */
		public function ConsultarSupervisores(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_sistema_usuarios');
			$Consulta->Columnas('tbl_sistema_usuarios.idUsuario, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno');
			$Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_sistema_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
			$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
			$Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
			$Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre = 'Supervisor'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * AsignarTroncal($Datos = false)
		 * 
		 * Se guardan los datos de asignacion de troncal
		 * @param $Datos: Arreglo de datos de asignacion
		 */
		public function AsignarTroncal($Datos = false){
			if($Datos == true and is_array($Datos) == true){
				return self::GuardarDatos($Datos, 'tbl_troncal_asignada_supervisor', array('IdTroncalAsignada'), APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ReasignarTroncal($Datos = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Se actualizan los datos de asignacion de troncal
		 * @param $Datos: Arreglo de datos de asignacion
		 */
		public function ReasignarTroncal($Datos = false, $Condicion = false, $Omitidos = false){
			if($Datos == true and is_array($Datos) == true and $Condicion == true and $Omitidos == true){
				return self::ActualizarDatos($Datos, $Condicion, 'tbl_troncal_asignada_supervisor', $Omitidos, APP);
			}
		}
	}