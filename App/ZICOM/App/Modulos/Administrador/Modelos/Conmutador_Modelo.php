<?php

	class Conmutador_Modelo extends AppSQLConsultas {

		/**
		 * Metodo Constuctor
		 */
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico
		 * ConsultarConfiguracionConmutador()
		 *
		 * Consultar la infomacion del comutador
		 */
		public function ConsultarConfiguracionConmutador(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_configuracion_conmutador');
			$Consulta->Columnas(self::ListarColumnas('tbl_configuracion_conmutador', false, false, APP));
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarConfiguracion($ArregloDatos = false, $Condicion = false)
		 * 
		 * Actualiza los datos de configuracion
		 * @param $ArregloDatos: Datos de configuracion del conmutador
		 * @param $Condicion: Id del registro a modificar
		 * @return Resultado de la actualizacion
		 */
		public function ActualizarConfiguracion($ArregloDatos = false, $Condicion = false, $Omitidos = false){
			if($ArregloDatos == true AND is_array($ArregloDatos) AND $Condicion == true){
				return self::ActualizarDatos($ArregloDatos, $Condicion, 'tbl_configuracion_conmutador', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarConfiguaracion($ArregloDatos = false)
		 * 
		 * Guarda los datos de configuracion inicial
		 * @param $ArregloDatos: Datos de configuracion del conmutador
		 * @return Resultado de la insercion
		 */
		public function GuardarConfiguaracion($ArregloDatos = false){
			if($ArregloDatos == true AND is_array($ArregloDatos)){
				return self::GuardarDatos($ArregloDatos, 'tbl_configuracion_conmutador', array('idConfiguracion'), APP);
			}
		}
	}