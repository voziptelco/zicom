<?php

	class Supervisor_Modelo extends AppSQLConsultas{

		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico
		 * ConsultarSupervisores()
		 * 
		 * Consulta todos loa supervisores disponibles.
		 * @return Matriz de consulta
		 */
		public function ConsultarSupervisores(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_sistema_usuarios');
			$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_sistema_usuarios', false, false, APP), self::ListarColumnas('tbl_informacion_usuarios', array('idUsuario', 'Status'), false, APP)));
			$Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_sistema_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
			$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
			$Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
			$Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre = 'Supervisor'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaUsuarioExistente($Usuario = false, $Correo = true)
		 * 
		 * Consulta si un usuario se repite.
		 * @param $Usuario: nombre de usuario a comparar
		 * @param $Correo: correo electronico a comparar
		 * @return matriz de consulta
		 */
		public function ConsultaUsuarioExistente($Usuario = false, $Correo = true){
			if($Usuario == true AND $Correo == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.Usuario = '$Usuario' OR tbl_informacion_usuarios.Correo = '$Correo'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarUnSupervisor($Id = false)
		 * 
		 * Consulta un solo resgistro, dado por su ID
		 * @param $Id: identificador del registro a consultar
		 * @return matriz con los datos de ese registro
		 */
		public function ConsultarUnSupervisor($Id = false){
			if($Id == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_sistema_usuarios', false, false, APP), self::ListarColumnas('tbl_informacion_usuarios', array('idUsuario', 'Status'), false, APP)));
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.IdUsuario = '$Id'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarUsuario($Usuario = false)
		 * 
		 * Consulta el ID de un registro, dado por su nombre de usuario
		 * @param $Usuario: Nombre de usuario a comparar.
		 * @return Id del registro.
		 */
		public function BuscarUsuario($Usuario = false){
			if($Usuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas('IdUsuario');
				$Consulta->Condicion("Usuario = '$Usuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarPerfil($NombrePerfil = false)
		 * 
		 * Consulta el id de un perfil
		 * @param $NombrePerfil: Descripciopn del prefil a buscar.
		 * @return Matriz con Id del prefil
		 */
		public function BuscarPerfil($NombrePerfil = false){
			if($NombrePerfil == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios_perfil');
				$Consulta->Columnas('IdPerfil');
				$Consulta->Condicion("Nombre = '$NombrePerfil'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarIdInformacionUsuario($IdUsuario = false)
		 * 
		 * consulta el Id del registro que contiene la infomacion de un usuario
		 * @param $IdUsuario: identificador del usuario
		 * @return Matriz con el Id del registro de informacion de usuario.
		 */
		public function BuscarIdInformacionUsuario($IdUsuario = false){
			if($IdUsuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_informacion_usuarios');
				$Consulta->Columnas('tbl_informacion_usuarios.IdInformacion');
				$Consulta->Condicion("idUsuario = '$IdUsuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarImagen($Id = false)
		 * 
		 * Consulta en bd la imagen del usuario
		 * @param $Id:	identificador del usuario
		 * @return Retorna una matriz con los datos de la imagen
		 */
		public function ConsultarImagen($Id = false){
			if($Id == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_imagen_usuario');
				$Consulta->Columnas('Imagen, Tipo');
				$Consulta->Condicion("IdUsuario = '$Id'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		public function GuardarDatosSupervisor($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_sistema_usuarios', array('IdUsuario'), APP);
			}
		}

		public function GuardarInformacionSupervisor($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_informacion_usuarios', array('IdInformacion', 'RUC', 'RazonSocial', 'Direccion', 'ExportarExcel', 'IdProvincia', 'Status'), APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarImagen($Arreglo = false)
		 * 
		 * Guarda en BD una imagen de perfil del usuario
		 * @param $Arrelo:	Arreglo con los datos de la imagen
		 */
		public function GuardarImagen($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				$SQL = new NeuralBDGab(APP, 'tbl_imagen_usuario');
				foreach($Arreglo as $Columna => $Valor){
					$SQL->Sentencia($Columna, $Valor);	
				}
			return 	$SQL->Insertar();
			}
		}
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false) 
		 *
		 *  Modifica el estatus de un registros
		 * @param boolean $Arreglo   Array de datos nuevos
		 * @param boolean $Condicion Identificdor del registro
		 * @param boolean $Omitidos  array de columnas omitidas
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false) {
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_sistema_usuarios', $Omitidos, APP);
			}
		}

		/**
		 * Metodo Publico
		 * ActualizaInformacionSupervisor($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualiza la informcacion del supervsor
		 * @param boolean $Arreglo   Array de datos nuevos
		 * @param boolean $Condicion Identificador del regtistro
		 * @param boolean $Omitidos  array de columnas omitidas
		 */
		public function ActualizaInformacionSupervisor($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_informacion_usuarios', $Omitidos, APP);
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarUsuarioSupervisor($Arreglo = false, $Condicion = false, $Omitidos = false)
		 *
		 * Actualiza los datos de usuario
		 * @param boolean $Arreglo   Array de datos nuevos
		 * @param boolean $Condicion Idenficador del registro
		 * @param boolean $Omitidos  Array de datos nuevos
		 */
		public function ActualizarUsuarioSupervisor($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_sistema_usuarios', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarFoto($Imagen = false, $Condicion = false)
		 * 
		 * Actualiza la imagen de perfil del usuario
		 * @param $Imagen:	array de datos de la imagen 
		 * @param $Condicion: Identificador (id de usuario)
		 */
		public function ActualizarFoto($Imagen = false, $Condicion = false){
			if($Imagen == true and $Condicion == true and is_array($Imagen) == true){
				$SQL = new NeuralBDGab(APP, 'tbl_imagen_usuario');
				$SQL->Sentencia('Imagen', $Imagen['Imagen']);
				$SQL->Sentencia('Tipo', $Imagen['Tipo']);
				$SQL->Condicion('IdUsuario', $Condicion);
				$SQL->Actualizar();
			}
		}
	}