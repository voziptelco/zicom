<?php
	
	class GraficaGestiones_Modelo extends Modelo{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo publico
		 * ConsultaGerencias()
		 * 
		 * Consulta todas la gerencias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 * 
		 * Consulta todas la Carteras
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 * 
		 * Consulta todos las Provincias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaDistritos()
		 * 
		 * Consulta todos los Distritos
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaDistritos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta todos los Agentes asignados a un Supervisor dado
		 * @param $IdSupervisor: Condición de búsqueda
		 * @return Arreglo de Agentes
		 */
		public function ConsultarAgentes($IdSupervisor = false){
			if($IdSupervisor == True){
				$SQL = "SELECT tbl_agentes_asignado_supervisor.IdAgente, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno ".
   	  				   "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_informacion_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario ".
   	  				   "INNER JOIN tbl_sistema_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario ".
   	  				   "WHERE tbl_sistema_usuarios.Status != 'ELIMINADO' AND tbl_sistema_usuarios.Status = 'ACTIVO' AND tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaGestiones($ArregloCriterios = false, $IdSupervisor = false, $Tabla = false, $Condicion = false){
		 * 
		 * Consulta Gestiones (campo | telefonica) segu	n los criterios
		 * @param $ArreloCriterios: Arreglo de criterios
		 * @param $IdSupervisor: Indice de agente asignado a registros.
		 * @param $Tabla: Indica si es una gesion de campo o telefonica
		 * @param $Condicion: Identificador de la tabla gestion (segun sea)
		 * @return Matriz de consulta.
		 */
		public function ConsultaGestiones($ArregloCriterios = false, $IdSupervisor = false, $Tabla = false, $Condicion = false){
			if($IdSupervisor == true AND $Condicion == true AND $Tabla == true){
                $SQL = "SELECT tbl_datos_agenda.NombreTitular, COUNT($Tabla.$Condicion) AS Cantidad, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal,  MAX($Tabla.FechaHora_Captura) AS FechaHora_Captura, $Tabla.Compromiso ".
              		   "FROM tbl_agentes_asignado_supervisor ".
              		   "INNER JOIN $Tabla ON tbl_agentes_asignado_supervisor.IdAgente = $Tabla.IdAgente ".
			  		   "INNER JOIN tbl_datos_agenda ON $Tabla.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
			  		   "LEFT JOIN tbl_fecha_compromiso ON $Tabla.$Condicion = tbl_fecha_compromiso.IdGestion ".
					   "WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO' ";           
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true){
					foreach($ArregloCriterios AS $Condicion){
						$SQL .= " AND ".$Condicion;
					}	
				}
				$SQL .= " GROUP BY tbl_datos_agenda.IdDatoAgenda ORDER BY FechaHora_Captura DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);	
			}			
		}
	}