<?php

	class Bitacora_Modelo extends Modelo{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		/**
		 * Metodo Publico
		 * ConsultarBitacoraTotal($Id = false)
		 * 
		 * Consulta un  total de registros
		 * @param $Id: Parametro de busqueda (Id del supervisor)
		 * @return Resultado de la consulta
		 */
		public function ConsultarBitacoraTotal($Id = false){
			if($Id == true){
				$SQL = "SELECT tbl_bitacora_agente.Fecha ".
                        "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_bitacora_agente ON tbl_agentes_asignado_supervisor.IdAgente = tbl_bitacora_agente.IdAgente ".
			 			"INNER JOIN tbl_datos_agenda ON tbl_bitacora_agente.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
     					"INNER JOIN tbl_informacion_usuarios ON tbl_bitacora_agente.IdAgente = tbl_informacion_usuarios.idUsuario ".
                       	"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agentes_asignado_supervisor.IdSupervisor = '$Id'";                      
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->rowCount();
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarBitacora($Id = false, $Inicio = false, $Cantidad = false, $Criterio = false)
		 * 
		 * Consulta los registrros de la bitacora
		 * @param $Id: Parametro de busqueda (Id del Supervisor)
		 * @param $Inicio: Inicio de la paginacion
		 * @param $Contidad: registros a consultar
		 * @param $Criterio: criterio de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultarBitacora($Id = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($Id == true){
				$SQL = "SELECT tbl_bitacora_agente.Fecha, tbl_bitacora_agente.Hora, CONCAT(tbl_informacion_usuarios.Nombres, ' ', tbl_informacion_usuarios.ApellidoPaterno) AS Agente, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular AS Registro ".
                       "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_bitacora_agente ON tbl_agentes_asignado_supervisor.IdAgente = tbl_bitacora_agente.IdAgente ".
			 			"INNER JOIN tbl_datos_agenda ON tbl_bitacora_agente.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
     					"INNER JOIN tbl_informacion_usuarios ON tbl_bitacora_agente.IdAgente = tbl_informacion_usuarios.idUsuario ".
                       	"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agentes_asignado_supervisor.IdSupervisor = '$Id'";                    
				if($Criterio != ""){
					$SQL .= " AND (ClienteUnico LIKE '%$Criterio%' OR NombreTitular LIKE '%$Criterio%' OR tbl_bitacora_agente.Fecha LIKE '%$Criterio%' OR tbl_bitacora_agente.Hora LIKE '%$Criterio%' OR tbl_informacion_usuarios.Nombres LIKE '%$Criterio%' OR tbl_informacion_usuarios.ApellidoPaterno LIKE '%$Criterio%')"; 
				}
				if($Cantidad != -1)
					$SQL .= " LIMIT ".$Inicio .", ". $Cantidad;
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
			}
		}
	}
?>