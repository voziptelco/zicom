<?php
	
	class Cartera_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCarteras()
		 * 
		 * Consulta las carteras existentes
		 */
		public function ConsultarCarteras(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);			
		}
		
		/**
		 * Metodo Publico
		 * ConsultaCarteraExistente($Descripcion = false)
		 * 
		 * Consulta si una cartera en especifico ya existe.
		 * @param $Descripcion: descripcion de la cartera.
		 * @return Resultado de la consulta.
		 */
		public function ConsultaCarteraExistente($Descripcion = false){
			if($Descripcion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_carteras');
				$Consulta->Columnas('IdCartera, Status');
				$Consulta->Condicion("Descripcion = '$Descripcion'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCartera($IdCartera = false)
		 * 
		 * Consulta una cartera en especifico segun el ID
		 * @param $IdCartera: condicion de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultarCartera($IdCartera = false){
			if($IdCartera == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_carteras');
				$Consulta->Columnas('Descripcion');
				$Consulta->Condicion("IdCartera = '$IdCartera'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * AgregarCartera($Arreglo = false, $Omitidos)
		 * 
		 * Agregar datos a la base de datos en tabla Cartera
		 * @param $Arreglo: array con Datos a insertar
		 * @param $Omitidos: array con columnas omitidas
		 * @return resultado de la operacion
		 */
		public function AgregarCartera($Arreglo = false, $Omitidos){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::GuardarDatos($Arreglo, 'tbl_carteras', $Omitidos, APP);			
			}
		}
		
		/**
		 * Metodo Publico
		 * EditarCartera($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualizar datos en tabla Cartera
		 * @param $Arreglo: array con Datos a insertar
		 * @param $Condicion: array con el id del registro a modificar
		 * @param $Omitidos: array con columnas omitidas
		 * @return resultado de la operacion
		 */
		public function EditarCartera($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Condicion == true AND is_array($Condicion) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_carteras', $Omitidos, APP);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualizar el status de un registro a ELIMINADO
		 * @param $Arreglo: array con Datos a insertar
		 * @param $Condicion: array con el id del registro a modificar
		 * @param $Omitidos: array con columnas omitidas
		 * @return resultado de la operacion
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_carteras', $Omitidos, APP);
			}
		}
	}
