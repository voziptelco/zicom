<?php

	class Provincia_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		/**
		 * Metodo Publico
		 * ConsultarProvincias()
		 *
		 * Consulta el total de rgistros
		 * @return  Total 
		 */
			public function ConsultarProvincias(){
			$SQL = "SELECT tbl_provincia.IdProvincia FROM tbl_provincia ".
				   "WHERE tbl_provincia.Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->rowCount();			
		}

		/**
		 * Metodo Publico 
		 * ConsultarDistritos()
		 * 
		 * Consulta todos los distritos disponibles
		 * @return Resultado de la consulta
		 */
		public function ConsultarProvinciasPagina($Inicio = false, $Cantidad = false, $Criterio = false){
			$SQL = "SELECT tbl_provincia.IdProvincia, Descripcion FROM tbl_provincia ".
				   "WHERE tbl_provincia.Status != 'ELIMINADO' ";
			if($Criterio != ""){
				$SQL .= " AND ( tbl_provincia.Descripcion LIKE '%$Criterio%')"; 
			}
			$SQL .= " LIMIT ".$Inicio .", ". $Cantidad;
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
			$Resultado['Cantidad'] = $Consulta->rowCount();
			return $Resultado;			
		}
		
		/**
		 * Metodo Publico
		 * ConsultarProvincia($IdProvincia = false)
		 *
		 * Consultar una provincia por su id
		 * @param boolean $IdProvincia Identificador de la provincia
		 */
		public function ConsultarProvincia($IdProvincia = false){
			if($IdProvincia == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_provincia');
				$Consulta->Columnas('Descripcion');
				$Consulta->Condicion("IdProvincia = '$IdProvincia'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaProvinciaExistente($Descripcion = false)
		 *
		 * Consulta la existencia de una provincia en la base de datos
		 * @param boolean $Descripcion  :Nombre de provinica
		 * @return Arreglo de consulta
		 **/
		public function ConsultaProvinciaExistente($Descripcion = false){
			if($Descripcion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_provincia');
				$Consulta->Columnas('IdProvincia, Status');
				$Consulta->Condicion("Descripcion = '$Descripcion'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * AgregarProvincia($Arreglo = false, $Omitidos)
		 *
		 * Guardar datos de provincia en BD 
		 * @param boolean $Arreglo  Datos a guardar
		 * @param [type]  $Omitidos array de amitidos
		 */
		public function AgregarProvincia($Arreglo = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::GuardarDatos($Arreglo, 'tbl_provincia', $Omitidos, APP);			
			}
		}
		/**
		 * Metodo Publico
		 * EditarProvincia($Arreglo = false, $Condicion = false, $Omitidos = false)
		 *
		 * Edicion de datos de registros de provincias
		 * @param boolean $Arreglo   :Datos nuevos
		 * @param boolean $Condicion :Identificador del registro
		 * @param boolean $Omitidos  :Columans omitidas
		 */
		public function EditarProvincia($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Condicion == true AND is_array($Condicion) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_provincia', $Omitidos, APP);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 *
		 * En todo caso elimina un registro del sistema
		 * @param boolean $Arreglo   Nuevo status del registro
		 * @param boolean $Condicion Id del registro a modificar
		 * @param boolean $Omitidos  array de columnas omitidas
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_provincia', $Omitidos, APP);
			}
		}		
	}
?>