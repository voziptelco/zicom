<?php

	class CargaLaboral_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCarteras()
		 * 
		 * Consulta las carteras existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		public function ConsultarCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion('Status != "ELIMINADO"');
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGerencias()
		 * 
		 * Consulta las Gerencias existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		public function ConsultarGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion('Status != "ELIMINADO"');
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultaMaximoSemanas()
		 * 
		 * Consulta el valor maximo en el campo SemanasAtraso
		 * @return El numero maximo 
		 */
		public function ConsultaMaximoSemanasAtraso(){
			$SQL = 'SELECT MAX(SemanaAtraso) FROM tbl_datos_agenda';
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetch();
		}
		
		/**
		 * Metodo Publico
		 * ConsultarProvincias()
		 * 
		 * Consulta las Provincias existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		public function ConsultarProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion('Status != "ELIMINADO"');
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarDistritos()
		 * 
		 * Consulta los distritos existentes
		 * @return Retorno el resultado de la consulta
		 */ 
		public function ConsultarDistritos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion('Status != "ELIMINADO"');
			return $Consulta->Ejecutar(false, true);
		}		
		
		/**
		 * Metodo Publico
		 * ConsultaRegistrosAgenda($Arreglo = false, $IdSupervisor = false)
		 * 
		 * Hace una busqueda de Registros en Agenda, dependiendo de las condiciones
		 * @param $Arreglo: Arreglo de condiciones
		 * @param $IdSupervisor: Id de Supervisor asignado a los registros
		 * @return El Resultado de la consulta
		 */ 
		public function ConsultaTotalRegistroNoAsignados($Arreglo = false, $IdSupervisor = false){
			if($IdSupervisor == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda FROM tbl_asignacion_registros ". 
					   "INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ". 
					   "WHERE tbl_asignacion_registros.Status = 'NOASIGNADO' AND tbl_datos_agenda.IdSupervisor = '".$IdSupervisor."'";
				if(!empty($Arreglo)){
					foreach ($Arreglo as $key => $Condicion) {
						$SQL .= " AND ".$Condicion;
					}
				}
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
					return $Resultado;
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaAgentes($IdUsuario = false)
		 * 
		 * Consulta los Agentes asignados a un Supervisor
		 * 
		 * @param $IdUsuario: Condicion de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultaAgentes($IdUsuario = false){
			if($IdUsuario == true){
				$SQL = "SELECT tbl_agentes_asignado_supervisor.IdAgente, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno ".
   	  				   "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_informacion_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario ".
   	  				   "INNER JOIN tbl_sistema_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario ".
   	  				   "WHERE tbl_sistema_usuarios.Status != 'ELIMINADO' AND tbl_sistema_usuarios.Status = 'ACTIVO' AND tbl_agentes_asignado_supervisor.IdSupervisor = '$IdUsuario'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaRegistrosAsignadosAgentes($IdAgente = false)
		 * 
		 * Consulta los registros asignados a un Agente
		 * @param $IdAgente: Identificador del Agente o criterio de busqueda
		 * @return Resultado de la consulta. numero de registros asignados a un Agente
		 */
		public function ConsultaRegistrosAsignadosAgentesTotal($IdAgente = false){
			if($IdAgente == true){
				$SQL = "SELECT tbl_agente_asignado.IdAgente FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda WHERE tbl_asignacion_registros.Status = 'ASIGNADO' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->rowCount();				
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaRegistrosAsignadosAgentes($IdAgente = false)
		 * 
		 * Consulta los registros asignados a un Agente
		 * @param $IdAgente: Identificador del Agente o criterio de busqueda
		 * @return Resultado de la consulta. numero de registros asignados a un Agente
		 */
		public function ConsultaRegistrosAsignadosAgentes($IdAgente = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($IdAgente == true){
				$Columnas = implode(", ", self::ListarColumnas('tbl_datos_agenda', false, false, APP));
				$SQL = "SELECT ".$Columnas.", tbl_gerencias.Descripcion AS Gerencia, tbl_carteras.Descripcion AS Cartera, tbl_provincia.Descripcion AS Provincia, tbl_distritos.Descripcion AS Distrito FROM tbl_agente_asignado ".
				 "INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
	  			 "INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
	  			 "LEFT JOIN tbl_gerencias ON tbl_datos_agenda.IdGerencia = tbl_gerencias.IdGerencia ".
	  			 "LEFT JOIN tbl_carteras ON tbl_datos_agenda.IdCartera = tbl_carteras.IdCartera ".
	  			 "LEFT JOIN tbl_provincia ON tbl_datos_agenda.IdProvincia = tbl_provincia.IdProvincia ".
	  			 "LEFT JOIN tbl_distritos ON tbl_datos_agenda.IdDistrito = tbl_distritos.IdDistrito ".
			    "WHERE tbl_asignacion_registros.Status = 'ASIGNADO' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente' ";
			    if($Criterio != ""){
					$SQL .= " AND (ClienteUnico LIKE '%$Criterio%' OR NombreTitular LIKE '%$Criterio%' OR SemanaAtraso LIKE '%$Criterio%' OR Saldo LIKE '%$Criterio%' OR SaldoTotal LIKE '%$Criterio%')"; 
				}
				$SQL .= " LIMIT ".$Inicio.", ".$Cantidad." ";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
			}
		}
		
		/**
		 * Metodo Publico
		 * AsignacionDeCargaLaboral($Matriz = false, $Condiciones = false)
		 *
		 * Asigna los registros de clientes a un agente
		 * @param bool $Matriz: Matriz de datos
		 * @param bool $Condiciones: Condicion de asignacion
		 */
		public function AsignacionDeCargaLaboral($Matriz = false, $Condiciones = false){
			if($Matriz == true AND $Condiciones == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_asignacion_registros');
				$TotalMatriz = count($Matriz);
				for($i = 0; $i < $TotalMatriz; $i++){
					$SQL->Sentencia('FechaAsginacion', $Matriz[$i]['FechaAsginacion']);
					$SQL->Sentencia('Status', $Matriz[$i]['Status']);					
					$SQL->Condicion('IdAsignacionRegistro', $Condiciones[$i]['IdAsignacionRegistro']);
					$SQL->Actualizar();
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * AsignarAgenteRegistrios($Matriz = false)
		 * 
		 * Asigna uno o varios agentes a los Registros seleccionados
		 * @param $Matriz: Una matriz de datos a insertar
		 */
		public function AsignarAgenteRegistrios($Matriz = false){
			if($Matriz == true AND is_array($Matriz) == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_agente_asignado');
				foreach($Matriz AS $Arreglo){
					foreach($Arreglo as $Campo => $Valor){
						$SQL->Sentencia($Campo, $Valor);
					}
					$SQL->Insertar();
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaLlavesdeRegistrosNoAsignados($Matriz = false)
		 * 
		 * Intercambia el IdDatoAgenda por las llaves principales de la tabla
		 * @param $Matriz: Matriz con los Ids o datos a buscar.
		 * @return El resultado de la consulta
		 */
		public function ConsultaLlavesdeRegistrosNoAsignados($MatrizLlaves = false){
			if($MatrizLlaves == true AND is_array($MatrizLlaves) == true){	
				foreach($MatrizLlaves AS $Arreglo){
					foreach($Arreglo AS $Columna => $Valor){
						$Matriz[]['IdAsignacionRegistro'] = self::ConsultaLlave($Columna, $Valor);
					}
				}
				return $Matriz;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaLlave($Columna, $Valor)
		 * 
		 * Consulta un dato, mediande el valor de busqueda
		 * @param $Columna: Campo en el que se realiza la busqueda
		 * @param $Valor: Datos que se quiere buscar
		 * @return Resultado indice del registro
		 */
		private function  ConsultaLlave($Columna = false, $Valor = false){
			if($Columna == true AND $Valor == true){						
			    $SQL = 'SELECT IdAsignacionRegistro FROM tbl_asignacion_registros WHERE '.$Columna.'='.$Valor;
  				$Consulta = $this->Conexion->prepare($SQL);
  				$Consulta->execute();
 		    	$Resultado = $Consulta->fetch(PDO::FETCH_ASSOC);
 				return $Resultado['IdAsignacionRegistro'];
			}			
		}
		
			/**
		 * Metodo Publico 
		 * ConsultaCantidadRegistros($Arreglo = false)
		 * 
		 * Consulta el numero de registro asignados a cada Agente
		 * @param $Arreglo: Arreglo con los datos de los agentes
		 * @return $Arregoo: Arreglo con la nueva columna (catidad)
		 */
		public function ConsultaCantidadRegistros($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				for($i = 0; $i < count($Arreglo); $i++){
					if($Arreglo[$i]['IdAgente']){
						$Consulta = self::ConsultaRegistrosAsignadosAgentesCantidad($Arreglo[$i]['IdAgente']);
						$Arreglo[$i]['Cantidad'] = $Consulta['Cantidad'];
					}
				}
				return $Arreglo;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaRegistrosAsignadosAgentes($IdAgente = false)
		 * 
		 * Consulta el numero de registros asignados a un Agente
		 * @param $IdAgente: Identificador del Agente o criterio de busqueda
		 * @return Resultado de la consulta. numero de registros asignados a un Agente
		 */
		private function ConsultaRegistrosAsignadosAgentesCantidad($IdAgente = false){
				$SQL = "SELECT tbl_agente_asignado.IdAgente FROM tbl_agente_asignado ".
					   "INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ". 
					   "INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					   "WHERE tbl_asignacion_registros.Status = 'ASIGNADO' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND  tbl_agente_asignado.IdAgente = '$IdAgente'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
		}
	}