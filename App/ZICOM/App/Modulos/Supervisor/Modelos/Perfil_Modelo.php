<?php

	class Perfil_Modelo extends AppSQLConsultas {

		function __Construct(){
			parent::__Construct();
		}

		public function BuscarPassword($IdUsuario = false, $Password = false){
			if($IdUsuario == true AND $Password == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas('Usuario');
				$Consulta->Condicion("IdUsuario = '$IdUsuario'");
				$Consulta->Condicion("Password = '$Password'");
				return $Consulta->Ejecutar(true, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarImagen($Id = false)
		 * 
		 * Consulta en bd la imagen del usuario
		 * @param $Id:	identificador del usuario
		 * @return Retorna una matriz con los datos de la imagen
		 */
		public function ConsultarImagen($Id = false){
			if($Id == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_imagen_usuario');
				$Consulta->Columnas('Imagen, Tipo');
				$Consulta->Condicion("IdUsuario = '$Id'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico 
		 * GuardarImagen($Arreglo = false)
		 * 
		 * Guarda en BD una imagen de perfil del usuario
		 * @param $Arrelo:	Arreglo con los datos de la imagen
		 */
		public function GuardarImagen($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				$SQL = new NeuralBDGab(APP, 'tbl_imagen_usuario');
				foreach($Arreglo as $Columna => $Valor){
					$SQL->Sentencia($Columna, $Valor);	
				}
			return 	$SQL->Insertar();
			}
		}

		public function ActualizarPassword($IdUsuario = false, $Password = false){
			if($IdUsuario == true AND $Password == true){
				$SQL = new NeuralBDGab(APP, 'tbl_sistema_usuarios');
				$SQL->Sentencia('Password', $Password);
				$SQL->Condicion('IdUsuario', $IdUsuario);
				$SQL->Actualizar();
			}
		}

		/**
		 * Metodo Publico 
		 * ActualizarDatosUsuario($IdUsuario = false, $Datos = false)
		 * 
		 * @param boolean $IdUsuario Identificador del usuario
		 * @param boolean $Datos     Nuevos datos
		 */
		public function ActualizarDatosUsuario($IdUsuario = false, $Datos = false){
			if($IdUsuario == true AND $Datos == true){
				$SQL = new NeuralBDGab(APP, 'tbl_informacion_usuarios');
				foreach ($Datos as $key => $value) {
					if(!empty($value)){
						$SQL->Sentencia($key, $value);
					}
					$SQL->Condicion('IdUsuario', $IdUsuario);
				}
				$SQL->Actualizar();	
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarFoto($Imagen = false, $Condicion = false)
		 * 
		 * Actualiza la imagen de perfil del usuario
		 * @param $Imagen:	array de datos de la imagen 
		 * @param $Condicion: Identificador (id de usuario)
		 */
		public function ActualizarFoto($Imagen = false, $Condicion = false){
			if($Imagen == true and $Condicion == true and is_array($Imagen) == true){
				$SQL = new NeuralBDGab(APP, 'tbl_imagen_usuario');
				$SQL->Sentencia('Imagen', $Imagen['Imagen']);
				$SQL->Sentencia('Tipo', $Imagen['Tipo']);
				$SQL->Condicion('IdUsuario', $Condicion);
				$SQL->Actualizar();
			}
		}
	}