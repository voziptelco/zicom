<?php

	class CodigosGestion_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCodigos()
		 * 
		 * Consulta lso codigos de gestion disponibles
		 */
		public function ConsultarCodigos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_codigos');
			$Consulta->Columnas('IdCodigo, Codigo, Descripcion, Color');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarCodigoExistente($Codigo = false, $Descripcion = false)
		 * 
		 * Consulta el numero de repeticiones de un registro.
		 * @param $Codigo: Numero codigo
		 * @param $Descripcion
		 * @return Si exite retorna total y consulta.
		 */
		public function ConsultarCodigoExistente($Codigo = false, $Descripcion = false){
			if($Codigo == true AND $Descripcion == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_codigos');
				$Consulta->Columnas('IdCodigo, Codigo, Descripcion');
				$Consulta->Condicion("Status != 'ELIMINADO'");
				$Consulta->Condicion("Codigo = '$Codigo'");
				$Consulta->Condicion("Descripcion = '$Descripcion'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarCodigo($IdCodigo = false)
		 * 
		 * Consulta un registro especifico
		 * @param $IdCodigo: identificador
		 */		
		public function ConsultarCodigo($IdCodigo = false){
			if($IdCodigo == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_codigos');
				$Consulta->Columnas('Codigo, Descripcion, Color');
				$Consulta->Condicion("Status != 'ELIMINADO'");
				$Consulta->Condicion("IdCodigo = '$IdCodigo'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarCodigo($Datos = false)
		 * 
		 * Guarda los datos
		 * @param $Datos: arreglo de datos 
		 */
		public function GuardarCodigo($Datos = false){
			if($Datos == true AND is_array($Datos) == true){
				return self::GuardarDatos($Datos, 'tbl_codigos', array('IdCodigo', 'Status'), APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * EditarCodigo($IdCodigo = false, $Datos = false, $Omitidos = false)
		 * 
		 * Edicion de datos
		 * @param $IdCodigo: identificador 
		 * @param $Datos: arreglo de datos
		 * @param $Omitidos: datos omitidos
		 */
		public function EditarCodigo($IdCodigo = false, $Datos = false, $Omitidos = false){
			if($IdCodigo == true AND $Datos == true AND $Omitidos == true){
				return self::ActualizarDatos($Datos, $IdCodigo, 'tbl_codigos', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * Eliminar($IdCodigo = false, $Datos = false, $Omitidos = false)
		 * 
		 * Desactivacion de registros
		 * @param $Condiciones: identificador 
		 * @param $Datos: arreglo de datos
		 * @param $Omitidos: datos omitidos
		 */
		public function Eliminar($Arreglo = false, $Condiciones = false, $Omitidos = false)	{
			if($Arreglo == true AND is_array($Arreglo) AND $Condiciones == true  AND is_array($Arreglo) AND $Omitidos == true AND is_array($Omitidos)){
				Ayudas::print_r($Arreglo);
				return self::ActualizarDatos($Arreglo, $Condiciones, 'tbl_codigos', $Omitidos, APP);
			}
		}
	}