<?php
	
	class Cliente_Modelo extends AppSQLConsultas{
		
		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
		}
		/**
		 * Metod Público 
		 * ConsultarClientes($IdSupervisor = false)
		 * 
		 * Consulta los clientes que esten Asignados a un determinado Supervisor
		 * @param $IdSupervisor: Identificador del supervisor
		 * @return Resultado en una matriz, de la consulta
		 */
		public function ConsultarClientes($IdSupervisor = false){
			if($IdSupervisor == True){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_clientes_asignados_supervisor');
				$Consulta->Columnas('tbl_clientes_asignados_supervisor.IdCliente, tbl_sistema_usuarios.Usuario, tbl_sistema_usuarios.Status, tbl_informacion_usuarios.RUC, tbl_informacion_usuarios.RazonSocial, tbl_informacion_usuarios.Correo');
				$Consulta->InnerJoin('tbl_sistema_usuarios', 'tbl_clientes_asignados_supervisor.IdCliente', 'tbl_sistema_usuarios.IdUsuario');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_clientes_asignados_supervisor.IdCliente', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_clientes_asignados_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaUnCliente($IdCliente = false)
		 * 
		 * Consulta los datos de un Cliente espicífico
		 * @param $IdCliente: Id del Cliente a consultar
		 * @return Consulta con datos del cliente
		 */
		Public function ConsultaUnCliente($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_sistema_usuarios', false, false, APP), self::ListarColumnas('tbl_informacion_usuarios', array('idUsuario', 'Status'), false, APP)));
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.IdUsuario = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaProvincias()
		 * 
		 * Consulta las Provincias disponibles
		 * @return una matriz de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultaUsuarioExistente($Usuario = false, $Correo = true)
		 * 
		 * Cunsulta si un Registro ya existe en la Base de datos
		 * @param $Usuario: Nombre de usuario a buscar
		 * @param $Correo: Correo del usuario a buscar
		 * @return Devulve el numero de veces que se repite el registro
		 */
		public function ConsultaUsuarioExistente($Correo = true, $RazonSocial = false){
			if( $Correo == true AND $RazonSocial == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_informacion_usuarios.Correo = '$Correo' OR tbl_informacion_usuarios.RazonSocial = '$RazonSocial'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarEliminado($Correo = false, $RazonSocial = false)
		 * 
		 * Consulta el estatus de un registro
		 */
		public function ConsultarEliminado($Correo = false, $RazonSocial = false){
			if($RazonSocial == true AND $Correo == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas('tbl_sistema_usuarios.IdUsuario');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("(tbl_informacion_usuarios.Correo = '$Correo' OR tbl_informacion_usuarios.RazonSocial = '$RazonSocial') AND tbl_sistema_usuarios.Status = 'ELIMINADO'");
				return $Consulta->Ejecutar(true, true);
			}			
		}
		
		/**
		 * Metodo Publico
		 * ConsultaUsuarioExitenteEditar($IdUsuario = false, $Correo = false, $RUC = false, $RazonSocial = false)
		 * 
		 * Cunsulta si un Registro ya existe en la Base de datos
		 * @param $Usuario: Nombre de usuario a buscar
		 * @param $Correo: Correo del usuario a buscar
		 * @return Devulve el numero de veces que se repite el registro
		 */
		public function ConsultaUsuarioExitenteEditar($IdUsuario = false, $Correo = false, $RazonSocial = false){
			if($Correo == true AND $RazonSocial == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.idUsuario != '$IdUsuario' AND (tbl_informacion_usuarios.Correo = '$Correo' OR tbl_informacion_usuarios.RazonSocial = '$RazonSocial')");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarIdUsuario($Usuario = false)
		 * 
		 * Busca el Id del nuevo usuario insertado
		 * @param $Usuario: Nombre del usuario a consultar
		 * @return El Id del usuario
		 */
		public function BuscarIdUsuario($Usuario = false){
			if($Usuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas('IdUsuario');
				$Consulta->Condicion("Usuario = '$Usuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
	
		/**
		 * Metodo Publico
		 * BuscarIdInformacionUsuario($IdUsuario = false)
		 * 
		 * Consulta la llave del registro que contiene la informacion 
		 * del usuario que se busca
		 * @param $IdUsuario: condición de Búsqueda
		 * @return Resultado de la consulta
		 */
		public function BuscarIdInformacionUsuario($IdUsuario = false){
			if($IdUsuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_informacion_usuarios');
				$Consulta->Columnas('IdInformacion');
				$Consulta->Condicion("idUsuario = '$IdUsuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * BuscarPerfil($NombrePerfil = false)
		 * 
		 * Consulta los permisos del perfil
		 * @param $NombrePerfil: Perfial a buscar
		 * @return Resultado de la consulta
		 */
		public function BuscarPerfil($NombrePerfil = false){
			if($NombrePerfil == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios_perfil');
				$Consulta->Columnas('IdPerfil');
				$Consulta->Condicion("Nombre = '$NombrePerfil'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarDatosCliente($Arreglo = false)
		 * 
		 * Inserta un nuevo registro en la tabla sistema_usuarios
		 * @param $Arreglo: Arreglo de datos
		 * @return El resultado de la consulta
		 */
		public function GuardarDatosCliente($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_sistema_usuarios', array('IdUsuario'), APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarInformacionCliente($Arreglo = false)
		 * 
		 * Inserta un nuevo registro en la tabla informacion_usuarios
		 * @param $Arreglo: Arreglo de datos
		 * @return El resultado de la consulta
		 */
		public function GuardarInformacionCliente($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_informacion_usuarios', array('IdInformacion', 'TelefonoCasa2', 'TelefonoMovil2', 'Extension', 'callerid','TipoCanal', 'ExportarExcel', 'Status'), APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarAgenteAsignado($Arreglo = false)
		 * 
		 * Asigna aun Supervisor el nuevo Cliente
		 */
		public function GuardarClienteAsignado($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_clientes_asignados_supervisor', array('IdClienteAsignado'), APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizaInformacionAgente($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Modifica los datos del Cliente
		 * @param $Arreglo: Arreglo de datos
		 * @param $Condicion: Indice de condicion 
		 * @param $Omitidos: columnas omitidas
		 * @return Resultado de la Actualización
		 */
		public function ActualizaInformacionCliente($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_informacion_usuarios', $Omitidos, APP);	
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarUsuarioCliente($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actializa los datos de usuario de sistema
 		 * @param $Arreglo: Arreglo de datos
		 * @param $Condicion: Indice de condicion 
		 * @param $Omitidos: columnas omitidas
		 * @return Resultado de la Actualización  
		 */
		public function ActualizarUsuarioCliente($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_sistema_usuarios', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Modifica el estado de un Regitro a ELIMINADo
		 * @param $Arreglo: Nuevo estado
		 * @param $Condicion: Indice de Busqueda
		 * @param $Omitidos: Campos que se omiten
		 * @return Resultado de la Actualizacion
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_sistema_usuarios', $Omitidos, APP);
			}
		}
	}