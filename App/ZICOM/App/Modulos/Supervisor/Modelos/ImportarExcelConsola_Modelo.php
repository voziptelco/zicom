<?php

	class ImportarExcelConsola_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaTareasProgramadas($Fecha = false)
		 * 
		 * Consulta las tarea del dia actual
		 * @param $Fecha de busqueda
		 */
		public function ConsultaTareasProgramadas($Fecha = false){
			if($Fecha == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla("tbl_archivos_programados");
				$Consulta->Columnas(self::ListarColumnas('tbl_archivos_programados', false, false, APP));
				$Consulta->Condicion("FechaHoraCarga LIKE '$Fecha%'");
				$Consulta->Condicion("Status = 'ENPROCESO'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo publico 
		 * ConsultaNumeroErroresLog($IdTarea = false)
		 * 
		 * consulta el numero de errores del proceso de carga
		 */
		public function ConsultaNumeroErroresLog($IdTarea = false){
			if($IdTarea == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla("tbl_log_registro");
				$Consulta->Columnas('IdLogRegistro, NumeroRegistro');
				$Consulta->Agrupar("NumeroRegistro");
				$Consulta->Condicion("StatusRegistro = 'ERROR'");
				$Consulta->Condicion("Idtarea = '$IdTarea'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscaIndice($Columna = false, $Valor = false)
		 *
		 * Consulta el Id de un registro
		 * @param $Columna: Columna en cual buscar
		 * @param $Valor: Es la condición para buscar
		 * @return El resultado de la consulta
		 */
		private function BuscaIndice($Columna = false, $Valor = false){
			if($Columna == true AND $Valor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas('IdDatoAgenda');
				$Consulta->Condicion("$Columna = '$Valor'");
				return $Consulta->Ejecutar(false, true);
			}
		}
	}
