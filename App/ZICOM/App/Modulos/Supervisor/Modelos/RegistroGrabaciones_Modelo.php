<?php
	
	class RegistroGrabaciones_Modelo extends Modelo{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta todos los Agentes asignados a un Supervisor dado
		 * @param $IdSupervisor: Condición de búsqueda
		 * @return Arreglo de Agentes
		 */
		public function ConsultarAgentes($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor, tbl_sistema_usuarios, tbl_informacion_usuarios');
				$Consulta->Columnas('tbl_agentes_asignado_supervisor.IdAgente, tbl_sistema_usuarios.Usuario, tbl_sistema_usuarios.Status, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno');
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario");
				$Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGrabaciones($ArregloCondiciones = false, $IdSupervisor = false)
		 * 
		 * Consulta las grabaciones disponibles para determinado supervisor
		 * @param $ArregloCondiciones: Condiciones de busqueda
		 * @param $IdSupervisor: identificador del supervisor
		 */
		public function ConsultarGrabacionesTotal($ArregloCondiciones = false, $IdSupervisor = false){
			if($IdSupervisor == true){
				$SQL = "SELECT tbl_cdr.uniqueid ".
                       "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_informacion_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario ".
                       "INNER JOIN tbl_gestion_telefonica ON tbl_agentes_asignado_supervisor.IdAgente = tbl_gestion_telefonica.IdAgente ".
                       "INNER JOIN tbl_cdr ON tbl_gestion_telefonica.uniqueid = tbl_cdr.uniqueid ".
                       "INNER JOIN tbl_datos_agenda ON tbl_gestion_telefonica.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
                       "WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'";
				if($ArregloCondiciones == true AND is_array($ArregloCondiciones) == true AND count($ArregloCondiciones) > 0){
					foreach($ArregloCondiciones AS $Condicion){
						$SQL = $SQL." AND ".$Condicion;  
					}	
				}
				$SQL .= " ORDER BY tbl_gestion_telefonica.FechaHora_Captura DESC";
			    $Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarGrabaciones($ArregloCondiciones = false, $IdSupervisor = false)
		 * 
		 * Consulta las grabaciones disponibles para determinado supervisor
		 * @param $ArregloCondiciones: Condiciones de busqueda
		 * @param $IdSupervisor: identificador del supervisor
		 */
		public function ConsultarGrabaciones($ArregloCondiciones = false, $IdSupervisor = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($IdSupervisor == true){
				$SQL = "SELECT CONCAT(tbl_informacion_usuarios.Nombres,' ',tbl_informacion_usuarios.ApellidoPaterno) as Nombres, tbl_gestion_telefonica.NumeroTelefono, tbl_gestion_telefonica.FechaHora_Captura, tbl_gestion_telefonica.Grabacion, tbl_datos_agenda.NombreTitular, tbl_cdr.duration, tbl_cdr.uniqueid ".
                       "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_informacion_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario ".
                       "INNER JOIN tbl_gestion_telefonica ON tbl_agentes_asignado_supervisor.IdAgente = tbl_gestion_telefonica.IdAgente ".
                       "INNER JOIN tbl_cdr ON tbl_gestion_telefonica.uniqueid = tbl_cdr.uniqueid ".
                       "INNER JOIN tbl_datos_agenda ON tbl_gestion_telefonica.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
                       "WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'";
				if($ArregloCondiciones == true AND is_array($ArregloCondiciones) == true AND count($ArregloCondiciones) > 0){
					foreach($ArregloCondiciones AS $Condicion){
						$SQL = $SQL." AND ".$Condicion;  
					}	
				}
				$SQL .= " AND (tbl_informacion_usuarios.Nombres LIKE '%$Criterio%' OR tbl_gestion_telefonica.NumeroTelefono LIKE '%$Criterio%' OR tbl_gestion_telefonica.FechaHora_Captura LIKE '%$Criterio%' OR tbl_datos_agenda.NombreTitular LIKE '%$Criterio%' OR tbl_cdr.duration LIKE '%$Criterio%') ORDER BY tbl_gestion_telefonica.FechaHora_Captura DESC";
				if($Cantidad != -1){ 
					$SQL .=" LIMIT $Inicio, $Cantidad "; 
				}
			    $Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
			}
		}
	}