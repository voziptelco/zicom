<?php
	
	class FiltradoClientes_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo publico
		 * ConsultaGerencias()
		 * 
		 * Consulta todas la gerencias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 * 
		 * Consulta todas la Carteras
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 * 
		 * Consulta todos las Provincias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaDistritos()
		 * 
		 * Consulta todos los Distritos
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaDistritos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico 
		 * ConsultaCodigos()
		 * 
		 * Consulta los codigos de gestion 
		 */
		public function ConsultarCodigos(){
			$SQL = "SELECT IdCodigo, Codigo, Descripcion, Color FROM tbl_codigos WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaClientes()
		 * 
		 * Consulta los clientes asignados a el supervisor de la sesion
		 * @param $IdSupervisor: Condicon de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultaClientes($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_clientes_asignados_supervisor');
				$Consulta->Columnas('tbl_informacion_usuarios.idUsuario as IdCliente, tbl_informacion_usuarios.RazonSocial');
				$Consulta->InnerJoin('tbl_sistema_usuarios', 'tbl_clientes_asignados_supervisor.IdCliente', 'tbl_sistema_usuarios.IdUsuario');
				$Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_sistema_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
				$Consulta->Condicion("tbl_clientes_asignados_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre = 'Cliente'");
				return $Consulta->Ejecutar(false, true);	
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta todos los Agentes asignados a un Supervisor dado
		 * @param $IdSupervisor: Condición de búsqueda
		 * @return Arreglo de Agentes
		 */
		public function ConsultarAgentes($IdSupervisor = false){
			if($IdSupervisor == True){
				$SQL = "SELECT tbl_agentes_asignado_supervisor.IdAgente, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno ".
   	  				   "FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_informacion_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario ".
   	  				   "INNER JOIN tbl_sistema_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario ".
   	  				   "WHERE tbl_sistema_usuarios.Status != 'ELIMINADO' AND tbl_sistema_usuarios.Status = 'ACTIVO' AND tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaCompromiso($ArregloCriterios = false, $IdSupervisor = false)
		 * 
		 * Consulta los registros con compromiso de pago, con distintos criterios
		 * @param $ArreloCriterios: Arreglo de criterios
		 * @param $IdSupervisor: Indice de agente asignado a registros.
		 * @return Matriz de consulta.
		 */
		public function ConsultaCompromiso($ArregloCriterios = false, $IdSupervisor = false, $Tabla = false, $IdGestion = false, $IdAgente = false, $CondicionesExtra = false){
			if($IdSupervisor == true and $Tabla == true){
                $SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.IdCliente, tbl_datos_agenda.IdGerencia, tbl_datos_agenda.IdCartera, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.DomicilioContractual, tbl_datos_agenda.IdProvincia, tbl_datos_agenda.IdDistrito, tbl_datos_agenda.NombreAval, tbl_datos_agenda.DistritoAval, tbl_datos_agenda.DomicilioAval, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal, tbl_datos_agenda.Telefono1, tbl_datos_agenda.Telefono2, tbl_datos_agenda.Telefono3, tbl_datos_agenda.Telefono4, tbl_datos_agenda.StatusCobro, tbl_codigos.Color, tbl_codigos.Codigo, MAX($Tabla.FechaHora_Captura) AS FechaHora_Captura ".
                       "FROM tbl_datos_agenda INNER JOIN $Tabla  ON tbl_datos_agenda.IdDatoAgenda = $Tabla.IdDatoAgenda ".
                       "INNER JOIN tbl_asignacion_registros ON tbl_datos_agenda.IdDatoAgenda = tbl_asignacion_registros.IdDatoAgenda ".
                       "LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					   "INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
                       "INNER JOIN tbl_agentes_asignado_supervisor ON tbl_agente_asignado.IdAgente = tbl_agentes_asignado_supervisor.IdAgente ";
                if(isset($CondicionesExtra['FechasCompromiso'])){
                	$SQL .= "INNER JOIN tbl_fecha_compromiso ON $Tabla.$IdGestion = tbl_fecha_compromiso.IdGestion ";
               	}
               	$SQL .="WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_asignacion_registros.Status = 'ASIGNADO' AND tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor' ";
               	if(!(empty($IdAgente))){
               		$SQL .= "AND tbl_agente_asignado.IdAgente = '$IdAgente'";
				}
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
					foreach($ArregloCriterios AS $Condicion){
						$SQL .= " AND ".$Condicion;
					}	
				}
				if(isset($CondicionesExtra['FechasGestion']))
					$SQL .= " AND ". $CondicionesExtra['FechasGestion'];
				$SQL .= " GROUP BY tbl_datos_agenda.NombreTitular ORDER BY FechaHora_Captura DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);	
			}			
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaRegistros($ArregloCriterios = false, $IdSupervisor = false)
		 * 
		 * Consulta los registros que cumplen con los criterios de busqueda
		 * @param $ArregloCriterios: Arreglo con las condiciones de busqueda
		 * @param $IdSupervisor: Id del supervisor de los registro a buscar
		 * @return Matriz Resultado de la consulta
		 */
		public function ConsultaRegistros($ArregloCriterios = false, $IdSupervisor = false, $IdAgente = false){
			if($IdSupervisor == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.IdCliente, tbl_datos_agenda.IdGerencia, tbl_datos_agenda.IdCartera, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.DomicilioContractual, tbl_datos_agenda.IdProvincia, tbl_datos_agenda.IdDistrito, tbl_datos_agenda.NombreAval, tbl_datos_agenda.DistritoAval, tbl_datos_agenda.DomicilioAval, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal, tbl_datos_agenda.Telefono1, tbl_datos_agenda.Telefono2, tbl_datos_agenda.Telefono3, tbl_datos_agenda.Telefono4, tbl_datos_agenda.StatusCobro, tbl_codigos.Color, tbl_codigos.Codigo ".
                       "FROM tbl_asignacion_registros INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
                       "INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
                       "LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					   "INNER JOIN tbl_agentes_asignado_supervisor ON tbl_agente_asignado.IdAgente = tbl_agentes_asignado_supervisor.IdAgente ".
                       "WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO'";
    			if(!(empty($IdAgente))){
					$SQL .= " AND tbl_agente_asignado.IdAgente = '$IdAgente' ";
				}
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
					foreach($ArregloCriterios AS $Condicion){
						$SQL .= " AND ".$Condicion;
					}	
				}
				$SQL .= " GROUP BY NombreTitular";   
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarRegistrosTotal($IdSupervisor = false)
		 * 
		 * consulta todos los registros asociados al supervisor
		 * @param $IdSupervisor: identificador.
		 */
		public function ConsultarRegistrosTotal($IdSupervisor = false){
			if($IdSupervisor == true){
				 	$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.IdCliente, tbl_datos_agenda.IdGerencia, tbl_datos_agenda.IdCartera, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.DomicilioContractual, tbl_datos_agenda.IdProvincia, tbl_datos_agenda.IdDistrito, tbl_datos_agenda.NombreAval, tbl_datos_agenda.DistritoAval, tbl_datos_agenda.DomicilioAval, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal, tbl_datos_agenda.Telefono1, tbl_datos_agenda.Telefono2, tbl_datos_agenda.Telefono3, tbl_datos_agenda.Telefono4, tbl_codigos.Color, tbl_codigos.Codigo ".
                       "FROM tbl_asignacion_registros INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
                       "INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
                       "LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					   "INNER JOIN tbl_agentes_asignado_supervisor ON tbl_agente_asignado.IdAgente = tbl_agentes_asignado_supervisor.IdAgente ".
                       "WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor' AND tbl_datos_agenda.StatusRegistro != 'ELIMINADO'";
				$SQL .= " GROUP BY NombreTitular"; 
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);			
			}
		}
		
			/**
		 * Metodo Publico 
		 * ConsultaCorreo($IdSupervisor = false)
		 * 
		 * Consulta el correo de un usuario
		 * @param $IdSupervisor: identificador
		 */
		public function ConsultaCorreo($IdSupervisor = false){
			if($IdSupervisor == true){
				$SQL = "SELECT Correo FROM tbl_informacion_usuarios WHERE idUsuario = $IdSupervisor";
				$Consulta = $this->Conexion->prepare($SQL);
	   			$Consulta->execute();
	   			return $Consulta->fetch(PDO::FETCH_ASSOC);
			}
		}	
	}