<?php

	class LimiteLlamada_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarConfiguracionTeimpoLimite($IdSupervisor = false)
		 * 
		 * Consultar Datos de la tabla tiempo_llamada, para un supervisor dado
		 * @param $IdSupervisor: Identificador del supervisor
		 * @return Matriz de consulta.
		 */
		public function ConsultarConfiguracionTeimpoLimite($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_tiempo_llamada');
				$Consulta->Columnas('IdTiempoLlamada, IdSupervisor, TiempoLlamada');
				$Consulta->Condicion("IdSupervisor = '$IdSupervisor'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarTiempo($Arreglo = false)
		 * 
		 * Guardar datos por primara vez
		 * @param $Arreglo: Datos para la tabla
		 */
		public function GuardarTiempo($Arreglo = false){
			if($Arreglo == true and is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_tiempo_llamada', array('IdTiempoLlamada'), APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarTeimpo($Datos = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualiza el tiempo limite de llamada
		 * @param $Datos: Datos a modificar
		 * @param $Condicion: Identificador del registro a modificar
		 * @param $Omitidos: Datos que no se requiren modificar
		 */
		public function ActualizarTeimpo($Datos = false, $Condicion = false, $Omitidos = false){
			if($Datos == true and is_array($Datos) and $Condicion == true and is_array($Condicion) and $Omitidos == true and is_array($Omitidos)){
				return self::ActualizarDatos($Datos, $Condicion, 'tbl_tiempo_llamada', $Omitidos, APP);	
			}
		}
	}