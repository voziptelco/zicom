<?php
	
	class ImportarRegistros_Modelo extends AppSQLConsultas{
		
		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico
		 * GuardarInformacionArchivo($ArrgloDataos = false)
		 * 
		 * Guarda en bd la informacion de la programacion de la carga de un archivo
		 * @param boolean $ArrgloDataos Datos generales del la carga
		 */
		public function GuardarInformacionArchivo($ArrgloDataos = false){
			if($ArrgloDataos == true and is_array($ArrgloDataos)){
				return self::GuardarDatos($ArrgloDataos, 'tbl_archivos_programados', array('IdArchivosProgramados', 'Status', 'ArchivoLog', 'FechaHoraTerminado'), APP);
			}
		}
	}