<?php

	class MonitorAgentes_Modelo extends AppSQLConsultas{

		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta todos los Agentes asignados a un Supervisor dado
		 * @param $IdSupervisor: Condición de búsqueda
		 * @return Arreglo de Agentes
		 */
		public function ConsultarAgentes($IdSupervisor = false){
			if($IdSupervisor == True){
				$SQL = "SELECT tbl_agentes_asignado_supervisor.IdAgente, tbl_sistema_usuarios.Usuario, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno, CONCAT(tbl_informacion_usuarios.TipoCanal, '/',  tbl_informacion_usuarios.Extension) AS Peer ".
						"FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_sistema_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario ".
						"INNER JOIN tbl_informacion_usuarios ON tbl_sistema_usuarios.IdUsuario = tbl_informacion_usuarios.idUsuario ".
						"WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor' AND tbl_sistema_usuarios.Status != 'ELIMINADO'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico 
		 * ConsultarAgentesId($IdSupervisor = false)
		 * 
		 * Consulta todos los Agentes asignados a un Supervisor dado
		 * @param $IdSupervisor: Condición de búsqueda
		 * @return Arreglo de Agentes
		 */
		public function ConsultarAgentesId($IdSupervisor = false){
			if($IdSupervisor == True){
				$SQL = "SELECT tbl_agentes_asignado_supervisor.IdAgente, tbl_sistema_usuarios.Usuario, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno ".
						"FROM tbl_agentes_asignado_supervisor INNER JOIN tbl_sistema_usuarios ON tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario ".
						"INNER JOIN tbl_informacion_usuarios ON tbl_sistema_usuarios.IdUsuario = tbl_informacion_usuarios.idUsuario ".
						"WHERE tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor' AND tbl_sistema_usuarios.Status != 'ELIMINADO'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarAgente($IdAgente = false)
		 *
		 * Consulta la informacion del agente
		 * @param boolean $IdAgente identificador del agente
		 */
		public function ConsultarAgente($IdAgente = false){
			if ($IdAgente == true) {
				$SQL = "SELECT TipoCanal, Extension, CONCAT(Nombres, ' ', ApellidoPaterno) AS Nombres FROM tbl_informacion_usuarios WHERE idUsuario = $IdAgente";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC); 
			}
		}

		/**
		 * Metodo Publico 
		 * ConsultarConfiguracionConmutador()
		 * 
		 * Consulta los datos de Configuracion del Conmutador
		 * @return Matriz de datos de consulta
		 */
		public function ConsultarConfiguracionConmutador(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_configuracion_conmutador');
			$Consulta->Columnas(self::ListarColumnas('tbl_configuracion_conmutador', array('idConfiguracion'), false, APP));
			return $Consulta->Ejecutar(false, true);
		}
	}