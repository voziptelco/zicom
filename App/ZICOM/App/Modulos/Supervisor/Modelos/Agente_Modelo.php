<?php

	class Agente_Modelo extends AppSQLConsultas{

		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta todos los Agentes asignados a un Supervisor dado
		 * @param $IdSupervisor: Condición de búsqueda
		 * @return Arreglo de Agentes
		 */
		public function ConsultarAgentes($IdSupervisor = false){
			if($IdSupervisor == True){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor, tbl_sistema_usuarios, tbl_informacion_usuarios');
				$Consulta->Columnas('tbl_agentes_asignado_supervisor.IdAgente, tbl_sistema_usuarios.Usuario, tbl_sistema_usuarios.Status, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno');
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario");
				$Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
				$Consulta->Agrupar('tbl_informacion_usuarios.Nombres');
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaProvincias()
		 * 
		 * Consulta las Provincias disponibles
		 * @return una matriz de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaUsuarioExistente($Usuario = false, $Correo = true)
		 * 
		 * Consulta si un Agente ya existe en la base de datos
		 * @param $Usuario: Nombre usuario ingresado
		 * @param $Correo: Correo de usuario ingresado
		 * @return Catidad de registros repetidos
		 */
		public function ConsultaUsuarioExistente($Usuario = false, $Correo = true){
			if($Usuario == true AND $Correo == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.Usuario = '$Usuario' OR tbl_informacion_usuarios.Correo = '$Correo'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarUnAgente($IdAgente = false)
		 * 
		 * Consulta los datos de un Agente específico
		 * @param $IdAgente: Id del agente a consultar
		 * @return Datos del Agente consultado
		 */
		public function ConsultarUnAgente($IdAgente = false){
			if ($IdAgente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_sistema_usuarios', false, false, APP), self::ListarColumnas('tbl_informacion_usuarios', array('idUsuario', 'Status'), false, APP)));
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.idUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.IdUsuario = '$IdAgente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarIdUsuario($Usuario = false)
		 * 
		 * Consulta el ID de un Usuario Específico
		 * @param $Usuario: Nombre del Usuario a buscar
		 * @return Resulta de la consulta
		 */
		public function BuscarIdUsuario($Usuario = false){
			if($Usuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios');
				$Consulta->Columnas('IdUsuario');
				$Consulta->Condicion("Usuario = '$Usuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * BuscarPerfil($NombrePerfil = false)
		 * 
		 * Busca el Id del perfil especificado
		 * @param $NombrePerfil: Condición de búsqueda
		 * @return Resultado de la búsqueda
		 */
		public function BuscarPerfil($NombrePerfil = false){
			if($NombrePerfil == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_sistema_usuarios_perfil');
				$Consulta->Columnas('IdPerfil');
				$Consulta->Condicion("Nombre = '$NombrePerfil'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		/**
		 * Metodo Publico 
		 * BuscarIdInformacionUsuario($IdUsuario = false)
		 * 
		 * Busca el ID del Registro que contiene la informacion del Agente
		 * @param $IdUsuario: Candicion de busqueda
		 * @return El ID del Registro
		 */
		public function BuscarIdInformacionUsuario($IdUsuario = false){
			if($IdUsuario == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_informacion_usuarios');
				$Consulta->Columnas('IdInformacion');
				$Consulta->Condicion("idUsuario = '$IdUsuario'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarImagen($Id = false)
		 * 
		 * Consulta en bd la imagen del usuario
		 * @param $Id:	identificador del usuario
		 * @return Retorna una matriz con los datos de la imagen
		 */
		public function ConsultarImagen($Id = false){
			if($Id == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_imagen_usuario');
				$Consulta->Columnas('Imagen, Tipo');
				$Consulta->Condicion("IdUsuario = '$Id'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarDatosAgente($Arreglo = false)
		 * 
		 * Inserta los datos de sistema 
		 * @param $Arreglo: Arreglo de datos
		 * @return Resultado de la consulta
		 */
		public function GuardarDatosAgente($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_sistema_usuarios', array('IdUsuario'), APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarInformacionAgente($Arreglo = false)
		 * 
		 * Inserta en la base de datos la informacion del nuevo Agente
		 * @param $Arreglo: Arreglo de datos del Agente
		 * @return Resultado de la inserción
		 */
		public function GuardarInformacionAgente($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_informacion_usuarios', array('IdInformacion', 'RUC', 'RazonSocial', 'Direccion', 'TelefonoCasa2', 'TelefonoMovil2', 'callerid', 'Status'), APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarAgenteAsignado($Arreglo = false)
		 * 
		 * Asocia los datos de una agente a un Supervisor
		 * @param $Arreglo: Arreglo de datos del Agente
		 * @return Resultado de la inserción
		 */
		public function GuardarAgenteAsignado($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_agentes_asignado_supervisor', array('IdAgenteAsignado'), APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarImagen($Arreglo = false)
		 * 
		 * Guarda en BD una imagen de perfil del usuario
		 * @param $Arrelo:	Arreglo con los datos de la imagen
		 */
		public function GuardarImagen($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				$SQL = new NeuralBDGab(APP, 'tbl_imagen_usuario');
				foreach($Arreglo as $Columna => $Valor){
					$SQL->Sentencia($Columna, $Valor);	
				}
			return 	$SQL->Insertar();
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizaInformacionAgente($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Edita en la base de datos la informacion del Agente
		 * @param $Arreglo: Arreglo de datos del Agente
		 * @param $Condicion: Id del registro
		 * @param $Omitidos: Campos que no se requeiren modificar
		 * @return Resultado de la Actualización
		 */
		public function ActualizaInformacionAgente($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Condicion == true AND is_array($Condicion) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_informacion_usuarios', $Omitidos, APP);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarUsuarioAgente($Arreglo = false, $Condicion = false, $Omitidos = false){
		 * 
		 * Edita los datos de sistema del Agente
		 * @param $Arreglo: Arreglo de datos del Agente
		 * @param $Condicion: Id del registro
		 * @param $Omitidos: Campos que no se requeiren modificar
		 * @return Resultado de la Actualización
		 */
		public function ActualizarUsuarioAgente($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_sistema_usuarios', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualiza el Status del registro a ELIMINADO
		 * @param $Arreglo: Nuevo estado del Registro
		 * @param $Condicion: Id del registro a modificar
		 * @param $Omitidos: Campos no que no requieren modificación
		 * @return Resultado de modificación
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_sistema_usuarios', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarFoto($Imagen = false, $Condicion = false)
		 * 
		 * Actualiza la imagen de perfil del usuario
		 * @param $Imagen:	array de datos de la imagen 
		 * @param $Condicion: Identificador (id de usuario)
		 */
		public function ActualizarFoto($Imagen = false, $Condicion = false){
			if($Imagen == true and $Condicion == true and is_array($Imagen) == true){
				$SQL = new NeuralBDGab(APP, 'tbl_imagen_usuario');
				$SQL->Sentencia('Imagen', $Imagen['Imagen']);
				$SQL->Sentencia('Tipo', $Imagen['Tipo']);
				$SQL->Condicion('IdUsuario', $Condicion);
				$SQL->Actualizar();
			}
		}
	}