<?php
	
	class HistorialDeCarga_Modelo extends AppSQLConsultas{
		
		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarHistorialCarga($IdSupervisor = false)
		 * 
		 * Consulta el historial de archivos programados de un ssupervisor
		 */
		public function ConsultarHistorialCarga($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla("tbl_archivos_programados");
				$Consulta->Columnas('IdArchivosProgramados, NombreArchivo, FechaHoraCarga, Status, ArchivoLog');
				$Consulta->Condicion("IdSupervisor = '$IdSupervisor'");
				$Consulta->Ordenar('FechaHoraCarga', 'DESC');
				return $Consulta->Ejecutar(false, true);
			}			
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarRegistro($IdRegistro = false)
		 * 
		 * Consulta el registro
		 */
		public function ConsultarRegistro($IdRegistro = false){
			if($IdRegistro == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla("tbl_archivos_programados");
				$Consulta->Columnas('FechaHoraCarga');
				$Consulta->Condicion("IdArchivosProgramados = '$IdRegistro'");
				return $Consulta->Ejecutar(false, true);
			}			
		}
		
		/**
		 * Metodo Publico
		 * Editar($Datos = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Edicion de la hora de carga
		 * @param boolean $Datos     Array con datos nuevos
		 * @param boolean $Condicion Identificador del registro [0] => Campo1, [1]...
		 * @param boolean $Omitidos  [description]
		 */
		public function Editar($Datos = false, $Condicion = false, $Omitidos = false){
			if($Datos == true and $Condicion == true and $Omitidos == true){
				return self::ActualizarDatos($Datos, $Condicion, 'tbl_archivos_programados', $Omitidos, APP);
			}
		}
	}