<?php 
	
	class FiltroSemanasAtraso_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metodo publico
		 * ConsultaGerencias()
		 * 
		 * Consulta todas la gerencias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 * 
		 * Consulta todas la Carteras
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 * 
		 * Consulta todos las Provincias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaDistritos()
		 * 
		 * Consulta todos los Distritos
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaDistritos(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaRegistros($ArregloCriterios = false, $IdAgente = false)
		 * 
		 * Consulta los registros que cumplen con los criterios de busqueda
		 * @param $ArregloCriterios: Arreglo con las condiciones de busqueda
		 * @param $IdAgente: Id del Agente de los registro a buscar
		 * @return Matriz Resultado de la consulta
		 */
		public function ConsultaRegistros($ArregloCriterios = false, $IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_asignacion_registros');
				$Consulta->Columnas('tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal, tbl_datos_agenda.StatusCobro');
				$Consulta->InnerJoin('tbl_datos_agenda', 'tbl_asignacion_registros.IdDatoAgenda', 'tbl_datos_agenda.IdDatoAgenda');
				$Consulta->InnerJoin('tbl_agente_asignado', 'tbl_asignacion_registros.IdAsignacionRegistro', 'tbl_agente_asignado.IdAsignacionRegistro');
				$Consulta->InnerJoin('tbl_agentes_asignado_supervisor', 'tbl_agente_asignado.IdAgente', 'tbl_agentes_asignado_supervisor.IdAgente');
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_datos_agenda.StatusRegistro != 'ELIMINADO'");
				$Consulta->Agrupar('NombreTitular');
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
					foreach($ArregloCriterios AS $Condicion){
						$Consulta->Condicion($Condicion);
					}	
				}
				return $Consulta->Ejecutar(false, true);
			}
		}
		
	}