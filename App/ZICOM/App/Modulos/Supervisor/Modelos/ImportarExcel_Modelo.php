<?php

	class ImportarExcel_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		/**
		 * Metodo Publico
		 * ConsultarGerencias()
		 *
		 * Consulta las gerencias activas
		 * @return array: Consulta realizada
		 */
		public function ConsultarGerencias(){
			$SQL = "SELECT IdGerencia, Descripcion FROM tbl_gerencias WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 *
		 * Consulta de carteras activas
		 * @return array: Consulta de carteras
		 */
		public function ConsultaCarteras(){
			$SQL = "SELECT IdCartera, Descripcion FROM tbl_carteras WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 *
		 * Consulta de provincias activas
		 * @return array: Consulta de provincias
		 */
		public function ConsultaProvincias(){
			$SQL = "SELECT IdProvincia, Descripcion FROM tbl_provincia WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * ConsultaDistritos()
		 *
		 * Consulta de distritos activos
		 * @return array: Consulta de Distritos
		 */
		public function ConsultaDistritos(){
			$SQL = "SELECT IdDistrito, Descripcion FROM tbl_distritos WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo publico 
		 * ConsultaCodigos()
		 * 
		 * Consulta los codigos de gestion 
		 */
		public function ConsultaCodigos(){
			$SQL = "SELECT IdCodigo, Codigo FROM tbl_codigos WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaClientes()
		 * 
		 * Consulta los clientes asignados a el supervisor de la sesion
		 * @param $IdSupervisor: Condicon de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultaClientes($IdSupervisor = false){
			if($IdSupervisor == true){
				$SQL = "SELECT tbl_informacion_usuarios.idUsuario as IdCliente, tbl_informacion_usuarios.RazonSocial FROM tbl_clientes_asignados_supervisor ".
					   "INNER JOIN tbl_sistema_usuarios ON tbl_clientes_asignados_supervisor.IdCliente = tbl_sistema_usuarios.IdUsuario ".
					   "INNER JOIN tbl_sistema_usuarios_perfil ON tbl_sistema_usuarios.IdPerfil = tbl_sistema_usuarios_perfil.IdPerfil ".
					   "INNER JOIN tbl_informacion_usuarios ON tbl_sistema_usuarios.IdUsuario = tbl_informacion_usuarios.idUsuario ".
					   "WHERE tbl_sistema_usuarios.Status = 'ACTIVO' AND tbl_clientes_asignados_supervisor.IdSupervisor = '$IdSupervisor' AND tbl_sistema_usuarios_perfil.Nombre = 'Cliente'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);	
			}
		}

		/**
		 * Metodo Publico
		 * AgregarRegistros($Matriz = false)
		 *
		 * Agregar registros a la tabla registro de clientes
		 * @param $Matriz: Matriz de datos
		 */
		public function AgregarRegistros($Matriz = false){
			if($Matriz == true AND is_array($Matriz) == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');
				foreach($Matriz AS $Arreglo){
					foreach($Arreglo AS $Columna => $Valor){
						$SQL->Sentencia($Columna, $Valor);
					}
					$SQL->Insertar();
				}
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarRegistros($Matriz = false, $Compara = false)
		 *
		 * Actualiza registros a la tabla registro de clientes
		 * @param $Matriz: Matriz de datos
		 * @param $Compara: Condicion de busqueda para actualizar
		 */
		public function ActualizarRegistros($Matriz = false, $Compara = false){
			if($Matriz == true AND $Compara == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');
				$Ids = self::BuscaIndices($Compara, $Matriz);
				foreach($Matriz AS $Arreglo){
					foreach($Arreglo AS $Columna => $Valor){
						if(trim($Columna) == $Compara){
							$ConsultaId = self::BuscaIndice($Valor, $Ids);
						}
						else{
							if($Columna != 'IdCodigo')
								$SQL->Sentencia($Columna, $Valor);
							elseif($Columna == 'IdCodigo')
								if($Valor != 0)
									$SQL->Sentencia($Columna, $Valor);
						}
					}
					$SQL->Condicion('IdDatoAgenda', $ConsultaId);
					$SQL->Actualizar();
				}
			}
		}

		/**
		 * Metodo Publico
		 * BuscaIndice($Valor = false, $Lista = false)
		 *
		 * Consulta el Id de un registro
		 * @param $Lista: Lista de id nombre
		 * @param $Valor: Es la condicion para buscar
		 * @return El resultado de la consulta
		 */
	 	private function BuscaIndice($Valor = false, $Lista = false){
	 		foreach($Lista AS $Llave => $Registro){
	 			if($Valor == $Registro['NombreTitular']){
	 				return $Registro['IdDatoAgenda'];
	 			}
	 		}
	 	}
	 	
	 	/**
	 	 * Metodo Pirvado
	 	 * BuscaIndices($Columna = false, $Lista = false)
	 	 * 
	 	 * Consulta los id de los registros a actualizar
	 	 * @param $Columna:  Columna de comparacion
	 	 * @param $Lista: Matriz de datos
	 	 */
		private function BuscaIndices($Columna = false, $Lista = false){
			if($Columna == true AND $Lista == true){
				$Lista = AppUtilidades::arrayColumn($Lista, $Columna);
				$Funct = function($Valor){
					return '"'.$Valor.'"';
				};
				$Lista = array_map($Funct, $Lista);
				$Lista = implode(', ', $Lista);
				$SQL = "SELECT IdDatoAgenda, NombreTitular FROM tbl_datos_agenda WHERE $Columna IN (".$Lista.")";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
	}
