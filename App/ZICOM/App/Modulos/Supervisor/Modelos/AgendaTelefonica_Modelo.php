<?php

	class AgendaTelefonica_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}

		/**
		 * Metodo Publico
		 * ConsultaGerencias()
		 *
		 * Consulta de las genrencias disponibles
		 * @return array: Consulta de resultados
		 */
		public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}

		/**
		 * Metodo Publico
		 * ConsultaCarteras()
		 *
		 * Consulta de las carteras disponibles
		 * @return array: Consulta de resultados
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}

		/**
		 * Metodo Publico
		 * ConsultaProvincia()
		 *
		 * Consulta las provincias disponibles
		 * @return array: Consulta de resultados
		 */
		public function ConsultaProvincia(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}

		/**
		 * Metodo Publico
		 * ConsultaDistritos($IdProvincia = false)
		 *
		 * Consulta los distritos pertenecientes a la provincia
		 * @param bool $IdProvincia: Llave de la provincia
		 * @return array: Consulta de resultados
		 */
		public function ConsultaDistritos($IdProvincia = false){
			if($IdProvincia == true AND is_numeric($IdProvincia)){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_distritos');
				$Consulta->Columnas('IdDistrito, Descripcion');
				$Consulta->Condicion("Status != 'ELIMINADO'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo publico 
		 * ConsultaCodigos()
		 * 
		 * Consulta los codigos de gestion 
		 */
		public function ConsultaCodigos(){
			$SQL = "SELECT IdCodigo, Codigo, Descripcion FROM tbl_codigos WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}

		/**
		 * Metodo Publico
		 * ConsultarAgenda($IdSupervisor = false)
		 *
		 * Consulta de datos de los clientes registrados
		 * @param bool $IdSupervisor: llave del supervisor
		 * @return array: consulta de resultados
		 */
		public function ConsultarAgenda($IdSupervisor = false, $Inicio = false, $NumeroRegistros = false, $Criterio = false){
			if($IdSupervisor == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal,
			  		tbl_datos_agenda.StatusRegistro, tbl_asignacion_registros.Status, tbl_codigos.Color FROM tbl_datos_agenda
			   		INNER JOIN tbl_asignacion_registros ON tbl_datos_agenda.IdDatoAgenda = tbl_asignacion_registros.IdDatoAgenda 
			   		LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo
				    WHERE (tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_datos_agenda.IdSupervisor = ".$IdSupervisor.") AND 
					(tbl_datos_agenda.ClienteUnico LIKE '%".$Criterio."%' OR tbl_datos_agenda.NombreTitular LIKE '%".$Criterio."%' OR tbl_datos_agenda.SemanaAtraso LIKE '%".$Criterio."%' OR tbl_datos_agenda.Saldo LIKE '%".$Criterio."%' OR 
					tbl_datos_agenda.SaldoTotal LIKE '%".$Criterio."%' ) LIMIT ".$Inicio.", ".$NumeroRegistros." ";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaNumeroRegistros($IdSupervisor = false, $Criterio = false)
		 * 
		 * Consulta el numero de registros de acuerdo a las condiciones
		 * @param $IdSupervisor: identificador del usuario
		 * @param $Criterio: string a buscar en los campos
		 */
		public function ConsultaNumeroRegistros($IdSupervisor = false, $Criterio = false){
			$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda FROM tbl_datos_agenda
			   		INNER JOIN tbl_asignacion_registros ON tbl_datos_agenda.IdDatoAgenda = tbl_asignacion_registros.IdDatoAgenda 
				    WHERE (tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_datos_agenda.IdSupervisor = ".$IdSupervisor.") AND (tbl_datos_agenda.ClienteUnico LIKE '%".$Criterio."%' OR tbl_datos_agenda.NombreTitular LIKE '%".$Criterio."%' OR tbl_datos_agenda.SemanaAtraso LIKE '%".$Criterio."%' OR tbl_datos_agenda.Saldo LIKE '%".$Criterio."%' OR 
					tbl_datos_agenda.SaldoTotal LIKE '%".$Criterio."%')";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->rowCount();		
		}

		/**
		 * Metodo Publico
		 * ConsultarRegistroExistente($NombreTitular =  false)
		 *
		 * Consulta si el registro ya existe en la base de datos
		 * @param bool $NombreTitular: nombre del titular
		 * @return array: cantidad de registros y arreglo de consultas
		 */
		public function ConsultarRegistroExistente($NombreTitular =  false){
			if($NombreTitular == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas('NombreTitular');
				$Consulta->Condicion("NombreTitular = '$NombreTitular'");
				return $Consulta->Ejecutar(true, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarRegistroAgenda($IdDatoAgenda = false)
		 *
		 * Consulta el registro de clientes registrados
		 * @param bool $IdDatoAgenda: llave primaria
		 * @return array: consulta de resultados
		 */
		public function ConsultarRegistroAgenda($IdDatoAgenda = false){
			if($IdDatoAgenda == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas(self::ListarColumnas('tbl_datos_agenda', array('IdDatoAgenda', 'IdSupervisor', 'StatusCobro'), false, APP));
				$Consulta->Condicion("IdDatoAgenda = '$IdDatoAgenda'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaRegistroAsociado($IdDatoAgenda = false)
		 * 
		 * Consulta los agentes asignados a un cliente 
		 * @param $IdDatoAgenda: Condicion de busqueda
		 * @return Resultado de la busqueda
		 */
		public function ConsultaRegistroAsociado($IdDatoAgenda = false, $IdSupervisor = false){
			if($IdDatoAgenda == true and $IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_asignacion_registros');
				$Consulta->Columnas('tbl_informacion_usuarios.idUsuario, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno');
				$Consulta->InnerJoin('tbl_agente_asignado', 'tbl_asignacion_registros.IdAsignacionRegistro', 'tbl_agente_asignado.IdAsignacionRegistro');
				$Consulta->InnerJoin('tbl_agentes_asignado_supervisor', 'tbl_agente_asignado.IdAgente', 'tbl_agentes_asignado_supervisor.IdAgente');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_agentes_asignado_supervisor.IdAgente', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_asignacion_registros.IdDatoAgenda = '$IdDatoAgenda'");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta los agentes asociados al Supervisor de la sesión
		 */
		public function ConsultarAgentes($IdSupervisor = false){
			if($IdSupervisor == True){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor, tbl_sistema_usuarios, tbl_informacion_usuarios');
				$Consulta->Columnas('tbl_agentes_asignado_supervisor.IdAgente, tbl_sistema_usuarios.Usuario, tbl_sistema_usuarios.Status, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno');
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarAgentes($IdSupervisor = false)
		 * 
		 * Consulta los agentes asociados al Supervisor de la sesion que no este eliminados
		 * @param $IdSupervisor: Identificador del supervisor
		 * @return Matriz de consulta. 
		 */
		public function ConsultarAgentesNoEliminados($IdSupervisor = false){
			if($IdSupervisor == True){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor, tbl_sistema_usuarios, tbl_informacion_usuarios');
				$Consulta->Columnas('tbl_agentes_asignado_supervisor.IdAgente, tbl_sistema_usuarios.Usuario, tbl_sistema_usuarios.Status, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, tbl_informacion_usuarios.ApellidoMaterno');
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_sistema_usuarios.IdUsuario");
				$Consulta->Condicion("tbl_agentes_asignado_supervisor.IdAgente = tbl_informacion_usuarios.idUsuario");
				$Consulta->Condicion("tbl_sistema_usuarios.Status != 'ELIMINADO'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaClientes()
		 * 
		 * Consulta los clientes asignados a el supervisor de la sesion
		 * @param $IdSupervisor: Condicon de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultaClientes($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_clientes_asignados_supervisor');
				$Consulta->Columnas('tbl_informacion_usuarios.idUsuario, tbl_informacion_usuarios.RazonSocial');
				$Consulta->InnerJoin('tbl_sistema_usuarios', 'tbl_clientes_asignados_supervisor.IdCliente', 'tbl_sistema_usuarios.IdUsuario');
				$Consulta->InnerJoin('tbl_sistema_usuarios_perfil', 'tbl_sistema_usuarios.IdPerfil', 'tbl_sistema_usuarios_perfil.IdPerfil');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_sistema_usuarios.IdUsuario', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_sistema_usuarios.Status = 'ACTIVO'");
				$Consulta->Condicion("tbl_clientes_asignados_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_sistema_usuarios_perfil.Nombre = 'Cliente'");
				return $Consulta->Ejecutar(false, true);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaDatosAsignacion($IdDatoAgenda = false)
		 * 
		 * Consulta los agente asignado a un Cliente
		 * @param $IdDatoAgenda: Identificadir de l cliente a consultar
		 */
		public function ConsultaDatosAsignacion($IdDatoAgenda = false){
			if($IdDatoAgenda == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_asignacion_registros');
				$Consulta->Columnas('tbl_asignacion_registros.IdAsignacionRegistro');
		     	$Consulta->Condicion("tbl_asignacion_registros.IdDatoAgenda = '$IdDatoAgenda'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaIdAgenteAsignado($IdAsignacion = false)
		 * 
		 * Consulta los id que relacionan al cliente con los agentes
		 * @param $IdAsignacion: Condición de busqueda
		 */
		public function ConsultaIdAgenteAsignado($IdAsignacion = false){
			if($IdAsignacion == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla("tbl_agente_asignado");
				$Consulta->Columnas("IdAgenteAsignado");
				$Consulta->Condicion("IdAsignacionRegistro = '$IdAsignacion'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * GuardarDatosAgenda($Arreglo = false)
		 *
		 * Guarda informacion del registro de clientes
		 * @param bool $Arreglo: Arreglo de datos de clientes
		 * @return bool|string
		 */
		public function GuardarDatosAgenda($Arreglo = false){
			if($Arreglo == true AND is_array($Arreglo)){
				return self::GuardarDatos($Arreglo, 'tbl_datos_agenda', array('IdDatoAgenda', 'StatusCobro'), APP);
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarInformacionAgenda($Arreglo = false, $Omitidos = false, $Condicion = false)
		 *
		 * Actualiza informacion de registros de clientes
		 * @param bool $Arreglo: Arreglo de datos
		 * @param bool $Omitidos: Arreglo de elementos omitidos
		 * @param bool $Condicion: Condicion de actualizacion
		 * @return string
		 */
		public function ActualizarInformacionAgenda($Arreglo = false, $Omitidos = false, $Condicion = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_datos_agenda', $Omitidos, APP);
			}
		}

		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Status = false, $IdDatoAgenda = false)
		 *
		 * Modifica estado de registros
		 * @param bool $Status: Status
		 * @param bool $IdDatoAgenda: Llave primaria
		 */
		public function ModificarEstadoRegistro($Status = false, $IdDatoAgenda = false){
			if($Status == true AND $IdDatoAgenda == True){
				$Consulta = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');
				$Consulta->Sentencia('StatusRegistro', $Status);
				$Consulta->Condicion('IdDatoAgenda', $IdDatoAgenda);
				return $Consulta->Actualizar();
			}
		}
		
		/**
		 * Metodo Publico
		 * ReasignarAgentes($Arreglo = false, $Condicion = false, $Fecha = false)
		 * 
		 * Reasigna agentes a un cliente
		 * @param $Arreglo: Lista de Id de los nuevos Agentes
		 * @param $Condicion: Arreglo de Id del registro a modificar
		 * @param $Fecha: Fecha actual
		 * @return Void
		 */
		public function ReasignarAgentes($Arreglo = false, $Condicion = false, $Fecha = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_agente_asignado');
				$Cont = 0;
				foreach($Arreglo as $Columna => $Valor){
					$SQL->Sentencia('IdAgente', $Valor);
					$SQL->Sentencia('FechaAsginacion', $Fecha);
					$SQL->Condicion('IdAgenteAsignado', $Condicion[$Cont]['IdAgenteAsignado']);
					$SQL->Actualizar();	
					$Cont++;
				}	
			}
		}

	}
