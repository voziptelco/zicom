<?php

	class Gerencia_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
		}
		
		/**
		 * Metod Público 
		 * ConsultarGerencias()
		 * 
		 * Consulta las Gerecias
		 * @return Resultado en una matriz, de la consulta
		 */
		public function ConsultarGerencias(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);			
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGerencia($IdGerencia = false)
		 * 
		 * Consulta si una cartera en especifico ya existe.
		 * @param $IdGerencia: Id  de la gerencia.
		 * @return Resultado de la consulta.
		 */
		public function ConsultarGerencia($IdGerencia = false){
			if($IdGerencia == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_gerencias');
				$Consulta->Columnas('Descripcion');
				$Consulta->Condicion("IdGerencia = '$IdGerencia'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaGerenciaExistente($Descripcion = false)
		 * 
		 * Consulta si una cartera en especifico ya existe.
		 * @param $Descripcion: descripcion de la Gerencia.
		 * @return Resultado de la consulta.
		 */
		public function ConsultaGerenciaExistente($Descripcion = false){
			if($Descripcion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_gerencias');
				$Consulta->Columnas('IdGerencia, Status');
				$Consulta->Condicion("Descripcion = '$Descripcion'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * AgregarGerencia($Arreglo = false, $Omitidos)
		 * 
		 * Agregar datos a la base de datos en tabla Gerencia
		 * @param $Arreglo: array con Datos a insertar
		 * @param $Omitidos: array con columnas omitidas
		 * @return resultado de la operacion
		 */
		public function AgregarGerencia($Arreglo = false, $Omitidos){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::GuardarDatos($Arreglo, 'tbl_gerencias', $Omitidos, APP);			
			}
		}
		
		/**
		 * Metodo Publico
		 * EditarGerencia($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualizar datos en tabla Gerencia
		 * @param $Arreglo: array con Datos a insertar
		 * @param $Condicion: array con el id del registro a modificar
		 * @param $Omitidos: array con columnas omitidas
		 * @return resultado de la operacion
		 */		
		public function EditarGerencia($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Condicion == true AND is_array($Condicion) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_gerencias', $Omitidos, APP);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 * 
		 * Actualizar el status de un registro a ELIMINADO
		 * @param $Arreglo: array con Datos a insertar
		 * @param $Condicion: array con el id del registro a modificar
		 * @param $Omitidos: array con columnas omitidas
		 * @return resultado de la operacion
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_gerencias', $Omitidos, APP);
			}
		}		
	}
