<?php

	class ImportarProvincias_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 *
		 * Consulta de provincias activas
		 * @return array: Consulta de provincias
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * AgregarProvincias($Matriz = false)
		 *
		 * Agregar Provinicas
		 * @param $Matriz: Matriz de datos
		 */
		public function AgregarProvincias($Matriz = false){
			if($Matriz == true AND is_array($Matriz) == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_provincia');
				foreach($Matriz AS $Arreglo){
					foreach($Arreglo AS $Columna => $Valor){
						if(!(empty($Valor))){
							$SQL->Sentencia($Columna, $Valor);
						}
					}
					$SQL->Insertar();
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * AgregarDistritos($Matriz = false)
		 *
		 * Agregar distritos
		 * @param $Matriz: Matriz de datos
		 */
		public function AgregarDistritos($Matriz = false){
			if($Matriz == true AND is_array($Matriz) == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_distritos');
				foreach($Matriz AS $Arreglo){
					$SQL->Sentencia('IdProvincia', $Arreglo['IdProvincia']);
					$SQL->Sentencia('Descripcion', $Arreglo['Distrito']);
					$SQL->Insertar();
				}
			}
		}
	}