<?php

	class Distrito_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarDistritos()
		 * 
		 * Consulta todos los distritos disponibles
		 * @return Resultado de la consulta
		 */
		public function ConsultarDistritos(){
			$SQL = "SELECT tbl_distritos.IdDistrito FROM tbl_distritos ".
				   "INNER JOIN tbl_provincia ON tbl_distritos.IdProvincia = tbl_provincia.IdProvincia ".
				   "WHERE tbl_distritos.Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->rowCount();			
		}

		/**
		 * Metodo Publico 
		 * ConsultarDistritos()
		 * 
		 * Consulta todos los distritos disponibles
		 * @return Resultado de la consulta
		 */
		public function ConsultarDistritosPagina($Inicio = false, $Cantidad = false, $Criterio = false){
			$SQL = "SELECT tbl_distritos.IdDistrito,tbl_distritos.Descripcion, tbl_provincia.Descripcion AS Descripcion2 FROM tbl_distritos ".
				   "INNER JOIN tbl_provincia ON tbl_distritos.IdProvincia = tbl_provincia.IdProvincia ".
				   "WHERE tbl_distritos.Status != 'ELIMINADO'";
			if($Criterio != ""){
					$SQL .= " AND (tbl_distritos.Descripcion LIKE '%$Criterio%' OR tbl_provincia.Descripcion LIKE '%$Criterio%')"; 
				}
			$SQL .= " LIMIT ".$Inicio .", ". $Cantidad;
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
			$Resultado['Cantidad'] = $Consulta->rowCount();
			return $Resultado;			
		}
		
		/**
		 * Metodo Publico 
		 * BuscarProvincias()
		 * 
		 * Consulta las Provicnias desponibles
		 * @return Resultado de la consulta
		 */
		public function BuscarProvincias(){
			$Consulta = new NeuralBDConsultas(APP);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);		
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarDistrito($IdDistrito = false)
		 * 
		 * Consulta un distrito dado por el ID
		 * @parm $IdDistrito: Condición de busqueda
		 * @return Resultado de la consulta
		 */
		public function ConsultarDistrito($IdDistrito = false){
			if($IdDistrito == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_distritos');
				$Consulta->Columnas('tbl_distritos.IdDistrito, tbl_provincia.IdProvincia, tbl_distritos.Descripcion, tbl_provincia.Descripcion AS "Descripcion2"');
				$Consulta->InnerJoin('tbl_provincia', 'tbl_distritos.IdProvincia', 'tbl_provincia.IdProvincia');
				$Consulta->Condicion("IdDistrito = '$IdDistrito'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaDistritoExistente($Descripcion = false)
		 * 
		 * Busca si un registro esta duplicado.
		 * @param $Descripcion: Descripcion del Distrito.
		 * @return Resultado de la consulta.
		 */
		public function ConsultaDistritoExistente($Descripcion = false){
			if($Descripcion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_distritos');
				$Consulta->Columnas('IdDistrito, Status');
				$Consulta->Condicion("Descripcion = '$Descripcion'");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * AgregarDistrito($Arreglo = false, $Omitidos)
		 *
		 * Inserción de datos para distritos
		 * @param boolean $Arreglo  array de datos
		 * @param [type]  $Omitidos Datos default
		 */
		public function AgregarDistrito($Arreglo = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::GuardarDatos($Arreglo, 'tbl_distritos', $Omitidos, APP);			
			}
		}
		
		/**
		 * Metodo Publico
		 * EditarDistrito($Arreglo = false, $Condicion = false, $Omitidos = false)
		 *
		 * Edición de datos de un registro
		 * @param boolean $Arreglo   arreglo con datos nuevos
		 * @param boolean $Condicion Identificador del registro seleccionado
		 * @param boolean $Omitidos  arreglo de columnas omitidas
		 */
		public function EditarDistrito($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) == true AND $Condicion == true AND is_array($Condicion) == true AND $Omitidos == true AND is_array($Omitidos) == true){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_distritos', $Omitidos, APP);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false)
		 *
		 * Cambio de status a ELIMINADO del registro seleccionado
		 * @param boolean $Arreglo   Nuevo estatus del registro
		 * @param boolean $Condicion Identificador del registro seleccionado
		 * @param boolean $Omitidos  Array de datos omiticdos
		 */
		public function ModificarEstadoRegistro($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_distritos', $Omitidos, APP);
			}
		}		
	}
?>