<?php

	class RegistroGrabaciones extends Controlador {

		var $Informacion;
		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroGrabaciones', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmFiltradoClientes()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de clientes
		 */
		public function frmFiltradoGrabaciones(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Agentes = $this->Modelo->ConsultarAgentes($this->Informacion['Informacion']['idUsuario']);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Numero('minimoDuracion', 'Solo Números');
				$Validacion->Numero('maximoDuracion', 'Solo Números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Agentes', $Agentes);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmFiltroGrabaciones'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroGrabaciones', 'Filtrar', 'FiltroGrabaciones.html')));
				unset($Agentes, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGrabaciones()
		 * 
		 * Prepara criteris paraq hacer una busqueda de grabaciones
		 */
		public function ConsultarGrabaciones(){
			set_time_limit(0);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL(AppUtilidades::ArregloEstados($_POST));
					if(isset($DatosPost['Agente'])){
						$DatosPost['tbl_agentes_asignado_supervisor.IdAgente'] = $DatosPost['IdAgente'];
						unset($DatosPost['IdAgente']);
					}
					$Condiciones = AppCriteriosFiltro::GeneraCondicones($DatosPost);
					$Consulta = $this->Modelo->ConsultarGrabacionesTotal($Condiciones, $this->Informacion['Informacion']['idUsuario'], 1, 10);
					if($Consulta['Cantidad'] > 0){
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Condiciones = json_encode($Condiciones);
						$Plantilla->Parametro('Condiciones', $Condiciones);
						$Plantilla->Parametro('Total', $Consulta['Cantidad']);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroGrabaciones', 'Listado', 'Listado.html')));
						unset($DatosPost, $Condiciones, $Consulta, $Plantilla);
						exit();	
					}
					else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroGrabaciones', 'Error', 'NoResultados.html')));
						unset($DatosPost, $Condiciones, $Consulta, $Plantilla);
						exit();
					}						
				}
			}	
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGrabacionesPagina()
		 * 
		 * Consulta grabaciones segun la pagina. 
		 */
		public function ConsultarGrabacionesPagina(){
			set_time_limit(0);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
					$Condiciones = json_decode($_POST['Consulta']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$Pagina = $DatosPost['current'];
					$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
					$NumeroRegistros = $DatosPost['rowCount'];
					$Busqueda = $DatosPost['searchPhrase'];
					$Consulta = $this->Modelo->ConsultarGrabaciones($Condiciones, $this->Informacion['Informacion']['idUsuario'], $Inicio, $NumeroRegistros, $Busqueda);
					if($Consulta['Cantidad'] > 0){
						$total =($Busqueda == "")? $DatosPost['ToltalRegistros']:$Consulta['Cantidad'];
						unset($Consulta['Cantidad']);
						$Consulta = AppFiltrosRapidos::AplicarFiltrosColumnas($Consulta, array("NumeroTelefono", "duration"));
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>$total);
						$Consulta = json_encode($Consulta);
						echo $Consulta;	
					}
					else{
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;
					}					
			}	
		}
		
		/**
		 * Metodo Publcico
		 * VerGrabacion()
		 * 
		 * Pantalla para reproducir audio
		 */
		public function VerGrabacion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('uniqueid', $_POST['uniqueid']);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('RegistroGrabaciones', 'Audio', 'Audio.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * EscuharGrabacion()
		 * 
		 * Accede a la grabación 
		 */
		public function EscucharGrabacion($Uniqueid = false){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				set_time_limit(0);
				$Audio = new AppGrabaciones($Uniqueid);
				echo $Audio->ObtenerAudio();
			}
		}
	}