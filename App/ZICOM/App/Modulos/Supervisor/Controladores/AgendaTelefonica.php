<?php

	class AgendaTelefonica extends Controlador{
		
		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio de Registro de Clientes
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * frmListado()
		 *
		 * Pantalla de listado de registros de clientes activos
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Registros = $this->Modelo->ConsultaNumeroRegistros($this->Informacion['Informacion']['idUsuario']);
				$Paginas = ceil($Registros / 10);
				$Plantilla = new NeuralPlantillasTwig(APP);	
				$Plantilla->Parametro("NumeroPaginas", $Paginas);
				$Plantilla->Parametro("Registros", $Registros);	
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Listado', 'Listado.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaPagina()
		 * 
		 * paginacion de los registros
		 */
		public function ConsultaPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$Pagina = $DatosPost['NumeroPagina'];
				if(!$Pagina){
					$Inicio = 0;
				}
				else{
					$Inicio = ($Pagina - 1) * 10;	
				}
				$Consulta = $this->Modelo->ConsultarAgenda($IdSupervisor, $Inicio, 10, $DatosPost['Busqueda']);
				$Consulta = json_encode(AppArreglos::FiltrarDatos($Consulta, 'IdDatoAgenda', 'Saldo', 'SaldoTotal'));
				echo $Consulta;
			}
		}
		
		/**
		 * Metodo Publico
		 * CalcularLimite()
		 * 
		 * Calcula el total de registros y paginas de acuerdo a la busqueda
		 */
		public function CalcularLimite(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Registros = $this->Modelo->ConsultaNumeroRegistros($this->Informacion['Informacion']['idUsuario'], $DatosPost['Busqueda']);
				$Paginas = ceil($Registros / 10);
				$Datos = json_encode(array("Registros"=>$Registros -1, "Paginas"=>$Paginas));
				echo $Datos;
				unset($DatosPost, $Registros, $Paginas);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * frmAgregar()
		 *
		 * Pantalla de Agregar nuevo registro de cliente
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$ConsultaGerencias = $this->Modelo->ConsultaGerencias();
				$ConusltaCarteras  = $this->Modelo->ConsultaCarteras();
				$ConsultaProvincia = $this->Modelo->ConsultaProvincia();
				$consultaCodigos = $this->Modelo->ConsultaCodigos();
				$Clientes = $this->Modelo->ConsultaClientes($this->Informacion['Informacion']['idUsuario']);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('IdCliente', '* Campo Requerido');
				$Validacion->Requerido('IdGerencia');
				$Validacion->Requerido('IdCartera');
				$Validacion->Requerido('ClienteUnico');
				$Validacion->Remoto('ClienteUnico', NeuralRutasApp::RutaUrlAppModulo('Supervisor/AgendaTelefonica/ValidaClienteUnico'), 'POST', true, 'Error el numero de cliete no es válido');
				$Validacion->Requerido('NombreTitular');
				$Validacion->Requerido('DomicilioContractual');
				$Validacion->Requerido('IdProvincia');
				$Validacion->Requerido('IdDistrito');
				$Validacion->Requerido('SemanaAtraso');
				$Validacion->Numero('SemanaAtraso');
				$Validacion->Requerido('Saldo');
				$Validacion->Requerido('SaldoTotal');
				$Validacion->Requerido('IdCodigo');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarAgendaTelefonica'));
				$Plantilla->Parametro('Gerencias', $ConsultaGerencias);
				$Plantilla->Parametro('Carteras', $ConusltaCarteras);
				$Plantilla->Parametro('Provincias', $ConsultaProvincia);
				$Plantilla->Parametro('Clientes', $Clientes);
				$Plantilla->Parametro('Codigos', $consultaCodigos);				
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Agregar', 'Agregar.html')));
				unset($ConsultaGerencias, $ConusltaCarteras, $consultaProvincia, $Validacion, $Plantilla);
				exit();	
			}				
		}
		
		/**
		 * Metodo Publico 
		 * ValidaClienteUnico()
		 * 
		 * Valida que el numero de clientUnico sea correcto
		 */
		public function ValidaClienteUnico(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST['ClienteUnico'])){
					$Validado = (preg_match("/^([0-9]{1})-([0-9]{1,2})-([0-9]{3,4})-([0-9]{3,5})$/", $_POST['ClienteUnico'])) ? "true" : "false";
					echo $Validado;
					unset($Validado);
					exit();
				}		
			}
		}

		/**
		 * Metodo Publico
		 * Distritos()
		 *
		 * Ajax que genera los distritos apartir de idProvincia
		 */
		public function Distritos(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultaDistritos($_POST['IdProvincia']);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Distritos', $Consulta);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Agregar', 'Distritos.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * Agregar()
		 *
		 * Agrega datos del nuevo registro de cliente a la base de datos
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('NombreAval', 'DomicilioAval', 'DistritoAval', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'StatusRegistro') ) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('IdCliente', 'IdGerencia', 'IdCartera', 'IdProvincia', 'IdDistrito', 'DomicilioAval', 'DistritoAval', 'SemanaAtraso', 'Saldo', 'SaldoTotal', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'ClienteUnico'));
						$DatosPost = AppUtilidades::FormatoNumeros($DatosPost, array('Saldo', 'SaldoTotal'));
						$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
						$DatosPost['IdSupervisor'] = $IdSupervisor;
						$DatosPost['NombreAval'] = (empty($DatosPost['NombreAval'])) ? "Sin Aval" : $DatosPost['NombreAval'];
						$DatosPost['DomicilioAval'] = (empty($DatosPost['DomicilioAval'])) ? "Sin Domicilio" : $DatosPost['DomicilioAval'];
						$DatosPost['DistritoAval'] = (empty($DatosPost['DistritoAval'])) ? "Sin Poblacion" : $DatosPost['DistritoAval'];
						$DatosPost['StatusRegistro'] = (isset($DatosPost['StatusRegistro'])) ? "ACTIVO"  : "INACTIVO";
						$Consulta = $this->Modelo->ConsultarRegistroExistente($DatosPost['NombreTitular']);
						if($Consulta['Cantidad'] > 0){
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $IdSupervisor, $Consulta, $Plantilla);
							exit();
						}
						else{
							$this->Modelo->GuardarDatosAgenda($DatosPost);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Agregar', 'Exito.html')));
							unset($DatosPost, $IdSupervisor, $Consulta, $Plantilla);
							exit();
						}
						
					}
					else{
					
					}
				}
			}
		}

		/**
		 * Metodo Publico
		 * frmEditar()
		 *
		 * Pantalla Editar registro de cliente
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdDatoAgenda = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Consulta = $this->Modelo->ConsultarRegistroAgenda($IdDatoAgenda);
				$ConsultaGerencias = $this->Modelo->ConsultaGerencias();
				$ConusltaCarteras  = $this->Modelo->ConsultaCarteras();
				$consultaProvincia = $this->Modelo->ConsultaProvincia();
				$consultaCodigos = $this->Modelo->ConsultaCodigos();
				$Clientes = $this->Modelo->ConsultaClientes($this->Informacion['Informacion']['idUsuario']);
				$ConsultaDistritos = $this->Modelo->ConsultaDistritos($consultaProvincia[0]['IdProvincia']);
				$Validacion = new  NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('IdCliente');
				$Validacion->Requerido('IdGerencia');
				$Validacion->Requerido('IdCartera');
				$Validacion->Requerido('ClienteUnico');
				$Validacion->Remoto('ClienteUnico', NeuralRutasApp::RutaUrlAppModulo('Supervisor/AgendaTelefonica/ValidaClienteUnico'), 'POST', true, 'Error el numero de cliete no es válido');
				$Validacion->Requerido('NombreTitular');
				$Validacion->Requerido('DomicilioContractual');
				$Validacion->Requerido('IdProvincia');
				$Validacion->Requerido('IdDistrito');
				$Validacion->Requerido('SemanaAtraso');
				$Validacion->Numero('SemanaAtraso');
				$Validacion->Requerido('Saldo');
				$Validacion->Requerido('SaldoTotal');
				$Validacion->Requerido('IdCodigo');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdUsuario', $IdDatoAgenda);
				$Plantilla->Parametro('Gerencias', $ConsultaGerencias);
				$Plantilla->Parametro('Carteras', $ConusltaCarteras);
				$Plantilla->Parametro('Provincias', $consultaProvincia);
				$Plantilla->Parametro('Distritos', $ConsultaDistritos);
				$Plantilla->Parametro('Clientes', $Clientes);
				$Plantilla->Parametro('Codigos', $consultaCodigos);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarAgenda'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Editar', 'Editar.html')));
				unset($IdDatoAgenda, $Consulta, $ConsultaGerencias, $ConusltaCarteras, $consultaProvincia, $ConsultaDistritos, $Validacion, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * Actualizar()
		 *
		 * Actualuza el registro de cliente
		 */
		public function Actualizar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('NombreAval', 'DomicilioAval', 'DistritoAval', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'StatusRegistro') ) == false) {
						$IdDatoAgenta =$_POST['IdUsuario'];
						unset($_POST['IdUsuario']);
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('IdCliente', 'IdGerencia', 'IdCartera', 'IdProvincia', 'IdDistrito', 'DomicilioAval', 'DistritoAval', 'SemanaAtraso', 'Saldo', 'SaldoTotal', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'ClienteUnico'));
						$DatosPost = AppUtilidades::FormatoNumeros($DatosPost, array('Saldo', 'SaldoTotal'));
						$DatosPost['NombreAval'] = (empty($DatosPost['NombreAval'])) ? "Sin Aval" : $DatosPost['NombreAval'];
						$DatosPost['DomicilioAval'] = (empty($DatosPost['DomicilioAval'])) ? "Sin Domicilio" : $DatosPost['DomicilioAval'];
						$DatosPost['DistritoAval'] = (empty($DatosPost['DistritoAval'])) ? "Sin Poblacion" : $DatosPost['DistritoAval'];
						$DatosPost['StatusRegistro'] = (isset($DatosPost['StatusRegistro'])) ? "ACTIVO"  : "INACTIVO";
						$Omitidos = array('IdDatoAgenda', 'IdSupervisor', 'StatusCobro');
						$Condicion = array('IdDatoAgenda' => $IdDatoAgenta);
						$this->Modelo->ActualizarInformacionAgenda($DatosPost, $Omitidos, $Condicion);
						unset($IdDatoAgenta, $DatosPost, $Omitidos, $Condicion);
						exit();
					}
				}
			}	
		}

		/**
		 * Metodo Publico
		 * Eliminar()
		 *
		 * Eliminar el registro del cliente
		 */
		public function Eliminar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Status = 'ELIMINADO';
					$this->Modelo->ModificarEstadoRegistro($Status, $Id);
					unset($Id, $Status);
					exit();
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * frmAsignacionAgentes()
		 * 
		 * Pantalla de reasignacion de Agentes
		 */
		public function frmReAsignacionAgentes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$ConsultaAgentesAsignados = $this->Modelo->ConsultaRegistroAsociado(NeuralCriptografia::DeCodificar($_POST['id'], APP), $IdSupervisor);
				$ConsultaAgentes = $this->Modelo->ConsultarAgentes($IdSupervisor);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consultas', $ConsultaAgentesAsignados);
				$Plantilla->Parametro('Agentes', $ConsultaAgentes);
				$Plantilla->Parametro('IdCliente', $_POST['id']);
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Asignacion', 'Asignacion.html')));
				unset($ConsultaAgentesAsignados, $IdSupervisor, $ConsultaAgentes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ReducirAgentes()
		 * 
		 * Select de agentes disponibles para asignar
		 */
		public function ReducirAgentes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$Llaves = AppPost::LimpiarInyeccionSQL($_POST);
					$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
					$ConsultaAgentes = $this->Modelo->ConsultarAgentesNoEliminados($IdSupervisor);
					$Llaves = AppUtilidades::ArregloEstados($Llaves);
					$ConsultaAgentes = AppUtilidades::DescartarRegistro($ConsultaAgentes, $Llaves);
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro("Agentes", $ConsultaAgentes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Asignacion', 'select.html')));
					exit();	
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * ReasignarAgente()
		 * 
		 * Prepara datos de reasignacion de agentes
		 */
		public function ReasignarAgente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVacios($_POST) == false){
						$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
						$IdAsignacion = $this->Modelo->ConsultaDatosAsignacion(NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP));
						unset($DatosPost['IdDatoAgenda']);
						$Fecha = AppFechas::ObtenerDatetimeActual();
						$Condicones = $this->Modelo->ConsultaIdAgenteAsignado($IdAsignacion[0]['IdAsignacionRegistro']);
						$this->Modelo->ReasignarAgentes($DatosPost, $Condicones, $Fecha);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('agendaTelefonica', 'Asignacion', 'ExitoAsignacion.html')));
						unset($DatosPost, $IdAsignacion, $Fecha, $Plantilla);
						exit();				
					}
				}
			}
		}
	}

