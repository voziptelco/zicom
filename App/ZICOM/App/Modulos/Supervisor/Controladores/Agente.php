<?php

	class Agente extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agente', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * frmListado()
		 *
		 * Pantalla de listado
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarAgentes($IdSupervisor);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agente', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmAgregar()
		 * 
		 * Pantalla de Agregar nuevo Agente
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Nombres', '* Campo Requerido');
				$Validacion->Requerido('ApellidoPaterno', '* Campo Requerido');
				$Validacion->Requerido('ApellidoMaterno', '* Campo Requerido');
				$Validacion->Requerido('Extension', '* Campo Requerido');
				$Validacion->Digitos('Extension','* Solo se pueden ingresar números');
				$Validacion->Requerido('Correo', '* Campo Requerido');
				$Validacion->Requerido('Usuario', '* Campo Requerido');
				$Validacion->Requerido('Password', '* Campo Requerido');
				$Validacion->Requerido('IdProvincia', '* Campo Requerido');
				$Validacion->Email('Correo', '* Ingresa un correo valido');
				$Validacion->CampoIgual('RepitePassword', 'Password', '* La contraseña no coincide');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarAgente'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agente', 'Agregar', 'Agregar.html')));
				unset($Provincias, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * Agregar()
		 * 
		 * Prepara los datos para guardarlos en la base de datos
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					if($_POST['Password'] == $_POST['RepitePassword'] ) {

						unset($_POST['Key'], $_POST['RepitePassword']);
						if(AppPost::DatosVaciosOmitidos($_POST, array('Cargo', 'TelefonoCasa', 'TelefonoMovil', 'Status', 'ExportarExcel', 'IdProvincia') ) == false) {
							$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TelefonoCasa', 'TelefonoMovil', 'Correo', 'TipoCanal', 'ExportarExcel', 'Usuario', 'TeimpoLlamada','Password', 'Status'));
							$Perfil = $this->Modelo->BuscarPerfil('Agente');
							$DatosPost['Status'] = (isset($DatosPost['Status'])) ? 'ACTIVO' : 'SUSPENDIDO';
							$DatosPost['ExportarExcel'] = (isset($DatosPost['ExportarExcel'])) ? 'ACTIVO' : 'INACTIVO';
							$DatosUsuario = array("IdPerfil" => $Perfil[0]['IdPerfil'], "Usuario" => $DatosPost['Usuario'], "Password" => hash('sha256', $DatosPost['Password']), 'Status' => $DatosPost['Status']);
							$Consulta = $this->Modelo->ConsultaUsuarioExistente($DatosPost['Usuario'], $DatosPost['Correo']);
							if($Consulta['Cantidad'] > 0){
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agente', 'Error', 'ErrorRegistroDuplicado.html')));
								unset($DatosPost, $Perfil, $DatosUsuario, $Consulta, $Plantilla);
								exit();
							}
							else{
								$this->Modelo->GuardarDatosAgente($DatosUsuario);
								$Usuario = $this->Modelo->BuscarIdUsuario($DatosUsuario['Usuario']);
								$DatosInformacionUsuario = array("idUsuario" => $Usuario[0]['IdUsuario'], "Nombres" => $DatosPost['Nombres'], "ApellidoPaterno" => $DatosPost['ApellidoPaterno'], "ApellidoMaterno" => $DatosPost['ApellidoMaterno'], "Cargo" => $DatosPost['Cargo'], "TelefonoCasa" => $DatosPost['TelefonoCasa'], "TelefonoMovil" => $DatosPost['TelefonoMovil'], "Correo" => $DatosPost['Correo'], "Extension" => $DatosPost['Extension'], "TipoCanal" => "SIP", "ExportarExcel" => $DatosPost['ExportarExcel'], "IdProvincia" => $DatosPost['IdProvincia']);
								$this->Modelo->GuardarInformacionAgente($DatosInformacionUsuario);
								$DatosAgenteAsignado = array("IdSupervisor" => $this->Informacion['Informacion']['idUsuario'], "IdAgente" => $Usuario[0]['IdUsuario']);
								$this->Modelo->GuardarAgenteAsignado($DatosAgenteAsignado);
								if(isset($_FILES['Imagen']) AND !empty($_FILES['Imagen']['name'])){
									$Tipo = $_FILES['Imagen']['type'];
									if($Tipo == 'image/jpg' OR $Tipo == 'image/png' OR $Tipo == 'image/gif' OR $Tipo == 'image/jpeg' OR $Tipo == 'image/bmp'){
										$fp = fopen($_FILES['Imagen']['tmp_name'], "r");
	  									$tfoto = fread($fp, filesize($_FILES['Imagen']['tmp_name']));
							 			fclose($fp);
							 			$resp = $this->Modelo->GuardarImagen(array("Imagen"=>$tfoto, "Tipo"=>$Tipo, "IdUsuario" => $Usuario[0]['IdUsuario']));	
									}
								}
							 	$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agente', 'Agregar', 'Exito.html')));
								unset($DatosPost, $Perfil, $DatosUsuario, $Consulta, $Usuario, $DatosInformacionUsuario, $DatosAgenteAsignado, $Plantilla, $fp, $tfoto, $resp);
								exit();
							}
						}
					}
				}
			}
		} 
		
		/**
		 * Metodo Publico
		 * frmEditar()
		 * 
		 * Pantalla de Edición de datos de Agente
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Imagen = $this->Modelo->ConsultarImagen($IdSupervisor);
				$Consulta = $this->Modelo->ConsultarUnAgente($IdSupervisor);
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Nombres', '* Campo Requerido');
				$Validacion->Requerido('ApellidoPaterno', '* Campo Requerido');
				$Validacion->Requerido('ApellidoMaterno', '* Campo Requerido');
				$Validacion->Requerido('Extension', '* Campo Requerido');
				$Validacion->Digitos('Extension','* Solo se pueden ingresar números');
				$Validacion->Requerido('Correo', '* Campo Requerido');
				$Validacion->Requerido('Provincia', '* Campo Requerido');
				$Validacion->Email('Correo', '* Ingresa un correo valido');
				$Validacion->CampoIgual('RepitePassword', 'Password', '* La contrasea no coincide');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdUsuario', $_POST['Id']);
				if(isset($Imagen[0]['Imagen']))
					$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarAgente'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Agente', 'Editar', 'Editar.html')));
				unset($IdSupervisor, $Consulta, $Validacion, $Plantilla, $Imagen);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Actualizar()
		 * 
		 * Prepara los datos de Actualizacion del Agente
		 */
		public function Actualizar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if((AppPost::DatosVaciosOmitidos($_POST, array('Cargo', 'TelefonoCasa', 'TelefonoMovil', 'Password', 'RepitePassword'))) == false ) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TelefonoCasa', 'TelefonoMovil', 'Correo', 'Usuario', 'Password', 'RepitePassword', 'Status', 'TipoCanal', 'TiempoLlamada', 'ExportarExcel', 'IdProvincia'));
						$IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
						$IdInformacion = $this->Modelo->BuscarIdInformacionUsuario($IdUsuario);
						$DatosPost['ExportarExcel'] = (isset($DatosPost['ExportarExcel'])) ? 'ACTIVO' : 'INACTIVO';
						$Matriz = array('Nombres' => $DatosPost['Nombres'], 'ApellidoPaterno' => $DatosPost['ApellidoPaterno'], 'ApellidoMaterno' => $DatosPost['ApellidoMaterno'], 'Cargo' => $DatosPost['Cargo'], 'TelefonoCasa' => $DatosPost['TelefonoCasa'], 'TelefonoMovil' => $DatosPost['TelefonoMovil'], 'Correo' => $DatosPost['Correo'], 'Extension' => $DatosPost['Extension'], 'TipoCanal' => "SIP", 'ExportarExcel' => $DatosPost['ExportarExcel'], 'IdProvincia' => $DatosPost['IdProvincia']);
						$Omitidos = array('IdInformacion', 'idUsuario', 'RUC', 'RazonSocial', 'Direccion', 'TelefonoCasa2', 'TelefonoMovil2', 'TiempoLlamada', 'Status', 'callerid');
						$Condicion = array('IdInformacion' => $IdInformacion[0]['IdInformacion']);
						$this->Modelo->ActualizaInformacionAgente($Matriz, $Condicion, $Omitidos);
						if(isset($_POST['Password']) OR isset($_POST['Status'])){
							$DatosPost['Password'] = (!empty($DatosPost['Password'])) ? ($DatosPost['Password'] == $DatosPost['RepitePassword']) ? $DatosPost['Password'] : '' : '';
							$DatosPost['Status'] = (isset($DatosPost['Status'])) ? 'ACTIVO' : 'SUSPENDIDO';
							if(empty($DatosPost['Password'])){
								$Matriz = array('Status' => $DatosPost['Status']);
								$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
								$Condicion = array('IdUsuario' => $IdUsuario);
								$this->Modelo->ActualizarUsuarioAgente($Matriz, $Condicion, $Omitidos);
							}
							else {
								$Matriz = array('Password' =>  hash('sha256', $DatosPost['Password']), 'Status' => $DatosPost['Status']);
								$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario');
								$Condicion = array('IdUsuario' => $IdUsuario);
								$this->Modelo->ActualizarUsuarioAgente($Matriz, $Condicion, $Omitidos);
							}
						}
						if(isset($_FILES['Imagen']) AND !empty($_FILES['Imagen']['name'])){
							$fp = fopen($_FILES['Imagen']['tmp_name'], "r");
							$tfoto = fread($fp, filesize($_FILES['Imagen']['tmp_name']));
			 				fclose($fp);
			 				$Imagen = $this->Modelo->ConsultarImagen($IdUsuario);
			 				if(isset($Imagen[0]['Imagen'])){
			 					$this->Modelo->ActualizarFoto(array('Imagen'=>$tfoto, 'Tipo'=>$_FILES['Imagen']['type']), $IdUsuario);
			 				}else{
			 					$this->Modelo->GuardarImagen(array("Imagen"=>$tfoto, "Tipo"=>$_FILES['Imagen']['type'], "IdUsuario" => $IdUsuario));
			 				}	
						}		
						unset($DatosPost, $IdUsuario, $IdInformacion ,$Matriz, $Omitidos, $Condicion, $Imagen, $tfoto, $fp);
						exit();
					}
				}
			}
		}
		
		/**
		 * Metod Publico
		 * Eliminar()
		 * 
		 * Modifica el estado del registro a ELIMINADO
		 */
		public function Eliminar() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdUsuario' => $Id);
					$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
					$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
				}
			}
		}
	}
