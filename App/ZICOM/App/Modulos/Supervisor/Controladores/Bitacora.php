<?php

	class Bitacora extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Piblico 
		 * Index()
		 * 
		 * Pantalla principal 
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Bitacora', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * frmListado()
		 * 
		 * Lista la bitacora
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarBitacoraTotal($IdSupervisor);
				if($Consulta > 0):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('ToltalRegistros', $Consulta);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Bitacora', 'Listado', 'Listado.html')));
					unset($IdSupervisor, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Bitacora', 'Error', 'NoResultados.html')));
					unset($IdSupervisor, $Consulta, $Plantilla);
					exit();
				endif;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ListadoPagina()
		 * 
		 * consulta registros segun la pagina
		 */
		public function ListadoPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];	
				$Consulta = $this->Modelo->ConsultarBitacora($this->Informacion['Informacion']['idUsuario'], $Inicio, $NumeroRegistros, $Busqueda);		
				if($Consulta['Cantidad']){
						$total =($Busqueda == "")? $DatosPost['ToltalRegistros']:$Consulta['Cantidad'];
						unset($Consulta['Cantidad']);			        
				        $Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
	     				echo json_encode($Consulta);
	     				unset($DatosPost, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);	
					}
					else{
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;
					}
			}
		}
		
	}
?>