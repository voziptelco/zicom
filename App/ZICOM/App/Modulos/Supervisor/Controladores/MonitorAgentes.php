<?php

	class MonitorAgentes extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MonitorAgentes', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}

		/**
		 * Metodo Publico
		 * frmListado()
		 *
		 * Pantalla de listado
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarAgentes($IdSupervisor);
				$Status = new AppMonitorAgentes();
				$Consulta = $Status->StatusAgente($IdSupervisor, $Consulta);
				if($Consulta != 'Error'):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Consulta', $Consulta);
					$Plantilla->Filtro('Cifrado', function($Parametro){
						return NeuralCriptografia::Codificar($Parametro, APP);
					});
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MonitorAgentes', 'Listado', 'Listado.html')));
					unset($IdSupervisor, $Status, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('MonitorAgentes', 'Error', 'Error.html')));
					unset($IdSupervisor, $Status, $Consulta, $Plantilla);
					exit();
				endif;	
			}
		}

		/**
		 * Metodo Publico
		 * Monitor()
		 *
		 * configura datos para monitorear el agente
		 */
		public function Monitor(){
		   if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
		      if (isset($_POST)) {
		        $DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
		        $Agente = $this->Modelo->ConsultarAgente(NeuralCriptografia::DeCodificar($DatosPost['Id']));
		         if($Agente == true AND is_array($Agente)){
		            $Consulta = $this->Modelo->ConsultarConfiguracionConmutador();
		            if ($Consulta == true AND is_array($Consulta) == true) {
		               $Configuracion = $Consulta[0];
		               $Configuracion['password'] = NeuralCriptografia::DeCodificar($Consulta[0]['password'], APP);
		               $Conector = new AppAsterisk($Configuracion);
		               $Respuesta = $Conector->MonitorAgentes($Agente[0]['TipoCanal'], $this->Informacion['Informacion']['Extension'], $Agente[0]['Extension'] );
		               echo json_encode(array('Respuesta'=>$Respuesta, 'Agente'=>$Agente[0]['Nombres']));
		               unset($DatosPost, $Consulta, $Configuracion, $Conector);
		               exit();
		            }else{
		               $Plantilla = new NeuralPlantillasTwig(APP);
		               echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Index', 'Error', 'ErrorElementosRequeridos.html')));
		               unset($Plantilla);
		               exit();
		            }
		         }
		         unset($DatosPost, $Condiciones, $Agente, $Supervisor);
		         exit();
		      }
		   }
		}		
	}
