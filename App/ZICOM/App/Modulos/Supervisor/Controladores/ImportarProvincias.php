<?php
	
	class ImportarProvincias extends Controlador{
		
		var $Informacion;

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
 		
 		/**
 		 * Metodo Publico 
 		 * frmSubirArchivo()
 		 * 
 		 * Formulario para seleccionar Archivo
 		 */
		public function frmSubirArchivo(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']){
				$Plantilla = new NeuralPlantillasTwig(APP);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Importacion', 'AgregarArchivoExcel.html')));
				unset($Plantilla);
				exit();	
			}
		}
		
		/**
		 * Metodo Publico
		 * CargarArchivoExcel()
		 *
		 * Pantalla para analisis del archivo excel
		 */
		public function CargarArchivoExcel(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$ArchivoCargado = AppArchivos::CargarArchivo($_FILES);
				if($ArchivoCargado != 'ERROR'){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('ruta', NeuralCriptografia::Codificar($ArchivoCargado, APP));
					$Plantilla->Parametro('extension', $_FILES["file-0"]['type']);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Examinar', 'ExaminarArchivo.html')));
					unset($ArchivoCargado, $Plantilla);
					exit();
				}
				else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Error', 'ErrorCargaArchivo.hmtl')));
					unset($Plantilla);
					exit();
				}	
			}
		}
		
		/**
		 * Metodo Publico
		 * ExaminarArchivo()
		 *
		 * Pantalla muestra el resultado del analisis del archivo excel
		 */
		public function ExaminarArchivo(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST)){
					$Ruta = NeuralCriptografia::DeCodificar($_POST['ruta'], APP);
					$Archivo = new AppPHPExcel;
					$Archivo->LeerExcelColumnas($Ruta, $_POST['extension']);
					$ColumnasRequeridas = array('Provincia', 'Distrito');
					$ColumnasArchivo = $Archivo->LeerColumnas();
					$ColumnasArchivo = AppPost::ConvertirTextoUcwords($ColumnasArchivo);
					set_time_limit(0);
					if(AppArreglosExcel::ComparaColumnas($ColumnasArchivo, $ColumnasRequeridas)){
						$DatosArchivo = $Archivo->DatosExcel();
						$DatosArchivo = AppArreglosExcel::EliminarRepeticionesArchivo($DatosArchivo);	
						$DatosArchivo = AppArreglosExcel::FormatoTexto($DatosArchivo);						
						$DatosArchivo = AppArreglosExcel::CombinarMatrizLLave($DatosArchivo, $ColumnasRequeridas);
						$DatosArchivo = AppArreglosExcel::BuscarExistencias($DatosArchivo);
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Ruta', NeuralCriptografia::Codificar($Ruta,APP));
						$Plantilla->Parametro('Extension', $_POST['extension']);
						$Plantilla->Parametro('Registros', $DatosArchivo);
						$Errores = AppArreglosExcel::EliminarErrores($DatosArchivo);
						if(count($Errores) == 0){
							$Plantilla->Parametro('Datos', true);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Error', 'ErrorNoElementos.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $DatosArchivo, $ConsultaGerencias, $ConsultaCarteras, $ConsultaProvincias, $ConsultaDistritos, $ConsultaClientes, $Omitidos, $ArregloEstados, $Plantilla);
							exit();
						}
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Examinar', 'ListadoRegistroExcel.html')));
						unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $DatosArchivo, $ConsultaGerencias, $ConsultaCarteras, $ConsultaProvincias, $ConsultaDistritos, $ConsultaClientes, $Omitidos, $ArregloEstados, $Plantilla);
						exit();
					}
					else {
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Error', 'ErrorArchivoInvalido.html')));
						unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $Plantilla);
						exit();
					}
				}
			}
		}
		
			/**
		 * Metodo Publico
		 * GuardarArchivoBaseDatos()
		 *
		 * Proceso de almacenado de la informacion importada a la base de datos
		 */
		public function GuardarArchivoBaseDatos(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST)){
					$Ruta = NeuralCriptografia::DeCodificar($_POST['Ruta'], APP);
					$Archivo = new AppPHPExcel;
					$Archivo->LeerExcelColumnas($Ruta, $_POST['Extension']);
					$ColumnasRequeridas = array('Provincia', 'Distrito');
					$ColumnasArchivo = $Archivo->LeerColumnas();
					$ColumnasArchivo = AppPost::ConvertirTextoUcwords($ColumnasArchivo);
					set_time_limit(0);
					if(AppArreglosExcel::ComparaColumnas($ColumnasArchivo, $ColumnasRequeridas)){
						$DatosArchivo = $Archivo->DatosExcel();
					    $DatosArchivo = AppArreglosExcel::EliminarRepeticionesArchivo($DatosArchivo);
						$DatosArchivo = AppArreglosExcel::FormatoTexto($DatosArchivo);
						$DatosArchivo = AppArreglosExcel::CombinarMatrizLLave($DatosArchivo, $ColumnasRequeridas);
						$DatosArchivo = AppArreglosExcel::BuscarExistencias($DatosArchivo);
					    $DatosArchivo = AppArreglosExcel::EliminarErrores($DatosArchivo);
					    $tamaño = count($DatosArchivo);
						if($tamaño > 0){
							$Provincias = AppArreglosExcel::ExtraerColumna($DatosArchivo, "Provincia");
							$Provincias = AppUtilidades::AgruparMatriz($Provincias, 'Descripcion');							
							$ArregloEstados = AppArreglosExcel::BuscarExistenciasArray($Provincias, array('Descripcion'), 'tbl_provincia');
							$NuevasProvincias = AppArreglosExcel::MatrizNueva($Provincias, $ArregloEstados, 'NUEVO REGISTRO');
							if(count($NuevasProvincias) > 0){
								$this->Modelo->AgregarProvincias($NuevasProvincias);	
							}
							$ConsultaProvincias = $this->Modelo->ConsultaProvincias();
							$DatosArchivo = AppArreglosExcel::SustituyeIndicesProvincias($DatosArchivo, $ConsultaProvincias, 'Provincia', 'IdProvincia');
							$this->Modelo->AgregarDistritos($DatosArchivo);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Exito', 'Exito.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $ConsultaGerencias, $ConsultaCarteras, $ConsultaProvincias, $ConsultaDistritos, $Omitidos, $ArregloEstados, $DatosArchivoNuevos, $DatosArchivoActualizar, $Plantilla);
							exit();
						}
						else {
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Error', 'ErrorNoElementos.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $DatosArchivo, $DatosArchivoNuevos, $Plantilla);
							exit();
						}
					}
					else {
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarProvincias', 'Error', 'ErrorArchivoInvalido.html')));
						unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $DatosArchivo, $Plantilla);
						exit();
					}
				}
			}
		}
	}