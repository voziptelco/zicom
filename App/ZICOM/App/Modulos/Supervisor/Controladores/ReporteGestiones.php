<?php
	
	class ReporteGestiones extends Controlador{

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();			
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ReporteGestion', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmFiltroGestiones()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de Gestiones
		 */
		public function frmFiltroGestiones(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Agentes = $this->Modelo->ConsultarAgentes($this->Informacion['Informacion']['idUsuario']);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Agentes', $Agentes);
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ReporteGestion', 'Filtrar', 'FiltroGestiones.html')));
				unset($Agentes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * GeneraReporte()
		 * 
		 * Genera reporter de gestion Telefonica / de Campo
		 */
		public function GeneraReporte(){
			set_time_limit(0);
			if($_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					if(isset($DatosPost['Agente'])){
						$DatosPost['tbl_agentes_asignado_supervisor.IdAgente'] = $DatosPost['IdAgente'];
						unset($DatosPost['IdAgente']);
					}
					if(isset($DatosPost['TipoGestion']) AND $DatosPost['TipoGestion'] == "Telefonica"):
						$Criterios = AppCriteriosFiltro::GeneraCondicones($DatosPost);	
						$Consulta = $this->Modelo->ConsultarGestionesTelefonicas($this->Informacion['Informacion']['idUsuario'], $Criterios);	
						$DatosColumnas['Columnas'] = array('A1'=>'Agente', 'B1'=>'Extension', 'C1'=>'Titular', 'D1'=>'Telefono', 'E1'=>'Fecha Gestion', 'F1'=>'Inicio Llamada', 'G1'=>'Tirmino Llamada', 'H1'=>'Duracion', 'I1'=>'Estado', 'J1'=>'Detalle', 'K1'=>'Compromiso', 'L1'=>'Codigo');
						$DatosColumnas['Datos'] = array('A'=>'Nombres', 'B'=>'Extension', 'C'=>'NombreTitular', 'D'=>'NumeroTelefono', 'E'=>'FechaHora_Captura', 'F'=>'calldate', 'G'=>'Fin', 'H'=>'duration', 'I'=>'StatusLlamada', 'J'=>'Observaciones', 'K'=>'Compromiso', 'L'=>'Codigo');
						$DatosColumnas['Limites'] = "A1:L1"; 
					elseif(isset($DatosPost['TipoGestion']) AND $DatosPost['TipoGestion'] == "de Campo"):
						if(isset($DatosPost['NumeroTelefono'])){
							$Criterio = "(tbl_datos_agenda.Telefono1 = '". $DatosPost['NumeroTelefono']."' OR tbl_datos_agenda.Telefono2 = '". $DatosPost['NumeroTelefono']."' OR tbl_datos_agenda.Telefono3 = '". $DatosPost['NumeroTelefono']."' OR tbl_datos_agenda.Telefono4 = '". $DatosPost['NumeroTelefono']."') ";
						}
						unset($DatosPost['NumeroTelefono']);
						$Criterios = AppCriteriosFiltro::GeneraCondicones($DatosPost);
						if($Criterio){
							$Criterios[] = $Criterio;
						}
						$Consulta = $this->Modelo->ConsultarGestionesCampo($this->Informacion['Informacion']['idUsuario'], $Criterios);
						$DatosColumnas['Columnas'] = array('A1'=>'Agente', 'B1'=>'Titular', 'C1'=>'Direccion', 'D1'=>'Fecha Gestion', 'E1'=>'Gestor', 'F1'=>'Fecha Visita', 'G1'=>'Detalle', 'H1'=>'Compromiso', 'I1'=>'Codigo');
						$DatosColumnas['Datos'] = array('A'=>'Nombres', 'B'=>'NombreTitular', 'C'=>'DomicilioContractual', 'D'=>'FechaHora_Captura', 'E'=>'Nombre_Gestor', 'F'=>'Fecha_Visita', 'G'=>'Observaciones', 'H'=>'Compromiso', 'I'=>'Codigo');
						$DatosColumnas['Limites'] = "A1:I1";
					elseif(isset($DatosPost['TipoGestion']) AND $DatosPost['TipoGestion'] == "Manual"):
						if(isset($DatosPost['NumeroTelefono'])){
							$Criterio = "(tbl_datos_agenda.Telefono1 = '". $DatosPost['NumeroTelefono']."' OR tbl_datos_agenda.Telefono2 = '". $DatosPost['NumeroTelefono']."' OR tbl_datos_agenda.Telefono3 = '". $DatosPost['NumeroTelefono']."' OR tbl_datos_agenda.Telefono4 = '". $DatosPost['NumeroTelefono']."') ";
						}
						unset($DatosPost['NumeroTelefono']);
						$Criterios = AppCriteriosFiltro::GeneraCondicones($DatosPost);
						if($Criterio){
							$Criterios[] = $Criterio;
						}
						$Consulta = $this->Modelo->ConsultarGestionesManuales($this->Informacion['Informacion']['idUsuario'], $Criterios);
						$DatosColumnas['Columnas'] = array('A1'=>'Agente', 'B1'=>'Titular', 'C1'=>'Direccion', 'D1'=>'Fecha Gestion', 'E1'=>'Detalle', 'F1'=>'Compromiso', 'G1'=>'Codigo');
						$DatosColumnas['Datos'] = array('A'=>'Nombres', 'B'=>'NombreTitular', 'C'=>'DomicilioContractual', 'D'=>'FechaHora_Captura', 'E'=>'Observaciones', 'F'=>'Compromiso', 'G'=>'Codigo');
						$DatosColumnas['Limites'] = "A1:G1";
					endif;
					if($Consulta['Cantidad'] > 0){
						unset($Consulta['Cantidad']);
						$Consulta = ($DatosPost['TipoGestion'] == "Telefonica") ? AppUtilidades::ColumnaFinalLlamada($Consulta) : $Consulta;
						$Consulta = ($DatosPost['TipoGestion'] == "Telefonica") ? AppFiltrosRapidos::FormatoTelefonoSinPrefijo($Consulta) : $Consulta; 
						$Colores = $this->Modelo->ConsultaColores();
						header("X-XSS-Protection:0");
	     				header('Set-Cookie: fileDownload=true; path=/');
	                    $Nombre = 'Gestion y Cobranzas, ZICOM Group';
	                    $Titulo = 'Registros Gestion '.$DatosPost['TipoGestion'].' '.str_replace('-', '_', str_replace(':', '_', AppFechas::ObtenerDatetimeActual()));
	         			$Categoria = 'categorias';
	     				$Excel = new AppExportarExcel;     				
	     			 	$Excel->ExportarInfoUsuario($Nombre, $Titulo, $Categoria);
				        $Excel->EstablecerValoresDeArreglo($DatosColumnas['Columnas']);
				        $Excel->EstablecerAlineacionHorizontal($DatosColumnas['Limites'], 'Centrado');
				        $Excel->EstablecerColorFondo($DatosColumnas['Limites'], 'EEEEEE');
				        $Excel->EstablecerBordeDelgado($DatosColumnas['Limites']);
				        $Excel->ExportarArregloDatos($Consulta, 2, $DatosColumnas['Datos'], 'Codigo', $Colores);
				        if($DatosPost['OpcionReporte'] == "Correo"):
			       	 		$Ruta = $Excel->ExportarSalvar('xlsx', $Titulo);
			       	 		$Correo = $this->Modelo->ConsultaCorreo($this->Informacion['Informacion']['idUsuario']);
				      		AppUtilidades::EnviarCorreoArchivo($Ruta, $Correo['Correo'], $DatosPost['TipoGestion']);
	       	 			else:
				      		$Excel->ExportarArchivoExcel('xlsx', $Titulo);
			      		endif;
				        unset($Nombre, $Titulo, $Categoria, $Excel);
				        exit();	
			        }
			        else{
			        	unset($DatosPost, $Criterios, $Consulta);
						echo "fail";	
			        }
				}
			}
		}
	}