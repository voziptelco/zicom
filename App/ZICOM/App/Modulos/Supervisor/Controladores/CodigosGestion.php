<?php
	
	class CodigosGestion extends Controlador{
		
		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CodigosGestion', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * frmListado()
		 *
		 * Pantalla de listado
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarCodigos();
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CodigosGestion', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmAgregar()
		 * 
		 * Pantalla de Agregar nuevo Codigo
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Codigo', '* Campo Requerido');
				$Validacion->Digitos('Codigo','* Solo se pueden ingresar números');
				$Validacion->Requerido('Descripcion', '* Campo Requerido');
				$Validacion->Requerido('Color', '*Campo Requerido');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarCodigos'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CodigosGestion', 'Agregar', 'Agregar.html')));
				unset($Provincias, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * Agregar()
		 * 
		 * Prepara los datos para guardarlos en la base de datos
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('Codigo')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('Codigo'));
						$Consulta = $this->Modelo->ConsultarCodigoExistente($DatosPost['Codigo'], $DatosPost['Descripcion']);
						if($Consulta['Cantidad'] > 0){
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CodigosGestion', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $Consulta, $Plantilla);
							exit();
						}
						else{
							$this->Modelo->GuardarCodigo($DatosPost);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CodigosGestion', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $Usuario, $DatosInformacionUsuario, $DatosAgenteAsignado, $Plantilla);
							exit();
						}
					}
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * frmEditar()
		 * 
		 * Pantalla de Edición de datos de Agente
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdCodigo = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Consulta = $this->Modelo->ConsultarCodigo($IdCodigo);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Codigo', '* Campo Requerido');
				$Validacion->Digitos('Codigo','* Solo se pueden ingresar numeros');
				$Validacion->Requerido('Descripcion', '* Campo Requerido');
				$Validacion->Requerido('Color', '*Campo Requerido');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta[0]);
				$Plantilla->Parametro('IdCodigo', $_POST['Id']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarCodigos'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CodigosGestion', 'Editar', 'Editar.html')));
				unset($IdSupervisor, $Consulta, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * Agregar()
		 * 
		 * Prepara los datos para guardarlos en la base de datos
		 */
		public function Editar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVaciosOmitidos($_POST, array('Codigo')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('Codigo', 'Color'));
						unset($DatosPost['IdCodigo']);
						$IdCodigo = array('IdCodigo' => NeuralCriptografia::DeCodificar($_POST['IdCodigo'], APP));
						$Omitidos = array('IdCodigo', 'Status');
						Ayudas::print_r($DatosPost);
						$this->Modelo->EditarCodigo($IdCodigo, $DatosPost, $Omitidos);
						unset($DatosPost, $IdCodigo, $Omitidos, $Plantilla);
						exit();
					}
				}
			}
		}
		
			/**
		 * Metod Publico
		 * Eliminar()
		 * 
		 * Modifica el estado del registro a ELIMINADO
		 */
		public function Eliminar() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdCodigo' => $Id);
					$Omitidos = array('IdCodigo', 'Codigo', 'Descripcion', 'Color');
					$this->Modelo->Eliminar($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}