<?php
	
	class HistorialDeCarga extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla principal 
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('HistorialDeCarga', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * public function frmListado()
		 * 
		 * Pantalla de listado
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarHistorialCarga($this->Informacion['Informacion']['idUsuario']);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				$Plantilla->Filtro('Archivo', function($Parametro){
					return AppFiltrosRapidos::QuitaPrefijoArchivo($Parametro);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('HistorialDeCarga', 'Listado', 'Listado.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * frmEditar()
		 * 
		 * Pantalla de Edicion 
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarRegistro(NeuralCriptografia::DeCodificar($_POST['Id'], APP));
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Remoto('FechaHoraCarga', NeuralRutasApp::RutaUrlApp('Supervisor/HistorialDeCarga/CompararFechaActual'), 'POST', false, "*La fecha no debe ser anterior a la fecha actual");
				$Validacion->Requerido('FechaHoraCarga');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta[0]['FechaHoraCarga']);
				$Plantilla->Parametro('Id', $_POST['Id']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarCarga'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('HistorialDeCarga', 'Editar', 'Editar.html')));
				unset($Validacion, $Plantilla);
			}
		}
		
		/**
		 * Metodo Publico
		 * CompararFechaActual()
		 * 
		 * Compara la fecha actual con la que se ingresa
		 */
		public function CompararFechaActual(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST['FechaHoraCarga'])){
					echo AppFiltrosRapidos::ComparaTiempo($_POST['FechaHoraCarga']);
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * Editar()
		 * 
		 * Prepara datos para una reprogrmacion
		 */
		public function Editar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$Omitidos = array('IdArchivosProgramados', 'IdSupervisor', 'NombreArchivo', 'Ruta', 'Extension', 'FechaHoraCaptura','Status', 'ArchivoLog', 'FechaHoraTerminado');
					$DatosPost['FechaHoraCarga'] = AppFechas::ValidaHora($DatosPost['FechaHoraCarga']);
					$Arreglo = array('FechaHoraCarga' => AppFechas::FormatoFechaTiempo($DatosPost['FechaHoraCarga']));
					$Condicion = array('IdArchivosProgramados' => NeuralCriptografia::DeCodificar($DatosPost['Id']));
					$this->Modelo->Editar($Arreglo, $Condicion, $Omitidos);
					unset($DatosPost, $Omitidos, $Arreglo, $Condicion);	
				}
			}
		}
		
		/**
		 * Metod Publico
		 * Cancelar()
		 * 
		 * Modifica el estado del registro a CANCELADO
		 */
		public function Cancelar() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'CANCELADO');
					$Condicion = array('IdArchivosProgramados' => $Id);
					$Omitidos = array('IdArchivosProgramados', 'IdSupervisor', 'NombreArchivo', 'Ruta', 'Extension', 'FechaHoraCaptura', 'FechaHoraCarga', 'ArchivoLog', 'FechaHoraTerminado');
					$this->Modelo->Editar($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}