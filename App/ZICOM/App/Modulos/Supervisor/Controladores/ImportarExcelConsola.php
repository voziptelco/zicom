<?php

	class ImportarExcelConsola extends Controlador{
	
		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Modelo = new ImportarExcelConsola_Modelo();
		}
		
		/**
		 * Metodo Publico 
		 * CargarArchivoExcelConsola()
		 *
		 * Proceso de importación de registros desde consola
		 */
		public function CargarArchivoExcelConsola(){
			$Fecha = AppFechas::ObtenerFecha(AppFechas::ObtenerDateTimeActual());
			$Consulta = $this->Modelo->ConsultaTareasProgramadas($Fecha);
			$Consulta = AppUtilidades::ComparaFechaSistema($Consulta);
			if($Consulta){
				foreach($Consulta AS $Tarea){
					$Archivo = new AppPHPExcel();
					$Archivo->LeerExcelColumnas($Tarea['Ruta'], $Tarea['Extension']);
					$Columnas = $Archivo->LeerColumnas();
					$DatosArchivo = $Archivo->DatosExcel();
					$FilasOriginales = count($DatosArchivo);
					$DatosArchivo[0] = $Columnas;
					$Proceso = new AppArchivoConsola(APP, $Tarea['IdArchivosProgramados'], $Tarea['IdSupervisor']);
					$Proceso->init($DatosArchivo);
					$NumeroErrores = $this->Modelo->ConsultaNumeroErroresLog($Tarea['IdArchivosProgramados']);
					if($FilasOriginales == $NumeroErrores['Cantidad']):
						$Proceso->CambiarEstatusTarea("ERROR", "Estos Registros presentaron errores.", AppFechas::ObtenerDatetimeActual());
						echo "Estos Registros presentaron errores.";
					elseif($NumeroErrores['Cantidad'] == 0):
						$Proceso->CambiarEstatusTarea("TERMINADO", "Proceso terminado sin error.", AppFechas::ObtenerDatetimeActual());
					elseif($NumeroErrores['Cantidad'] > 0):
						if($NumeroErrores[0]['NumeroRegistro'] == 0):
							$Proceso->CambiarEstatusTarea("ERROR", "Error de archivo excel.", AppFechas::ObtenerDatetimeActual());	
							echo "Error de archivo excel.";
						else:
							$Proceso->CambiarEstatusTarea("TERMINADO", "Proceso terminado con los siguientes errores.", AppFechas::ObtenerDatetimeActual());
						endif;
					endif;
					$Actualizados = $Proceso->NumeroActualizaciones();
					$Nuevos = $Proceso->NumeroNuevos();
					$Array = $Proceso->ConsultaLog($Tarea['IdArchivosProgramados']);
    				$Alerta = array_pop($Array);
				    $Alertas[] = $Alerta;
					$Alertas[] = array('Registro', 'Columna', 'Tipo de Error', 'Estatus del Registro');
					$Alertas = array_merge($Alertas, $Array); 
					$Alertas[] = array('TotalCargados', $Nuevos['Cantidad'] + $Actualizados['Cantidad'], 'Nuevos', $Nuevos['Cantidad'], 'Actualizados', $Actualizados['Cantidad']);
					$Ruta =  implode(DIRECTORY_SEPARATOR, array(__SysNeuralFileRoot__,'App', 'ZICOM', 'Web', 'Log', AppFechas::ObtenerTiempoFechaActual().".csv"));
					$Log = AppUtilidades::EscribirErrores($Alertas, $Ruta, ',');
					if($Log){
						$Proceso->AgregarArchivoLog(AppFechas::ObtenerTiempoFechaActual().".csv");	
					}
				 	unset($DatosArchivo, $FilasOriginales, $Proceso, $NumeroErrores, $Actualizados, $Nuevos, $Array, $Alerta, $Alertas, $Archivo);
				}	
			}
			exit();
		}
	}
