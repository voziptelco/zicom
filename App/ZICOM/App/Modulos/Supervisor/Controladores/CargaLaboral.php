<?php

	class CargaLaboral extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * frmListado()
		 * 
		 * Muestra una lista de la carga asignada a un agente
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$Agentes = $this->Modelo->ConsultaAgentes($IdSupervisor);
				$Agentes = $this->Modelo->ConsultaCantidadRegistros($Agentes);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Agentes);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Listado', 'Listado.html')));
				unset($IdSupervisor, $Agentes, $Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * VerClientesAsignados()
		 * 
		 * Listas de Clientes asignados a un Agente
		 */
		public function VerClientesAsignados(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$TotalClientes = $this->Modelo->ConsultaRegistrosAsignadosAgentesTotal(NeuralCriptografia::DeCodificar($_POST['IdAgente']));
				$Paginas = ceil($TotalClientes / 5);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('NumeroPaginas', $Paginas);
				$Plantilla->Parametro('TotalRegistros', $TotalClientes);
				$Plantilla->Parametro('IdAgente', $_POST['IdAgente']);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Listado', 'Partes', 'VerClientes.html')));
				unset($TotalClientes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * VerClientesAsignados()
		 * 
		 * Listas de Clientes asignados a un Agente
		 */
		public function VerClientesAsignadosPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];	
				$Consulta = $this->Modelo->ConsultaRegistrosAsignadosAgentes(NeuralCriptografia::DeCodificar($_POST['IdAgente']), $Inicio, $NumeroRegistros, $Busqueda);		
				if($Consulta['Cantidad']){
						$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
						unset($Consulta['Cantidad']);
						$Consulta = AppFiltrosRapidos::AplicarFiltrosCifradoMoneda($Consulta);			        
				        $Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
	     				echo json_encode($Consulta);
	     				unset($DatosPost, $ArregloCriterios, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);	
					}
					else{
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;
					}
			}
		}
		
		/**
		 * Metodo Publico
		 * frmAsignarCargaLaboral()
		 * 
		 * Prepara una vista para seleccinar los criterios de asignacion de Carga Laboral
		 */
		public function frmAsignarCargaLaboral(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Carteras = $this->Modelo->ConsultarCarteras();
				$Gerencias = $this->Modelo->ConsultarGerencias();
				$Provincias = $this->Modelo->ConsultarProvincias();
				$Distritos = $this->Modelo->ConsultarDistritos();
				$MaximoSemanas = $this->Modelo->ConsultaMaximoSemanasAtraso();
				$Plantilla->Parametro('Carteras', $Carteras);
				$Plantilla->Parametro('Gerencias', $Gerencias);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Distritos', $Distritos);
				$Plantilla->Parametro('MaximoSemanas', $MaximoSemanas[0]);
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'AsignarCargaLaboral.html')));
				unset($Plantilla, $Carteras, $Gerencias, $Provincias);
				exit();
			}
		}
	
		/**
		 * Metodo Publico
		 * AsignarCargaLaboral()
		 * 
		 * Consulta el numero de registros a asignar y los Agentes disponibles
		 * y muestra una pantalla de Asignacion
		 */
		public function AsignarCargaLaboral(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloDatos = AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($DatosPost, array('minimoSemanas', 'maximoSemanas', 'minimoSaldo', 'maximoSaldo', 'minimoSaldoTotal', 'maximoSaldoTotal')));
					$ArregloCondiciones = AppUtilidades::ArregloCriterios($ArregloDatos, $DatosPost);
					$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
					$Consulta = $this->Modelo->ConsultaTotalRegistroNoAsignados($ArregloCondiciones, $IdSupervisor);					
					$Cantidad = $Consulta['Cantidad'];
					if($Cantidad > 0){
						unset($Consulta['Cantidad']);
						$Consulta = AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(json_encode($Consulta), APP));
						$ListaAgentes = $this->Modelo->ConsultaAgentes($IdSupervisor);
						$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
						$Validacion->Requerido('IdAgente', '* Campo Requerido');
						$Validacion->Requerido('NumAgentes', '* Campo Requerido');
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Consulta', $Consulta);
						$Plantilla->Parametro('Agentes', $ListaAgentes);
						$Plantilla->Parametro('CantidadRegistros', $Cantidad);
						$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAsignarCargaAgente'));
						$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'AsignarCargaAgente.html')));
						unset($DatosPost, $ArregloCondiciones, $IdSupervisor, $Consulta, $Cantidad, $Validacion, $ListaAgentes, $Plantilla, $Condicion);
						exit();
					}
					else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Error', 'NoExistenDatos.html')));	
						unset($DatosPost, $ArregloCondiciones, $IdSupervisor, $Consulta, $Cantidad, $Plantilla);
						exit();
					}
				}
			}	
		}
		
		/**
		 * Metodo Publico
		 * Agente2()
		 *
		 * Consulta para descartar un Agente duplicado
		 */
		public function Agente2(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$ListaAgentes = $this->Modelo->ConsultaAgentes($IdSupervisor);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente']);
				if($ListaAgentes != false){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Agentes', $ListaAgentes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'Agente2.html')));
					unset($IdSupervisor, $ListaAgentes, $Plantilla);
					exit();
				}
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('IdAgente', 'IdAgente2');
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Error', 'AgentesAgotados.html')));
				unset($IdSupervisor, $ListaAgentes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Agente3()
		 *
		 * Consulta para descartar un Agente duplicado
		 */
		public function Agente3(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$ListaAgentes = $this->Modelo->ConsultaAgentes($IdSupervisor);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente1']);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente2']);
				if($ListaAgentes != false){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Agentes', $ListaAgentes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'Agente3.html')));
					unset($IdSupervisor, $ListaAgentes, $Plantilla);
					exit();
				}
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('IdAgente', 'IdAgente3');
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Error', 'AgentesAgotados.html')));
				unset($IdSupervisor, $ListaAgentes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Agente4()
		 *
		 * Consulta para descartar un Agente duplicado
		 */
		public function Agente4(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$ListaAgentes = $this->Modelo->ConsultaAgentes($IdSupervisor);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente1']);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente2']);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente3']);
				if($ListaAgentes != false){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Agentes', $ListaAgentes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'Agente4.html')));
					unset($IdSupervisor, $ListaAgentes, $Plantilla);
					exit();
				}
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('IdAgente', 'IdAgente4');
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Error', 'AgentesAgotados.html')));
				unset($IdSupervisor, $ListaAgentes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Agente5()
		 *
		 * Consulta para descartar un Registro duplicado
		 */
		public function Agente5(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$ListaAgentes = $this->Modelo->ConsultaAgentes($IdSupervisor);
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente1']);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente2']);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente3']);
				$ListaAgentes = AppUtilidades::EliminaRegistro($ListaAgentes, $DatosPost['IdAgente4']);
				if($ListaAgentes != false){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Agentes', $ListaAgentes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'Agente5.html')));
					unset($IdSupervisor, $ListaAgentes, $Plantilla);
					exit();
				}
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('IdAgente', 'IdAgente5');
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'Error', 'AgentesAgotados.html')));
				unset($IdSupervisor, $ListaAgentes, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * AsignarCargaAgente()
		 * 
		 * Asocia un conjunto de Registros a un Agente 
		 */
		public function AsignarCargaAgente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$MatrizRegistros = json_decode(NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['ArregloRegistros'])), true);
    				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloAgentes = AppUtilidades::ArregloEstados($DatosPost);
					$ArregloAgentes = AppUtilidades::ArregloAgentes($ArregloAgentes, array('IdAgente', 'IdAgente2', 'IdAgente3', 'IdAgente4', 'IdAgente5'));
					$RegistrosRequeridos = array_slice($MatrizRegistros, 0, $DatosPost['TotalRegistrosAsignar']);
					$RegistrosRequeridos = $this->Modelo->ConsultaLlavesdeRegistrosNoAsignados($RegistrosRequeridos);
					$Fecha = AppFechas::ObtenerDatetimeActual();
					$MatrizDatosAsignacion = AppUtilidades::MatrizInsercion($RegistrosRequeridos, $ArregloAgentes, $Fecha);
					$this->Modelo->AsignarAgenteRegistrios($MatrizDatosAsignacion);
					$MatrizDatos = AppUtilidades::MatrizCargaLaboral($DatosPost['TotalRegistrosAsignar'], $Fecha);
					$this->Modelo->AsignacionDeCargaLaboral($MatrizDatos, $RegistrosRequeridos);
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('CargaLaboral', 'AsignarCarga', 'ExitoAsignacion.html')));
					unset($MatrizRegistros, $DatosPost, $ArregloAgentes, $RegistrosRequeridos, $Fecha,$MatrizDatosAsignacion, $MatrizDatos, $Plantilla);
					exit();
				}
			}
		}
	}