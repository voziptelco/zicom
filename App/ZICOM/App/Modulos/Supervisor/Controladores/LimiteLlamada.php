<?php

	class LimiteLlamada extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LimiteLlamada', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmAgregar()
		 * 
		 * Formulario de insercion 
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarConfiguracionTeimpoLimite($this->Informacion['Informacion']['idUsuario']);
				if(count($Consulta) > 0){
					$Consulta[0]['TiempoLlamada'] = AppFechas::ConvertirSegundosTime($Consulta[0]['TiempoLlamada']);
				}
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('TiempoLlamada');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro("Configuracion", $Consulta);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarLimiteLlamada'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LimiteLlamada', 'Agregar', 'Agregar.html')));
				unset($Validacion, $Plantilla);
			}	
		}
		
		/**
		 * Metodo Publico
		 * Agregar()
		 * 
		 * Prepara datos para ser insertados o modificados
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
					$DatosPost['TiempoLlamada'] = AppFechas::ConvertirTimeSegundos($DatosPost['TiempoLlamada']);
					if(isset($DatosPost['IdTiempoLlamada'])){
						$DatosActualizar = array("TiempoLlamada" => $DatosPost['TiempoLlamada']);
						$Omitidos = array('IdTiempoLlamada', 'IdSupervisor');
						$Condicion = array("IdTiempoLlamada" => $DatosPost['IdTiempoLlamada']);
						$this->Modelo->ActualizarTeimpo($DatosActualizar, $Condicion, $Omitidos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LimiteLlamada', 'Agregar', 'Exito.html')));
						unset($DatosPost, $DatosActualizar, $Omitidos, $Condicion, $Plantilla);
						exit();
					}
					else{
						$DatosInsercion = array("IdSupervisor" => $this->Informacion['Informacion']['idUsuario'], "TiempoLlamada" => $DatosPost['TiempoLlamada']);
						$this->Modelo->GuardarTiempo($DatosInsercion);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LimiteLlamada', 'Agregar', 'Exito.html')));
						unset($DatosPost, $DatosInsercion, $Plantilla);
						exit();		
					}
				}
			}
		}
	}