<?php
	
	class Cliente extends Controlador{
		
		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()		  
		 *
		 * Pantalla de inicio
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * frmListado()
		 *
		 * Muestra una Lista de los Clientes Asignados al Supervisor
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarClientes($IdSupervisor);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Listado', 'Listado.html')));
				unset($Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmAgregar()
		 * 
		 * Pantalla para Agregar un nuevo Cliente
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('RazonSocial', '* Campo Requerido');
				$Validacion->Requerido('Correo', '* Campo Requerido');
				$Validacion->Email('Correo', '* Ingresa un correo valido');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarCliente'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Agregar', 'Agregar.html')));
				unset($Provincias, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * Agregar()
		 * 
		 * Guardado de los datos del cliente en base de datos
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					if(AppPost::DatosVaciosOmitidos($_POST, array('RUC', 'Direccion', 'Nombres', 'ApellidoPaterno', 'ApellidoMaterno', 'Cargo', 'TelefonoCasa', 'TelefonoMovil', 'IdProvincia', 'Status') ) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TelefonoCasa', 'TelefonoMovil', 'IdProvincia', 'Correo', 'Status'));
						$Perfil = $this->Modelo->BuscarPerfil('Cliente');
						$DatosPost['Status'] = (isset($DatosPost['Status'])) ? 'ACTIVO' : 'SUSPENDIDO';
						$Password = AppCorreos::GeneradorPassword();
						$DatosUsuario = array("IdPerfil" => $Perfil[0]['IdPerfil'], "Usuario" => $DatosPost['Correo'], "Password" => hash('sha256', $Password), 'Status' => $DatosPost['Status']);
						$Consulta = $this->Modelo->ConsultaUsuarioExistente($DatosPost['Correo'], $DatosPost['RazonSocial']);				
						if($Consulta['Cantidad'] > 0){
						    $Status = $this->Modelo->ConsultarEliminado($DatosPost['Correo'], $DatosPost['RazonSocial']);
							if($Status['Cantidad'] > 0){
								$Arreglo = array('Status' => 'ACTIVO');
								$Condicion = array('IdUsuario' => $Status[0]['IdUsuario']);
								$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
								$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Agregar', 'Exito.html')));
								unset($DatosPost, $Perfil, $DatosUsuario, $Consulta,$Status, $Plantilla);
								exit();
							}
							else{
								$Plantilla = new NeuralPlantillasTwig(APP);
								echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Error', 'ErrorRegistroDuplicado.html')));
								unset($DatosPost, $Perfil, $Password, $DatosUsuario, $Consulta, $Status, $Plantilla);
								exit();
							}								
						}
						else{
							$this->Modelo->GuardarDatosCliente($DatosUsuario);
							$Usuario = $this->Modelo->BuscarIdUsuario($DatosUsuario['Usuario']);
							$DatosInformacionUsuario = array("idUsuario" => $Usuario[0]['IdUsuario'], "RUC" => $DatosPost['RUC'], "RazonSocial" => $DatosPost['RazonSocial'], "Direccion" => $_POST['Direccion'], "Nombres" => $DatosPost['Nombres'], "ApellidoPaterno" => $DatosPost['ApellidoPaterno'], "ApellidoMaterno" => $DatosPost['ApellidoMaterno'], "Cargo" => $DatosPost['Cargo'], "TelefonoCasa" => $DatosPost['TelefonoCasa'], "TelefonoMovil" => $DatosPost['TelefonoMovil'], "Correo" => $DatosPost['Correo'], "IdProvincia" => $DatosPost['IdProvincia']);
							$this->Modelo->GuardarInformacionCliente($DatosInformacionUsuario);
							$DatosAgenteAsignado = array("IdSupervisor" => $this->Informacion['Informacion']['idUsuario'], "IdCliente" => $Usuario[0]['IdUsuario']);
							$this->Modelo->GuardarClienteAsignado($DatosAgenteAsignado);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Perfil, $DatosUsuario, $Consulta, $Usuario, $DatosInformacionUsuario, $DatosAgenteAsignado, $Plantilla);
							exit();
						}
					}
				}
			}
		}
		
		/**
		 * Metodo publico
		 * frmEditar()
		 * 
		 * Pantalla de edición de datos de Cliente
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdCliente = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Consulta = $this->Modelo->ConsultaUnCliente($IdCliente);
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('RazonSocial', '* Campo Requerido');
				$Validacion->Requerido('Correo', '* Campo Requerido');
				$Validacion->Email('Correo', '* Ingresa un correo valido');
				$Validacion->CampoIgual('RepitePassword', 'Password', '* La contrasea no coincide');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdUsuario', $_POST['Id']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarCliente'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Editar', 'Editar.html')));
				unset($IdSupervisor, $Consulta, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Atualizar()
		 * 
		 * Edita los datos de un cliente en especifio
		 */
		public function Actualizar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if((AppPost::DatosVaciosOmitidos($_POST, array('RUC', 'Direccion', 'Nombres', 'ApellidoPaterno', 'ApellidoMaterno', 'Cargo', 'TelefonoCasa', 'TelefonoMovil', 'Password', 'RepitePassword', 'IdProvincia'))) == false ) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TelefonoCasa', 'TelefonoMovil', 'Correo', 'Usuario', 'Password', 'RepitePassword', 'Status', 'IdProvincia'));
						$IdUsuario = NeuralCriptografia::DeCodificar($_POST['IdUsuario'], APP);
						$IdInformacion = $this->Modelo->BuscarIdInformacionUsuario($IdUsuario);						
						$Matriz = array('RUC' => $DatosPost['RUC'], 'RazonSocial' => $DatosPost['RazonSocial'], 'Direccion' => $DatosPost['Direccion'],'Nombres' => $DatosPost['Nombres'], 'ApellidoPaterno' => $DatosPost['ApellidoPaterno'], 'ApellidoMaterno' => $DatosPost['ApellidoMaterno'], 'Cargo' => $DatosPost['Cargo'], 'TelefonoCasa' => $DatosPost['TelefonoCasa'], 'TelefonoMovil' => $DatosPost['TelefonoMovil'], 'Correo' => $DatosPost['Correo'], 'IdProvincia' => $DatosPost['IdProvincia']);
						$Omitidos = array('IdInformacion', 'idUsuario', 'Extension', 'TipoCanal', 'ExportarExcel', 'callerid', 'Status', 'TelefonoCasa2', 'TelefonoMovil2');
						$Condicion = array('IdInformacion' => $IdInformacion[0]['IdInformacion']);						
						$Consulta = $this->Modelo->ConsultaUsuarioExitenteEditar($IdUsuario, $DatosPost['Correo'], $DatosPost['RazonSocial']);
						if($Consulta['Cantidad'] > 0){
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $Perfil, $DatosUsuario, $Consulta, $Plantilla);
							exit();
						}
						else{
							$this->Modelo->ActualizaInformacionCliente($Matriz, $Condicion, $Omitidos);
							if(isset($_POST['Password']) OR isset($_POST['Status'])){
								$DatosPost['Password'] = (!empty($DatosPost['Password'])) ? ($DatosPost['Password'] == $DatosPost['RepitePassword']) ? $DatosPost['Password'] : '' : '';
								$DatosPost['Status'] = (isset($DatosPost['Status'])) ? 'ACTIVO' : 'SUSPENDIDO';
								if(empty($DatosPost['Password'])){
									$Matriz = array('Status' => $DatosPost['Status']);
									$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
									$Condicion = array('IdUsuario' => $IdUsuario);
									$this->Modelo->ActualizarUsuarioCliente($Matriz, $Condicion, $Omitidos);
								}
								else {
									$Matriz = array('Password' =>  hash('sha256', $DatosPost['Password']), 'Status' => $DatosPost['Status']);
									$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario');
									$Condicion = array('IdUsuario' => $IdUsuario);
									$this->Modelo->ActualizarUsuarioCliente($Matriz, $Condicion, $Omitidos);
								}
							}
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cliente', 'Listado', 'Partes', 'JsPrincipal.html')));
							unset($DatosPost, $IdUsuario, $IdInformacion ,$Matriz, $Omitidos, $Condicion, $Consulta);
							exit();	
						}						
					}
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * Eliminar()
		 * 
		 * Elimina un registro con perfil de cliente
		 */
		public function Eliminar() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdUsuario' => $Id);
					$Omitidos = array('IdUsuario', 'IdPerfil', 'Usuario', 'Password');
					$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}