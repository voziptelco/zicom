<?php

	class ImportarExcel extends Controlador{
		
		var $Informacion;
		var $Consultas;

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla opciones de carga de archivo excel
		 */
		public function Index(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Plantilla = new NeuralPlantillasTwig(APP);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'ImportarArchivoExcel.html')));
				unset($Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * frmAgregarArchivo()
		 *
		 * Pantalla para cargar archivo excel
		 */
		public function frmAgregarArchivo(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('archivo');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarArchivoExcel'));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'AgregarArchivoExcel.html')));
				unset($Validacion, $Plantilla);
				exit();	
			}		
		}

		/**
		 * Metodo Publico
		 * CargarArchivoExcel()
		 *
		 * Pantalla para analisis del archivo excel
		 */
		public function CargarArchivoExcel(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$ArchivoCargado = AppArchivos::CargarArchivo($_FILES);
				if($ArchivoCargado != 'ERROR'){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('ruta', NeuralCriptografia::Codificar($ArchivoCargado, APP));
					$Plantilla->Parametro('extension', $_FILES["file-0"]['type']);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'ExaminarArchivo.html')));
					unset($ArchivoCargado, $Plantilla);
					exit();
				}
			}
		}

		/**
		 * Metodo Publico
		 * ExaminarArchivo()
		 *
		 * Pantalla muestra el resultado del analisis del archivo excel
		 */
		public function ExaminarArchivo(){
			set_time_limit(0);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST)){
					$Ruta = NeuralCriptografia::DeCodificar($_POST['ruta'], APP);
					$Archivo = new AppPHPExcel;
					$Archivo->LeerExcelColumnas($Ruta, $_POST['extension']);
					$ColumnasRequeridas = array('Cliente', 'Gerencia', 'Cartera', 'ClienteUnico', 'NombreTitular', 'DomicilioContractual', 'Provincia', 'Distrito', 'NombreAval', 'DomicilioAval', 'DistritoAval', 'SemanaAtraso', 'Saldo', 'SaldoTotal', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'Codigo');
					$ColumnasArchivo = $Archivo->LeerColumnas();
					if(AppArreglosExcel::ComparaColumnas($ColumnasArchivo, $ColumnasRequeridas)){
						$DatosArchivo = $Archivo->DatosExcel();
						$this->Consultas = $this->Consultas();
						if(AppUtilidades::CompruebaConsultas($this->Consultas['ConsultaGerencias'], $this->Consultas['ConsultaCarteras'], $this->Consultas['ConsultaProvincias'], $this->Consultas['ConsultaDistritos'], $this->Consultas['ConsultaClientes'], $this->Consultas['ConsultaCodigos'])){					
							$Omitidos = array(3, 11, 12, 13, 14, 15, 16, 17, 18);
							$DatosArchivo = AppArreglosExcel::FormatoUcwords($DatosArchivo, $Omitidos);
							$DatosArchivo = AppArreglosExcel::FormatoNumeros($DatosArchivo, array(12, 13));
							$DatosArchivo = AppArreglosExcel::FormatoTelefonos($DatosArchivo, array(14, 15, 16, 17));
							$DatosArchivo = AppArreglosExcel::FormatoClaveClienteUnico($DatosArchivo, array(3));
							$DatosArchivo = AppArreglosExcel::CombinarMatrizLLave($DatosArchivo, $ColumnasRequeridas);
							$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaGerencias'], 'Gerencia', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaCarteras'], 'Cartera', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaProvincias'], 'Provincia', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaDistritos'], 'Distrito', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaClientes'], 'Cliente', 'RazonSocial');
							$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaCodigos'], 'Codigo', 'Codigo');
							$ArregloEstados = AppArreglosExcel::BuscarEstadoRegistros($DatosArchivo, array('NombreTitular'), 'tbl_datos_agenda');
							$DatosArchivo= AppArreglosExcel::CombinarArreglos($DatosArchivo, $ArregloEstados);
							$Plantilla = new NeuralPlantillasTwig(APP);
							$Plantilla->Parametro('Ruta', NeuralCriptografia::Codificar($Ruta,APP));
							$Plantilla->Parametro('Extension', $_POST['extension']);
							$Plantilla->Parametro('Registros', $DatosArchivo);
							$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
								return number_format($Parametro, 2, '.', ',');
							});
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'ListadoRegistroExcel.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $DatosArchivo, $Omitidos, $ArregloEstados, $Plantilla);
							exit();
						}
						else{
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'Error', 'NoExistenRegistros.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $DatosArchivo, $Plantilla);
							exit();
						}
					}
					else {
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'Error', 'ErrorArchivoInvalido.html')));
						unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $Plantilla);
						exit();
					}
				}
			}
		}

		/**
		 * Metodo Publico
		 * GuardarArchivoBaseDatos()
		 *
		 * Proceso de almacenado de la informacion importada a la base de datos
		 */
		public function GuardarArchivoBaseDatos(){
			set_time_limit(0);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST)){
					$Ruta = NeuralCriptografia::DeCodificar($_POST['Ruta'], APP);
					$Archivo = new AppPHPExcel;
					$Archivo->LeerExcelColumnas($Ruta, $_POST['Extension']);
					$ColumnasRequeridas = array('Cliente', 'Gerencia', 'Cartera', 'ClienteUnico', 'NombreTitular', 'DomicilioContractual', 'Provincia', 'Distrito', 'NombreAval', 'DomicilioAval', 'DistritoAval', 'SemanaAtraso', 'Saldo', 'SaldoTotal', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'Codigo');
					$ColumnasArchivo = $Archivo->LeerColumnas();
					if(AppArreglosExcel::ComparaColumnas($ColumnasArchivo, $ColumnasRequeridas)){
						$DatosArchivo = $Archivo->DatosExcel();
						$this->Consultas = $this->Consultas();
						$Omitidos = array(3, 11, 12, 13, 14, 15, 16, 17, 18);
						$DatosArchivo = AppArreglosExcel::FormatoUcwords($DatosArchivo, $Omitidos);
						$DatosArchivo = AppArreglosExcel::FormatoNumeros($DatosArchivo, array(12, 13));
						$DatosArchivo = AppArreglosExcel::FormatoTelefonos($DatosArchivo, array(14, 15, 16, 17));
						$DatosArchivo = AppArreglosExcel::FormatoClaveClienteUnico($DatosArchivo, array(3));
						$DatosArchivo = AppArreglosExcel::CombinarMatrizLLave($DatosArchivo, $ColumnasRequeridas);
						$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaGerencias'], 'Gerencia', 'Descripcion');
						$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaCarteras'], 'Cartera', 'Descripcion');
						$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaProvincias'], 'Provincia', 'Descripcion');
						$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaDistritos'], 'Distrito', 'Descripcion');
						$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaClientes'], 'Cliente', 'RazonSocial');
					//	$DatosArchivo = AppArreglosExcel::BuscarInformacionConsulta($DatosArchivo, $this->Consultas['ConsultaCodigos'], 'Codigo', 'Codigo');
						$DatosArchivo = AppArreglosExcel::EliminarErrores($DatosArchivo);
						$Cantidad = count($DatosArchivo);
						if($Cantidad > 0){
							$DatosArchivo = AppArreglosExcel::SustituyeIndices($DatosArchivo, $this->Consultas['ConsultaGerencias'], 'Gerencia', 'IdGerencia', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::SustituyeIndices($DatosArchivo, $this->Consultas['ConsultaCarteras'], 'Cartera', 'IdCartera', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::SustituyeIndices($DatosArchivo, $this->Consultas['ConsultaProvincias'], 'Provincia', 'IdProvincia', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::SustituyeIndices($DatosArchivo, $this->Consultas['ConsultaDistritos'], 'Distrito', 'IdDistrito', 'Descripcion');
							$DatosArchivo = AppArreglosExcel::SustituyeIndices($DatosArchivo, $this->Consultas['ConsultaClientes'], 'Cliente', 'IdCliente', 'RazonSocial');
							$DatosArchivo = AppArreglosExcel::SustituyeIndices($DatosArchivo, $this->Consultas['ConsultaCodigos'], 'Codigo', 'IdCodigo', 'Codigo');
							$DatosArchivo = AppArreglosExcel::AgregarColumna($DatosArchivo, 'IdSupervisor', $this->Informacion['Informacion']['idUsuario']);
							$ArregloEstados = AppArreglosExcel::BuscarEstadoRegistros($DatosArchivo, array('NombreTitular'), 'tbl_datos_agenda');
							$DatosArchivoNuevos = AppArreglosExcel::CreaMatrizEstados($DatosArchivo, $ArregloEstados, 'NUEVO REGISTRO');
							$DatosArchivoActualizar = AppArreglosExcel::CreaMatrizEstados($DatosArchivo, $ArregloEstados, 'ACTUALIZAR REGISTRO');
							$this->Modelo->AgregarRegistros($DatosArchivoNuevos);
							$this->Modelo->ActualizarRegistros($DatosArchivoActualizar, 'NombreTitular');
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'Exito.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $ConsultaGerencias, $ConsultaCarteras, $ConsultaProvincias, $ConsultaDistritos, $Omitidos, $ArregloEstados, $DatosArchivoNuevos, $DatosArchivoActualizar, $Plantilla);
							exit();
						}
						else {
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'Error', 'ErrorNoElementos.html')));
							unset($Ruta, $Archivo, $ColumnasRequeridas, $DatosArchivo, $DatosArchivoNuevos, $Plantilla);
							echo "errores";
							exit();
						}
					}
					else {
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'ImportarExcel', 'Error', 'ErrorArchivoInvalido.html')));
						unset($Ruta, $Archivo, $ColumnasRequeridas, $ColumnasArchivo, $DatosArchivo, $Plantilla);
						exit();
					}
				}
			}
		}

		/**
		 * Metodo Privado
		 * Consultas()
		 *
		 * Datos generales de validación de entradas
		 */
		private function Consultas(){
			$ConsultaGerencias = $this->Modelo->ConsultarGerencias();
			$ConsultaCarteras = $this->Modelo->ConsultaCarteras();
			$ConsultaProvincias = $this->Modelo->ConsultaProvincias();
			$ConsultaDistritos = $this->Modelo->ConsultaDistritos();
			$ConsultaCodigos = $this->Modelo->ConsultaCodigos();
			$ConsultaClientes = $this->Modelo->ConsultaClientes($this->Informacion['Informacion']['idUsuario']);					
			return array('ConsultaGerencias' => $ConsultaGerencias, 'ConsultaCarteras' => $ConsultaCarteras, 'ConsultaProvincias' => $ConsultaProvincias, 'ConsultaDistritos' => $ConsultaDistritos, 'ConsultaClientes' => $ConsultaClientes, 'ConsultaCodigos' => $ConsultaCodigos);
		}
	}
