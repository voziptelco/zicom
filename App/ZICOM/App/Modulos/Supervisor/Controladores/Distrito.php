<?php

	class Distrito extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal 
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmListado()
		 * 
		 * Pantalla de listado de Distritos
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarDistritos();
				if($Consulta > 0):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('TotalRegistros', $Consulta);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Listado', 'Listado.html')));
					unset($IdSupervisor, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Error', 'NoResultados.html')));
					unset($IdSupervisor, $Consulta, $Plantilla);
					exit();
				endif;
			}
		}

		/**
		 * Metodo Publico 
		 * ConsultaDistritosPagina()
		 *
		 * consulta distritos segun la pagina
		 */
		public function ConsultaDistritosPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];	
				$Consulta = $this->Modelo->ConsultarDistritosPagina($Inicio, $NumeroRegistros, $Busqueda);		
				if($Consulta['Cantidad']){
						$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
						unset($Consulta['Cantidad']);
						$Consulta = AppFiltrosRapidos::AplicarFiltrosCifrado($Consulta, 'IdDistrito');			        
				        $Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
	     				echo json_encode($Consulta);
	     				unset($DatosPost, $ArregloCriterios, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);	
					}
					else{
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;
					}
			}
		}
		
		/**
		 * Metodo Publico
		 * frmAgregar()
		 * 
		 * Pantalla para agregar un distrito
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$ArregloProvincias = $this->Modelo->BuscarProvincias();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Descripcion');
				$Validacion->Requerido('IdProvincia');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Provincias', $ArregloProvincias);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarDistrito'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Agregar', 'Agregar.html')));
				unset($ArregloProvincias, $Validacion, $Plantilla);
				exit();
			}	
		}
		
		/**
		 * Metodo Publico 
		 * Agregar()
		 * 
		 * Guarda la informacion de un distrito
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('IdProvincia'));	
					$Consulta = $this->Modelo->ConsultaDistritoExistente($DatosPost['Descripcion']);
					if($Consulta['Cantidad'] > 0){
						if($Consulta[0]['Status'] == 'ELIMINADO'){
							$Arreglo = array('Status' => 'ACTIVO');
							$Condicion = array('IdDistrito' => $Consulta[0]['IdDistrito']);
							$Omitidos = array('IdDistrito', 'IdProvincia', 'Descripcion');
							$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $Arreglo, $Condicion, $Omitidos, $Plantilla);
							exit();
						}else{
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $Consulta, $Plantilla);
							exit();	
						}
					}
					else{
						$Omitidos = array('IdDistrito', 'Status');
						$this->Modelo->AgregarDistrito($DatosPost, $Omitidos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Agregar', 'Exito.html')));
						unset($DatosPost, $Consulta, $Omitidos, $Plantilla);
						exit();	
					}	
				}
			}
		}
		
		/**
		 * Metodo Publcio 
		 * frmEditar()
		 * 
		 * Pantalla dpara editar Distritos
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdDistrito = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$ArregloProvincias = $this->Modelo->BuscarProvincias();
				$Consulta = $this->Modelo->ConsultarDistrito($IdDistrito);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('IdProvincia');
				$Validacion->Requerido('Descripcion');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Provincias', $ArregloProvincias);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdDistrito', $_POST['Id']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarDistrito'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Editar', 'Editar.html')));
				unset($IdDistrito, $Consulta, $ArregloProvincias, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * Editar()
		 * 
		 * Ediciond ela informacion de los distritos
		 */
		public function Editar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$IdDistrito = NeuralCriptografia::DeCodificar($_POST['IdDistrito'], APP);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('IdDistrito', 'IdProvincia'));				
					$Consulta = $this->Modelo->ConsultaDistritoExistente($DatosPost['Descripcion']);
					if($Consulta['Cantidad'] > 0){
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Distrito', 'Error', 'ErrorRegistroDuplicado.html')));
						unset($IdDistrito, $DatosPost, $Consulta, $Plantilla);
						exit();	
					}
					else{
						$Omitidos = array('IdDistrito', 'Status');
						$Arreglo = array('IdProvincia' => $DatosPost['IdProvincia'], 'Descripcion' => $DatosPost['Descripcion']);
						$Condicion = array('IdDistrito' => $IdDistrito);
						$this->Modelo->EditarDistrito($Arreglo, $Condicion, $Omitidos);
						unset($IdDistrito, $Consulta, $DatosPost, $Omitidos, $Arreglo, $Condicion);
						exit();		
					}					
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * Eliminar()
		 * 
		 * Prepara datos para modificar el estado de un resgistro y pasarlo como ELIMINDO
		 */
		public function Eliminar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdDistrito' => $Id);
					$Omitidos = array('IdDistrito', 'IdProvincia', 'Descripcion');
					$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}
?>