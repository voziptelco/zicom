<?php
	
	class ImportarRegistros extends Controlador{

		var $Informacion;

		/**
		 * Metodo Contructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla Principal
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarRegistros', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}

		/**
 		 * Metodo Publico 
 		 * frmSubirArchivo()
 		 * 
 		 * Formulario para seleccionar Archivo
 		 */
		public function frmSubirArchivo(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST']){
				$Plantilla = new NeuralPlantillasTwig(APP);
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarRegistros', 'Importacion', 'AgregarArchivoExcel.html')));
				unset($Plantilla);
				exit();	
			}
		}

		/**
		 * Metodo Publico
		 * CargarArchivoExcel()
		 *
		 * Pantalla para analisis del archivo excel
		 */
		public function CargarArchivoExcel(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$ArchivoCargado = AppArchivos::CargarArchivoTemporal($_FILES);
				if($ArchivoCargado != 'ERROR'){
					$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
					$Validacion->Requerido('FechaHoraCarga');
					$Validacion->Remoto('FechaHoraCarga', NeuralRutasApp::RutaUrlApp("Supervisor/ImportarRegistros/CompararFechaActual"), 'POST', false, "*La fecha no debe ser anterior a la fecha actual");
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('ruta', NeuralCriptografia::Codificar($ArchivoCargado['Destino'], APP));
					$Plantilla->Parametro('extension', $_FILES["file-0"]['type']);
					$Plantilla->Parametro('nombre', NeuralCriptografia::Codificar($ArchivoCargado['Nombre'], APP));
					$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
					$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarArchivo'));
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarRegistros', 'Examinar', 'ExaminarArchivo.html')));
					unset($ArchivoCargado, $Plantilla);
					exit();
				}
				else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarRegistros', 'Error', 'ErrorCargaArchivo.html')));
					unset($Plantilla);
					exit();
				}	
			}
		}
		
		/**
		 * Metodo Publico
		 * CompararFechaActual()
		 * 
		 * Compara la fecha actual con la que se ingresa
		 */
		public function CompararFechaActual(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST['FechaHoraCarga'])){
					echo AppFiltrosRapidos::ComparaTiempo($_POST['FechaHoraCarga']);
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * CargarArchivo()
		 * 
		 * Guarda en bd los datos para hacer una carga programada.
		 */
		public function CargarArchivo(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(AppPost::DatosVacios($_POST) == false){
						$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
						$NombreArchivo = NeuralCriptografia::DeCodificar($DatosPost['Nombre'], APP);
						$Ruta = NeuralCriptografia::DeCodificar($DatosPost['Ruta'], APP);
						$Extension = $DatosPost['Extension'];
						$DatosPost['FechaHoraCarga'] = AppFechas::ValidaHora($DatosPost['FechaHoraCarga']);
						$ArregloDatos = array("IdSupervisor" => $this->Informacion['Informacion']['idUsuario'], "FechaHoraCaptura" => AppFechas::ObtenerDatetimeActual(), "NombreArchivo" => $NombreArchivo, "Ruta" => $Ruta, "Extension" => $Extension, "FechaHoraCarga" => AppFechas::FormatoFechaTiempo($DatosPost['FechaHoraCarga']));
						$this->Modelo->GuardarInformacionArchivo($ArregloDatos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('ImportarRegistros', 'Exito', 'Exito.html')));
						unset($DatosPost, $NombreArchivo, $Ruta, $Extension, $ArregloDatos, $Plantilla);
					}
				}
			}
		}
	}