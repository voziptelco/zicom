<?php

	class Perfil extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Metodo que modifica las contraseña del usuario
		 */
		public function Index(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['KeyPerfil']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['KeyPerfil']), APP) == date("Y-m-d")) == true){
					unset($_POST['KeyPerfil']);
					if(AppPost::DatosVacios($_POST) == false){
						$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
						$ConsultaDatosActuales = $this->Modelo->BuscarPassword($this->Informacion['Informacion']['idUsuario'], hash('sha256', $DatosPost['PasswordActual']));
						if($ConsultaDatosActuales['Cantidad'] != 0){
							$Msj = "Su contraseña ha sido modificada, para efecto reinicie su sesión de trabajo";
							$this->Modelo->ActualizarPassword($this->Informacion['Informacion']['idUsuario'], hash('sha256', $DatosPost['PasswordNuevo']));
						}
						else {
							$Msj = "La contraseña no coincide con la del usuario";
						}
						echo $Msj;
						unset($DatosPost, $ConsultaDatosActuales, $Msj);
						exit();
					}
				}
			}
		}

		/**
		 * Metodo Publico
		 * CambiarInfomacion()
		 *
		 * Se Modifica la infomacion general del usuario
		 */
		public function CambiarInfomacion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['KeyPerfil']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['KeyPerfil']), APP) == date("Y-m-d")) == true){
					unset($_POST['KeyPerfil']);
						$DatosPost = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
						if($DatosPost['NombreUsuario'] != "" OR $DatosPost['ApellidoPaternoUsuario'] != "" OR $DatosPost['ApellidoMaternoUsuario'] != ""){
							$this->Modelo->ActualizarDatosUsuario($this->Informacion['Informacion']['idUsuario'], array("Nombres" => $DatosPost['NombreUsuario'], "ApellidoPaterno" =>$DatosPost['ApellidoPaternoUsuario'], "ApellidoMaterno"=>$DatosPost['ApellidoMaternoUsuario']));
						}
						if(isset($_FILES['ImagenPerfil']) AND !empty($_FILES['ImagenPerfil']['name'])){
							$fp = fopen($_FILES['ImagenPerfil']['tmp_name'], "r");
							$tfoto = fread($fp, filesize($_FILES['ImagenPerfil']['tmp_name']));
			 				fclose($fp);
			 				$Imagen = $this->Modelo->ConsultarImagen($this->Informacion['Informacion']['idUsuario']);
			 				if(isset($Imagen[0]['Imagen'])){
			 					$this->Modelo->ActualizarFoto(array('Imagen'=>$tfoto, 'Tipo'=>$_FILES['ImagenPerfil']['type']), $this->Informacion['Informacion']['idUsuario']);
			 				}else{
			 					$this->Modelo->GuardarImagen(array("Imagen"=>$tfoto, "Tipo"=>$_FILES['ImagenPerfil']['type'], "IdUsuario" => $this->Informacion['Informacion']['idUsuario']));
			 				}	
						}		
						echo "Los datos han sido guardados correctamente, para efecto reinicie su sesión de trabajo";
						unset($DatosPost, $ConsultaDatosActuales, $Msj);
						exit();
				}
			}
		}

	}