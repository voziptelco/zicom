<?php

	class FiltradoClientes extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();		
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio
		 */
			public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmFiltradoClientes()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de clientes
		 */
		public function frmFiltradoClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Gerencias = $this->Modelo->ConsultaGerencias();
				$Carteras = $this->Modelo->ConsultaCarteras();
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Distritos = $this->Modelo->ConsultaDistritos();
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Agentes = $this->Modelo->ConsultarAgentes($this->Informacion['Informacion']['idUsuario']);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Numero('minimoSemanas', "Solo numérico");
				$Validacion->Numero('minimoSaldo', "Solo numérico");
				$Validacion->Numero('minimoSaldoTotal', "Solo numérico");
				$Validacion->Numero('maximoSemanas', "Solo numérico");
				$Validacion->Numero('maximoSaldo', "Solo numérico");
				$Validacion->Numero('maximoSaldoTotal', "Solo numérico");
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Gerencias', $Gerencias);
				$Plantilla->Parametro('Carteras', $Carteras);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Distritos', $Distritos);
				$Plantilla->Parametro('Codigos', $Codigos);
				$Plantilla->Parametro('Agentes', $Agentes);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmFiltrosClientes'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Filtrar', 'FiltroClientes.html')));
				unset($Gerencias, $Carteras, $Distritos, $Provincias, $MaximoSemanas, $MaximoSaldo, $MaximoSaldoTotal, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarClientes()
		 * 
		 * Prepara los datos del formulario en condiciones de busqueda en base de datos
		 * para despues mostrarlos en una pantalla de listado.
		 */		
		public function ConsultarClientes(){
			set_time_limit(0);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloDatos = AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($DatosPost, array('minimoSemanas', 'maximoSemanas', 'minimoSaldo', 'maximoSaldo', 'minimoSaldoTotal', 'maximoSaldoTotal')));
					$ArregloCriterios = AppUtilidades::ArregloCriterios($ArregloDatos, $DatosPost);
					$IdAgente = (isset($ArregloDatos['Agente'])) ? $ArregloDatos['IdAgente'] : "";
					$CondicionesExtra = AppUtilidades::ArregloExtra($ArregloDatos);
					$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
					$Cantidad = count($CondicionesExtra);
					if($Cantidad > 0){
						$ConsultaCampo = $this->Modelo->ConsultaCompromiso($ArregloCriterios, $IdSupervisor, 'tbl_gestion_campo', 'IdGestionCampo', $IdAgente, $CondicionesExtra);
						$ConsultaTelefonica = $this->Modelo->ConsultaCompromiso($ArregloCriterios, $IdSupervisor, 'tbl_gestion_telefonica', 'IdGestionTelefonica', $IdAgente, $CondicionesExtra);
						$Consulta = array_merge($ConsultaCampo, $ConsultaTelefonica);
						$Consulta = AppUtilidades::AgruparMatriz($Consulta, 'NombreTitular');	
					}else{
						$Consulta = $this->Modelo->ConsultaRegistros($ArregloCriterios, $IdSupervisor, $IdAgente);
					}
					$Cantidad = count($Consulta);
					if($Cantidad > 0){
					//	$Consulta = AppUtilidades::EstadosGestion($Consulta, 'tbl_gestion_campo', 'IdDatoAgenda');
					//	$Consulta = AppUtilidades::EstadosGestion($Consulta, 'tbl_gestion_telefonica', 'IdDatoAgenda');
						$TotalSaldo = AppUtilidades::ObtenerTotalColumna($Consulta, "Saldo");
						$TotalSaldoTotal = AppUtilidades::ObtenerTotalColumna($Consulta, "SaldoTotal");
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Consulta', $Consulta);
						$Plantilla->Parametro('TotalSaldo', $TotalSaldo);
						$Plantilla->Parametro('TotalSaldoTotal', $TotalSaldoTotal);
						$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
							return number_format($Parametro, 2, '.', ',');
						});
						$Plantilla->Filtro('Cifrado', function($Parametro){
							return NeuralCriptografia::Codificar($Parametro, APP);
						});
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Listado', 'Listado.html')));
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $IdAgente, $IdSupervisor, $CondicionesExtra, $ConsultaCampo, $ConsultaTelefonica, $Consulta, $Plantilla);
						exit();
					}
					else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Error', 'NoResultados.html')));
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $IdAgente, $CondicionesExtra, $IdSupervisor, $Plantilla);
						exit();
					}									
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarClientesDescargar()
		 * 
		 * Consulta de clientes para descarga en excel
		 */
		public function ConsultarClientesDescargar(){
			set_time_limit(0);
			if($_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$Consultas['Clientes'] = $this->Modelo->ConsultaClientes($this->Informacion['Informacion']['idUsuario']);
					$Consultas['Gerencias'] = $this->Modelo->ConsultaGerencias();
					$Consultas['Carteras'] = $this->Modelo->ConsultaCarteras();
					$Consultas['Provincias'] = $this->Modelo->ConsultaProvincias();
					$Consultas['Distritos'] = $this->Modelo->ConsultaDistritos();
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloDatos = AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($DatosPost, array('minimoSemanas', 'maximoSemanas', 'minimoSaldo', 'maximoSaldo', 'minimoSaldoTotal', 'maximoSaldoTotal')));
					if($ArregloDatos):
						$ArregloCriterios = AppUtilidades::ArregloCriterios($ArregloDatos, $DatosPost);
						$IdAgente = (isset($ArregloDatos['Agente'])) ? $ArregloDatos['IdAgente'] : "";
						$CondicionesExtra = AppUtilidades::ArregloExtra($ArregloDatos);
						$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
						$Cantidad = count($CondicionesExtra);
						if($Cantidad > 0){
							$ConsultaCampo = $this->Modelo->ConsultaCompromiso($ArregloCriterios, $IdSupervisor, 'tbl_gestion_campo', 'IdGestionCampo', $IdAgente, $CondicionesExtra);
							$ConsultaTelefonica = $this->Modelo->ConsultaCompromiso($ArregloCriterios, $IdSupervisor, 'tbl_gestion_telefonica', 'IdGestionTelefonica', $IdAgente, $CondicionesExtra);
							$Consulta = array_merge($ConsultaCampo, $ConsultaTelefonica);
							$Consulta = AppUtilidades::AgruparMatriz($Consulta, 'NombreTitular');	
						}else{
							$Consulta = $this->Modelo->ConsultaRegistros($ArregloCriterios, $IdSupervisor, $IdAgente);
						}
					else:
						$Consulta = $this->Modelo->ConsultarRegistrosTotal($this->Informacion['Informacion']['idUsuario']);						
					endif;
					$Cantidad = count($Consulta);
					if($Cantidad > 0):
						$Campos = array(array('Id'=>'IdCliente', 'valor'=>'RazonSocial'), array('Id'=>'IdGerencia', 'valor'=>'Descripcion'), array('Id'=>'IdCartera', 'valor'=>'Descripcion'), array('Id'=>'IdProvincia', 'valor'=>'Descripcion'), array('Id'=>'IdDistrito', 'valor'=>'Descripcion'));
						$Consulta = AppUtilidades::RemplazaIndicesValores($Consulta, $Consultas, $Campos);
						header("X-XSS-Protection:0");
	     				header('Set-Cookie: fileDownload=true; path=/');
	                    $Nombre = 'Gestion y Cobranzas, ZICOM Group';
	                    $Titulo = 'Registros de Clientes '.str_replace('-', '_', str_replace(':', '_', AppFechas::ObtenerDatetimeActual()));
	         			$Categoria = 'categorias';
	         			$DatosColumnas['Columnas'] = array('A1'=>'Cliente', 'B1'=>'Gerencia', 'C1'=>'Cartera', 'D1'=>'ClienteUnico', 'E1'=>'NombreTitular', 'F1'=>'DomicilioContractual', 'G1'=>'Provincia', 'H1'=>'Distrito', 'I1'=>'NombreAval', 'J1'=>'DomicilioAval', 'K1'=>'DistritoAval', 'L1'=>'SemanaAtraso', 'M1'=>'Saldo', 'N1'=>'SaldoTotal', 'O1'=>'Telefono1', 'P1'=>'Telefono2', 'Q1'=>'Telefono3', 'R1'=>'Telefono4', 'S1'=>'Codigo');
						$DatosColumnas['Datos'] = array('A'=>'IdCliente', 'B'=>'IdGerencia', 'C'=>'IdCartera', 'D'=>'ClienteUnico', 'E'=>'NombreTitular', 'F'=>'DomicilioContractual', 'G'=>'IdProvincia', 'H'=>'IdDistrito', 'I'=>'NombreAval', 'J'=>'DomicilioAval','K'=>'DistritoAval','L'=>'SemanaAtraso', 'M'=>'Saldo', 'N'=>'SaldoTotal', 'O'=>'Telefono1', 'P'=>'Telefono2', 'Q'=>'Telefono3', 'R'=>'Telefono4', 'S'=>'Codigo');
						$DatosColumnas['Limites'] = "A1:S1";
	     				$Excel = new AppExportarExcel;     				
	     			 	$Excel->ExportarInfoUsuario($Nombre, $Titulo, $Categoria);
				        $Excel->EstablecerValoresDeArreglo($DatosColumnas['Columnas']);
				        $Excel->EstablecerAlineacionHorizontal($DatosColumnas['Limites'], 'Centrado');
				        $Excel->EstablecerColorFondo($DatosColumnas['Limites'], 'EEEEEE');
				        $Excel->EstablecerBordeDelgado($DatosColumnas['Limites']);
				        $Excel->ExportarArregloDatos($Consulta, 2, $DatosColumnas['Datos']);
    					$Excel->ExportarArchivoExcel('xlsx', $Titulo);
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $IdAgente, $IdSupervisor, $CondicionesExtra, $ConsultaCampo, $ConsultaTelefonica, $Consulta, $Plantilla);
						exit();	
					else:
						echo 'fail';
						exit();
					endif;							
				}
			}
		}
		/**
		 * Metdo Publico 
		 * ConsultarClientesEnviar()
		 * 
		 * Consulta de clientes para envio de excel por correo
		 */
		public function ConsultarClientesEnviar(){
			set_time_limit(0);
			if($_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$Consultas['Clientes'] = $this->Modelo->ConsultaClientes($this->Informacion['Informacion']['idUsuario']);
					$Consultas['Gerencias'] = $this->Modelo->ConsultaGerencias();
					$Consultas['Carteras'] = $this->Modelo->ConsultaCarteras();
					$Consultas['Provincias'] = $this->Modelo->ConsultaProvincias();
					$Consultas['Distritos'] = $this->Modelo->ConsultaDistritos();
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloDatos = AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($DatosPost, array('minimoSemanas', 'maximoSemanas', 'minimoSaldo', 'maximoSaldo', 'minimoSaldoTotal', 'maximoSaldoTotal')));
					if($ArregloDatos):
						$ArregloCriterios = AppUtilidades::ArregloCriterios($ArregloDatos, $DatosPost);
						$IdAgente = (isset($ArregloDatos['Agente'])) ? $ArregloDatos['IdAgente'] : "";
						$CondicionesExtra = AppUtilidades::ArregloExtra($ArregloDatos);
						$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];
						$Cantidad = count($CondicionesExtra);
						if($Cantidad > 0){
							$ConsultaCampo = $this->Modelo->ConsultaCompromiso($ArregloCriterios, $IdSupervisor, 'tbl_gestion_campo', 'IdGestionCampo', $IdAgente, $CondicionesExtra);
							$ConsultaTelefonica = $this->Modelo->ConsultaCompromiso($ArregloCriterios, $IdSupervisor, 'tbl_gestion_telefonica', 'IdGestionTelefonica', $IdAgente, $CondicionesExtra);
							$Consulta = array_merge($ConsultaCampo, $ConsultaTelefonica);
							$Consulta = AppUtilidades::AgruparMatriz($Consulta, 'NombreTitular');	
						}else{
							$Consulta = $this->Modelo->ConsultaRegistros($ArregloCriterios, $IdSupervisor, $IdAgente);
						}
					else:
						$Consulta = $this->Modelo->ConsultarRegistrosTotal($this->Informacion['Informacion']['idUsuario']);						
					endif;
					if($Consulta):
						$Campos = array(array('Id'=>'IdCliente', 'valor'=>'RazonSocial'), array('Id'=>'IdGerencia', 'valor'=>'Descripcion'), array('Id'=>'IdCartera', 'valor'=>'Descripcion'), array('Id'=>'IdProvincia', 'valor'=>'Descripcion'), array('Id'=>'IdDistrito', 'valor'=>'Descripcion'));
						$Consulta = AppUtilidades::RemplazaIndicesValores($Consulta, $Consultas, $Campos);
						header("X-XSS-Protection:0");
	     				header('Set-Cookie: fileDownload=true; path=/');
	                    $Nombre = 'Gestion y Cobranzas, ZICOM Group';
	                    $Titulo = 'Registros de Clientes '.str_replace('-', '_', str_replace(':', '_', AppFechas::ObtenerDatetimeActual()));
	         			$Categoria = 'categorias';
	         			$DatosColumnas['Columnas'] = array('A1'=>'Cliente', 'B1'=>'Gerencia', 'C1'=>'Cartera', 'D1'=>'ClienteUnico', 'E1'=>'NombreTitular', 'F1'=>'DomicilioContractual', 'G1'=>'Provincia', 'H1'=>'Distrito', 'I1'=>'NombreAval', 'J1'=>'DomicilioAval', 'K1'=>'DistritoAval', 'L1'=>'SemanaAtraso', 'M1'=>'Saldo', 'N1'=>'SaldoTotal', 'O1'=>'Telefono1', 'P1'=>'Telefono2', 'Q1'=>'Telefono3', 'R1'=>'Telefono4', 'S1'=>'Codigo');
						$DatosColumnas['Datos'] = array('A'=>'IdCliente', 'B'=>'IdGerencia', 'C'=>'IdCartera', 'D'=>'ClienteUnico', 'E'=>'NombreTitular', 'F'=>'DomicilioContractual', 'G'=>'IdProvincia', 'H'=>'IdDistrito', 'I'=>'NombreAval', 'J'=>'DomicilioAval','K'=>'DistritoAval','L'=>'SemanaAtraso', 'M'=>'Saldo', 'N'=>'SaldoTotal', 'O'=>'Telefono1', 'P'=>'Telefono2', 'Q'=>'Telefono3', 'R'=>'Telefono4', 'S'=>'Codigo');
						$DatosColumnas['Limites'] = "A1:S1";
	     				$Excel = new AppExportarExcel;     				
	     			 	$Excel->ExportarInfoUsuario($Nombre, $Titulo, $Categoria);
				        $Excel->EstablecerValoresDeArreglo($DatosColumnas['Columnas']);
				        $Excel->EstablecerAlineacionHorizontal($DatosColumnas['Limites'], 'Centrado');
				        $Excel->EstablecerColorFondo($DatosColumnas['Limites'], 'EEEEEE');
				        $Excel->EstablecerBordeDelgado($DatosColumnas['Limites']);
				        $Excel->ExportarArregloDatos($Consulta, 2, $DatosColumnas['Datos']);
    					$Ruta = $Excel->ExportarSalvar('xlsx', $Titulo);
     	 				$Correo = $this->Modelo->ConsultaCorreo($this->Informacion['Informacion']['idUsuario']);
				      	AppUtilidades::EnviarCorreoArchivo($Ruta, $Correo['Correo'], "", "Archivo adjunto del reporte de registros de clientes en Excel ");
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $IdAgente, $IdSupervisor, $CondicionesExtra, $ConsultaCampo, $ConsultaTelefonica, $Consulta, $Plantilla);
						exit();	
					else:
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $IdAgente, $IdSupervisor, $CondicionesExtra, $ConsultaCampo, $ConsultaTelefonica, $Consulta);
						echo 'fail';
						exit();
					endif;							
				}
			}
		}
	}