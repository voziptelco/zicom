<?php
	class FiltroSemanasAtraso extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}

		/**
		 * Metodo Publico
		 * Index()
		 *
		 * Pantalla de inicio
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltroSemanasAtraso', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmFiltradoClientes()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de clientes
		 */
		public function frmFiltradoClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Numero('minimoSemanas');
				$Validacion->Numero('maximoSemanas');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmFiltrosClientes'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltroSemanasAtraso', 'Filtrar', 'FiltroClientes.html')));
				unset($Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarClientes()
		 * 
		 * Prepara los datos del formulario en condiciones de busqueda en base de datos
		 * para despues mostrarlos en una pantalla de listado.
		 */		
		public function ConsultarClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL(AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($_POST,array('minimoSemanas', 'maximoSemanas'))));
					$DatosPost['Semanas'] = true;
					$ArregloCriterios = AppUtilidades::ArregloCriterios($DatosPost);
					$Consulta = $this->Modelo->ConsultaRegistros($ArregloCriterios, $this->Informacion['Informacion']['idUsuario']);
					$TotalSaldo = AppUtilidades::ObtenerTotalColumna($Consulta, "Saldo");
					$TotalSaldoTotal = AppUtilidades::ObtenerTotalColumna($Consulta, "SaldoTotal");
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Consulta', $Consulta);
					$Plantilla->Parametro('TotalSaldo', $TotalSaldo);
					$Plantilla->Parametro('TotalSaldoTotal', $TotalSaldoTotal);
					$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
						return number_format($Parametro, 2, '.', ',');
					});
					$Plantilla->Filtro('Cifrado', function($Parametro){
						return NeuralCriptografia::Codificar($Parametro, APP);
					});
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltroSemanasAtraso', 'Listado', 'Listado.html')));
					unset($DatosPost, $ArregloDatos, $ArregloCriterios, $ConsultaCampo, $ConsultaTelefonica, $Consulta, $Plantilla);
					exit();
				}
			}
		}
}