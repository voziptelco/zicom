<?php
	
	class Cartera extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		/**
		 * Metodo Publico 
		 * Index()
		 * 
		 * Pantalla principal 
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * public function frmListado()
		 * 
		 * Pantalla de listado de carteras
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarCarteras();
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Listado', 'Listado.html')));
				unset($Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * frmAgregar()
		 * 
		 * Pantalla para capturar Carteras
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Descripcion');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarCartera'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Agregar', 'Agregar.html')));
				unset($Validacion, $Plantilla);
			}
		}
		
		/**
		 * Metodo Publico
		 * Agregar()
		 * 
		 * Agregar la informacion de una cartera
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::ConvertirTextoUcwords(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)));	
					$Consulta = $this->Modelo->ConsultaCarteraExistente($DatosPost['Descripcion']);
					if($Consulta['Cantidad'] > 0){
						if($Consulta[0]['Status'] == 'ELIMINADO'){
							$Arreglo = array('Status' => 'ACTIVO');
							$Condicion = array('IdCartera' => $Consulta[0]['IdCartera']);
							$Omitidos = array('IdCartera', 'Descripcion');
							$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $Arreglo, $Condicion, $Omitidos, $Plantilla);
							exit();
						}else{
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $Consulta, $Plantilla);
							exit();	
						}
					}
					else{
						$Omitidos = array('IdCartera', 'Status');
						$this->Modelo->AgregarCartera($DatosPost, $Omitidos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Agregar', 'Exito.html')));
						unset($DatosPost, $Consulta, $Omitidos, $Plantilla);
						exit();	
					}	
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * frmEditar()
		 * 
		 * Pantalla para editar carteras
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdCartera = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Consulta = $this->Modelo->ConsultarCartera($IdCartera);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Descripcion');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdCartera', $_POST['Id']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarCartera'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Editar', 'Editar.html')));
				unset($Validacion, $Plantilla);
			}
		}
		
		/**
		 * Metodo Publico
		 * Editar()
		 * 
		 * Edidicion de la información de las carteras
		 */
		public function Editar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$IdCartera = NeuralCriptografia::DeCodificar($_POST['IdCartera'], APP);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('IdCartera'));
					$Consulta = $this->Modelo->ConsultaCarteraExistente($DatosPost['Descripcion']);
					if($Consulta['Cantidad'] > 0){
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Cartera', 'Error', 'ErrorRegistroDuplicado.html')));
						unset($IdCartera, $DatosPost, $Consulta, $Plantilla);
						exit();
					}
					else{
						$Omitidos = array('IdCartera', 'Status');
						$Arreglo = array('Descripcion' => $DatosPost['Descripcion']);
						$Condicion = array('IdCartera' => $IdCartera);
						$this->Modelo->EditarCartera($Arreglo, $Condicion, $Omitidos);
						unset($IdCartera, $DatosPost, $Omitidos, $Arreglo, $Condicion);
						exit();	
					}
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * Eliminar()
		 * 
		 * Prepara un cambio de estado del aregistro a ELIMINADO
		 */
		public function Eliminar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdCartera' => $Id);
					$Omitidos = array('IdCartera', 'Descripcion');
					$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}
