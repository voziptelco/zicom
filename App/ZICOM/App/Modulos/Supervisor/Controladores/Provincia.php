<?php

	class Provincia extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Piblico 
		 * Index()
		 * 
		 * Pantalla principal 
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmListado()
		 * 
		 * Pantalla de listado de provincias
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarProvincias();
				if($Consulta > 0):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('TotalRegistros', $Consulta);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Listado', 'Listado.html')));
					unset($IdSupervisor, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Error', 'NoResultados.html')));
					unset($IdSupervisor, $Consulta, $Plantilla);
					exit();
				endif;
			}
		}

		/**
		 * Metodo Publico 
		 * ConsultarProvinciasPagina()
		 *
		 * consulta provincias segun la pagina
		 */
		public function ConsultarProvinciasPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];	
				$Consulta = $this->Modelo->ConsultarProvinciasPagina($Inicio, $NumeroRegistros, $Busqueda);		
				if($Consulta['Cantidad']){
						$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
						unset($Consulta['Cantidad']);
						$Consulta = AppFiltrosRapidos::AplicarFiltrosCifrado($Consulta, 'IdProvincia');			        
				        $Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
	     				echo json_encode($Consulta);
	     				unset($DatosPost, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);	
					}
					else{
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;
					}
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmAgregar()
		 * 
		 * Formulario de insercion de datos de Provincia
		 */
		public function frmAgregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Descripcion');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmAgregarProvincia'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Agregar', 'Agregar.html')));
				unset($Validacion, $Plantilla);
			}	
		}
		
		/**
		 * Metodo Publico 
		 * frmAgregar()
		 * 
		 * Se preparan datos de Provincia para ser insetados
		 */
		public function Agregar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::ConvertirTextoUcwords(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)));	
					$Consulta = $this->Modelo->ConsultaProvinciaExistente($DatosPost['Descripcion']);
					if($Consulta['Cantidad'] > 0){
						if($Consulta[0]['Status'] == 'ELIMINADO'){
							$Arreglo = array('Status' => 'ACTIVO');
							$Condicion = array('IdProvincia' => $Consulta[0]['IdProvincia']);
							$Omitidos = array('IdProvincia', 'Descripcion');
							$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $Arreglo, $Condicion, $Omitidos, $Plantilla);
							exit();
						}else{
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Error', 'ErrorRegistroDuplicado.html')));
							unset($DatosPost, $Consulta, $Plantilla);
							exit();
						}
					}
					else{
						$Omitidos = array('IdProvincia', 'Status');
						$this->Modelo->AgregarProvincia($DatosPost, $Omitidos);
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Agregar', 'Exito.html')));
						unset($DatosPost, $Consulta, $Omitidos, $Plantilla);
						exit();	
					}	
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmEditar()
		 * 
		 * Pantalla de edicion de Provincias
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdProvincia = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Consulta = $this->Modelo->ConsultarProvincia($IdProvincia);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Descripcion');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('IdProvincia', $_POST['Id']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarProvincia'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Editar', 'Editar.html')));
				unset($Validacion, $Plantilla);
			}
		}
		
		/**
		 * Metodo Publico 
		 * Editar()
		 * 
		 * Edicion de la informacion de las provincias.
		 */
		public function Editar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$IdProvincia = NeuralCriptografia::DeCodificar($_POST['IdProvincia'], APP);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('IdProvincia'));	
					$Consulta = $this->Modelo->ConsultaProvinciaExistente($DatosPost['Descripcion']);
					if($Consulta['Cantidad'] > 0){
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('Provincia', 'Error', 'ErrorRegistroDuplicado.html')));
						unset($IdProvincia, $DatosPost, $Consulta, $Plantilla);
						exit();
					}
					else{
						$Omitidos = array('IdProvincia', 'Status');
						$Arreglo = array('Descripcion' => $_POST['Descripcion']);
						$Condicion = array('IdProvincia' => $IdProvincia);
						$this->Modelo->EditarProvincia($Arreglo, $Condicion, $Omitidos);
						unset($IdProvincia, $DatosPost, $Omitidos, $Arreglo, $Condicion);
						exit();		
					}	
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * Elimininar()
		 * 
		 * Prepara datos para cambiar el estado fde un resgistro
		 */
		public function Eliminar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) == true AND isset($_POST['Id']) == true  ) {
					$Id = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
					$Arreglo = array('Status' => 'ELIMINADO');
					$Condicion = array('IdProvincia' => $Id);
					$Omitidos = array('IdProvincia', 'Descripcion');
					$this->Modelo->ModificarEstadoRegistro($Arreglo, $Condicion, $Omitidos);
					unset($Id, $Arreglo, $Condicion, $Omitidos);
					exit();
				}
			}
		}
	}
?>