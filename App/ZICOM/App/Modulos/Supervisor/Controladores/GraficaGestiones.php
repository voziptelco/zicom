<?php
	
	class GraficaGestiones extends Controlador {

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();			
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
			public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GraficasGestiones', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
	     * Metodo Publico 
		 * frmFiltradoClientes()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de Gestiones
		 */
		public function frmFiltroGestiones(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Gerencias = $this->Modelo->ConsultaGerencias();
				$Carteras = $this->Modelo->ConsultaCarteras();
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Distritos = $this->Modelo->ConsultaDistritos();
				$Agentes = $this->Modelo->ConsultarAgentes($this->Informacion['Informacion']['idUsuario']);
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Numero('minimoSemanas', "Solo numérico");
				$Validacion->Numero('minimoSaldo', "Solo numérico");
				$Validacion->Numero('minimoSaldoTotal', "Solo numérico");
				$Validacion->Numero('maximoSemanas', "Solo numérico");
				$Validacion->Numero('maximoSaldo', "Solo numérico");
				$Validacion->Numero('maximoSaldoTotal', "Solo numérico");
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Gerencias', $Gerencias);
				$Plantilla->Parametro('Carteras', $Carteras);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Distritos', $Distritos);
				$Plantilla->Parametro('Agentes', $Agentes);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmFiltrosGestiones'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GraficasGestiones', 'Filtrar', 'FiltroGestiones.html')));
				unset($Gerencias, $Carteras, $Distritos, $Provincias, $Validacion, $Plantilla);
				exit();
			}
		}

		
		/**
		 * Metodo Publico
		 * ConsultarGestiones()
		 * 
		 * Consulta gestiones para ser graficadas 
		 */
		public function ConsultarGestiones(){
			set_time_limit(0);
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$ArregloDatos = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloDatos = AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($ArregloDatos, array('minimoSemanas', 'maximoSemanas', 'minimoSaldo', 'maximoSaldo', 'minimoSaldoTotal', 'maximoSaldoTotal')));
					if($ArregloDatos['TipoGestion'] == 'Telefonica'):
						unset($ArregloDatos['TipoGestion']);
						$Criterios = (!empty($ArregloDatos))? AppUtilidades::ArregloCriterios($ArregloDatos) : false;
						$Consulta = $this->Modelo->ConsultaGestiones($Criterios, $this->Informacion['Informacion']['idUsuario'], 'tbl_gestion_telefonica', 'IdGestionTelefonica');	 
						$Saldo = AppUtilidades::arrayColumn($Consulta, 'Saldo');
						$SaldoTotal = AppUtilidades::arrayColumn($Consulta, 'SaldoTotal');
						$Compromiso = AppUtilidades::arrayColumn($Consulta, 'Compromiso');
						$Compromiso = array_filter($Compromiso);
						$Compromiso = AppUtilidades::PorcentajeCompromiso($Compromiso);
						$Consulta = AppUtilidades::ObtenerColumnas($Consulta, array('NombreTitular', 'SemanaAtraso'));
						$Datos = json_encode(array('saldo' => $Saldo, 'saldototal' => $SaldoTotal, 'Compromiso' => $Compromiso, 'Semanas' =>$Consulta), JSON_NUMERIC_CHECK);
						echo $Datos;
						unset($ArregloDatos, $Criterios, $Consulta, $Saldo, $SaldoTotal, $Compromiso, $Datos);
					elseif($ArregloDatos['TipoGestion'] == 'Campo'):
						unset($ArregloDatos['TipoGestion']);
						$Criterios = (!empty($ArregloDatos))? AppUtilidades::ArregloCriterios($ArregloDatos) : false;
						$Consulta = $this->Modelo->ConsultaGestiones($Criterios, $this->Informacion['Informacion']['idUsuario'], 'tbl_gestion_campo', 'IdGestionCampo');	 
						$Saldo = AppUtilidades::array_column($Consulta, 'Saldo');
						$SaldoTotal = AppUtilidades::array_column($Consulta, 'SaldoTotal');
						$Compormiso = AppUtilidades::array_column($Consulta, 'Compromiso');
						$Compormiso = AppUtilidades::PorcentajeCompromiso($Compormiso);
						$Consulta = AppUtilidades::ObtenerColumnas($Consulta, array('NombreTitular', 'SemanaAtraso'));
						$Datos = json_encode(array('saldo' => $Saldo, 'saldototal' => $SaldoTotal, 'Compromiso' => $Compormiso, 'Semanas' =>$Consulta), JSON_NUMERIC_CHECK);
						echo $Datos;
						unset($ArregloDatos, $Criterios, $Consulta, $Saldo, $SaldoTotal, $Compormiso, $Datos);
					elseif($ArregloDatos['TipoGestion'] == 'Manual'):
						unset($ArregloDatos['TipoGestion']);
						$Criterios = (!empty($ArregloDatos))? AppUtilidades::ArregloCriterios($ArregloDatos) : false;
						$Consulta = $this->Modelo->ConsultaGestiones($Criterios, $this->Informacion['Informacion']['idUsuario'], 'tbl_gestion_manual', 'IdGestionManual');	 
						$Saldo = AppUtilidades::array_column($Consulta, 'Saldo');
						$SaldoTotal = AppUtilidades::array_column($Consulta, 'SaldoTotal');
						$Compormiso = AppUtilidades::array_column($Consulta, 'Compromiso');
						$Compormiso = AppUtilidades::PorcentajeCompromiso($Compormiso);
						$Consulta = AppUtilidades::ObtenerColumnas($Consulta, array('NombreTitular', 'SemanaAtraso'));
						$Datos = json_encode(array('saldo' => $Saldo, 'saldototal' => $SaldoTotal, 'Compromiso' => $Compormiso, 'Semanas' =>$Consulta), JSON_NUMERIC_CHECK);
						echo $Datos;
						unset($ArregloDatos, $Criterios, $Consulta, $Saldo, $SaldoTotal, $Compormiso, $Datos);
					endif;	
				}
			}
		} 
	}