<?php
	
	class GestionTelefonica extends Controlador{
		
		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);			
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmListado()
		 * 
		 * Pantalla de listado de gestiones
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdAgente = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarGestionTotal($IdAgente);
				if($Consulta > 0):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Total', $Consulta);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Listado', 'Listado.html')));
					unset($IdAgente, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Error', 'NoResultados.html')));
					unset($IdAgente, $Consulta, $Plantilla);
					exit();
				endif;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaPagina()
		 * 
		 * Consulta registros segun la busqueda y numero de pagina
		 */
		public function ConsultaPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];				
				$Consulta = $this->Modelo->ConsultarGestion($this->Informacion['Informacion']['idUsuario'], $Inicio, $NumeroRegistros, $Busqueda);
				if($Consulta['Cantidad'] > 0){
					$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
					unset($Consulta['Cantidad']);
					$Consulta = $this->Modelo->EstatusTelefonos($Consulta, 'IdDatoAgenda');
					$Consulta = AppFiltrosRapidos::AplicarFiltrosCifrado($Consulta, 'idGestionTelefonica');			        
     				$Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
     				echo json_encode($Consulta);
	     			unset($DatosPost, $ArregloCriterios, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);
				}
				else{
					$Consulta = array("current" => $Pagina,
					"rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;	
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * Marcacion()
		 * 
		 * Prepara datos para hacer una llamada.
		 */
		public function Marcacion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$IdAgente = $this->Informacion['Informacion']['idUsuario'];
					$Supervisor = $this->Modelo->ConsultarSupervisorAsignado($IdAgente);
					$IdSupervisor = $Supervisor[0]['IdSupervisor'];
					$CallerIdSupervisor = $this->Modelo->ConsultaCallerId($IdSupervisor);
					$CallerIdSupervisor = AppUtilidades::ObtenerCallerId($CallerIdSupervisor[0]);
					$Telefono = AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST));
					$Consulta = $this->Modelo->ConsultarConfiguracionConmutador();
					$Tamaño = count($Consulta);
					if($Tamaño > 0):
						$Prefijo = $this->Modelo->ConsultarCodigoPais($IdSupervisor);
						$DatosConmutador = array('host' => $Consulta[0]['host'], 'port' => $Consulta[0]['port'], 'user' => $Consulta[0]['user'], 'password' => NeuralCriptografia::DeCodificar($Consulta[0]['password'], APP), 'Prefijo' => $Prefijo[0]['CodigoPais'], 'callerid' => $CallerIdSupervisor, 'contexto' => $Consulta[0]['contexto']);
						$Conmutador = new AppAsterisk($DatosConmutador);
						$InformacionLlamada = $Conmutador->Marcar($this->Informacion['Informacion']['TipoCanal'], $this->Informacion['Informacion']['Extension'], $this->Informacion['Informacion']['idUsuario'], NeuralCriptografia::DeCodificar($Telefono['IdDatoAgenda'], APP), 7471103130/*$Telefono['Telefono']*/, $IdSupervisor);
						echo $InformacionLlamada;
						unset($Telefono, $Consulta, $DatosConmutador, $InformacionLlamada);
						exit();	
					else:
						echo "Fallo";
					endif;
				}	
			}
		}

		/**
		 * Metodo Publico 
		 * frmGestionarLlamada()
		 * 
		 * Pantalla de gestion telfonica.
		 */
		public function frmGestionarLlamada(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {		
				$ConsultaGestion = $this->Modelo->ConsultaOtrasGestiones(NeuralCriptografia::DeCodificar($_POST['IdRegistro'], APP));
				$Cliente= $this->Modelo->ConsultaDatosCliente(NeuralCriptografia::DeCodificar($_POST['IdRegistro'], APP));
				$LimiteLlamada = $this->Modelo->ConsultaTiempoLlamada($this->Informacion['Informacion']['idUsuario']);
				$TiempoLlamada = (isset( $LimiteLlamada[0]['TiempoLlamada'])) ? $LimiteLlamada[0]['TiempoLlamada'] : 300;
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Observaciones');
				$Validacion->Requerido('IdCodigo');
				$Validacion->Digitos('Importe', "Inserte solo números");
				$Validacion->Digitos('Cuotas', 'Inserte solo números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Telefono', $_POST['Telefono']);
				$Plantilla->Parametro('Cliente', $Cliente);
				$Plantilla->Parametro('Gestion', $ConsultaGestion);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmGestionarLlamada'));
				$Plantilla->Parametro('IdDatoAgenda', $_POST['IdRegistro']);
				$Plantilla->Parametro('Formulario', $_POST['Formulario']);
				$Plantilla->Parametro('ExtensionAgente',  $this->Informacion['Informacion']['Extension']);
				$Plantilla->Parametro('Tiempo', $TiempoLlamada);
				$Plantilla->Parametro('Codigos', $Codigos);
				$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
					return number_format($Parametro, 2, '.', ',');
				});
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Agregar', 'GestionarLlamada.html')));
				unset($Cliente, $ConsultaGestion, $Validacion, $Plantilla);
			}			
		}

		/**
		 * Metodo Publico
		 * frmVerMas()
		 *
		 * Ventana modal de ver mas detalles de la gestion
		 */
		public function frmVerMas(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdGestion = NeuralCriptografia::DeCodificar($_POST['id'], APP);
				$Detalles = $this->Modelo->ConsultaOtrasGestionesDetalles($IdGestion);
				$FechasImportes = $this->Modelo->ConsultarFechasCompromiso($IdGestion);
				if($Detalles){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Detalles', $Detalles[0]);
					$Plantilla->Parametro('FechasImportes', $FechasImportes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Agregar', 'DetallesGestion.html')));
					unset($IdGestion, $FechasImportes, $Detalles, $Plantilla);
					exit();
				}
				else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Error', 'ErrorDeConsulta.html')));
					unset($IdGestion, $FechasImportes, $Detalles, $Plantilla);
					exit();
				}
			}
		}

		/**
		 * Metodo Publico
		 * MonitorAgente()
		 *
		 * Monitorea el estado del agente
		 * @Return $StatusMonitor: Estado del agente
		 */
		public function MonitorAgente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Status = $this->Conectar();
				$Canales = $Status->ObtenerCanales();
				$CanalAgente = false;
				if(is_array($Canales) == true){
					foreach($Canales as $Canal) {
						if (AppUtilidades::ObtenerExension($Canal['Channel']) == $_POST['extension']) {
							$CanalAgente = $Canal;
						}
					}
					foreach($Canales as $Canal){
						if($CanalAgente['BridgedChannel'] == $Canal['Channel'] and $CanalAgente['Extension'] == $Canal['CallerIDnum']){
							if($Canal['ChannelStateDesc'] == "Up" AND $CanalAgente['ChannelStateDesc'] == "Up"){
								echo 'Contestado';
							}
							elseif($Canal['ChannelStateDesc'] == "" AND $CanalAgente['ChannelStateDesc'] == ""){
								echo 'Colgado';
							}
							else {
								echo 'NoContestado';
							}
						}
					}	
				}
				else{
					echo 'Colgado';
				}
			}
		}

		/**
		 * Metodo Publico
		 * GuardarGestion()
		 *
		 * Guarda los datos de la gestion telefonica
		 */
		public function GuardarGestion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(isset($_POST['Fecha']) AND isset($_POST['Importes'])){
						$DatosCompromiso = AppUtilidades::CombinaDosArrelos(AppPost::LimpiarInyeccionSQL(AppFormatos::FormatoFecha($_POST['Fecha'])),AppPost::LimpiarInyeccionSQL($_POST['Importes']), "Fecha", "Importe");
						unset($_POST['Fecha'], $_POST['Importes']);
					}
					if(AppPost::DatosVaciosOmitidos($_POST, array('TipoPago', 'Fecha_Compromiso', 'Importe', 'Cuotas', 'Prioridad', 'Formulario')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TipoPago', 'Compromiso', 'Importe', 'Cuotas', 'Fecha_Compromiso', 'IdDatoAgenda', 'Prioridad'));
						$Consulta = $this->Modelo->ConsultaUltimaGestion(NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP), $this->Informacion['Informacion']['idUsuario']);
						if($Consulta['Cantidad'] > 0){
							$DatosGestion = array('Compromiso' => $DatosPost['Compromiso'], "TipoPago" => $DatosPost['TipoPago'], "Cuotas" => $DatosPost['Cuotas'], 'Observaciones' => $DatosPost['Observaciones'], 'FechaHora_Captura' => AppFechas::ObtenerDatetimeActual(), 'Status' => 'CAPTURADO', 'IdCodigo' => $DatosPost['IdCodigo']);
							$DatosGestion['Prioridad'] = (isset($DatosPost['Prioridad'])) ? 'SI' : 'NO';
							$Omitidos = array('idGestionTelefonica', 'IdDatoAgenda', 'IdAgente', 'uniqueid', 'NumeroTelefono', 'Grabacion', 'StatusLlamada');
							$Condicion = array('idGestionTelefonica' => $Consulta[0]['idGestionTelefonica']);
							if($DatosPost['Compromiso'] == "SI"){
								if($DatosPost['TipoPago'] == "Parcial"){
									$this->Modelo->GuardarFechas($DatosCompromiso, $Consulta[0]['idGestionTelefonica'], "TELEFONICA");
									unset($DatosCompromiso);
								}
								elseif($DatosPost['TipoPago'] == "Total"){
									$DatosGestion['Cuotas'] = 1;
									$DatosPost['Fecha_Compromiso'] = AppFechas::FormatoFecha($DatosPost['Fecha_Compromiso']);
									$this->Modelo->GuardarFechas(array(array("Fecha" => $DatosPost['Fecha_Compromiso'],"Importe" => $DatosPost['Importe'])), $Consulta[0]['idGestionTelefonica'], "TELEFONICA");
								}
							}
							$this->Modelo->GuardarDatosDeGestionTelefonica($DatosGestion, $Condicion, $Omitidos);
							$this->Modelo->ActualizarCodigoGestion($DatosPost['IdCodigo'], $DatosPost['IdDatoAgenda']);	
							$this->Modelo->ActualizarStatusCobro(array('StatusCobro' => 'GESTIONADO'), array('IdDatoAgenda' => $DatosPost['IdDatoAgenda']));
							$Plantilla = new NeuralPlantillasTwig(APP);
							$Plantilla->Parametro('Formulario', $_POST['Formulario']);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Agregar', 'Exito.html')));
							unset($DatosPost, $Consulta, $DatosGestion, $Omitidos, $Condicion, $Plantilla);
							exit();
						}
						else{
							$Plantilla = new NeuralPlantillasTwig(APP);
							echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Error', 'ErrorLlamada.html')));
							unset($DatosPost, $Consulta);
							exit();
						}
					}
				}
			}
		}
		
		/**
		 * Metodo Publico
		 * frmEditarGestionTelefonica()
		 * 
		 * Pantalla de captura de gestión (si no se capturo)
		 */
		public function frmEditarGestionTelefonica(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosGestion = $this->Modelo->ConsultaGestionEditar(NeuralCriptografia::DeCodificar($_POST['Id']));
				$ConsultaGestion = $this->Modelo->ConsultaOtrasGestiones($DatosGestion[0]['IdDatoAgenda']);
				$Cliente = $this->Modelo->ConsultaDatosCliente($DatosGestion[0]['IdDatoAgenda']);
				$DatosGestion = AppFiltrosRapidos::FormatoTelefonoSinPrefijo($DatosGestion);
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('Observaciones');
				$Validacion->Requerido('IdCodigo');
				$Validacion->Digitos('Importe', "Inserte solo números");
				$Validacion->Digitos('Cuotas', 'Inserte solo números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
					return number_format($Parametro, 2, '.', ',');
				});
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				$Plantilla->Parametro('Telefono', $DatosGestion[0]['NumeroTelefono']);
				$Plantilla->Parametro('IdDatoAgenda', $DatosGestion[0]['IdDatoAgenda']);
				$Plantilla->Parametro('idGestionTelefonica', $_POST['Id']);
				$Plantilla->Parametro('Cliente', $Cliente);
				$Plantilla->Parametro('Gestion', $ConsultaGestion);
				$Plantilla->Parametro('Codigos', $Codigos);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarGestionLlamada'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Editar', 'EditarGestionLlamada.html')));
				unset($DatosGestion, $ConsultaGestion, $Cliente, $Validacion, $Plantilla);
				exit();
			}
		}

		/**
		 * Metodo Publico
		 * EditarGestionTelefonica()
		 *
		 * Prepara los datos que serán guardados en gestion telefonica (Caso que no se capturo la gestion)
		 */
		public function EditarGestionTelefonica(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(isset($_POST['Fecha']) AND isset($_POST['Importes'])){
						$DatosCompromiso = AppUtilidades::CombinaDosArrelos(AppPost::LimpiarInyeccionSQL(AppFormatos::FormatoFecha($_POST['Fecha'])),AppPost::LimpiarInyeccionSQL($_POST['Importes']), "Fecha", "Importe");
						unset($_POST['Fecha'], $_POST['Importes']);
					}
					if(AppPost::DatosVaciosOmitidos($_POST, array('TipoPago', 'Fecha_Compromiso', 'Importe', 'Cuotas', 'Prioridad')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('idGestionTelefonica', 'TipoPago', 'Compromiso', 'Importe', 'Cuotas', 'Fecha_Compromiso', 'Prioridad'));
						$DatosGestion = array('Compromiso' => $DatosPost['Compromiso'], "TipoPago" => $DatosPost['TipoPago'], "Cuotas" => $DatosPost['Cuotas'], 'Observaciones' => $DatosPost['Observaciones'], 'FechaHora_Captura' => AppFechas::ObtenerDatetimeActual(), 'Status' => 'CAPTURADO', 'IdCodigo' => $DatosPost['IdCodigo']);
						$DatosGestion['Prioridad'] = (isset($DatosPost['Prioridad'])) ? 'SI' : 'NO';
						$Omitidos = array('idGestionTelefonica', 'IdDatoAgenda', 'IdAgente', 'uniqueid', 'NumeroTelefono', 'Grabacion', 'StatusLlamada');
						$IdGestion = NeuralCriptografia::DeCodificar($DatosPost['idGestionTelefonica'], APP);
						$Condicion = array('idGestionTelefonica' => $IdGestion);
						if($DatosPost['Compromiso'] == "SI"){
							if($DatosPost['TipoPago'] == "Parcial"){
								$this->Modelo->GuardarFechas($DatosCompromiso, $IdGestion, "TELEFONICA");
								unset($DatosCompromiso);
							}
							if($DatosPost['TipoPago'] == "Total"){
								$DatosGestion['Cuotas'] = 1;
								$DatosPost['Fecha_Compromiso'] = AppFechas::FormatoFecha($DatosPost['Fecha_Compromiso']);
								$this->Modelo->GuardarFechas(array(array("Fecha" => $DatosPost['Fecha_Compromiso'],"Importe" => $DatosPost['Importe'])), $IdGestion, "TELEFONICA");
							}
						}
						$this->Modelo->GuardarDatosDeGestionTelefonica($DatosGestion, $Condicion, $Omitidos);
						$this->Modelo->ActualizarCodigoGestion($DatosPost['IdCodigo'], $DatosPost['IdDatoAgenda']);	
						$this->Modelo->ActualizarStatusCobro(array('StatusCobro' => 'GESTIONADO'), array('IdDatoAgenda' => $DatosPost['IdDatoAgenda']));
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Agregar', 'Exito.html')));
						unset($Omitidos, $Condicion, $Plantilla);
						exit();
					}
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmActualizarDatosCliente()
		 * 
		 * Pantalla de edicion de datos del cliente
		 * @return void
		 */
		public function frmActualizarDatosCliente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$IdDatoAgenda = NeuralCriptografia::DeCodificar($_POST['IdDatoAgenda'], APP);
				$Consulta = $this->Modelo->ConsultarDatosEspecificosCliente($IdDatoAgenda);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Cliente', $Consulta[0]);
				$Plantilla->Parametro('IdDatoAgenda', NeuralCriptografia::Codificar($IdDatoAgenda));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Editar', 'ActualizarDatosCliente.html')));
				unset($IdDatoAgenda, $Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarDatosCliente()
		 * 
		 * Prepara los datos que se pueden actualizar en una gestion de Telefonica
		 */
		public function ActualizarDatosCliente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::LimpiarInyeccionSQL($_POST), array('Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'IdDatoAgenda'));
					$Condicion = array("IdDatoAgenda" => NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP));
					unset($DatosPost['IdDatoAgenda']);
					$this->Modelo->ActualizarDatosCliente($DatosPost, $Condicion, $this->Informacion['Informacion']['idUsuario']);
					unset($DatosPost, $Condicion);
					exit();
				}
			}	
		}
		
		/**
		 * Realiza la conexion al conmutador
		 * @return AppControlAgentes; Devuelve el conector.
		 * @throws NeuralException
		 */
		private function Conectar(){
			$ConfiguracionConmutador = $this->Modelo->ConsultarConfiguracionConmutador();
  			if($ConfiguracionConmutador == true AND is_array($ConfiguracionConmutador)){
     			$this->ParametrosAcceso = array(
		         'host'=>$ConfiguracionConmutador[0]['host'],
		         'port'=>($ConfiguracionConmutador[0]['port'] == '') ? 5038 : $ConfiguracionConmutador[0]['port'],
		         'user'=>$ConfiguracionConmutador[0]['user'],
		         'password'=>NeuralCriptografia::DeCodificar($ConfiguracionConmutador[0]['password'], APP),
		      	);
		      	$Conexion = new AppConexionConmutador($this->ParametrosAcceso);
		      	if($Conexion->ObtenerConexion() == true) {
       	 			$Conexion->AutenticarConexion();
         			return $Conexion;
		      	}
			  	else{
					return 'ErrorConexion';
		      	}
   			}
		   	else{
		   		return 'ErrorAcceso';
		   	};
		}
	}