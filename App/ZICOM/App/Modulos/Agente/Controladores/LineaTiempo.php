<?php
	
	class LineaTiempo extends Controlador{

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LineaTiempo', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * HistorialGestiones()
		 * 
		 * Prepara la informacion para mostrar una patalla de historial de gestiones
		 */
		public function HistorialGestiones(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {				
				$IdCliente = NeuralCriptografia::DeCodificar($_POST['IdDatoAgenda']);
				$DatosCliente = $this->Modelo->ConsultaDatosCliente($IdCliente);
				$GestionesTelefonicas = $this->Modelo->ConsultaGestionCliente($IdCliente, 'tbl_gestion_telefonica', 'idGestionTelefonica');
				$GestionesCampo = $this->Modelo->ConsultaGestionCliente($IdCliente, 'tbl_gestion_campo', 'IdGestionCampo');
				$GestionesManuales = $this->Modelo->ConsultaGestionCliente($IdCliente, 'tbl_gestion_manual', 'IdGestionManual');
				$Gestiones = array_merge( $GestionesCampo, $GestionesTelefonicas, $GestionesManuales);
				if($Gestiones){
					$Gestiones = AppUtilidades::OrdenarMatrizColumna($Gestiones, "FechaHora_Captura", "DESC");
					$Gestiones = array_slice($Gestiones, 0, 10);
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro("DatosCliente", $DatosCliente[0]);
					$Plantilla->Parametro("Gestiones", $Gestiones);
					$Plantilla->Filtro('Cifrado', function($Parametro){
						return NeuralCriptografia::Codificar($Parametro, APP);
					});
					$Plantilla->Funcion('ParImpar', function($Parametro_1) {
					    return ($Parametro_1 % 2 == 0) ? 0 : 1;
					});
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LineaTiempo', 'Historial', 'HistorialGestiones.html')));
					unset($IdCliente, $DatosCliente, $GestionesTelefonicas, $GestionesCampo, $Gestiones, $Plantilla);
					exit();
				}else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LineaTiempo', 'Alertas', 'NoExistenGestiones.html')));
					unset($IdCliente, $DatosCliente, $GestionesTelefonicas, $GestionesCampo, $Gestiones, $Plantilla);
					exit();
				}
				
			}
		}
		
		/**
		 * Metodo Publico 
		 * VerDetalleCompormiso()
		 * 
		 * Pantalla de detalles de compromiso de pago
		 */
		public function VerDetalleCompormiso(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$Consulta = $this->Modelo->ConsultaFechasImportes(NeuralCriptografia::DeCodificar($_POST['IdGestion']), $_POST['TipoGestion']);
				if($Consulta){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('FechasImportes', $Consulta);
					$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
						return number_format($Parametro, 2, '.', ',');
					});
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LineaTiempo', 'Historial', 'DetalleCompromiso.html')));
					unset($Consulta, $Plantilla);
					exit();
				}
				else{
					$Plantilla = new NeuralPlantillasTwig(APP);
				    echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('LineaTiempo', 'Alertas', 'NoHayFechas.html')));
				    unset($Consulta, $Plantilla);
				    exit();
				}	
			}
		}
	}