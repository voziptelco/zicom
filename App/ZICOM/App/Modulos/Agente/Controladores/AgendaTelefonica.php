<?php

	class AgendaTelefonica extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);			
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * frmListado()
		 * 
		 * Lista los registros asignados a un Agente
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdAgente = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarAgendaTotal($IdAgente);
				if($Consulta > 0):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Total', $Consulta);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Listado', 'Listado.html')));
					unset($IdAgente, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Error', 'NoResultados.html')));
					unset($IdAgente, $Consulta, $Plantilla);
					exit();
				endif;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ListadoPagina()
		 * 
		 * consulta registros segun la pagina
		 */
		public function ListadoPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];	
				$Consulta = $this->Modelo->ConsultarAgenda($this->Informacion['Informacion']['idUsuario'], $Inicio, $NumeroRegistros, $Busqueda);		
				if($Consulta['Cantidad']){
					$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
					unset($Consulta['Cantidad']);
					$Consulta = AppFiltrosRapidos::AplicarFiltrosCifradoMoneda($Consulta);			        
			        $Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
     				echo json_encode($Consulta);
     				unset($DatosPost, $ArregloCriterios, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);	
				}
				else{
					$Consulta = array("current" => $Pagina,
									  "rowCount" => $NumeroRegistros,
					                  "rows" => $Consulta, 
									  "total"=>0);
					$Consulta = json_encode($Consulta);
					echo $Consulta;
				}
			}
		}		
		
		/**
		 * Metodo Publico
		 * frmEditar()
		 * 
		 * Prepara una pantalla para la edicion de un Registro dado
		 */
		public function frmEditar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdDatoAgenda = NeuralCriptografia::DeCodificar($_POST['Id'], APP);
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Consulta = $this->Modelo->ConsultarRegistroAgenda($IdDatoAgenda);
				$Validacion = new  NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido('DomicilioContractual');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Consulta', $Consulta);
				$Plantilla->Parametro('Codigos', $Codigos);
				$Plantilla->Parametro('IdRegistro', $IdDatoAgenda);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmEditarAgenda'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('AgendaTelefonica', 'Editar', 'Editar.html')));
				unset($IdDatoAgenda, $Consulta, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico
		 * Actuaalizar()
		 * 
		 * Actualiza los datos indicados de un registro en la Base de Datos
		 */
		public function Actualizar(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);										
					$IdDatoAgenta =$_POST['IdRegistro'];
					unset($_POST['IdRegistro']);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('DomicilioContractual', 'DistritoContractual', 'DomicilioAval', 'DistritoAval', 'SemanaAtraso', 'Saldo', 'SaldoTotal', 'Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'ClienteUnico'));
					$MatrizDatos = array('DomicilioContractual' => $DatosPost['DomicilioContractual'], 'Telefono1' => $DatosPost['Telefono1'], 'Telefono2' => $DatosPost['Telefono2'], 'Telefono3' => $DatosPost['Telefono3'], 'Telefono4' => $DatosPost['Telefono4']);
					$Omitidos = array('IdDatoAgenda', 'IdCliente' ,'IdSupervisor', 'IdGerencia', 'IdCartera', 'ClienteUnico', 'NombreTitular', 'IdProvincia', 'IdDistrito', 'NombreAval', 'DomicilioAval', 'DistritoAval', 'SemanaAtraso', 'Saldo', 'SaldoTotal', 'StatusRegistro', 'StatusCobro', 'IdCodigo');
					$Condicion = array('IdDatoAgenda' => $IdDatoAgenta);
				    $this->Modelo->ActualizarInformacionAgenda($MatrizDatos, $Omitidos, $Condicion, $this->Informacion['Informacion']['idUsuario']);
					unset($IdDatoAgenta, $MatrizDatos, $DatosPost, $Omitidos, $Condicion);
					exit();
				}
			}	
		}
	}

