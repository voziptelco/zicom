<?php
	
	class MarcacionSupervisor extends Controlador{
		
		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);			
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionTelefonica', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * Marcacion()
		 * 
		 * Prepara datos para hacer una llamada.
		 */
		public function Marcacion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdAgente = $this->Informacion['Informacion']['idUsuario'];
				$Supervisor = $this->Modelo->ConsultarSupervisorAsignado($IdAgente);
				$IdSupervisor = $Supervisor[0]['IdSupervisor'];					
				$Consulta = $this->Modelo->ConsultarConfiguracionConmutador();
				$Tamaño = count($Consulta);
				if($Tamaño > 0):
						$Prefijo = $this->Modelo->ConsultarCodigoPais($IdSupervisor);
						$DatosSupervisor = $this->Modelo->ConsultarDatosSupervisor($IdSupervisor);
						$CallerIdSupervisor = AppUtilidades::ObtenerCallerId($DatosSupervisor[0]);
						$DatosConmutador = array('host' => $Consulta[0]['host'], 'port' => $Consulta[0]['port'], 'user' => $Consulta[0]['user'], 'password' => NeuralCriptografia::DeCodificar($Consulta[0]['password'], APP), 'Prefijo' => $Prefijo[0]['CodigoPais'], 'callerid' => $CallerIdSupervisor, 'contexto' => $Consulta[0]['contexto']);
						$Conmutador = new AppAsterisk($DatosConmutador);
						$InformacionLlamada = $Conmutador->MarcarSupervisor($this->Informacion['Informacion']['TipoCanal'], $this->Informacion['Informacion']['Extension'], $DatosSupervisor[0]['Extension']);
						echo $InformacionLlamada;
						unset($Consulta, $DatosConmutador, $DatosSupervisor, $Marcacion, $InformacionLlamada);
						exit();	
					else:
						echo "Fallo";
					endif;
				}	
		}
		
		/**
		 * Metodo Publico
		 * MonitorAgente()
		 *
		 * Monitorea el estado del agente
		 * @Return $StatusMonitor: Estado del agente
		 */
		public function MonitorAgente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Consulta = $this->Modelo->ConsultarConfiguracionConmutador();
				$DatosConmutador = array('host' => $Consulta[0]['host'], 'port' => $Consulta[0]['port'], 'user' => $Consulta[0]['user'], 'password' => NeuralCriptografia::DeCodificar($Consulta[0]['password'], APP), 'Prefijo' => $Consulta[0]['Prefijo'], 'callerid' => $Consulta[0]['callerid'], 'contexto' => $Consulta[0]['contexto']);
				$Conmutador = new AppAsterisk($DatosConmutador);
				$StatusMonitor = $Conmutador->MonitorExtension($this->Informacion['Informacion']['Extension']);
				echo $StatusMonitor;
			}
		}
	}