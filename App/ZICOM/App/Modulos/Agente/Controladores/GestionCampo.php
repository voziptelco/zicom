<?php
	
	class GestionCampo extends Controlador{
		
		var $Informacion;

		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);			
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionCampo', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico
		 * frmListado()
		 * 
		 * Lista las Gestiones a Registros asignadas a un Agente
		 */
		public function frmListado(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdAgente = $this->Informacion['Informacion']['idUsuario'];
				$Consulta = $this->Modelo->ConsultarGestionTotal($IdAgente);
				if($Consulta > 0):
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Total', $Consulta);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionCampo', 'Listado', 'Listado.html')));
					unset($IdAgente, $Consulta, $Plantilla);
					exit();
				else:
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionCampo', 'Error', 'NoResultados.html')));
					unset($IdAgente, $Consulta, $Plantilla);
					exit();
				endif;
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaPagina()
		 * 
		 * Consulta registros segun la busqueda y numero de pagina
		 */
		public function ConsultaPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];				
				$Consulta = $this->Modelo->ConsultarGestion($this->Informacion['Informacion']['idUsuario'], $Inicio, $NumeroRegistros, $Busqueda);
				if($Consulta['Cantidad'] > 0){
					$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
					unset($Consulta['Cantidad']);
					$Consulta = $this->Modelo->EstatusTelefonos($Consulta, 'IdDatoAgenda');
					$Consulta = AppFiltrosRapidos::AplicarFiltrosCifrado($Consulta, 'IdGestionCampo');			        
     				$Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
     				echo json_encode($Consulta);
	     			unset($DatosPost, $ArregloCriterios, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);
				}
				else{
					$Consulta = array("current" => $Pagina,
										"rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;	
				}
			}
		}

		/**
		 * Metodo Publico 
		 * frmVerMas()
		 * 
		 * Pantalla de detalles de historial
		 */
		public function frmVerMas(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdGestion = NeuralCriptografia::DeCodificar($_POST['id'], APP);
				$Detalles = $this->Modelo->ConsultaOtrasGestionesDetalles($IdGestion);
				$FechasImportes = $this->Modelo->ConsultarFechasCompromiso($IdGestion);
				if($Detalles){
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro("Detalles", $Detalles[0]);
					$Plantilla->Parametro("FechasImportes", $FechasImportes);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionCampo', 'Detalle', 'DetallesGestion.html')));
					unset($IdGestion, $Detalles, $Plantilla);
					exit();
				}
				else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionCampo', 'Error', 'ErrorDeConsulta.html')));
					unset($IdGestion, $FechasImportes, $Detalles, $Plantilla);
					exit();
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmActualizarDatosCliente()
		 * 
		 * Pantalla de edicion de datos del cliente
		 * @return void
		 */
		public function frmActualizarDatosCliente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$IdDatoAgenda = NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['IdDatoAgenda']), APP);
				$Consulta = $this->Modelo->ConsultarDatosEspecificosCliente($IdDatoAgenda);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Cliente', $Consulta[0]);
				$Plantilla->Parametro('IdDatoAgenda', NeuralCriptografia::Codificar($IdDatoAgenda));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('GestionCampo', 'Editar', 'ActualizarDatosCliente.html')));
				unset($IdDatoAgenda, $Consulta, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarDatosCliente()
		 * 
		 * Prepara los datos que se pueden actualizar en una gestion de Telefonica
		 */
		public function ActualizarDatosCliente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::LimpiarInyeccionSQL($_POST), array('Telefono1', 'Telefono2', 'Telefono3', 'Telefono4', 'IdDatoAgenda'));
					$Condicion = array("IdDatoAgenda" => NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP));
					unset($DatosPost['IdDatoAgenda']);
					$this->Modelo->ActualizarDatosCliente($DatosPost, $Condicion, $this->Informacion['Informacion']['idUsuario']);
					unset($DatosPost, $Condicion);
					exit();
				}
			}	
		}
	}