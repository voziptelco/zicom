<?php
	
	class FiltradoClientes extends Controlador{

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();
			$this->Consultas = new AppFiltradoClientes(APP);
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);
			$Plantilla->Parametro('Nombres', $this->Informacion['Informacion']['Nombres']);			
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
		}
		
		/**
		 * Metodo Publico 
		 * frmFiltradoClientes()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de clientes
		 */
		public function frmFiltradoClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Gerencias = $this->Modelo->ConsultaGerencias();
				$Carteras = $this->Modelo->ConsultaCarteras();
				$Distritos = $this->Modelo->ConsultaDistritos();
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Colores = $this->Modelo->ConsultarCodigos();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Numero('minimoSemanas', "Solo numérico");
				$Validacion->Numero('minimoSaldo', "Solo numérico");
				$Validacion->Numero('minimoSaldoTotal', "Solo numérico");
				$Validacion->Numero('maximoSemanas', "Solo numérico");
				$Validacion->Numero('maximoSaldo', "Solo numérico");
				$Validacion->Numero('maximoSaldoTotal', "Solo numérico");
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Gerencias', $Gerencias);
				$Plantilla->Parametro('Carteras', $Carteras);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Distritos', $Distritos);
				$Plantilla->Parametro('Colores', $Colores);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmFiltrosClientes'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				if(isset($_POST['Formulario']) AND $_POST['Formulario'] != ""){
					$Plantilla->Parametro('Formulario', json_decode(AppConversores::HEX_ASCII($_POST['Formulario']), true));		
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Filtrar', 'FiltroClientesDatos.html')));
					unset($Gerencias, $Carteras, $Distritos, $Provincias, $Validacion, $Plantilla);
				}
				else{
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Filtrar', 'FiltroClientes.html')));
					unset($Gerencias, $Carteras, $Distritos, $Provincias, $Validacion, $Plantilla);
				}
				
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarClientes()
		 * 
		 * Hace una consulta con los parametros de busqueda recibidos
		 * y los muestra en una Tabla listado.
		 */
		public function ConsultarClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$Arreglo = AppUtilidades::ArregloEstados(AppUtilidades::ValidarCero($DatosPost, array('minimoSemanas', 'maximoSemanas', 'minimoSaldo', 'maximoSaldo', 'minimoSaldoTotal', 'maximoSaldoTotal')));
					$ArregloCriterios = AppUtilidades::ArregloCriterios($Arreglo, $DatosPost);
					$Consulta = $this->Consultas->ConsultaRegistrosTotal($ArregloCriterios, $this->Informacion['Informacion']['idUsuario']);				
					if($Consulta > 0){
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Formulario', AppConversores::ASCII_HEX(json_encode($_POST)));
						$Plantilla->Parametro('Consulta', NeuralCriptografia::Codificar(json_encode($ArregloCriterios, JSON_UNESCAPED_UNICODE),APP));
						$Plantilla->Parametro('Total', $Consulta);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Listado', 'Listado.html')));
						unset($DatosPost, $Arreglo, $ArregloCriterios, $Consulta, $Plantilla);	
					}
					else{
						$Plantilla = new NeuralPlantillasTwig(APP);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Error', 'NoResultados.html')));
						unset($DatosPost, $Condiciones, $Consulta, $Plantilla);
						exit();
					}
				}	
			}	
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarClientePagina()
		 * 
		 * Consulta registros segun la pagina
		 */
		public function ConsultarClientePagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
					set_time_limit(0);
					$ArregloCriterios = json_decode(NeuralCriptografia::DeCodificar($_POST['Consulta'], APP), true);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$Pagina = $DatosPost['current'];
					$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
					$NumeroRegistros = $DatosPost['rowCount'];
					$Busqueda = $DatosPost['searchPhrase'];
					$Consulta = $this->Consultas->ConsultaRegistros($ArregloCriterios, $this->Informacion['Informacion']['idUsuario'], $Inicio, $NumeroRegistros, $Busqueda);
					if($Consulta['Cantidad'] > 0){
						$total =($Busqueda == "")? $DatosPost['TotalRegistros']:$Consulta['Cantidad'];
						unset($Consulta['Cantidad']);
						//$Consulta = $this->Consultas->EstadosGestion($Consulta, 'tbl_gestion_campo', 'IdDatoAgenda');
						//$Consulta = $this->Consultas->EstadosGestion($Consulta, 'tbl_gestion_telefonica', 'IdDatoAgenda');
				        $Consulta = $this->Consultas->EstatusTelefonos($Consulta, 'IdDatoAgenda');
						$Consulta = AppFiltrosRapidos::AplicarFiltrosCifradoMoneda($Consulta);				        
				        $Consulta = array("current" => $Pagina,"rowCount" => $NumeroRegistros,"rows" => $Consulta, "total"=> $total);
	     				echo json_encode($Consulta);
	     				unset($DatosPost, $ArregloCriterios, $Consulta, $Pagina, $Inicio, $NumeroRegistros, $Busqueda);	
					}
					else{
						$Consulta = array("current" => $Pagina,
										  "rowCount" => $NumeroRegistros,
						                  "rows" => $Consulta, 
										  "total"=>0);
						$Consulta = json_encode($Consulta);
						echo $Consulta;
					}
					
			}
		}
		
		
		
		/**
		 * Metodo Publico
		 * frmGestionTelefonica()
		 * 
		 * Prepara una vista para mostrar los telefonos de un Cliente
		 */
		public function frmGestionTelefonica(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$ConsultaTelefonos = $this->Modelo->ConsultaTelefonos(NeuralCriptografia::DeCodificar($_POST['id'], APP));
				$Telefonos = $ConsultaTelefonos[0];
				$Telefonos = AppUtilidades::ArregloEstados($Telefonos); 
				$Telefonos = AppUtilidades::ObtenerIndices($Telefonos);
				if($Telefonos){
					$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
					$Validacion->Requerido('Telefono');
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
					$Plantilla->Parametro('Telefonos', $Telefonos);
					$Plantilla->Parametro('IdDatoAgenda', $_POST['id']);
					if(isset($_POST['Formulario']))
						$Plantilla->Parametro('Formulario', $_POST['Formulario']);
					$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmGestionTelefonica'));
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'GestionTelefonica.html')));
					unset($ConsultaTelefonos, $Telefonos, $Validacion, $Plantilla);
					exit();
				}
				else{
					$Plantilla = new NeuralPlantillasTwig(APP);
					$Plantilla->Parametro('IdDatoAgenda', $_POST['id']);
					if(isset($_POST['Formulario']))
						$Plantilla->Parametro('Formulario', $_POST['Formulario']);
					echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'SinTelefonos.html')));
					unset($ConsultaTelefonos, $Plantilla);
					exit();
				}
			}
		}
		
		/**
		 * Metodo publico 
		 * frmModificarTelefonos()
		 *
		 * pantalla para agregar o actualizar telefonos
		 * @return [type] [description]
		 */
		public function frmModificarTelefonos(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$IdDatoAgenda = NeuralCriptografia::DeCodificar($_POST['IdDatoAgenda'], APP);
				$Consulta = $this->Modelo->ConsultarDatosEspecificosCliente($IdDatoAgenda);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Cliente', $Consulta[0]);
				$Plantilla->Parametro('IdDatoAgenda', NeuralCriptografia::Codificar($IdDatoAgenda));
				if(isset($_POST['Formulario']))
					$Plantilla->Parametro('Formulario', $_POST['Formulario']);
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'ActualizarTelefonos.html')));
				unset($IdDatoAgenda, $Consulta, $Plantilla);
			}
		}
		/**
		 * Metodo Publico 
		 * frmGestionCampo()
		 * 
		 * Pantalla de registro de Gestion de Campo
		 * @return Void 
		 */
		public function frmGestionCampo(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Cliente = $this->Modelo->ConsultaDatosCliente(NeuralCriptografia::DeCodificar($_POST['id'], APP));
				$Gestion = $this->Modelo->ConsultaOtrasGestiones(NeuralCriptografia::DeCodificar($_POST['id'], APP), 'tbl_gestion_campo', 'IdGestionCampo');
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido("Fecha_Visita");
				$Validacion->Requerido("Nombre_Gestor");
				$Validacion->Requerido("IdCodigo");
				$Validacion->Requerido("Observaciones");
				$Validacion->Digitos('Importe', "Inserte solo números");
				$Validacion->Digitos('Cuotas', 'Inserte solo números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('IdCliente', $_POST['id']);
				$Plantilla->Parametro('IdDatoAgenda', AppConversores::ASCII_HEX($_POST['id']));
				$Plantilla->Parametro('Cliente', $Cliente);
				$Plantilla->Parametro('Gestion', $Gestion);
				$Plantilla->Parametro('Codigos', $Codigos);
				if(isset($_POST['Formulario']))
					$Plantilla->Parametro('Formulario', $_POST['Formulario']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmGestionCampo'));
				$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
					return number_format($Parametro, 2, '.', ',');
				});
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'GestionCampo.html')));
				unset($Cliente, $Gestion, $Validacion, $Plantilla);
			}
		}

		/**
		 * Metodo Publico
		 * GuardarGestion()
		 *
		 * Guarda la informacion de una gestion de campo
		 */
		public function GuardarGestion(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(isset($_POST['Fecha']) AND isset($_POST['Importes'])){
						$DatosCompromiso = AppUtilidades::CombinaDosArrelos(AppPost::LimpiarInyeccionSQL(AppFormatos::FormatoFecha($_POST['Fecha'])), AppPost::LimpiarInyeccionSQL($_POST['Importes']), "Fecha", "Importe");
						unset($_POST['Fecha'], $_POST['Importes']);
					}
					if(AppPost::DatosVaciosOmitidos($_POST, array('TipoPago', 'Fecha_Compromiso', 'Importe', 'Cuotas', 'Prioridad', 'Formulario')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TipoPago', 'Fecha_Visita', 'Compromiso', 'Importe', 'Cuotas', 'Fecha_Compromiso', 'IdDatoAgenda', 'Consulta'));
						$DatosPost['IdDatoAgenda'] = NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP);
						$DatosPost['Fecha_Visita'] = AppFechas::FormatoFecha($DatosPost['Fecha_Visita']);
						$DatosPost['Fecha_Compromiso'] = (!(empty($DatosPost['Fecha_Compromiso']))) ? AppFechas::FormatoFecha($DatosPost['Fecha_Compromiso']) : "";
						$DatosPost['IdAgente'] = $this->Informacion['Informacion']['idUsuario'];
						$DatosPost['Prioridad'] = (isset($DatosPost['Prioridad'])) ? 'SI' : 'NO';
						$DatosPost['FechaHora_Captura'] = AppFechas::ObtenerDatetimeActual();
						$DatosPost['Cuotas'] = ($DatosPost['TipoPago'] == "Total") ? 1 : $DatosPost['Cuotas'];
						$PagoTotal = array(array('Fecha' => $DatosPost['Fecha_Compromiso'], 'Importe' => $DatosPost['Importe']));
						unset($DatosPost['Fecha_Compromiso'], $DatosPost['Importe']);
						$IdGestion = $this->Modelo->GuardarInformacionGestion($DatosPost);
						$this->Modelo->ActualizarCodigoGestion($DatosPost['IdCodigo'], $DatosPost['IdDatoAgenda']);						
						if($DatosPost['Compromiso'] == "SI"){
							if($DatosPost['TipoPago'] == "Total"){
								$this->Modelo->GuardarFechas($PagoTotal, $IdGestion[0], "CAMPO");
							}else if($DatosPost['TipoPago'] == "Parcial"){
								$this->Modelo->GuardarFechas($DatosCompromiso, $IdGestion[0], "CAMPO");
								unset($DatosCompromiso);
							}
						}
						$this->Modelo->ActualizarStatusCobro(array('StatusCobro' => 'GESTIONADO'), array('IdDatoAgenda' => $DatosPost['IdDatoAgenda']));
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Formulario', $_POST['Formulario']);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'Exito.html')));
						unset($DatosPost, $PagoTotal, $result, $Plantilla);
						exit();
					}
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * frmGestionManual()
		 * 
		 * Pantalla de gestion manual 
		 */		
		public function frmGestionManual(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Cliente = $this->Modelo->ConsultaDatosCliente(NeuralCriptografia::DeCodificar($_POST['id'], APP));
				$Gestion = $this->Modelo->ConsultaOtrasGestiones(NeuralCriptografia::DeCodificar($_POST['id'], APP), 'tbl_gestion_manual', 'IdGestionManual');
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Requerido("Fecha_Visita");
				$Validacion->Requerido("IdCodigo");
				$Validacion->Requerido("Observaciones");
				$Validacion->Digitos('Importe', "Inserte solo números");
				$Validacion->Digitos('Cuotas', 'Inserte solo números');
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('IdCliente', $_POST['id']);
				$Plantilla->Parametro('IdDatoAgenda', AppConversores::ASCII_HEX($_POST['id']));
				$Plantilla->Parametro('Cliente', $Cliente);
				$Plantilla->Parametro('Gestion', $Gestion);
				$Plantilla->Parametro('Codigos', $Codigos);
				if(isset($_POST['Formulario']))
					$Plantilla->Parametro('Formulario', $_POST['Formulario']);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmGestionCampo'));
				$Plantilla->Filtro('Formato_Moneda',  function($Parametro){
					return number_format($Parametro, 2, '.', ',');
				});
				$Plantilla->Filtro('Cifrado', function($Parametro){
					return NeuralCriptografia::Codificar($Parametro, APP);
				});
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'GestionManual', 'GestionManual.html')));
				unset($Cliente, $Gestion, $Validacion, $Plantilla);		
			}			
		}
		
		/**
		 * Metodo Publico 
		 * GuardarGestionManual()
		 * 
		 * Guardar datos de gestion manual
		 */
		public function GuardarGestionManual(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					if(isset($_POST['Fecha']) AND isset($_POST['Importes'])){
						$DatosCompromiso = AppUtilidades::CombinaDosArrelos(AppPost::LimpiarInyeccionSQL(AppFormatos::FormatoFecha($_POST['Fecha'])), AppPost::LimpiarInyeccionSQL($_POST['Importes']), "Fecha", "Importe");
						unset($_POST['Fecha'], $_POST['Importes']);
					}	
					if(AppPost::DatosVaciosOmitidos($_POST, array('TipoPago', 'Fecha_Compromiso', 'Importe', 'Cuotas', 'Prioridad', 'Formulario')) == false) {
						$DatosPost = AppPost::ConvertirTextoUcwordsOmitido(AppPost::FormatoEspacio(AppPost::LimpiarInyeccionSQL($_POST)), array('TipoPago', 'Compromiso', 'Importe', 'Cuotas', 'Fecha_Compromiso', 'IdDatoAgenda', 'Consulta'));
						$DatosPost['IdDatoAgenda'] = NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP);
						$DatosPost['Fecha_Compromiso'] = (!(empty($DatosPost['Fecha_Compromiso']))) ? AppFechas::FormatoFecha($DatosPost['Fecha_Compromiso']) : "";
						$DatosPost['IdAgente'] = $this->Informacion['Informacion']['idUsuario'];
						$DatosPost['Prioridad'] = (isset($DatosPost['Prioridad'])) ? 'SI' : 'NO';
						$DatosPost['FechaHora_Captura'] = AppFechas::ObtenerDatetimeActual();
						$DatosPost['Cuotas'] = ($DatosPost['TipoPago'] == "Total") ? 1 : $DatosPost['Cuotas'];
						$PagoTotal = array(array('Fecha' => $DatosPost['Fecha_Compromiso'], 'Importe' => $DatosPost['Importe']));
						unset($DatosPost['Fecha_Compromiso'], $DatosPost['Importe']);
						$IdGestion = $this->Modelo->GuardarInformacionGestionManual($DatosPost);
						$this->Modelo->ActualizarCodigoGestion($DatosPost['IdCodigo'], $DatosPost['IdDatoAgenda']);						
						if($DatosPost['Compromiso'] == "SI"){
							if($DatosPost['TipoPago'] == "Total"){
								$this->Modelo->GuardarFechas($PagoTotal, $IdGestion[0], "MANUAL");
							}else if($DatosPost['TipoPago'] == "Parcial"){
								$this->Modelo->GuardarFechas($DatosCompromiso, $IdGestion[0], "MANUAL");
								unset($DatosCompromiso);
							}
						}
						$this->Modelo->ActualizarStatusCobro(array('StatusCobro' => 'GESTIONADO'), array('IdDatoAgenda' => $DatosPost['IdDatoAgenda']));
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Formulario', $_POST['Formulario']);
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'Exito.html')));
						unset($DatosPost, $PagoTotal, $result, $Plantilla);
						exit();
					}
				}
			}	
		}
		
		/**
		 * Metodo Publico 
		 * frmActualizarDatosCliente()
		 * 
		 * Pantalla de edicion de datos del cliente
		 * @return void
		 */
		public function frmActualizarDatosCliente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$IdDatoAgenda = NeuralCriptografia::DeCodificar($_POST['IdDatoAgenda'], APP);
				$Consulta = $this->Modelo->ConsultarDatosEspecificosCliente($IdDatoAgenda);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Cliente', $Consulta[0]);
				$Plantilla->Parametro('IdDatoAgenda', NeuralCriptografia::Codificar($IdDatoAgenda));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'ActualizarDatosCliente.html')));
				unset($IdDatoAgenda, $Consulta, $Plantilla);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarDatosCliente()
		 * 
		 * Prepara los datos que se pueden actualizar en una gestion de Telefonica
		 */
		public function ActualizarDatosCliente(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$Condicion = array("IdDatoAgenda" => NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP));
					unset($DatosPost['IdDatoAgenda']);
					$this->Modelo->ActualizarDatosCliente($DatosPost, $Condicion, $this->Informacion['Informacion']['idUsuario']);
					unset($DatosPost, $Condicion);
				}
			}	
		}
		
		/**
		 * Metodo Publico
		 * frmModificaDato()
		 *
		 * Prepara informacion para la modificacion de un solo dato
		 * @return [void] 
		 */
		public function frmModificaDato(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$IndiceTelefono = $DatosPost['Indice'];
				$IdDatoAgenda = NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP);
				$Consulta = $this->Modelo->ConsultarCampoEspecifico($IdDatoAgenda, $DatosPost['Indice']);
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Dato', $Consulta[0][$IndiceTelefono]);
				$Plantilla->Parametro('Indice', $DatosPost['Indice']);
				if(isset($_POST['Formulario']))
					$Plantilla->Parametro('Formulario', $_POST['Formulario']);
				$Plantilla->Parametro('IdDatoAgenda', NeuralCriptografia::Codificar($IdDatoAgenda));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoClientes', 'Gestion', 'ActualizarDato.html')));
				unset($IdDatoAgenda, $Consulta, $Plantilla);
			}
		}

		/**
		 * Metodo Publico 
		 *ActualizarDato()
		 * 
		 * Prepara los datos que se pueden actualizar en una gestion Telefonica
		 */
		public function ActualizarDato(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ){
				if(isset($_POST) AND isset($_POST['Key']) == true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$Condicion = array("IdDatoAgenda" => NeuralCriptografia::DeCodificar($DatosPost['IdDatoAgenda'], APP));
					unset($DatosPost['IdDatoAgenda']);
					$this->Modelo->ActualizarDatoEspecificoCliente($DatosPost, $Condicion, $this->Informacion['Informacion']['idUsuario']);
					unset($DatosPost, $Condicion);
				}
			}	
		}
	}