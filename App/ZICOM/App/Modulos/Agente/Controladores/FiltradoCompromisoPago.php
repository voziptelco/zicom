<?php
	
	class FiltradoCompromisoPago extends Controlador{

		var $Informacion;

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			AppSession::ValSessionGlobal();
			$this->Informacion = AppSession::InfomacionSession();			
		}
		
		/**
		 * Metodo Publico
		 * Index()
		 */
		public function Index(){
			$Menu = \Neural\WorkSpace\Miscelaneos::LeerModReWrite();
			$TipoUsuario = $this->Informacion['Permiso']['Nombre'];
			$Usuario = $this->Informacion['Informacion']['Nombres'] . ' ' . $this->Informacion['Informacion']['ApellidoPaterno'];
			$Imagen = AppUtilidades::ObtenerImagen($this->Informacion['Informacion']['idUsuario']);
			$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
			$Validacion->Requerido('PasswordActual');
			$Validacion->Requerido('PasswordNuevo');
			$Validacion->Requerido('PasswordVerifica');
			$Validacion->CampoIgual('PasswordVerifica', 'PasswordNuevo');
			$Plantilla = new NeuralPlantillasTwig(APP);
			$Plantilla->Parametro('Menu', $Menu[2]);
			$Plantilla->Parametro('TipoUsuario', $TipoUsuario);
			$Plantilla->Parametro('Usuario', $Usuario);			
			$Plantilla->Parametro('NombreUsuario', $this->Informacion['Informacion']['Nombres']);
			$Plantilla->Parametro('ApellidoPaterno', $this->Informacion['Informacion']['ApellidoPaterno']);
			$Plantilla->Parametro('ApellidoMaterno', $this->Informacion['Informacion']['ApellidoMaterno']);
			if(isset($Imagen[0]['Imagen']))
				$Plantilla->Parametro('Imagen', bin2hex($Imagen[0]['Imagen']));
			$Plantilla->Parametro('KeyPerfil', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
			$Plantilla->Parametro('ScriptPerfil', $Validacion->Constructor('frmCambioPassword'));
			echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoCompromisoPago', 'Index.html')));
			unset($Menu, $TipoUsuario, $Usuario, $Validacion, $Plantilla);
			exit();
		}
		
		/**
		 * Metodo Publico 
		 * frmFiltradoClientes()
		 * 
		 * Prepara una Patalla para seleccionar criterios de busqueda de clientes
		 */
		public function frmFiltradoClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Gerencias = $this->Modelo->ConsultaGerencias();
				$Carteras = $this->Modelo->ConsultaCarteras();
				$Distritos = $this->Modelo->ConsultaDistritos();
				$Provincias = $this->Modelo->ConsultaProvincias();
				$Codigos = $this->Modelo->ConsultarCodigos();
				$Validacion = new NeuralJQueryFormularioValidacion(true, true, false);
				$Validacion->Numero('minimoSemanas', "Solo numérico");
				$Validacion->Numero('minimoSaldo', "Solo numérico");
				$Validacion->Numero('minimoSaldoTotal', "Solo numérico");
				$Validacion->Numero('maximoSemanas', "Solo numérico");
				$Validacion->Numero('maximoSaldo', "Solo numérico");
				$Validacion->Numero('maximoSaldoTotal', "Solo numérico");
				$Plantilla = new NeuralPlantillasTwig(APP);
				$Plantilla->Parametro('Gerencias', $Gerencias);
				$Plantilla->Parametro('Carteras', $Carteras);
				$Plantilla->Parametro('Provincias', $Provincias);
				$Plantilla->Parametro('Distritos', $Distritos);
				$Plantilla->Parametro('Codigos', $Codigos);
				$Plantilla->Parametro('Scripts', $Validacion->Constructor('frmFiltrosClientes'));
				$Plantilla->Parametro('Key', AppConversores::ASCII_HEX(NeuralCriptografia::Codificar(date("Y-m-d"), APP)));
				echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoCompromisoPago', 'Filtrar', 'FiltroClientes.html')));
				unset($Gerencias, $Carteras, $Distritos, $Provincias, $MaximoSemanas, $MaximoSaldo, $MaximoSaldoTotal, $Validacion, $Plantilla);
				exit();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarClientes()
		 * 
		 * Prepara los datos del formulario en condiciones de busqueda en base de datos
		 * para despues mostrarlos en una pantalla de listado.
		 */		
		public function ConsultarClientes(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				if(isset($_POST) AND isset($_POST['Key'] )== true AND (NeuralCriptografia::DeCodificar(AppConversores::HEX_ASCII($_POST['Key']), APP) == date("Y-m-d")) == true){
					unset($_POST['Key']);
					$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
					$ArregloDatos = AppUtilidades::ArregloEstados($DatosPost);
					$ArregloCriterios = AppUtilidades::ArregloCriterios($ArregloDatos, $DatosPost);					
					$ConsultaCampo = $this->Modelo->ConsultaCompromisoTotal($ArregloCriterios, $this->Informacion['Informacion']['idUsuario'], "tbl_gestion_campo");
					$ConsultaTelefonica = $this->Modelo->ConsultaCompromisoTotal($ArregloCriterios, $this->Informacion['Informacion']['idUsuario'], "tbl_gestion_telefonica");				
					$ConsultaManual = $this->Modelo->ConsultaCompromisoTotal($ArregloCriterios, $this->Informacion['Informacion']['idUsuario'], "tbl_gestion_manual");
					$Consulta = array_merge($ConsultaCampo, $ConsultaTelefonica, $ConsultaManual);
					$Consulta = AppUtilidades::AgruparMatriz($Consulta, 'NombreTitular');
					$Total = count($Consulta);
					if ($Total > 0):
						$Criterios = json_encode($ArregloCriterios);
						$Plantilla = new NeuralPlantillasTwig(APP);
						$Plantilla->Parametro('Criterios', $Criterios);
						$Plantilla->Parametro('Total', $Total);				
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoCompromisoPago', 'Listado', 'Listado.html')));
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $ConsultaCampo, $ConsultaTelefonica, $Consulta, $Plantilla);
						exit();
					else:
						$Plantilla = new NeuralPlantillasTwig(APP);			
						echo $Plantilla->MostrarPlantilla(AppPlantilla::Separador(array('FiltradoCompromisoPago', 'Error', 'NoResultados.html')));
						unset($DatosPost, $ArregloDatos, $ArregloCriterios, $ConsultaCampo, $ConsultaTelefonica, $Consulta, $Plantilla);
						exit();
					endif;
				}
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaClientesPagina()
		 * 
		 * Consulta registros segun la pagina
		 */
		function ConsultaClientesPagina(){
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND $_SERVER['HTTP_REFERER'] != $_SERVER['HTTP_HOST'] ) {
				$Condiciones = json_decode($_POST['Criterios'], true);
				$DatosPost = AppPost::LimpiarInyeccionSQL($_POST);
				$Pagina = $DatosPost['current'];
				$Inicio = (!$Pagina) ? 0 : ($Pagina - 1) * $DatosPost['rowCount'];					
				$NumeroRegistros = $DatosPost['rowCount'];
				$Busqueda = $DatosPost['searchPhrase'];
				$IdSupervisor = $this->Informacion['Informacion']['idUsuario'];	
				$Campo = $this->Modelo->ConsultaCompromiso($Condiciones, $IdSupervisor, 'tbl_gestion_campo', $Busqueda);
				$Telefonica = $this->Modelo->ConsultaCompromiso($Condiciones, $IdSupervisor, 'tbl_gestion_telefonica', $Busqueda);
				$Manual = $this->Modelo->ConsultaCompromiso($Condiciones, $IdSupervisor, 'tbl_gestion_manual', $Busqueda);
				$Consulta = array_merge($Campo, $Telefonica, $Manual);
				$Consulta = AppUtilidades::AgruparMatriz($Consulta, 'NombreTitular');
				$Consulta = AppUtilidades::OrdenarMatrizColumna($Consulta, 'FechaHora_Captura', "DESC");
				$Consulta =($Consulta == true)? array_slice($Consulta, $Inicio, $NumeroRegistros):Null;
				$Total = count($Consulta);
				if($Total > 0){
					$total =($Busqueda == "")? $DatosPost['Total']:$Total;
					$Consulta = AppFiltrosRapidos::AplicarFiltrosCifradoMoneda($Consulta);
					$Consulta = array("current" => $Pagina,
									  "rowCount" => $NumeroRegistros,
					                  "rows" => $Consulta, 
									  "total"=>$total);
					$Consulta = json_encode($Consulta);
					echo $Consulta;	
				}
				else{
					$Consulta = array("current" => $Pagina,
									  "rowCount" => $NumeroRegistros,
					                  "rows" => $Consulta, 
									  "total" => 0);
					$Consulta = json_encode($Consulta);
					echo $Consulta;
				}
			}
		}
	}