<?php
	
	class GestionManual_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGestionTotal($Id = false)
		 * 
		 * Consulta el total de los registros asignados a un Agente 
		 * @param $Id: Parametro de busqueda (Id del Agente)
		 * @return Resultado de la consulta
		 */
		public function ConsultarGestionTotal($Id = false){
			if($Id == true){
				$SQL = "SELECT tbl_gestion_manual.IdDatoAgenda ".
					"FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					"INNER JOIN tbl_gestion_manual ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_manual.IdDatoAgenda ".
					"INNER JOIN tbl_datos_agenda ON tbl_gestion_manual.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					"INNER JOIN tbl_informacion_usuarios ON tbl_gestion_manual.IdAgente = tbl_informacion_usuarios.idUsuario ".
					"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$Id'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->rowCount();
			}
		}
		
			/**
		 * Metodo Publico
		 * ConsultarGestion($Id = false)
		 * 
		 * Consulta los registros asignados a un Agente 
		 * @param $Id: Parametro de busqueda (Id del Agente)
		 * @return Resultado de la consulta
		 */
		public function ConsultarGestion($Id = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($Id == true){
				$SQL = "SELECT tbl_gestion_manual.IdDatoAgenda, tbl_informacion_usuarios.Nombres, tbl_datos_agenda.NombreTitular, tbl_gestion_manual.FechaHora_Captura, tbl_gestion_manual.Observaciones, tbl_gestion_manual.Compromiso, tbl_gestion_manual.IdGestionManual ".
					"FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					"INNER JOIN tbl_gestion_manual ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_manual.IdDatoAgenda ".
					"INNER JOIN tbl_datos_agenda ON tbl_gestion_manual.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					"INNER JOIN tbl_informacion_usuarios ON tbl_gestion_manual.IdAgente = tbl_informacion_usuarios.idUsuario ".
					"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$Id'";
				if($Criterio != ""){
					$SQL .= " AND (tbl_informacion_usuarios.Nombres LIKE '%$Criterio%' OR NombreTitular LIKE '%$Criterio%' OR tbl_informacion_usuarios.ApellidoPaterno LIKE '%$Criterio%' OR tbl_gestion_manual.FechaHora_Captura LIKE '%$Criterio%' OR tbl_gestion_manual.TipoPago LIKE '%$Criterio%' OR tbl_gestion_manual.Compromiso LIKE '%$Criterio%')"; 
				}
				$SQL .= " ORDER BY tbl_gestion_manual.FechaHora_Captura DESC LIMIT $Inicio, $Cantidad";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				unset($Consulta, $SQL, $Id);
				return $Resultado;	
			}
		}
		
		/**
		 * Metodo Publico 
		 * EstatusTelefonos($Consulta = false, $Campo = false)
		 * 
		 * Consulta la disponibilidad de telefonos para un registro
		 * @param $Consulta: Array de datos.
		 */
		public function EstatusTelefonos($Consulta = false, $Campo = false){
			if($Consulta == true){
				foreach($Consulta as $Arreglo){
					foreach($Arreglo as $Columna=>$Valor){
						if($Columna == $Campo){
							$Arreglo['StatusTelefono'] = $this->ConsultaStatusTelefonos($Valor);
						}
					}
					$Lista[] = $Arreglo;
				}
				return $Lista;
			}
		}
		
				/**
		 * Metodo Privado
		 * ConsultaStatusTelefonos($IdAgenda = false)
		 * 
		 * Consulta si existe telefonos en la bd para un cliente dado.
		 * @param $IdAgenda: Identificador del agente
		 * @return string de estado (Disponible o nodisponible)
		 */
		private function ConsultaStatusTelefonos($IdAgenda = false){
			if($IdAgenda == true){
				$SQL = 'SELECT Telefono1, Telefono2, Telefono3, Telefono4 FROM tbl_datos_agenda WHERE IdDatoAgenda="'.$IdAgenda.'"';
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetch();
				$Resultado = AppUtilidades::ArregloEstados($Resultado);
				$Tamano = count($Resultado);
				unset($Resultado, $Consulta, $SQL, $Conexion);
				return ($Tamano > 0) ? "DISPONIBLE" : "NODISPONIBLE";
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarDatosEspecificosCliente($IdCliente = false)
		 *
		 * Consulta datos especificos del cliente 
		 * @param bool $IdCliente: identificador del cliente
		 * @return array: Matriz de resultados
		 */
		public function ConsultarDatosEspecificosCliente($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas("Telefono1, Telefono2, Telefono3, Telefono4, DomicilioContractual, DomicilioAval");
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
			/**
		 * Metodo Publico 
		 * ConsultaOtrasGestionesDetalles($IdGestion = false)
		 * 
		 * Consulta los detalles de una gestion del historial
		 * @param $IdGestion: Identificador de busqueda
		 * @return Resultado de la consulta.
		 */
		public function ConsultaOtrasGestionesDetalles($IdGestion = false){
			if($IdGestion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_gestion_manual');
				$Consulta->Columnas('tbl_gestion_manual.Compromiso, tbl_gestion_manual.Observaciones');
				$Consulta->Condicion("IdGestionManual = '$IdGestion'");
				$Consulta->Ordenar('tbl_gestion_manual.FechaHora_Captura', 'DESC');
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarFechasCompromiso($IdGestion = false)
		 * 
		 * Consulta las fechas/importes de compromiso de una gestion
		 * @param $IdGestion: Identificador para hacer la busqueda
		 * @return Matriz de resultados de consulta y numero de registros.
		 */
		public function ConsultarFechasCompromiso($IdGestion = false){
			if ($IdGestion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_fecha_compromiso');
				$Consulta->Columnas('Fecha, Importe');
				$Consulta->Condicion("IdGestion = '$IdGestion'");
				$Consulta->Condicion("TipoGestion = 'MANUAL'");
				return $Consulta->Ejecutar(false, true);
			}
		}
	
	}