<?php 
	
	class FiltradoCompromisoPago_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo publico
		 * ConsultaGerencias()
		 * 
		 * Consulta todas la gerencias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 * 
		 * Consulta todas la Carteras
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 * 
		 * Consulta todos las Provincias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaDistritos()
		 * 
		 * Consulta todos los Distritos
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaDistritos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCodigos()
		 * 
		 * Consulta lso codigos de gestion disponibles
		 */
		public function ConsultarCodigos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_codigos');
			$Consulta->Columnas('IdCodigo, Codigo, Descripcion, Color');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaCompromiso($ArregloCriterios = false, $IdAgente = false)
		 * 
		 * Consulta los registros con compromiso de pago, con distintos criterios
		 * @param $ArreloCriterios: Arreglo de criterios
		 * @param $IdAgente: Indice de agente asignado a registros.
		 * @return Matriz de consulta.
		 */
		public function ConsultaCompromiso($ArregloCriterios = false, $IdAgente = false, $Tabla = false, $Criterio = false){
			if($IdAgente == true and $Tabla == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, ClienteUnico, NombreTitular, SemanaAtraso, Saldo, SaldoTotal, MAX($Tabla.FechaHora_Captura) AS FechaHora_Captura, tbl_codigos.Color ".
					"FROM tbl_datos_agenda INNER JOIN $Tabla ON tbl_datos_agenda.IdDatoAgenda = $Tabla.IdDatoAgenda ".
					"INNER JOIN tbl_asignacion_registros ON tbl_datos_agenda.IdDatoAgenda = tbl_asignacion_registros.IdDatoAgenda ".
					"LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					"INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
					"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente' AND $Tabla.Compromiso = 'SI'";
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
						foreach($ArregloCriterios AS $Condicion){
							$SQL .= " AND ".$Condicion;
						}	
				}
				if($Criterio == true){
					$SQL .= " AND (ClienteUnico LIKE '%$Criterio%' OR NombreTitular LIKE '%$Criterio%' OR SemanaAtraso LIKE '%$Criterio%' OR Saldo LIKE '%$Criterio%' OR SaldoTotal LIKE '%$Criterio%') ";
				}
				$SQL .= " GROUP BY NombreTitular ORDER BY FechaHora_Captura";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				unset($Consulta, $SQL, $Id);
				return $Resultado;
			}			
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaCompromiso($ArregloCriterios = false, $IdAgente = false)
		 * 
		 * Consulta los registros con compromiso de pago, con distintos criterios
		 * @param $ArreloCriterios: Arreglo de criterios
		 * @param $IdAgente: Indice de agente asignado a registros.
		 * @return Matriz de consulta.
		 */
		public function ConsultaCompromisoTotal($ArregloCriterios = false, $IdAgente = false, $Tabla = false){
			if($IdAgente == true and $Tabla == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, NombreTitular ".
					"FROM tbl_datos_agenda INNER JOIN $Tabla ON tbl_datos_agenda.IdDatoAgenda = $Tabla.IdDatoAgenda ".
					"INNER JOIN tbl_asignacion_registros ON tbl_datos_agenda.IdDatoAgenda = tbl_asignacion_registros.IdDatoAgenda ".
					"LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					"INNER JOIN tbl_agente_asignado ON tbl_asignacion_registros.IdAsignacionRegistro = tbl_agente_asignado.IdAsignacionRegistro ".
					"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente' AND $Tabla.Compromiso = 'SI'";
				if($ArregloCriterios == true AND is_array($ArregloCriterios) == true AND count($ArregloCriterios) > 0){
					foreach($ArregloCriterios AS $Condicion){
						$SQL .= " AND ".$Condicion;
					}	
				}
				$SQL .= " GROUP BY NombreTitular";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				unset($IdAgente, $ArregloCriterios, $Tabla, $SQL);
			    return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}			
		}
	}