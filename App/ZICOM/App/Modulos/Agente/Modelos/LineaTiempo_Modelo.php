<?php
	
	class LineaTiempo_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultaGestionCliente($IdCliente = false, $Tabla = false, $Llave = false)
		 * 
		 * Consulta los registros de Gestion en los que aparezca el cliente dado
		 * @param $IdCleinte: ID del cliente a buscar
		 * @param $Tabla: Nombre de la tabla en funcion del tipo de Gestion(campo o telefonica)
		 * @param $Llave: Id de busqueda en funcion del tipo de Gestion(idGestionTelefonica, IdGestionCampo)
		 * @return Matriz de consulta.
		 */
		public function ConsultaGestionCliente($IdCliente = false, $Tabla = false, $Llave = false){
			if($IdCliente == true){
				$SQL = "SELECT $Llave, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno, Compromiso, Observaciones, FechaHora_Captura ".
   	                   "FROM $Tabla INNER JOIN tbl_informacion_usuarios ON $Tabla.IdAgente = tbl_informacion_usuarios.idUsuario ".
					   "WHERE $Tabla.IdDatoAgenda = '$IdCliente' AND $Tabla.Compromiso = 'SI' ORDER BY FechaHora_Captura DESC";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}		
		}
		
		/**
		 * Metodo Publico
		 * ConsultaDatosCliente($IdCliente = false)
		 * 
		 * Consulta el nombre y cliente unico del cliente 
		 * @param $IdCliente: Condicion de busqueda.
		 * @return Matriz de consulta.
		 */
		public function ConsultaDatosCliente($IdCliente = false){
			if($IdCliente == true){
				$SQL = "SELECT NombreTitular, ClienteUnico FROM tbl_datos_agenda WHERE IdDatoAgenda = '$IdCliente'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaFechasImportes($IdGestion = false, $TipoGestion = false)
		 * 
		 * Consulta todas la fechas e importes de compromiso de pago, para el tipo de gestion indicada
		 * @param $IdGestion: Identificador de la gestion
		 * @param $TipoGestion: String tipo de la gestion (CAMPO o TELEFONICA)
		 * @return Matriz de consulta
		 */
		public function ConsultaFechasImportes($IdGestion = false, $TipoGestion = false){
			if($IdGestion == true and $TipoGestion == true){
				$SQL = "SELECT Fecha, Importe FROM tbl_fecha_compromiso WHERE IdGestion = '$IdGestion' AND TipoGestion = '$TipoGestion'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->fetchAll(PDO::FETCH_ASSOC);
			}
		}
	}