<?php
	
	class FiltradoClientes_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo publico
		 * ConsultaGerencias()
		 * 
		 * Consulta todas la gerencias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaGerencias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_gerencias');
			$Consulta->Columnas('IdGerencia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCodigos()
		 * 
		 * Consulta lso codigos de gestion disponibles
		 */
		public function ConsultarCodigos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_codigos');
			$Consulta->Columnas('IdCodigo, Codigo, Descripcion, Color');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaCarteras()
		 * 
		 * Consulta todas la Carteras
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaCarteras(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_carteras');
			$Consulta->Columnas('IdCartera, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaProvincias()
		 * 
		 * Consulta todos las Provincias
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaProvincias(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_provincia');
			$Consulta->Columnas('IdProvincia, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo publico
		 * ConsultaDistritos()
		 * 
		 * Consulta todos los Distritos
		 * @return Matriz de resultado de consulta
		 */
		public function ConsultaDistritos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_distritos');
			$Consulta->Columnas('IdDistrito, Descripcion');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaTelefonos($IdCliente)
		 * 
		 * Consulta los telefonos de un Cliente
		 * @parm $IdCliente: El criterio de busqueda
		 * @return El resultado de la consulta
		 */
		public function ConsultaTelefonos($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas('Telefono1, Telefono2, Telefono3, Telefono4');
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultaDatosCliente($IdCliente = false)
		 *
		 * Consulta los datos de un cliente
		 * @param $IdCliente: Identificador de cliente
		 * @param Consulta con el nombre del Cliente
		 */
		public function ConsultaDatosCliente($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas("NombreTitular, NombreAval, SemanaAtraso, Saldo, SaldoTotal");
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultaOtrasGestiones($IdCliente = false)
		 *
		 * @param $IdCliente: identificador de busqueda
		 * Consulta un historial de las gestiones anteriores (si las hay)
		 */
		public function ConsultaOtrasGestiones($IdCliente = false, $Tabla = false, $Campo = false){
			if($IdCliente == true AND $Tabla == true AND $Campo == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla($Tabla);
				$Consulta->Columnas("$Tabla.$Campo, $Tabla.FechaHora_Captura, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno");
				$Consulta->InnerJoin('tbl_informacion_usuarios', "$Tabla.IdAgente", 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("$Tabla.IdDatoAgenda = '$IdCliente'");
				$Consulta->Ordenar("$Tabla.FechaHora_Captura", 'DESC');
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarDatosEspecificosCliente($IdCliente = false)
		 *
		 * Consulta datos especificos del cliente 
		 * @param bool $IdCliente: identificador del cliente
		 * @return array: Matriz de resultados
		 */
		public function ConsultarDatosEspecificosCliente($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas("Telefono1, Telefono2, Telefono3, Telefono4, DomicilioContractual");
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCampoEspecifico($IdCliente = false, $Indice = false)
		 *
		 * Consulta el dato que se desea modificar.
		 * @param boolean $IdCliente [description]
		 * @param boolean $Indice    [description]
		 */
		public function ConsultarCampoEspecifico($IdCliente = false, $Indice = false){
			if($IdCliente == true and $Indice == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla("tbl_datos_agenda");
				$Consulta->Columnas($Indice);
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * GuardarInformacionGestion($ArregloDatos = false)
		 * 
		 * Se guardan la informacionde de una gestion de campo
		 * @paramc $ArregloDatos: Datos a insertar.
		 * @return Id de gestion 
		 */
		public function GuardarInformacionGestion($ArregloDatos = false){
			if($ArregloDatos == true AND is_array($ArregloDatos)){
				$SQL = 'CALL AgregarGestionCampo(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, @IdGestionCampo)';
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->bindValue(1, $ArregloDatos['IdDatoAgenda']);
				$Consulta->bindValue(2, $ArregloDatos['IdAgente']);
				$Consulta->bindValue(3, $ArregloDatos['Fecha_Visita']);
				$Consulta->bindValue(4, $ArregloDatos['Nombre_Gestor']);
				$Consulta->bindValue(5, $ArregloDatos['Observaciones']);
				$Consulta->bindValue(6, $ArregloDatos['Compromiso']);
				$Consulta->bindValue(7, $ArregloDatos['TipoPago']);
				$Consulta->bindValue(8, $ArregloDatos['Cuotas']);
				$Consulta->bindValue(9, $ArregloDatos['Prioridad']);
				$Consulta->bindValue(10, $ArregloDatos['FechaHora_Captura']);
				$Consulta->bindValue(11, $ArregloDatos['IdCodigo']);
				$Consulta->execute();		 
				return $Consulta->fetch();
			}
		}
		
		/**
		 * Metodo Publico 
		 * GuardarInformacionGestionManual($ArregloDatos = false)
		 * 
		 * Guardar datos en bd de la gestion con retorno de id
		 * @param $ArregloDatos: array de datos a guardar
		 * @return id del ultimo ingresado
		 */
		public function GuardarInformacionGestionManual($ArregloDatos = false){
			if($ArregloDatos == true AND is_array($ArregloDatos)){
				$SQL = 'CALL AgregarGestionManual(?, ?, ?, ?, ?, ?, ?, ?, ?, @IdGestionManual)';
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->bindValue(1, $ArregloDatos['IdDatoAgenda']);
				$Consulta->bindValue(2, $ArregloDatos['IdAgente']);
				$Consulta->bindValue(3, $ArregloDatos['Observaciones']);
				$Consulta->bindValue(4, $ArregloDatos['Compromiso']);
				$Consulta->bindValue(5, $ArregloDatos['TipoPago']);
				$Consulta->bindValue(6, $ArregloDatos['Cuotas']);
				$Consulta->bindValue(7, $ArregloDatos['Prioridad']);
				$Consulta->bindValue(8, $ArregloDatos['FechaHora_Captura']);
				$Consulta->bindValue(9, $ArregloDatos['IdCodigo']);
				$Consulta->execute();		 
				return $Consulta->fetch();
			}
		}
		
		/**
		 * Medoto Publico
		 * ActualizarCodigoGestion($Codigo = false, $IdDatoAgenda = false)
		 * 
		 * Actualiza el codigo de la gestion
		 * @param $Codigo: nuevo codigo de gestion
		 * @param $IdDatoAgenda : id del registro
		 */
		public function ActualizarCodigoGestion($Codigo = false, $IdDatoAgenda = false){
			if($Codigo == true AND $IdDatoAgenda == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');				
				$SQL->Sentencia('IdCodigo', $Codigo);
				$SQL->Condicion('IdDatoAgenda', $IdDatoAgenda);
				$SQL->Actualizar();	
			}
		}

		/**
		 * Metodo Publico
		 * GuardarFechas($ArregloFechasImportes, $IdGestion = false, $TipoGestion = false)
		 *
		 * Guardar fechas de compromiso de pago
		 * @param bool $ArregloFechasImporte: Matriz de fechas e importes
		 * @param bool $IdGestion: identificador de la gestion
		 * @param bool $TipoGestion: Tipo de la Gestion
		 * @throws NeuralException
		 */
		public function GuardarFechas($ArregloFechasImportes = false, $IdGestion = false, $TipoGestion = false){
			if($ArregloFechasImportes == true AND $IdGestion == true AND $TipoGestion == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_fecha_compromiso');	
				foreach($ArregloFechasImportes as $Arreglo){
					$SQL->Sentencia('IdGestion', $IdGestion);
					$SQL->Sentencia('TipoGestion', $TipoGestion);
					foreach($Arreglo as $Columna => $Valor){
						$SQL->Sentencia($Columna, $Valor);	
					}
					$SQL->Insertar();
				}
			    		
			}
		}

		/**
		 * Metodo Publico
		 * ActualizarStatusCobro($Arreglo = false, $Condicion = false)
		 * 
		 * Actualiza el estatus de cobro de el cliente.
		 * @param $Arreglo: array de datos que se actualizaran.
		 * @param $Condicion: Id del registro a modificar.
		 */ 
		public function ActualizarStatusCobro($Arreglo = false, $Condicion = false){
			if($Arreglo == true AND $Condicion == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');
				$SQL->Sentencia('StatusCobro',$Arreglo['StatusCobro']);					
				$SQL->Condicion('IdDatoAgenda', $Condicion['IdDatoAgenda']);
				$SQL->Actualizar();
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarDatosCliente($MatrizDatos = false, $Condicion = false)
		 * 
		 * Actualiza datos de cliente dentro de una gestion
		 * @param $MatrizDatos: Matriz de datos 
		 * @param $Condicion: Id del registro a modificar
		 * @return void
		 */
		public function ActualizarDatosCliente($MatrizDatos = false, $Condicion = false, $IdAgente = false){
			if($MatrizDatos == true and is_array($MatrizDatos)){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');
				$SQL->Sentencia('Telefono1', $MatrizDatos['Telefono1']);
				$SQL->Sentencia('Telefono2', $MatrizDatos['Telefono2']);	
				$SQL->Sentencia('Telefono3', $MatrizDatos['Telefono3']);	
				$SQL->Sentencia('Telefono4', $MatrizDatos['Telefono4']);							
				$SQL->Condicion('IdDatoAgenda', $Condicion['IdDatoAgenda']);
				$SQL->Actualizar();
				$this->AgregarABitacora($IdAgente, $Condicion, $MatrizDatos);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarDatosCliente($MatrizDatos = false, $Condicion = false)
		 * 
		 * Actualiza un dato de cliente dentro de una gestion
		 * @param boolean $MatrizDatos [description]
		 * @param boolean $Condicion   [description]
		 * @return void
		 */
		public function ActualizarDatoEspecificoCliente($MatrizDatos = false, $Condicion = false, $IdAgente = false){
			if($MatrizDatos == true and is_array($MatrizDatos)){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');
				foreach ($MatrizDatos as $key => $value) {
					$SQL->Sentencia($key, $value);
				}							
				$SQL->Condicion('IdDatoAgenda', $Condicion['IdDatoAgenda']);
				$SQL->Actualizar();
				$this->AgregarABitacora($IdAgente, $Condicion, $MatrizDatos);
			}
		}
		
		/**
		 * Metodo Publico 
		 * AgregarABitacora($IdAgente = false, $IdDatoAgenda = false, $Datalles = false)
		 *
		 * Inserción  de datos a l atabla bitacora
		 * @param boolean $IdAgente     [description]
		 * @param boolean $IdDatoAgenda [description]
		 * @param boolean $Datalles     [description]
		 */
		private function AgregarABitacora($IdAgente = false, $IdDatoAgenda = false, $Datalles = false){
				$SQL = 'CALL AgregarBitacora(?, ?, ?, ?, ?)';
				$Conexion = NeuralConexionDB::DoctrineDBAL(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->bindValue(1, AppFechas::ObtenerFechaActual());
				$Consulta->bindValue(2, AppFechas::ObtenerHoraActual());
				$Consulta->bindValue(3, $IdAgente);
				$Consulta->bindValue(4, $IdDatoAgenda['IdDatoAgenda']);
				$Consulta->bindValue(5, implode(', ', array_keys(AppUtilidades::ArregloEstados($Datalles))));
				$Consulta->execute();
		}
	}
	