<?php 

	class MarcacionSupervisor_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarDatosSupervisor($IdSupervisor = false)
		 * 
		 * Consulta de datos del supervisor
		 * @param boolean $IdSupervisor Identificador del supervisor
		 */
		public function ConsultarDatosSupervisor($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla("tbl_informacion_usuarios");
				$Consulta->Columnas('callerid, Extension');
				$Consulta->Condicion("idUsuario = '$IdSupervisor'");
				return $Consulta->Ejecutar(false, true);	
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarSupervisorAsignado($IdAgente = false)
		 *
		 * Consulta el supervisor asignado al agente
		 * @param bool $IdAgente: identificador del supervisor
		 * @return array: arreglo de resultados
		 */
		public function ConsultarSupervisorAsignado($IdAgente = false){
			if($IdAgente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor');
				$Consulta->Columnas('IdSupervisor');
				$Consulta->Condicion("IdAgente = '$IdAgente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarConfiguracionConmutador()
		 * 
		 * Consulta los datos de Configuracion del Conmutador
		 * @return Matriz de datos de consulta
		 */
		public function ConsultarConfiguracionConmutador(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_configuracion_conmutador');
			$Consulta->Columnas(self::ListarColumnas('tbl_configuracion_conmutador', array('idConfiguracion'), false, APP));
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarCodigoPais($IdSupervisor = false)
		 * 
		 * Consulta el prefijo de pais de la troncal aisgnada al supervisor
		 */
		public function ConsultarCodigoPais($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_troncal_asignada_supervisor');
				$Consulta->Columnas('CodigoPais');
				$Consulta->InnerJoin('tbl_troncales', 'tbl_troncal_asignada_supervisor.IdTroncal', 'tbl_troncales.IdTroncal');
				$Consulta->Condicion("tbl_troncal_asignada_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_troncales.Status != 'Eliminado'");
				return $Consulta->Ejecutar(false, true);
			}
		}
	}