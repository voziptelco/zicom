<?php
	
	class ReporteGestiones_Modelo extends Modelo{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarGestionesTelefonicas($IdAgente = fale, $Criterios = false)
		 * 
		 * Consulta las gestiones Telefonicas
		 * @param $IdAgente: Identificador del agente
		 * @param $Criterios: Arrglo de criterios para la consulta
		 */
		public function ConsultarGestionesTelefonicas($IdAgente = false, $Criterios = false){
			if($IdAgente == true){
				$SQL = "SELECT CONCAT(tbl_informacion_usuarios.Nombres,' ', tbl_informacion_usuarios.ApellidoPaterno) AS Nombres, tbl_informacion_usuarios.Extension, tbl_datos_agenda.NombreTitular, tbl_gestion_telefonica.NumeroTelefono, tbl_cdr.calldate, SEC_TO_TIME(tbl_cdr.duration) AS duration, tbl_gestion_telefonica.StatusLlamada, tbl_gestion_telefonica.Observaciones, tbl_gestion_telefonica.Compromiso, CONCAT(tbl_codigos.Codigo, ' ', tbl_codigos.Descripcion) AS Codigo, tbl_gestion_telefonica.FechaHora_Captura ".
					   "FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					   "INNER JOIN tbl_gestion_telefonica ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_telefonica.IdDatoAgenda ".
					   "INNER JOIN tbl_informacion_usuarios ON tbl_gestion_telefonica.IdAgente = tbl_informacion_usuarios.idUsuario ".
					   "INNER JOIN tbl_datos_agenda ON tbl_gestion_telefonica.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					   "INNER JOIN tbl_cdr ON tbl_gestion_telefonica.uniqueid = tbl_cdr.uniqueid ".
					   "LEFT JOIN tbl_codigos ON tbl_gestion_telefonica.IdCodigo = tbl_codigos.IdCodigo ".
					   "WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente'";
	   			if($Criterios == true and is_array($Criterios)){
	   				foreach($Criterios AS $Condicion){
	   					$SQL .= " AND ".$Condicion;
	   				}
	   			}
	   			$Consulta = $this->Conexion->prepare($SQL);
	   			$Consulta->execute();
	   			$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);	
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;   			
			}
		}
		
		/**
		 * Metodo public 
		 * ConsultaPermisoAgente($IdAgente = false)}
		 * 
		 * Consulta los premisos de exportar excel de un agente
		 * @param $IdAgente:   Identificador del a agente a consultar
		 * @return Consulta
		 */
		public function ConsultaPermisoAgente($IdAgente = false){
			if($IdAgente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_informacion_usuarios');
				$Consulta->Columnas('ExportarExcel');
				$Consulta->Condicion("idUsuario = '$IdAgente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGestionesCampo($IdAgente = fale, $Criterios = false)
		 * 
		 * Consulta las gestiones de campo 
		 * @param $IdAgente: identificador del agente
		 * @param $Criterios: arreglo de criterios. 
		 */
		public function ConsultarGestionesCampo($IdAgente = fale, $Criterios = false){
			if($IdAgente == true){
				$SQL = "SELECT CONCAT(tbl_informacion_usuarios.Nombres,' ', tbl_informacion_usuarios.ApellidoPaterno) AS Nombres, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.DomicilioContractual, tbl_gestion_campo.FechaHora_Captura, tbl_gestion_campo.Nombre_Gestor, tbl_gestion_campo.Fecha_Visita, tbl_gestion_campo.Observaciones, tbl_gestion_campo.Compromiso, CONCAT(tbl_codigos.Codigo, ' ', tbl_codigos.Descripcion) AS Codigo ".
					   "FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					   "INNER JOIN tbl_gestion_campo ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_campo.IdDatoAgenda ".
					   "INNER JOIN tbl_informacion_usuarios ON tbl_gestion_campo.IdAgente = tbl_informacion_usuarios.idUsuario ".
					   "INNER JOIN tbl_datos_agenda ON tbl_gestion_campo.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					   "LEFT JOIN tbl_codigos ON tbl_gestion_campo.IdCodigo = tbl_codigos.IdCodigo ".
					   "WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente'";
	   			if($Criterios == true and is_array($Criterios)){
	   				foreach($Criterios AS $Condicion){
	   					$SQL .= " AND ".$Condicion;
	   				}
	   			}
	   			$Consulta = $this->Conexion->prepare($SQL);
	   			$Consulta->execute();
	   			$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);	
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;   			
			}
		}
		
			/**
		 * Metodo Publico
		 * ConsultarGestionesManuales($IdAgente = fale, $Criterios = false)
		 * 
		 * Consulta las gestiones manuales
		 * @param $IdAgente: identificador del agente
		 * @param $Criterios: arreglo de criterios. 
		 */
		public function ConsultarGestionesManuales($IdAgente = fale, $Criterios = false){
			if($IdAgente == true){
				$SQL = "SELECT CONCAT(tbl_informacion_usuarios.Nombres,' ', tbl_informacion_usuarios.ApellidoPaterno) AS Nombres, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.DomicilioContractual, tbl_gestion_manual.FechaHora_Captura, tbl_gestion_manual.Observaciones, tbl_gestion_manual.Compromiso, CONCAT(tbl_codigos.Codigo, ' ', tbl_codigos.Descripcion) AS Codigo ".
					   "FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					   "INNER JOIN tbl_gestion_manual ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_manual.IdDatoAgenda ".
					   "INNER JOIN tbl_informacion_usuarios ON tbl_gestion_manual.IdAgente = tbl_informacion_usuarios.idUsuario ".
					   "INNER JOIN tbl_datos_agenda ON tbl_gestion_manual.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					   "LEFT JOIN tbl_codigos ON tbl_gestion_manual.IdCodigo = tbl_codigos.IdCodigo ".
					   "WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$IdAgente'";
	   			if($Criterios == true and is_array($Criterios)){
	   				foreach($Criterios AS $Condicion){
	   					$SQL .= " AND ".$Condicion;
	   				}
	   			}
	   			$Consulta = $this->Conexion->prepare($SQL);
	   			$Consulta->execute();
	   			$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);	
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;   			
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaCorreo($IdAgente = false)
		 * 
		 * Consulta el correo de un usuario
		 * @param $IdAgente: identificador
		 */
		public function ConsultaCorreo($IdAgente = false){
			if($IdAgente == true){
				$SQL = "SELECT Correo FROM tbl_informacion_usuarios WHERE idUsuario = $IdAgente";
				$Consulta = $this->Conexion->prepare($SQL);
	   			$Consulta->execute();
	   			return $Consulta->fetch(PDO::FETCH_ASSOC);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultaColores()
		 * 
		 * Consulta la tabla de codigos
		 * @return Un areeglo con Codigo y color
		 */
		public function ConsultaColores(){		
		    $Consulta = new NeuralBDConsultas($this->Conexion);
		    $Consulta->Tabla('tbl_codigos');
		    $Consulta->Columnas('CONCAT(Codigo, " ", Descripcion) AS Codigo, Color');
		    $Consulta->Condicion('Status != "ELEMINADO"');
		    return $Consulta->Ejecutar(false, true);
		}
	}