<?php 

	class GestionTelefonica_Modelo extends AppSQLConsultas{

		/**
		 * Metodo Constructor
		 */
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGestionTotal($Id = false)
		 * 
		 * Consulta el total de los registros asignados a un Agente 
		 * @param $Id: Parametro de busqueda (Id del Agente)
		 * @return Resultado de la consulta
		 */
		public function ConsultarGestionTotal($Id = false){
			if($Id == true){
				$SQL = "SELECT tbl_gestion_telefonica.idGestionTelefonica ".
					"FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					"INNER JOIN tbl_gestion_telefonica ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_telefonica.IdDatoAgenda ".
					"INNER JOIN tbl_datos_agenda ON tbl_gestion_telefonica.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					"INNER JOIN tbl_informacion_usuarios ON tbl_gestion_telefonica.IdAgente = tbl_informacion_usuarios.idUsuario ".
					"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$Id'";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->rowCount();
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarGestion($Id = false)
		 * 
		 * Consulta los registros asignados a un Agente 
		 * @param $Id: Parametro de busqueda (Id del Agente)
		 * @return Resultado de la consulta
		 */
		public function ConsultarGestion($Id = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($Id == true){
				$SQL = "SELECT tbl_gestion_telefonica.idGestionTelefonica, tbl_gestion_telefonica.IdDatoAgenda, CONCAT(tbl_informacion_usuarios.Nombres,', ', tbl_informacion_usuarios.ApellidoPaterno) AS Nombres, tbl_datos_agenda.NombreTitular, tbl_gestion_telefonica.FechaHora_Captura, tbl_gestion_telefonica.NumeroTelefono, tbl_gestion_telefonica.TipoPago, tbl_gestion_telefonica.Observaciones, tbl_gestion_telefonica.Compromiso, tbl_gestion_telefonica.Status ".
					"FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
					"INNER JOIN tbl_gestion_telefonica ON tbl_asignacion_registros.IdDatoAgenda = tbl_gestion_telefonica.IdDatoAgenda ".
					"INNER JOIN tbl_datos_agenda ON tbl_gestion_telefonica.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
					"INNER JOIN tbl_informacion_usuarios ON tbl_gestion_telefonica.IdAgente = tbl_informacion_usuarios.idUsuario ".
					"WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$Id' ";
				if($Criterio != ""){
					$SQL .= " AND (tbl_informacion_usuarios.Nombres LIKE '%$Criterio%' OR NombreTitular LIKE '%$Criterio%' OR tbl_informacion_usuarios.ApellidoPaterno LIKE '%$Criterio%' OR tbl_gestion_telefonica.FechaHora_Captura LIKE '%$Criterio%' OR tbl_gestion_telefonica.TipoPago LIKE '%$Criterio%' OR tbl_gestion_telefonica.Compromiso LIKE '%$Criterio%')"; 
				}
				$SQL .= " ORDER BY tbl_gestion_telefonica.FechaHora_Captura DESC LIMIT $Inicio, $Cantidad";
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				unset($Consulta, $SQL, $Id);
				return $Resultado;	
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarFechasCompromiso($IdGestion = false)
		 * 
		 * Consulta las fechas/importes de compromiso de una gestion
		 * @param $IdGestion: Identificador para hacer la busqueda
		 * @return Matriz de resultados de consulta y numero de registros.
		 */
		public function ConsultarFechasCompromiso($IdGestion = false){
			if ($IdGestion == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_fecha_compromiso');
				$Consulta->Columnas('Fecha, Importe');
				$Consulta->Condicion("IdGestion = '$IdGestion'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarConfiguracionConmutador()
		 * 
		 * Consulta los datos de Configuracion del Conmutador
		 * @return Matriz de datos de consulta
		 */
		public function ConsultarConfiguracionConmutador(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_configuracion_conmutador');
			$Consulta->Columnas(self::ListarColumnas('tbl_configuracion_conmutador', array('idConfiguracion'), false, APP));
			return $Consulta->Ejecutar(false, true);
		}

		/**
		 * Metodo Publico
		 * ConsultaDatosCliente($IdCliente = false)
		 *
		 * Consulta los datos del cliente
		 * @param bool $IdCliente: identificador del cliente
		 * @return array: Matriz de resultados
		 */
		public function ConsultaDatosCliente($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas("NombreTitular, NombreAval, SemanaAtraso, Saldo, SaldoTotal");
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarDatosEspecificosCliente($IdCliente = false)
		 *
		 * Consulta datos especificos del cliente 
		 * @param bool $IdCliente: identificador del cliente
		 * @return array: Matriz de resultados
		 */
		public function ConsultarDatosEspecificosCliente($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas("Telefono1, Telefono2, Telefono3, Telefono4, DomicilioContractual, DomicilioAval");
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultaOtrasGestiones($IdCliente = false)
		 *
		 * Consulta Historial de gestiones
		 * @param bool $IdCliente: identificador del cliente
		 * @return array: Matriz de resultados
		 */
		Public function ConsultaOtrasGestiones($IdCliente = false){
			if($IdCliente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_gestion_telefonica');
				$Consulta->Columnas('tbl_gestion_telefonica.idGestionTelefonica, tbl_gestion_telefonica.FechaHora_Captura, tbl_informacion_usuarios.Nombres, tbl_informacion_usuarios.ApellidoPaterno');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_gestion_telefonica.IdAgente', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_gestion_telefonica.IdDatoAgenda = '$IdCliente'");
				$Consulta->Condicion("tbl_gestion_telefonica.Status = 'CAPTURADO'");
				$Consulta->Ordenar('tbl_gestion_telefonica.FechaHora_Captura', 'DESC');
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaGestionEditar($IdGestion = false)
		 * 
		 * Consulta una gestion que no se ha capturado
		 * @param $IdGestion: Identificador de la gestion
		 * @return Matriz de resultados
		 */
        public function ConsultaGestionEditar($IdGestion = false){
        	if($IdGestion == true){
        		$Consulta = new NeuralBDConsultas(APP);
        		$Consulta->Tabla('tbl_gestion_telefonica');
        		$Consulta->Columnas('IdDatoAgenda, NumeroTelefono');
        		$Consulta->Condicion("idGestionTelefonica = '$IdGestion'");
        		$Consulta->Condicion("tbl_gestion_telefonica.Status = 'NOCAPTURADO'");
        		return $Consulta->Ejecutar(false, true);
        	}
        }
		
		/**
		 * Metodo Publico
		 * ConsultaOtrasGestionesDetalles($IdGestion = false)
		 *
		 * Consulta mas informacion de la gestion
		 * @param bool $IdGestion: identificador de la gestion
		 * @return array: Matriz de resultados
		 */
		public function ConsultaOtrasGestionesDetalles($IdGestion = false){
			if($IdGestion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_gestion_telefonica');
				$Consulta->Columnas('tbl_gestion_telefonica.Compromiso, tbl_gestion_telefonica.Observaciones, tbl_gestion_telefonica.Grabacion');
				$Consulta->Condicion("idGestionTelefonica = '$IdGestion'");
				$Consulta->Condicion("tbl_gestion_telefonica.StatusLlamada != 'NOCONTESTADO'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultaFechasCompromiso($IdGestion = false)
		 *
		 * Consultar fechas de compromiso
		 * @param bool $IdGestion: identificador de la gestion
		 * @return array: Matriz de resultados
		 */
		public function ConsultaFechasCompromiso($IdGestion = false){
			if($IdGestion == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_fecha_compromiso');
				$Consulta->Columnas('tbl_fecha_compromiso.Fecha');
				$Consulta->InnerJoin('tbl_informacion_usuarios', 'tbl_gestion_telefonica.IdAgente', 'tbl_informacion_usuarios.idUsuario');
				$Consulta->Condicion("tbl_gestion_telefonica.IdDatoAgenda = '$IdCliente'");
				$Consulta->Ordenar('tbl_gestion_telefonica.FechaHora_Captura', 'DESC');
				return $Consulta->Ejecutar(false, true);
			}	
		}

		/**
		 * Metodo Publico
		 * ConsultaUltimaGestion($IdCliente = false, $IdAgente = false)
		 *
		 * Consulta la ultima gestion generada por el sistema
		 * @param bool $IdCliente: identificador del cliente
		 * @param bool $IdAgente: identificador del agente
		 * @return array: Matriz de resultados
		 */
		public function ConsultaUltimaGestion($IdCliente = false, $IdAgente = false){
			if($IdCliente == true AND $IdAgente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_gestion_telefonica');
				$Consulta->Columnas('idGestionTelefonica');
				$Consulta->Condicion("IdDatoAgenda = '$IdCliente'");
				$Consulta->Condicion("IdAgente = '$IdAgente'");
				$Consulta->Condicion("Status = 'NOCAPTURADO'");
				$Consulta->Ordenar('idGestionTelefonica', "DESC");
				return $Consulta->Ejecutar(true, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaTiempoLlamada($Idagente = false)
		 * 
		 * Consulta el tiempo limite de una llamada
		 * @param $IdAgente: Criterio de busqueda de supervisor 
		 * @return matriz de consulta
		 */
		public function ConsultaTiempoLlamada($IdAgente = false){
			if($IdAgente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor');
				$Consulta->Columnas('TiempoLlamada');
				$Consulta->InnerJoin('tbl_tiempo_llamada', 'tbl_agentes_asignado_supervisor.IdSupervisor', 'tbl_tiempo_llamada.IdSupervisor');
				$Consulta->Condicion("IdAgente = '$IdAgente'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * ConsultarSupervisorAsignado($IdAgente = false)
		 *
		 * Consulta el supervisor asignado al agente
		 * @param bool $IdAgente: identificador del supervisor
		 * @return array: arreglo de resultados
		 */
		public function ConsultarSupervisorAsignado($IdAgente = false){
			if($IdAgente == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_agentes_asignado_supervisor');
				$Consulta->Columnas('IdSupervisor');
				$Consulta->Condicion("IdAgente = '$IdAgente'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * ConsultaCallerId($IdSupervisor = false)
		 * 
		 * Consulta el caller Id de supervisor
		 */
		public function ConsultaCallerId($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas(APP);
				$Consulta->Tabla('tbl_informacion_usuarios');
				$Consulta->Columnas('callerid');
				$Consulta->Condicion("idUsuario = '$IdSupervisor'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico 
		 * EstatusTelefonos($Consulta = false, $Campo = false)
		 * 
		 * Consulta la disponibilidad de telefonos para un registro
		 * @param $Consulta: Array de datos.
		 */
		public function EstatusTelefonos($Consulta = false, $Campo = false){
			if($Consulta == true){
				foreach($Consulta as $Arreglo){
					foreach($Arreglo as $Columna=>$Valor){
						if($Columna == $Campo){
							$Arreglo['StatusTelefono'] = $this->ConsultaStatusTelefonos($Valor);
						}
					}
					$Lista[] = $Arreglo;
				}
				return $Lista;
			}
		}
		
		/**
		 * Metodo Privado
		 * ConsultaStatusTelefonos($IdAgenda = false)
		 * 
		 * Consulta si existe telefonos en la bd para un cliente dado.
		 * @param $IdAgenda: Identificador del agente
		 * @return string de estado (Disponible o nodisponible)
		 */
		private function ConsultaStatusTelefonos($IdAgenda = false){
			if($IdAgenda == true){
				$SQL = 'SELECT Telefono1, Telefono2, Telefono3, Telefono4 FROM tbl_datos_agenda WHERE IdDatoAgenda="'.$IdAgenda.'"';
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetch();
				$Resultado = AppUtilidades::ArregloEstados($Resultado);
				$Tamano = count($Resultado);
				unset($Resultado, $Consulta, $SQL, $Conexion);
				return ($Tamano > 0) ? "DISPONIBLE" : "NODISPONIBLE";
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarCodigos()
		 * 
		 * Consulta los codigos de gestion disponibles
		 */
		public function ConsultarCodigos(){
			$Consulta = new NeuralBDConsultas($this->Conexion);
			$Consulta->Tabla('tbl_codigos');
			$Consulta->Columnas('IdCodigo, Codigo, Descripcion, Color');
			$Consulta->Condicion("Status != 'ELIMINADO'");
			return $Consulta->Ejecutar(false, true);
		}
		
		/**
		 * Metodo Publico 
		 * ConsultarCodigoPais($IdSupervisor = false)
		 * 
		 * Consulta el prefijo de pais de la troncal aisgnada al supervisor
		 */
		public function ConsultarCodigoPais($IdSupervisor = false){
			if($IdSupervisor == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_troncal_asignada_supervisor');
				$Consulta->Columnas('CodigoPais');
				$Consulta->InnerJoin('tbl_troncales', 'tbl_troncal_asignada_supervisor.IdTroncal', 'tbl_troncales.IdTroncal');
				$Consulta->Condicion("tbl_troncal_asignada_supervisor.IdSupervisor = '$IdSupervisor'");
				$Consulta->Condicion("tbl_troncales.Status != 'Eliminado'");
				return $Consulta->Ejecutar(false, true);
			}
		}

		/**
		 * Metodo Publico
		 * GuardarFechas($ArregloFechasImportes, $IdGestion = false, $TipoGestion = false)
		 *
		 * Guardar fechas de compromiso de pago
		 * @param bool $ArregloFechasImporte: Matriz de fechas e importes
		 * @param bool $IdGestion: identificador de la gestion
		 * @param bool $TipoGestion: Tipo de la Gestion
		 * @throws NeuralException
		 */
		public function GuardarFechas($ArregloFechasImportes, $IdGestion = false, $TipoGestion = false){
			if($ArregloFechasImportes == true AND $IdGestion == true AND $TipoGestion == true){
				$SQL = new NeuralBDGab(APP, 'tbl_fecha_compromiso');	
				foreach($ArregloFechasImportes as $Arreglo){
					$SQL->Sentencia('IdGestion', $IdGestion);
					$SQL->Sentencia('TipoGestion', $TipoGestion);
					foreach($Arreglo as $Columna => $Valor){
						$SQL->Sentencia($Columna, $Valor);	
					}
					$SQL->Insertar();
				}
			    		
			}
		}

		/**
		 * Metodo Publico
		 * GuardarDatosDeGestionTelefonica($Arreglo = false, $Condicion = false, $Omitidos = false)
		 *
		 * Guardar Datos de la gestion telefonica
		 * @param bool $Arreglo: Arreglo de datos
		 * @param bool $Condicion: Condicion
		 * @param bool $Omitidos: Elementos omitidos
		 */
		public function GuardarDatosDeGestionTelefonica($Arreglo = false, $Condicion = false, $Omitidos = false){
			if($Arreglo == true and $Condicion == true and $Omitidos == true){
				return self::ActualizarDatos($Arreglo, $Condicion, 'tbl_gestion_telefonica', $Omitidos, APP);
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarStatusCobro($Arreglo = false, $Condicion = false)
		 * 
		 * Actualiza el estatus de cobro de el cliente.
		 * @param $Arreglo: array de datos que se actualizaran.
		 * @param $Condicion: Id del registro a modificar.
		 */ 
		public function ActualizarStatusCobro($Arreglo = false, $Condicion = false){
			if($Arreglo == true AND $Condicion == true){
				$SQL = new NeuralBDGab(APP, 'tbl_datos_agenda');
				$SQL->Sentencia('StatusCobro',$Arreglo['StatusCobro']);					
				$SQL->Condicion('IdDatoAgenda', $Condicion['IdDatoAgenda']);
				$SQL->Actualizar();
			}
		}
		
		/**
		 * Medoto Publico
		 * ActualizarCodigoGestion($Codigo = false, $IdDatoAgenda = false)
		 * 
		 * Actualiza el codigo de la gestion
		 * @param $Codigo: nuevo codigo de gestion
		 * @param $IdDatoAgenda : id del registro
		 */
		public function ActualizarCodigoGestion($Codigo = false, $IdDatoAgenda = false){
			if($Codigo == true AND $IdDatoAgenda == true){
				$SQL = new NeuralBDGab($this->Conexion, 'tbl_datos_agenda');					
				$SQL->Sentencia('IdCodigo', $Codigo);
				$SQL->Condicion('IdDatoAgenda', $IdDatoAgenda);
				$SQL->Actualizar();	
			}
		}
		
		/**
		 * Metodo Publico 
		 * ActualizarDatosCliente($MatrizDatos = false, $Condicion = false)
		 * 
		 * Actualiza datos de cliente dentro de una gestion
		 * @param $MatrizDatos: Matriz de datos 
		 * @param $Condicion: Id del registro a modificar
		 * @return void
		 */
		public function ActualizarDatosCliente($MatrizDatos = false, $Condicion = false, $Idagente = false){
			if($MatrizDatos == true and is_array($MatrizDatos)){
				$SQL = new NeuralBDGab(APP, 'tbl_datos_agenda');
				$SQL->Sentencia('Telefono1', $MatrizDatos['Telefono1']);
				$SQL->Sentencia('Telefono2', $MatrizDatos['Telefono2']);	
				$SQL->Sentencia('Telefono3', $MatrizDatos['Telefono3']);	
				$SQL->Sentencia('Telefono4', $MatrizDatos['Telefono4']);
				$SQL->Sentencia('DomicilioContractual', $MatrizDatos['DomicilioContractual']);
				$SQL->Sentencia('DomicilioAval', $MatrizDatos['DomicilioAval']);							
				$SQL->Condicion('IdDatoAgenda', $Condicion['IdDatoAgenda']);
				$SQL->Actualizar();
				$this->AgregarABitacora($Idagente, $Condicion, $MatrizDatos);
			}
		}
		
		/**
		 * Metodo Publcio 
		 * AgregarABitacora($IdAgente = false, $IdDatoAgenda = false, $Datalles = false)
		 *
		 * Registra una modificacione en la bitacora
		 * @param boolean $IdAgente     Identificador del agente
		 * @param boolean $IdDatoAgenda Identificador del registro modificado
		 * @param boolean $Datalles     Descripcion de la modificacion 
		 */
		private function AgregarABitacora($IdAgente = false, $IdDatoAgenda = false, $Datalles = false){
				$SQL = 'CALL AgregarBitacora(?, ?, ?, ?, ?)';
				$Conexion = NeuralConexionDB::DoctrineDBAL(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->bindValue(1, AppFechas::ObtenerFechaActual());
				$Consulta->bindValue(2, AppFechas::ObtenerHoraActual());
				$Consulta->bindValue(3, $IdAgente);
				$Consulta->bindValue(4, $IdDatoAgenda['IdDatoAgenda']);
				$Consulta->bindValue(5, implode(', ', array_keys(AppUtilidades::ArregloEstados($Datalles))));
				$Consulta->execute();
		}
	}