<?php
	
	class AgendaTelefonica_Modelo extends AppSQLConsultas{
		
		function __Construct(){
			parent::__Construct();
			$this->Conexion = NeuralConexionDB::DoctrineDBAL(APP);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarAgenda($Id = false)
		 * 
		 * Consulta los registros asignados a un Agente 
		 * @param $Id: Parametro de busqueda (Id del Agente)
		 * @return Resultado de la consulta
		 */
		public function ConsultarAgenda($Id = false, $Inicio = false, $Cantidad = false, $Criterio = false){
			if($Id == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda, tbl_datos_agenda.ClienteUnico, tbl_datos_agenda.NombreTitular, tbl_datos_agenda.SemanaAtraso, tbl_datos_agenda.Saldo, tbl_datos_agenda.SaldoTotal, tbl_datos_agenda.StatusRegistro, tbl_codigos.Color ".
                       "FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
                       "INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
                       "LEFT JOIN tbl_codigos ON tbl_datos_agenda.IdCodigo = tbl_codigos.IdCodigo ".
					   "WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$Id'";                      
				if($Criterio != ""){
					$SQL .= " AND (ClienteUnico LIKE '%$Criterio%' OR NombreTitular LIKE '%$Criterio%' OR SemanaAtraso LIKE '%$Criterio%' OR Saldo LIKE '%$Criterio%' OR SaldoTotal LIKE '%$Criterio%')"; 
				}
				$SQL .= " LIMIT ".$Inicio .", ". $Cantidad;
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				$Resultado = $Consulta->fetchAll(PDO::FETCH_ASSOC);
				$Resultado['Cantidad'] = $Consulta->rowCount();
				return $Resultado;
			}
		}
		
		/**
		 * Metodo publico 
		 * ConsultaCodigos()
		 * 
		 * Consulta los codigos de gestion 
		 */
		public function ConsultarCodigos(){
			$SQL = "SELECT IdCodigo, Codigo, Descripcion FROM tbl_codigos WHERE Status != 'ELIMINADO'";
			$Consulta = $this->Conexion->prepare($SQL);
			$Consulta->execute();
			return $Consulta->fetchAll(PDO::FETCH_ASSOC);
		}
		
		/**
		 * Metodo Publico
		 * ConsultarAgendaTotal($Id = false)
		 * 
		 * Consulta un  total de registros
		 * @param $Id: Parametro de busqueda (Id del Agente)
		 * @return Resultado de la consulta
		 */
		public function ConsultarAgendaTotal($Id = false){
			if($Id == true){
				$SQL = "SELECT tbl_datos_agenda.IdDatoAgenda ".
                       "FROM tbl_agente_asignado INNER JOIN tbl_asignacion_registros ON tbl_agente_asignado.IdAsignacionRegistro = tbl_asignacion_registros.IdAsignacionRegistro ".
                       "INNER JOIN tbl_datos_agenda ON tbl_asignacion_registros.IdDatoAgenda = tbl_datos_agenda.IdDatoAgenda ".
                       "WHERE tbl_datos_agenda.StatusRegistro != 'ELIMINADO' AND tbl_agente_asignado.IdAgente = '$Id'";                      
				$Consulta = $this->Conexion->prepare($SQL);
				$Consulta->execute();
				return $Consulta->rowCount();
			}
		}
		
		/**
		 * Metodo Publico
		 * ConsultarRegistroAgenda($IdDatoAgenda = false)
		 * 
		 * Consulta los datos del registro seleccionado
		 * @param $IdDatoAgenda: Parametro de busqueda
		 * @return El resultado de la Agenda
		 */
		public function ConsultarRegistroAgenda($IdDatoAgenda = false){
			if($IdDatoAgenda == true){
				$Consulta = new NeuralBDConsultas($this->Conexion);
				$Consulta->Tabla('tbl_datos_agenda');
				$Consulta->Columnas(array_merge(self::ListarColumnas('tbl_datos_agenda', array('IdDatoAgenda', 'IdSupervisor', 'IdGerencia', 'IdCartera', 'IdProvincia', 'IdDistrito', 'StatusCobro'), false, APP), self::ListarColumnas('tbl_gerencias', array('IdGerencia', 'Status'), array('Descripcion' => 'Gerencia'), APP), self::ListarColumnas('tbl_carteras', array('IdCartera', 'Status'), array('Descripcion' => 'Cartera'), APP), self::ListarColumnas('tbl_provincia', array('IdProvincia', 'Status'), array('Descripcion' => 'Provincia'), APP), self::ListarColumnas('tbl_distritos', array('IdDistrito', 'IdProvincia', 'Status'), array('Descripcion' => 'Distrito'), APP)));
				$Consulta->InnerJoin('tbl_gerencias', 'tbl_datos_agenda.IdGerencia', 'tbl_gerencias.IdGerencia');
				$Consulta->InnerJoin('tbl_carteras', 'tbl_datos_agenda.IdCartera', 'tbl_carteras.IdCartera');
				$Consulta->InnerJoin('tbl_provincia', 'tbl_datos_agenda.IdProvincia', 'tbl_provincia.IdProvincia');
				$Consulta->InnerJoin('tbl_distritos', 'tbl_datos_agenda.IdDistrito', 'tbl_distritos.IdDistrito');
				$Consulta->Condicion("IdDatoAgenda = '$IdDatoAgenda'");
				return $Consulta->Ejecutar(false, true);
			}
		}
		
		/**
		 * Metodo Publico
		 * ActualizarInformacionAgenda($Arreglo = false, $Omitidos = false, $Condicion = false)
		 * 
		 * Actualiza la informacion del registro seleccionado
		 * @param $Arreglo: Arreglo de datos a Actualizar
		 * @param $Omitidos: Arreglo de Columnas omitidas
		 * @param $Condicion: Id del registro a Actualizar
		 * @return Resultado de la Actualizacion
		 */
		public function ActualizarInformacionAgenda($Arreglo = false, $Omitidos = false, $Condicion = false, $IdAgente = false){
			if($Arreglo == true AND is_array($Arreglo) AND $Condicion == true AND is_array($Condicion) AND $Omitidos == true AND is_array($Omitidos)){
				self::ActualizarDatos($Arreglo, $Condicion, 'tbl_datos_agenda', $Omitidos, APP);
				$SQL = 'CALL AgregarBitacora(?, ?, ?, ?, ?)';
				$Conexion = NeuralConexionDB::DoctrineDBAL(APP);
				$Consulta = $Conexion->prepare($SQL);
				$Consulta->bindValue(1, AppFechas::ObtenerFechaActual());
				$Consulta->bindValue(2, AppFechas::ObtenerHoraActual());
				$Consulta->bindValue(3, $IdAgente);
				$Consulta->bindValue(4, $Condicion['IdDatoAgenda']);
				$Consulta->bindValue(5, implode(', ', array_keys(AppUtilidades::ArregloEstados($Arreglo))));
				$Consulta->execute();		 
			}
		}
	}