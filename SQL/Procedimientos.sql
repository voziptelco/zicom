
--- SISTEMA ZICOM 
---------------------------------------------------------------------------
-- Trigger para asignar en carga laboral
---------------------------------------------------------------------------
DELIMITER //
CREATE TRIGGER Asignacion_Carga
AFTER INSERT ON tbl_datos_agenda 
FOR EACH ROW
BEGIN
	INSERT INTO `tbl_asignacion_registros` (`IdDatoAgenda`) VALUES (NEW.IdDatoAgenda);
END
//
DELIMITER ;

---------------------------------------------------------------------------
-- Procedimiento para agregar gestiones de campo
---------------------------------------------------------------------------
DELIMITER //
CREATE PROCEDURE `AgregarGestionCampo`(IN IdDatoAgenda BIGINT(20), IN IdAgente BIGINT(20), IN Fecha_Visita DATE, IN Nombre_Gestor VARCHAR(185),IN Observaciones MEDIUMTEXT, IN Compromiso VARCHAR(2), IN TipoPago VARCHAR(45), IN Coutas VARCHAR(3), IN Prioridad VARCHAR(15), IN FechaHora_Captura DATETIME, OUT IdGestionCampo BIGINT(20))
BEGIN
	INSERT INTO `tbl_gestion_campo` (`IdDatoAgenda`, `IdAgente`, `Fecha_Visita`, `Nombre_Gestor`, `Observaciones`, `Compromiso`, `TipoPago`, `Cuotas`, `Prioridad`, `FechaHora_Captura`) VALUES (IdDatoAgenda, IdAgente, Fecha_Visita, Nombre_Gestor, Observaciones, Compromiso, TipoPago, Coutas, Prioridad, FechaHora_Captura);
	SET @IdGestionCampo = LAST_INSERT_ID();
	SELECT @IdGestionCampo;
END
//
DELIMITER ;