# ************************************************************
# Sequel Pro SQL dump
# Versión 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.6.21)
# Base de datos: zicom
# Tiempo de Generación: 2015-09-05 00:15:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla tbl_activacion_cuentas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_activacion_cuentas`;

CREATE TABLE `tbl_activacion_cuentas` (
  `IdActivacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdUsuario` bigint(20) NOT NULL,
  `Correo` varchar(85) DEFAULT NULL,
  `NewPassword` varchar(55) NOT NULL,
  `Fecha_Validacion` date DEFAULT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'PROCESANDO',
  PRIMARY KEY (`IdActivacion`),
  KEY `fk_tbl_activacion_cuentas_tbl_sistema_usuarios1_idx` (`IdUsuario`),
  CONSTRAINT `fk_tbl_activacion_cuentas_tbl_sistema_usuarios1` FOREIGN KEY (`IdUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_agente_asignado
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_agente_asignado`;

CREATE TABLE `tbl_agente_asignado` (
  `IdAgenteAsignado` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdAsignacionRegistro` bigint(20) NOT NULL,
  `IdAgente` varchar(45) NOT NULL,
  `FechaAsginacion` datetime NOT NULL,
  PRIMARY KEY (`IdAgenteAsignado`),
  KEY `fk_tbl_agente_asignado_tbl_asignacion_registros1_idx` (`IdAsignacionRegistro`),
  CONSTRAINT `fk_tbl_agente_asignado_tbl_asignacion_registros1` FOREIGN KEY (`IdAsignacionRegistro`) REFERENCES `tbl_asignacion_registros` (`IdAsignacionRegistro`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_agentes_asignado_supervisor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_agentes_asignado_supervisor`;

CREATE TABLE `tbl_agentes_asignado_supervisor` (
  `IdAgenteAsignado` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSupervisor` bigint(20) NOT NULL,
  `IdAgente` bigint(20) NOT NULL,
  PRIMARY KEY (`IdAgenteAsignado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_asignacion_registros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_asignacion_registros`;

CREATE TABLE `tbl_asignacion_registros` (
  `IdAsignacionRegistro` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDatoAgenda` bigint(20) NOT NULL,
  `FechaAsginacion` datetime DEFAULT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'NOASIGNADO',
  PRIMARY KEY (`IdAsignacionRegistro`),
  KEY `fk_tbl_asignacion_registros_tbl_datos_agenda1_idx` (`IdDatoAgenda`),
  CONSTRAINT `fk_tbl_asignacion_registros_tbl_datos_agenda1` FOREIGN KEY (`IdDatoAgenda`) REFERENCES `tbl_datos_agenda` (`IdDatoAgenda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_carteras
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_carteras`;

CREATE TABLE `tbl_carteras` (
  `IdCartera` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(55) NOT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdCartera`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_cdr
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_cdr`;

CREATE TABLE `tbl_cdr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `calldate` datetime NOT NULL,
  `clid` varchar(80) NOT NULL,
  `src` varchar(80) NOT NULL,
  `dst` varchar(80) NOT NULL,
  `dcontext` varchar(80) NOT NULL,
  `channel` varchar(80) NOT NULL,
  `dstchannel` varchar(80) NOT NULL,
  `lastapp` varchar(80) NOT NULL,
  `lastdata` varchar(80) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '0',
  `billsec` int(11) NOT NULL DEFAULT '0',
  `disposition` varchar(45) NOT NULL,
  `amaflags` int(11) NOT NULL DEFAULT '0',
  `accountcode` varchar(20) NOT NULL,
  `peeraccount` varchar(20) NOT NULL,
  `uniqueid` varchar(32) NOT NULL,
  `linkedid` varchar(80) NOT NULL,
  `userfield` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `callerid` (`clid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_clientes_asignados_supervisor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_clientes_asignados_supervisor`;

CREATE TABLE `tbl_clientes_asignados_supervisor` (
  `IdClienteAsignado` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdSupervisor` bigint(20) NOT NULL,
  `IdCliente` bigint(20) NOT NULL,
  PRIMARY KEY (`IdClienteAsignado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_configuracion_conmutador
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_configuracion_conmutador`;

CREATE TABLE `tbl_configuracion_conmutador` (
  `idConfiguracion` bigint(20) NOT NULL AUTO_INCREMENT,
  `host` varchar(45) NOT NULL,
  `port` varchar(45) NOT NULL,
  `user` varchar(45) NOT NULL,
  `password` varchar(145) NOT NULL,
  `Prefijo` varchar(45) NOT NULL,
  `callerid` varchar(45) NOT NULL DEFAULT 'CALL CENTER',
  `contexto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idConfiguracion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_datos_agenda
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_datos_agenda`;

CREATE TABLE `tbl_datos_agenda` (
  `IdDatoAgenda` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdCliente` bigint(20) NOT NULL,
  `IdSupervisor` bigint(20) NOT NULL,
  `IdGerencia` bigint(20) NOT NULL,
  `IdCartera` bigint(20) NOT NULL,
  `ClienteUnico` varchar(25) NOT NULL,
  `NombreTitular` varchar(100) NOT NULL,
  `DomicilioContractual` mediumtext NOT NULL,
  `IdDistrito` bigint(20) NOT NULL,
  `IdProvincia` bigint(20) NOT NULL,
  `NombreAval` varchar(100) NOT NULL,
  `DomicilioAval` mediumtext NOT NULL,
  `DistritoAval` varchar(255) DEFAULT NULL,
  `SemanaAtraso` varchar(5) NOT NULL,
  `Saldo` varchar(45) NOT NULL,
  `SaldoTotal` varchar(45) NOT NULL,
  `Telefono1` varchar(15) NOT NULL,
  `Telefono2` varchar(15) DEFAULT NULL,
  `Telefono3` varchar(15) DEFAULT NULL,
  `Telefono4` varchar(15) DEFAULT NULL,
  `StatusRegistro` varchar(15) DEFAULT 'ACTIVO',
  `StatusCobro` varchar(15) DEFAULT 'NOCOBRADO',
  PRIMARY KEY (`IdDatoAgenda`),
  KEY `fk_tbl_datos_agenda_tbl_gerencias1_idx` (`IdGerencia`),
  KEY `fk_tbl_datos_agenda_tbl_carteras1_idx` (`IdCartera`),
  KEY `fk_tbl_datos_agenda_tbl_distritos1_idx` (`IdDistrito`),
  KEY `fk_tbl_datos_agenda_tbl_provincia1_idx1` (`IdProvincia`),
  CONSTRAINT `fk_tbl_datos_agenda_tbl_carteras1` FOREIGN KEY (`IdCartera`) REFERENCES `tbl_carteras` (`IdCartera`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_datos_agenda_tbl_distritos1` FOREIGN KEY (`IdDistrito`) REFERENCES `tbl_distritos` (`IdDistrito`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_datos_agenda_tbl_gerencias1` FOREIGN KEY (`IdGerencia`) REFERENCES `tbl_gerencias` (`IdGerencia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_datos_agenda_tbl_provincia1` FOREIGN KEY (`IdProvincia`) REFERENCES `tbl_provincia` (`IdProvincia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER ;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION" */;;
/*!50003 CREATE */ /*!50003 TRIGGER `Asignacion_Carga` AFTER INSERT ON `tbl_datos_agenda` FOR EACH ROW BEGIN
	INSERT INTO `tbl_asignacion_registros` (`IdDatoAgenda`) VALUES (NEW.IdDatoAgenda);
END */;;
DELIMITER ;
/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;


# Volcado de tabla tbl_distritos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_distritos`;

CREATE TABLE `tbl_distritos` (
  `IdDistrito` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdProvincia` bigint(20) NOT NULL,
  `Descripcion` varchar(80) NOT NULL,
  `Status` varchar(45) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdDistrito`),
  KEY `fk_tbl_distritos_tbl_provincia1_idx` (`IdProvincia`),
  CONSTRAINT `fk_tbl_distritos_tbl_provincia1` FOREIGN KEY (`IdProvincia`) REFERENCES `tbl_provincia` (`IdProvincia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_fecha_compromiso
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_fecha_compromiso`;

CREATE TABLE `tbl_fecha_compromiso` (
  `IdFechaCompromiso` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdGestion` bigint(20) NOT NULL,
  `TipoGestion` varchar(45) NOT NULL,
  `Fecha` date NOT NULL,
  `Importe` varchar(45) NOT NULL,
  PRIMARY KEY (`IdFechaCompromiso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_gerencias
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_gerencias`;

CREATE TABLE `tbl_gerencias` (
  `IdGerencia` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(55) NOT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdGerencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_gestion_campo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_gestion_campo`;

CREATE TABLE `tbl_gestion_campo` (
  `IdGestionCampo` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDatoAgenda` bigint(20) NOT NULL,
  `IdAgente` bigint(20) NOT NULL,
  `Fecha_Visita` date NOT NULL,
  `Nombre_Gestor` varchar(185) NOT NULL,
  `Observaciones` mediumtext NOT NULL,
  `Compromiso` varchar(2) DEFAULT NULL,
  `TipoPago` varchar(45) DEFAULT NULL,
  `Cuotas` varchar(3) DEFAULT NULL,
  `Prioridad` varchar(15) DEFAULT 'NOPRIORITARIO',
  `FechaHora_Captura` datetime NOT NULL,
  PRIMARY KEY (`IdGestionCampo`),
  KEY `fk_tbl_gestion_campo_tbl_datos_agenda1_idx` (`IdDatoAgenda`),
  CONSTRAINT `fk_tbl_gestion_campo_tbl_datos_agenda1` FOREIGN KEY (`IdDatoAgenda`) REFERENCES `tbl_datos_agenda` (`IdDatoAgenda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_gestion_telefonica
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_gestion_telefonica`;

CREATE TABLE `tbl_gestion_telefonica` (
  `idGestionTelefonica` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdDatoAgenda` bigint(20) NOT NULL,
  `IdAgente` bigint(20) NOT NULL,
  `uniqueid` varchar(32) NOT NULL,
  `NumeroTelefono` varchar(15) NOT NULL,
  `Compromiso` varchar(2) DEFAULT NULL,
  `TipoPago` varchar(45) DEFAULT NULL,
  `Cuotas` varchar(3) DEFAULT NULL,
  `Observaciones` mediumtext,
  `Prioridad` varchar(15) DEFAULT NULL,
  `FechaHora_Captura` datetime DEFAULT NULL,
  `Grabacion` varchar(5) NOT NULL DEFAULT 'NO',
  `Status` varchar(15) NOT NULL DEFAULT 'NOCAPTURADO',
  `StatusLlamada` varchar(15) NOT NULL DEFAULT 'NOCONTESTADO',
  PRIMARY KEY (`idGestionTelefonica`),
  KEY `fk_tbl_gestion_telefonica_tbl_datos_agenda1_idx` (`IdDatoAgenda`),
  CONSTRAINT `fk_tbl_gestion_telefonica_tbl_datos_agenda1` FOREIGN KEY (`IdDatoAgenda`) REFERENCES `tbl_datos_agenda` (`IdDatoAgenda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_informacion_usuarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_informacion_usuarios`;

CREATE TABLE `tbl_informacion_usuarios` (
  `IdInformacion` bigint(20) NOT NULL AUTO_INCREMENT,
  `idUsuario` bigint(20) NOT NULL,
  `RUC` varchar(45) DEFAULT NULL,
  `RazonSocial` varchar(45) DEFAULT NULL,
  `Direccion` mediumtext,
  `Nombres` varchar(255) NOT NULL,
  `ApellidoPaterno` varchar(255) NOT NULL,
  `ApellidoMaterno` varchar(255) NOT NULL,
  `Cargo` varchar(255) DEFAULT NULL,
  `TelefonoCasa` varchar(15) DEFAULT NULL,
  `TelefonoCasa2` varchar(15) DEFAULT NULL,
  `TelefonoMovil` varchar(15) DEFAULT NULL,
  `TelefonoMovil2` varchar(15) DEFAULT NULL,
  `Correo` varchar(255) DEFAULT NULL,
  `Extension` varchar(10) NOT NULL,
  `TipoCanal` varchar(5) DEFAULT NULL,
  `ExportarExcel` varchar(15) NOT NULL DEFAULT 'INACTIVO',
  `IdProvincia` bigint(20) DEFAULT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdInformacion`),
  KEY `fk_tbl_informacion_usuarios_tbl_sistema_usuarios1_idx` (`idUsuario`),
  CONSTRAINT `fk_tbl_informacion_usuarios_tbl_sistema_usuarios1` FOREIGN KEY (`idUsuario`) REFERENCES `tbl_sistema_usuarios` (`IdUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_provincia
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_provincia`;

CREATE TABLE `tbl_provincia` (
  `IdProvincia` bigint(20) NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(80) NOT NULL,
  `Status` varchar(45) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdProvincia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_sistema_usuarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sistema_usuarios`;

CREATE TABLE `tbl_sistema_usuarios` (
  `IdUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `IdPerfil` bigint(20) NOT NULL,
  `Usuario` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'ACTIVO',
  PRIMARY KEY (`IdUsuario`),
  KEY `fk_tbl_sistema_usuarios_tbl_sistema_usuarios_perfil1_idx` (`IdPerfil`),
  CONSTRAINT `fk_tbl_sistema_usuarios_tbl_sistema_usuarios_perfil1` FOREIGN KEY (`IdPerfil`) REFERENCES `tbl_sistema_usuarios_perfil` (`IdPerfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla tbl_sistema_usuarios_perfil
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sistema_usuarios_perfil`;

CREATE TABLE `tbl_sistema_usuarios_perfil` (
  `IdPerfil` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(25) NOT NULL,
  `Status` varchar(15) NOT NULL DEFAULT 'ACTIVO',
  `Central` varchar(15) NOT NULL,
  `Error` varchar(15) NOT NULL,
  `Administrador` varchar(15) NOT NULL,
  `Supervisor` varchar(15) NOT NULL,
  `Agente` varchar(15) NOT NULL,
  `Cliente` varchar(15) NOT NULL,
  PRIMARY KEY (`IdPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




--
-- Dumping routines (PROCEDURE) for database 'zicom'
--
DELIMITER ;;

# Dump of PROCEDURE AgregarGestionCampo
# ------------------------------------------------------------

/*!50003 DROP PROCEDURE IF EXISTS `AgregarGestionCampo` */;;
/*!50003 SET SESSION SQL_MODE="NO_ENGINE_SUBSTITUTION"*/;;
/*!50003 CREATE*/ /*!50003 PROCEDURE `AgregarGestionCampo`(IN IdDatoAgenda BIGINT(20), IN IdAgente BIGINT(20), IN Fecha_Visita DATE, IN Nombre_Gestor VARCHAR(185),IN Observaciones MEDIUMTEXT, IN Compromiso VARCHAR(2), IN TipoPago VARCHAR(45), IN Coutas VARCHAR(3), IN Prioridad VARCHAR(15), IN FechaHora_Captura DATETIME, OUT IdGestionCampo BIGINT(20))
BEGIN
	INSERT INTO `tbl_gestion_campo` (`IdDatoAgenda`, `IdAgente`, `Fecha_Visita`, `Nombre_Gestor`, `Observaciones`, `Compromiso`, `TipoPago`, `Coutas`, `Prioridad`, `FechaHora_Captura`) VALUES (IdDatoAgenda, IdAgente, Fecha_Visita, Nombre_Gestor, Observaciones, Compromiso, TipoPago, Coutas, Pagado, Prioridad, FechaHora_Captura);
	SET @IdGestionCampo = LAST_INSERT_ID();
	SELECT @IdGestionCampo;
END */;;

/*!50003 SET SESSION SQL_MODE=@OLD_SQL_MODE */;;
DELIMITER ;

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

--
-- Volcado de datos para la tabla `tbl_carteras`
--

INSERT INTO `tbl_carteras` (`IdCartera`, `Descripcion`, `Status`) VALUES
(1, 'Legal 1', 'ACTIVO'),
(2, 'Legal 2', 'ACTIVO'),
(3, 'Legal 3', 'ACTIVO'),
(4, 'Legal 4', 'ACTIVO'),
(5, 'Legal 5', 'ACTIVO');

--
-- Volcado de datos para la tabla `tbl_gerencias`
--

INSERT INTO `tbl_gerencias` (`IdGerencia`, `Descripcion`, `Status`) VALUES
(1, 'Peru Centro', 'ACTIVO'),
(2, 'Peru Norte', 'ACTIVO');

--
-- Volcado de datos para la tabla `tbl_provincia`
--

INSERT INTO `tbl_provincia` (`IdProvincia`, `Descripcion`, `Status`) VALUES
(1, 'Lima', 'ACTIVO');

--
-- Volcado de datos para la tabla `tbl_distritos`
--

INSERT INTO `tbl_distritos` (`IdDistrito`, `IdProvincia`, `Descripcion`, `Status`) VALUES
(1, 1, 'Lince', 'ACTIVO'),
(2, 1, 'Jesus Maria', 'ACTIVO');

--
-- Volcado de datos para la tabla `tbl_configuracion_conmutador`
--

INSERT INTO `tbl_configuracion_conmutador` (`idConfiguracion`, `host`, `port`, `user`, `password`, `Prefijo`, `callerid`, `contexto`) VALUES
(1, '72.14.188.199', '', 'zicom', 'eNqL0TgmYZef/Sn8zezMEx8XN3DyS/5IMF2ZJ1ppdW564s5cAPeCDxk=', '52', '555555', 'from-salida');

--
-- Volcado de datos para la tabla `tbl_sistema_usuarios_perfil`
--

INSERT INTO `tbl_sistema_usuarios_perfil` (`IdPerfil`, `Nombre`, `Status`, `Central`, `Error`, `Administrador`, `Supervisor`, `Agente`, `Cliente`) VALUES
(1, 'Administrador', 'ACTIVO', 'true', 'true', 'true', 'false', 'false', 'false'),
(2, 'Supervisor', 'ACTIVO', 'true', 'true', 'false', 'true', 'false', 'false'),
(3, 'Agente', 'ACTIVO', 'true', 'true', 'false', 'false', 'true', 'false'),
(4, 'Cliente', 'ACTIVO', 'true', 'true', 'false', 'false', 'false', 'true');

--
-- Volcado de datos para la tabla `tbl_sistema_usuarios`
--

INSERT INTO `tbl_sistema_usuarios` (`IdUsuario`, `IdPerfil`, `Usuario`, `Password`, `Status`) VALUES
(1, 1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 'ACTIVO'),
(3, 2, 'ramsesaguirre', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'ACTIVO'),
(10, 4, 'jesusruiz', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'ACTIVO'),
(11, 3, 'luisconruiz', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'ACTIVO');

--
-- Volcado de datos para la tabla `tbl_informacion_usuarios`
--

INSERT INTO `tbl_informacion_usuarios` (`IdInformacion`, `idUsuario`, `RUC`, `RazonSocial`, `Direccion`, `Nombres`, `ApellidoPaterno`, `ApellidoMaterno`, `Cargo`, `TelefonoCasa`, `TelefonoCasa2`, `TelefonoMovil`, `TelefonoMovil2`, `Correo`, `Extension`, `TipoCanal`, `ExportarExcel`, `IdProvincia`, `Status`) VALUES
(1, 1, NULL, NULL, NULL, 'Angel', 'Hinostroza', 'Franco', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'INACTIVO', NULL, 'ACTIVO'),
(3, 3, NULL, NULL, NULL, 'Ramses', 'Aguirre', 'Farrera', 'Director De Ti', '(74) 74721450', '', '', '', 'ramsesaguirre@gmail.com', '103', NULL, 'INACTIVO', NULL, 'ACTIVO'),
(9, 10, '10256804439', 'Electronica Digital', 'Lima Sur 5', 'Jesus', 'Hernandez', 'Ruiz', 'Coordinador De Cobros', '(22) 11232131', NULL, '', NULL, 'jesus@electronica.com', '', NULL, 'INACTIVO', 1, 'ACTIVO'),
(10, 11, NULL, NULL, NULL, 'Luis', 'Contreras', 'Ruiz', 'Supervisor', '(74) 71010723', NULL, '', NULL, 'luisconruiz@gmail.com', '107', 'IAX2', 'ACTIVO', 1, 'ACTIVO');

--
-- Volcado de datos para la tabla `tbl_clientes_asignados_supervisor`
--

INSERT INTO `tbl_clientes_asignados_supervisor` (`IdClienteAsignado`, `IdSupervisor`, `IdCliente`) VALUES
(1, 3, 10);

--
-- Volcado de datos para la tabla `tbl_agentes_asignado_supervisor`
--

INSERT INTO `tbl_agentes_asignado_supervisor` (`IdAgenteAsignado`, `IdSupervisor`, `IdAgente`) VALUES
(3, 3, 11);

--
-- Volcado de datos para la tabla `tbl_datos_agenda`
--

INSERT INTO `tbl_datos_agenda` (`IdDatoAgenda`, `IdCliente`, `IdSupervisor`, `IdGerencia`, `IdCartera`, `ClienteUnico`, `NombreTitular`, `DomicilioContractual`, `IdDistrito`, `IdProvincia`, `NombreAval`, `DomicilioAval`, `DistritoAval`, `SemanaAtraso`, `Saldo`, `SaldoTotal`, `Telefono1`, `Telefono2`, `Telefono3`, `Telefono4`, `StatusRegistro`, `StatusCobro`) VALUES
(46, 10, 3, 1, 5, '6-1-9483-34976', 'Ronaldo Pingo Flores', 'Jr Pezet Y Monel 2121 D204 Lima', 1, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '34', '181.25', '2554.56', '(74)71637427', '', '', '', 'ACTIVO', 'NOCOBRADO'),
(47, 10, 3, 2, 5, '6-20-3655-13558', 'Lupe Liduvina Moron Aquije', 'Av Paso De Los Andes 138  Ur La Arequipena', 2, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '60', '33719.60', '6007.89', '(39)123234124', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(48, 10, 3, 1, 1, '6-9-8382-4055', 'Mario Alexis Guima Gushiken', 'Av Brasil Bk A 1361 601 Re San Felipe', 2, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '60', '2549.52', '3013.08', '(39)123234125', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(49, 10, 3, 1, 5, '6-20-7166-1333', 'Zoila Mirtha Infante Escobedo', 'Jr Coronel Domingo Casano 579  Lima', 1, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '62', '4157.19', '6660.21', '(39)123234126', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(50, 10, 3, 2, 5, '6-1-2247-50467', 'Henry Julio Farfan Barboza', 'Av Francisco Lazo 1949 6 Ur Risso', 1, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '62', '888.79', '1934.51', '(39)123234127', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(51, 10, 3, 1, 5, '6-20-3677-2222', 'Ana Noemi Carrillo Roman', 'Jr Larco Herrera 2do Piso 624  Magdalena Del Mar', 2, 1, 'Patricia Magdalena Carrillo Roman', 'Calle Huinco 129 Urb. San Miguel', 'San Miguel', '68', '16284.00', '25645.21', '(12)12344444', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(52, 10, 3, 1, 5, '6-1-9483-31050', 'Alfredo Herrera Noriega', 'Jr Tupac Amaru (jose Manu 2737 401 Ur Oyague', 1, 1, 'Gerfgerfgbear Gger Grgf Dgerfgbear', 'Jtydhjutyhujrtyu  Aija', 'Aija / Aija', '69', '2754.99', '5688.79', '(12)12344445', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(53, 10, 3, 1, 5, '6-20-3655-10458', 'Nelly Torres Yumbato', 'Av Afranio Mello Franco 517  Ur San Felipe', 2, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '70', '4392.40', '7543.71', '(12)12344446', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(54, 10, 3, 1, 5, '6-1-4278-12420', 'Ada Margie Alva Sandoval', 'Cl Miguel Iglesias (carlo 1940 5 Ur Risso', 1, 1, 'Mercedes Primitiva Reyes', 'Av Coronel Cesar Canevaro 267 Ur Lobaton', 'Lima / Lince', '70', '3007.13', '6431.61', '(12)12344447', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO'),
(55, 10, 3, 1, 5, '6-9-7489-3002', 'Percy Lopez Reategui', 'Jr Mama Ocllo 1955  Ur Lobaton', 1, 1, 'Sin Aval', 'Sin Domicilio Aval Sin Num.ext. Aval Sin Colonia Aval', 'Sin Poblacion Aval', '71', '862.49', '2041.35', '(12)12344448', NULL, NULL, NULL, 'ACTIVO', 'NOCOBRADO');

--
-- Volcado de datos para la tabla `tbl_asignacion_registros`
--

INSERT INTO `tbl_asignacion_registros` (`IdAsignacionRegistro`, `IdDatoAgenda`, `FechaAsginacion`, `Status`) VALUES
(46, 46, '2015-08-28 00:04:00', 'ASIGNADO'),
(47, 47, '2015-08-28 00:04:00', 'ASIGNADO'),
(48, 48, '2015-08-28 00:04:00', 'ASIGNADO'),
(49, 49, '2015-08-28 00:04:00', 'ASIGNADO'),
(50, 50, NULL, 'NOASIGNADO'),
(51, 51, NULL, 'NOASIGNADO'),
(52, 52, NULL, 'NOASIGNADO'),
(53, 53, NULL, 'NOASIGNADO'),
(54, 54, NULL, 'NOASIGNADO'),
(55, 55, NULL, 'NOASIGNADO');

--
-- Volcado de datos para la tabla `tbl_agente_asignado`
--

INSERT INTO `tbl_agente_asignado` (`IdAgenteAsignado`, `IdAsignacionRegistro`, `IdAgente`, `FechaAsginacion`) VALUES
(24, 46, '11', '2015-08-28 00:04:00'),
(25, 47, '11', '2015-08-28 00:04:00'),
(26, 48, '11', '2015-08-28 00:04:00'),
(27, 49, '11', '2015-08-28 00:04:00');
